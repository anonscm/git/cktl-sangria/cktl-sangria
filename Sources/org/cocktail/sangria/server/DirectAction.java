/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2010 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/

package org.cocktail.sangria.server;

import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;
import org.cocktail.fwkcktlpersonne.common.metier.PersonneDelegate;
import org.cocktail.fwkcktlrecherche.server.droits.RechercheApplicationAutorisationCache;
import org.cocktail.fwkcktlrecherche.server.metier.EOContratRecherche;
import org.cocktail.fwkcktlwebapp.common.CktlLog;
import org.cocktail.fwkcktlwebapp.common.CktlUserInfo;
import org.cocktail.fwkcktlwebapp.common.database.CktlUserInfoDB;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;
import org.cocktail.fwkcktlwebapp.server.CktlWebAction;
import org.cocktail.fwkcktlwebapp.server.components.CktlLogin;
import org.cocktail.fwkcktlwebapp.server.components.CktlLoginResponder;
import org.cocktail.sangria.server.components.Accueil;
import org.cocktail.sangria.server.components.Login;
import org.cocktail.sangria.server.components.LoginCAS;
import org.cocktail.sangria.server.components.Timeout;
import org.cocktail.sangria.server.components.contratsrecherche.ContratRechercheConsultation;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOApplication;
import com.webobjects.appserver.WOComponent;
import com.webobjects.appserver.WORedirect;
import com.webobjects.appserver.WORequest;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSDictionary;

import er.extensions.eof.ERXEC;
import er.extensions.eof.ERXQ;

// TODO: Auto-generated Javadoc
/**
 * The Class DirectAction.
 */
public class DirectAction extends CktlWebAction {

	/** The login comment. */
	private String loginComment;

	/**
	 * Instantiates a new direct action.
	 * 
	 * @param request
	 *            the request
	 */
	public DirectAction(WORequest request) {
		super(request);
	}

	
	@Override
	public Session session() {
		return (Session) super.session();
	}
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.webobjects.appserver.WODirectAction#defaultAction()
	 */
	public WOActionResults defaultAction() {
		if (useCasService()) {
			String url = DirectAction.getLoginActionURL(this.context(), false, null, true, null);
			WORedirect page = (WORedirect) pageWithName(WORedirect.class.getName());
			page.setUrl(url);
			return page;
		}
		else {
			return loginNoCasPage(null);
		}
	}

	
	public WOActionResults contratRechercheValidationAction() {
		NSDictionary<String, String> actionParams = getParamsFromRequest(request(), null);
		actionParams.put("nextPage", "contratRechercheValidation");
		if (useCasService()) {
			String url = DirectAction.getLoginActionURL(context(), false, null, true, actionParams);
			WORedirect page = (WORedirect) pageWithName(WORedirect.class.getName());
			page.setUrl(url);
			return page;
		} else {
			return loginNoCasPage(actionParams);
		}
	}
	
	/**
	 * Session expired action.
	 * 
	 * @return the wO action results
	 */
	public WOActionResults sessionExpiredAction() {
		Timeout nextPage = (Timeout) pageWithName(Timeout.class.getName());
		return nextPage;
	}

	public WOActionResults applicationExceptionAction() {
		Accueil nextPage = (Accueil) pageWithName(Accueil.class.getName());
		nextPage.setIsOpenFenetreException(true);
		return nextPage;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.cocktail.fwkcktlwebapp.server.CktlWebAction#loginCasFailurePage(java
	 * .lang.String, java.lang.String)
	 */
	@Override
	public WOActionResults loginCasFailurePage(String errorMessage, String errorCode) {
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.cocktail.fwkcktlwebapp.server.CktlWebAction#loginCasSuccessPage(java
	 * .lang.String, com.webobjects.foundation.NSDictionary)
	 */
	@Override
	public WOActionResults loginCasSuccessPage(String netid, NSDictionary actionParams) {
		
		EOEditingContext edc = ERXEC.newEditingContext();
		
		IPersonne pers = PersonneDelegate.fetchPersonneForLogin(edc, netid);
		
		RechercheApplicationAutorisationCache rechercheApplicationAutorisationCache = new RechercheApplicationAutorisationCache(RechercheApplicationAutorisationCache.APP_STRING_ID_SANGRIA, pers.persId());
		session().setRechercheApplicationAutorisationCache(rechercheApplicationAutorisationCache);

		session().setConnectedUser(netid);
		
		
		if (!rechercheApplicationAutorisationCache.hasDroitAccesSangria()) {
			LoginCAS page = (LoginCAS) pageWithName(LoginCAS.class.getName());
			page.setTitleComment(session().localizer().localizedStringForKey("login.application.nonautorise"));

			if (session() != null) {
				session().terminate();
			}
			
			return page;
		} else {
			
			session().setConnectedUser(netid);
			session().setPersId(pers.persId());
			Accueil accueil = (Accueil) pageWithName(Accueil.class.getName());
			
			if (actionParams != null && actionParams.containsKey("nextPage") && (actionParams.get("nextPage").equals("contratRechercheValidation"))) {
				Integer contratId;
				try {
					contratId = Integer.valueOf((String) actionParams.get("contratid"));
				} catch (NumberFormatException nfe) {
					return accueil;
				}
				
				EOContratRecherche contratRecherche = EOContratRecherche.fetchFirstByQualifier(edc, ERXQ.equals(EOContratRecherche.CONR_ORDRE_KEY, contratId));
				if (contratRecherche == null) {
					return accueil;
				}
				
				ContratRechercheConsultation contratPage = (ContratRechercheConsultation) pageWithName(ContratRechercheConsultation.class.getName());

				contratPage.setEdc(edc);
				contratPage.setContratRecherche(contratRecherche);
				contratPage.setModification(true);
				contratPage.setOngletIdentificationSelected(false);
				contratPage.setOngletPartieAdminSelected(true);
				return contratPage;
			}
			
			return accueil;

		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.cocktail.fwkcktlwebapp.server.CktlWebAction#loginNoCasPage(com.webobjects
	 * .foundation.NSDictionary)
	 */
	@Override
	public WOActionResults loginNoCasPage(NSDictionary actionParams) {
		Login page = (Login) pageWithName(Login.class);
		page.registerLoginResponder(getNewLoginResponder(actionParams));
		return page;
	}

	/**
	 * Login cas page.
	 * 
	 * @return the wO action results
	 */
	public WOActionResults loginCASPage(String casLoginLink) {
		LoginCAS page = (LoginCAS) pageWithName("LoginCAS");
		page.setTitleComment(loginComment());
		page.setCASLoginLink(casLoginLink);
		return page;
	}

	/**
	 * Login comment.
	 * 
	 * @return the string
	 */
	public String loginComment() {
		return loginComment;
	}

	/**
	 * Sets the login comment.
	 * 
	 * @param comment
	 *            the new login comment
	 */
	public void setLoginComment(String comment) {
		loginComment = comment;
	}

	/**
	 * Cas login link.
	 * 
	 * @return the string
	 */
	public String casLoginLink() {
		return null;
	}

	/**
	 * Valider login action.
	 * 
	 * @return the wO action results
	 */
	public WOActionResults validerLoginAction() {
		WOActionResults page = null;
		WORequest request = context().request();
		String login = StringCtrl.normalize((String) request.formValueForKey("identifiant"));
		String password = StringCtrl.normalize((String) request.formValueForKey("mot_de_passe"));
		String messageErreur = null;
		Session session = (Session) context().session();

		CktlLoginResponder loginResponder = getNewLoginResponder(getParamsFromRequest(request(), null));
		CktlUserInfo loggedUserInfo = new CktlUserInfoDB(cktlApp.dataBus());
		if (login.length() == 0) {
			messageErreur = "Vous devez renseigner l'identifiant.";
		} else if (!loginResponder.acceptLoginName(login)) {
			messageErreur = "Vous n'etes pas autorise(e) a utiliser cette application";
		} else {
			if (password == null)
				password = "";
			loggedUserInfo.setRootPass(loginResponder.getRootPassword());
			loggedUserInfo.setAcceptEmptyPass(loginResponder.acceptEmptyPassword());
			loggedUserInfo.compteForLogin(login, password, true);
			if (loggedUserInfo.errorCode() != CktlUserInfo.ERROR_NONE) {
				if (loggedUserInfo.errorMessage() != null)
					messageErreur = loggedUserInfo.errorMessage();
				CktlLog.rawLog(">> Erreur | " + loggedUserInfo.errorMessage());
			}
		}

		if (messageErreur == null) {
			session.setConnectedUserInfo(loggedUserInfo);
			session.setPersId(loggedUserInfo.persId().intValue());
			String erreur = session.setConnectedUser(loggedUserInfo.login());
			if (erreur != null) {
				messageErreur = erreur;
			} else {
				RechercheApplicationAutorisationCache rechercheApplicationAutorisationCache = new RechercheApplicationAutorisationCache(RechercheApplicationAutorisationCache.APP_STRING_ID_SANGRIA, loggedUserInfo.persId().intValue());
				((Session) session()).setRechercheApplicationAutorisationCache(rechercheApplicationAutorisationCache);

				if (rechercheApplicationAutorisationCache.hasDroitAccesSangria()) {
				} else {
					messageErreur = "Vous n'etes pas autorise(e) a utiliser cette application";
				}
			}
		}
		if (messageErreur != null) {
			if (session != null)
				session.terminate();
			page = (Login) pageWithName(Login.class.getName());
			((Login) page).setMessageErreur(messageErreur);
			return page;
		}

		return loginResponder.loginAccepted(null);

	}

	/**
	 * Mail users action.
	 * 
	 * @return the wO action results
	 */
	public WOActionResults mailUsersAction() {
		String destinataire = cktlApp.config().stringForKey("APP_ADMIN_MAIL");
		WORequest req = request();
		if (req.formValueForKey("dest") != null) {
			destinataire = (String) req.formValueForKey("dest");
		}
		cktlApp.mailBus().sendMail(destinataire, cktlApp.config().stringForKey("APP_ADMIN_MAIL"), null, "[" + cktlApp.name() + "]Utilisateur connectes a l'application ",
				"Liste des emails : \n" + ((Application) WOApplication.application()).utilisateurs().componentsJoinedByString(","));
		return defaultAction();
	}

	/**
	 * Gets the new login responder.
	 * 
	 * @param actionParams
	 *            the action params
	 * @return the new login responder
	 */
	public DefaultLoginResponder getNewLoginResponder(NSDictionary actionParams) {
		return new DefaultLoginResponder(actionParams);
	}

	/**
	 * The Class DefaultLoginResponder.
	 */
	public class DefaultLoginResponder implements CktlLoginResponder {

		/** The action params. */
		private NSDictionary actionParams;

		/**
		 * Instantiates a new default login responder.
		 * 
		 * @param actionParams
		 *            the action params
		 */
		public DefaultLoginResponder(NSDictionary actionParams) {
			this.actionParams = actionParams;
		}

		/**
		 * Action params.
		 * 
		 * @return the nS dictionary
		 */
		public NSDictionary actionParams() {
			return actionParams;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.cocktail.fwkcktlwebapp.server.components.CktlLoginResponder#
		 * loginAccepted(org.cocktail.fwkcktlwebapp.server.components.CktlLogin)
		 */
		public WOComponent loginAccepted(CktlLogin loginComponent) {
			Accueil accueil = (Accueil) pageWithName(Accueil.class.getName());
			
			EOEditingContext edc = ERXEC.newEditingContext();
			
			if (actionParams != null && actionParams.containsKey("nextPage") && (actionParams.get("nextPage").equals("contratRechercheValidation"))) {
				Integer contratId;
				try {
					contratId = Integer.valueOf((String) actionParams.get("contratid"));
				} catch (NumberFormatException nfe) {
					return accueil;
				}
				
				EOContratRecherche contratRecherche = EOContratRecherche.fetchFirstByQualifier(edc, ERXQ.equals(EOContratRecherche.CONR_ORDRE_KEY, contratId));
				if (contratRecherche == null) {
					return accueil;
				}
				
				ContratRechercheConsultation contratPage = (ContratRechercheConsultation) pageWithName(ContratRechercheConsultation.class.getName());

				contratPage.setEdc(edc);
				contratPage.setContratRecherche(contratRecherche);
				contratPage.setModification(true);
				contratPage.setOngletIdentificationSelected(false);
				contratPage.setOngletPartieAdminSelected(true);
				return contratPage;
			}
			
			return accueil;

		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.cocktail.fwkcktlwebapp.server.components.CktlLoginResponder#
		 * acceptLoginName(java.lang.String)
		 */
		public boolean acceptLoginName(String loginName) {
			return cktlApp.acceptLoginName(loginName);
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.cocktail.fwkcktlwebapp.server.components.CktlLoginResponder#
		 * acceptEmptyPassword()
		 */
		public boolean acceptEmptyPassword() {
			return cktlApp.config().booleanForKey("ACCEPT_EMPTY_PASSWORD");
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.cocktail.fwkcktlwebapp.server.components.CktlLoginResponder#
		 * getRootPassword()
		 */
		public String getRootPassword() {
			return cktlApp.getRootPassword();
		}
	}
}
