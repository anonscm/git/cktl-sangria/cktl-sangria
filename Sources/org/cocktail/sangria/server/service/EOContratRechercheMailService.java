package org.cocktail.sangria.server.service;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlpersonne.common.eospecificites.EOStructureForGroupeSpec;
import org.cocktail.fwkcktlpersonne.common.metier.EOCompte;
import org.cocktail.fwkcktlpersonne.common.metier.EOCompteEmail;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlrecherche.server.ParametresRecherche;
import org.cocktail.fwkcktlrecherche.server.metier.IContratRecherche;
import org.cocktail.fwkcktlrecherche.server.util.ContratRechercheMailService;
import org.cocktail.fwkcktlwebapp.server.CktlConfig;
import org.cocktail.fwkcktlwebapp.server.CktlWebApplication;
import org.cocktail.sangria.server.Application;
import org.cocktail.sangria.server.components.DemandeDeValidation;
import org.cocktail.sangria.server.components.ValidationContrat;

import com.webobjects.appserver.WOApplication;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WORequest;
import com.webobjects.appserver.WOResponse;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;

import er.extensions.appserver.ERXSession;
import er.extensions.appserver.ERXWOContext;
import er.extensions.eof.ERXEC;
import er.extensions.eof.ERXEOControlUtilities;
import er.extensions.foundation.ERXArrayUtilities;
import er.javamail.ERMailDeliveryHTML;
import er.javamail.ERMailUtils;

/**
 * Implémentation WO du service d'envoi de mail pour un contrat de recherche
 */
public class EOContratRechercheMailService implements ContratRechercheMailService {

	private EOEditingContext editingContext = ERXEC.newEditingContext();
	
	private Logger logger = Logger.getLogger(getClass());
	
	/**
	 * {@inheritDoc}
	 */
	public void envoyerMailDemandeDeValidation(IContratRecherche contrat, String typeValidation, Integer persId) {
		
		 ERMailDeliveryHTML mail = new ERMailDeliveryHTML();
		 WOContext currentContext = ERXWOContext.currentContext();
		 currentContext.generateCompleteURLs();
		 
		 DemandeDeValidation demandeDeValidation = (DemandeDeValidation) Application.application().pageWithName(DemandeDeValidation.class, ERXWOContext.newContext());
		 demandeDeValidation.setContrat(contrat);
		 demandeDeValidation.setUrl(currentContext.directActionURLForActionNamed("contratRechercheValidation", getParams(contrat), currentContext.secureMode(), false));

		 mail.setComponent(demandeDeValidation);
		 
		 try {
		   mail.newMail();
		   
		   String adresseEnvoi = getAdresseEnvoi(persId);
		   
		   mail.setFromAddress(adresseEnvoi);
		   mail.setReplyToAddress(adresseEnvoi);
		   mail.setSubject(getDemandeValidationSujet(typeValidation, contrat.numero()));
		   mail.setToAddresses(getAdressesPourDemandeValidation(typeValidation));
		   mail.sendMail();
		 } catch (Exception e) {
			 logger.warn("Erreur à l'envoi du mail de demande de validation", e);
		 }
		 
	}
	
	
	public NSDictionary<String, Object> getParams(IContratRecherche contrat) {
		NSDictionary<String, Object> params = new NSMutableDictionary<String, Object>();
		params.put("nextPage", "contratRechercheValidation");
		params.put("contratid", ERXEOControlUtilities.primaryKeyObjectForObject((EOEnterpriseObject) contrat).toString());
		return params;
	}
	
	/**
	 * {@inheritDoc}
	 */
	public void envoyerMailValidation(IContratRecherche contrat, String typeValidation, Integer persId) {
		 ERMailDeliveryHTML mail = new ERMailDeliveryHTML();
		 
		 WOContext currentContext = ERXWOContext.currentContext();
		 currentContext.generateCompleteURLs();

		 
		 ValidationContrat validation = (ValidationContrat) Application.application().pageWithName(ValidationContrat.class, ERXWOContext.newContext());	 
		 validation.setContrat(contrat);
		 validation.setUrl(currentContext.directActionURLForActionNamed("contratRechercheValidation", getParams(contrat), currentContext.secureMode(), false));

		 mail.setComponent(validation);

		 try {
		   mail.newMail();
		   
		   String adresseEnvoi = getAdresseEnvoi(persId);
		   
		   mail.setFromAddress(adresseEnvoi);
		   mail.setReplyToAddress(adresseEnvoi);
		   mail.setSubject(getValidationSujet(typeValidation, contrat.numero()));
		   mail.setToAddresses(getAdressesPourValidation());
		   mail.sendMail();
		 } catch (Exception e) {
			 logger.warn("Erreur à l'envoi du mail de validation", e);
		 }
	}
	
	protected String getDemandeValidationSujet(String typeValidation, String idContrat) {
		return "Demande de validation " + typeValidation + " pour le contrat " + idContrat;
	}

	protected String getValidationSujet(String typeValidation, String idContrat) {
		return "Validation " + typeValidation + " pour le contrat " + idContrat;
	}
	
	protected String getAdresseEnvoi(Integer persId) {
		EOCompte compte = EOCompte.compteForPersId(editingContext, persId);
		EOCompteEmail compteEmail = ERXArrayUtilities.firstObject(compte.toCompteEmails());
		if (compteEmail != null) {
			return compteEmail.getEmailFormatte();
		} else {
			return null;
		}
	}
	
	protected NSArray<String> getAdressesPourDemandeValidation(String typeValidation) {
		EOStructure groupe;
		try {
			groupe = 
				EOStructureForGroupeSpec.getGroupeForParamKey(
					editingContext, 
					ParametresRecherche.groupesParTypeValidations.get(typeValidation)
				);
		} catch (Exception e) {
			return new NSMutableArray<String>();
		}
		
		return getAdressesPourGroupe(groupe);
		

	}
	
	protected NSArray<String> getAdressesPourValidation() {
		EOStructure groupe;
		try {
			groupe = 
				EOStructureForGroupeSpec.getGroupeForParamKey(
					editingContext, 
					ParametresRecherche.GROUPE_GESTIONNAIRES_CONTRATS_KEY
				);
		} catch (Exception e) {
			return new NSMutableArray<String>();
		}
		
		return getAdressesPourGroupe(groupe);
		

	}
	
	protected NSArray<String> getAdressesPourGroupe(EOStructure groupe) {
		
		NSArray<String> adresses = new NSMutableArray<String>();

		NSArray<EOIndividu> individus = ERXArrayUtilities.arrayBySelectingInstancesOfClass(EOStructureForGroupeSpec.getPersonnesInGroupe(groupe), EOIndividu.class);
		for (EOIndividu individu : individus) {
			adresses.addAll(individu.getEmails(EOCompte.QUAL_CPT_VLAN_P_R));
		}
		
		return adresses;
	}


	
}
