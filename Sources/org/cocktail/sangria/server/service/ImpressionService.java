package org.cocktail.sangria.server.service;

import java.net.URL;

import org.cocktail.fwkcktlreportingextended.serveur.CktlReporterAjaxProgress;
import org.cocktail.fwkcktlreportingextended.serveur.services.impression.ReportImpressionData;
import org.cocktail.fwkcktlreportingextended.serveur.services.impression.ReportImpressionService;
import org.cocktail.fwkcktlreportingextended.serveur.services.impression.ReportImpressionServiceFactory;
import org.cocktail.reporting.common.location.finder.ReportLocationFinder;
import org.cocktail.reporting.server.CktlAbstractReporter;
import org.cocktail.sangria.server.reports.beans.FichePersonneBean;

import com.google.inject.Inject;

/**
 * Classe de service pour imprimer des documents
 */
public class ImpressionService {
	
	private final int reporterMaxProgress = 100;
	private CktlReporterAjaxProgress reporterProgress;
	private CktlAbstractReporter reporter;
	private ReportLocationFinder reportLocationFinder = new ReportLocationFinder();
	private String documentName;
	
	@Inject
	private ReportImpressionServiceFactory reportImpressionServiceFactory;
	
	/**
	 * imprime la fiche personne
	 * @param fichePersonneBean le bean avec les infos sur la personne
	 * @throws Exception en cas de soucis
	 */
	public void imprimerFichePersonne(FichePersonneBean fichePersonneBean) throws Exception {
		String reportName = "FichePersonne";
		setDocumentName("FichePersonne_" + fichePersonneBean.getNomAffichage() + "_" + fichePersonneBean.getPrenomAffichage() + ".pdf");
		String titreFenetreImpression = "Impression de la fiche personne";
		lancerImpression(reportName, fichePersonneBean, titreFenetreImpression);
	}
	
	public int getReporterMaxProgress() {
		return reporterMaxProgress;
	}

	public CktlReporterAjaxProgress getReporterProgress() {
		return reporterProgress;
	}

	public CktlAbstractReporter getReporter() {
		return reporter;
	}

	public String getDocumentName() {
		return documentName;
	}

	public void setDocumentName(String reportFilename) {
		this.documentName = reportFilename;
	}

	private void lancerImpression(String reportName, ReportImpressionData data, String titreFenetreImpression) throws Exception {
		reporterProgress = new CktlReporterAjaxProgress(reporterMaxProgress);
		URL jasperLocation = reportLocationFinder.searchJasperLocation(null, reportName);
		ReportImpressionService reportImpressionService = reportImpressionServiceFactory.create(jasperLocation, titreFenetreImpression, reporterProgress);
		reporter = reportImpressionService.imprimer(data);
	}
	
}
