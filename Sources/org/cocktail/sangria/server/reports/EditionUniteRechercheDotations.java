package org.cocktail.sangria.server.reports;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlrecherche.server.metier.EODotationAnnuelle;
import org.cocktail.fwkcktlwebapp.common.util.CktlXMLWriter;
import org.cocktail.reporting.server.CktlAbstractReporter;
import org.cocktail.reporting.server.jrxml.IJrxmlReportListener;
import org.cocktail.reporting.server.jrxml.JrxmlReporterWithXmlDataSource;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSComparator;

import er.extensions.eof.ERXQ;
import er.extensions.foundation.ERXArrayUtilities;

public class EditionUniteRechercheDotations extends EditionSangria {
	static public Logger LOG = Logger.getLogger(EditionUniteRechercheDotations.class);

	public static CktlAbstractReporter reporter(EOStructure uniteRecherche, IJrxmlReportListener listener) {
		
		String jasperFileName = "report/UniteRecherche/Dotations/Dotations.jasper";
		
        String recordPath = "/unite";
        JrxmlReporterWithXmlDataSource jr = null;
        try {
            StringWriter xmlString = creerXml(uniteRecherche);
            InputStream xmlFileStream = new ByteArrayInputStream(xmlString.toString().getBytes());
            jr = new JrxmlReporterWithXmlDataSource();
            Map<String, Object> parameters = new HashMap<String, Object>();
            jr.printWithThread("Dotations de l'unité de recherche", xmlFileStream, recordPath, pathForJasper(jasperFileName), parameters, CktlAbstractReporter.EXPORT_FORMAT_PDF, true, listener);
        }
        catch (Throwable e) {
            LOG.error("Une erreur s'est produite durant l'edition des dotations de l'unité de recherche", e);
        }
        return jr;
		
	}

	

    
    
    public static StringWriter creerXml(EOStructure uniteRecherche) throws Exception {
        
    	EOEditingContext editingContext = uniteRecherche.editingContext();
    	
    	StringWriter sw = new StringWriter();
        CktlXMLWriter w = new CktlXMLWriter(sw);
        w.setCharsToEscape("&");

        
        w.startDocument();
        w.startElement("unite");
        
        w.writeElement("libelle", uniteRecherche.llStructure());
        w.writeElement("acronyme", uniteRecherche.lcStructure());
        
        
               
        /** DOTATIONS **/
        
    	NSArray<EODotationAnnuelle> dotationsAnnuelles = EODotationAnnuelle.fetchAll(editingContext, EODotationAnnuelle.STRUCTURE.eq(uniteRecherche), null);
    	NSArray<Integer> annees = (NSArray<Integer>) dotationsAnnuelles.valueForKey(EODotationAnnuelle.ANNEE_KEY);
    
    	annees = ERXArrayUtilities.arrayWithoutDuplicates(annees).sortedArrayUsingComparator(NSComparator.DescendingNumberComparator);
    	Integer total = 0;
    	for(Integer annee : annees) {
    		NSArray<EODotationAnnuelle> dotationsPourAnnee = ERXQ.filtered(dotationsAnnuelles, EODotationAnnuelle.ANNEE.eq(annee));
    		Hashtable<String, String> parametresAnnee = new Hashtable<String, String>();
    		parametresAnnee.put("valeur", "" + annee);
    		w.startElement("annee", parametresAnnee);
    		Integer totalAnnee = 0;
    		for(EODotationAnnuelle dotationAnnuelle : dotationsPourAnnee) {
    			w.startElement("dotation");
    			w.writeElement("provenance", dotationAnnuelle.structureProvenance().llStructure());
    			w.writeElement("type", dotationAnnuelle.typeDotation().libelle());
    			w.writeElement("montant", "" + dotationAnnuelle.montant().intValue());
    			w.endElement();
    			
    			totalAnnee += dotationAnnuelle.montant().intValue();
    		}
    		
    		w.writeElement("totalAnnee", "" + totalAnnee);
    		total += totalAnnee;
           	w.endElement();
    	}
    	w.writeElement("total", "" + total);
        w.endElement();
        w.endDocument();
        w.close();

        
        return sw;
        
    }
}
