package org.cocktail.sangria.server.reports;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.cocktail.fwkcktlgrh.common.metier.EOAffectation;
import org.cocktail.fwkcktlgrh.common.metier.services.IndividusGradesService;
import org.cocktail.fwkcktlpersonne.common.metier.EOAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EOAssociationReseau;
import org.cocktail.fwkcktlpersonne.common.metier.EOCorps;
import org.cocktail.fwkcktlpersonne.common.metier.EOGrade;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartStructure;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlrecherche.server.metier.factory.FactoryAssociation;
import org.cocktail.fwkcktlsangriaguiajax.serveur.components.structures.controller.StructureRechercheMembresController;
import org.cocktail.fwkcktlwebapp.common.util.CktlXMLWriter;
import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;
import org.cocktail.reporting.server.CktlAbstractReporter;
import org.cocktail.reporting.server.jrxml.IJrxmlReportListener;
import org.cocktail.reporting.server.jrxml.JrxmlReporterWithXmlDataSource;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableSet;
import com.webobjects.foundation.NSSet;

import er.extensions.eof.ERXQ;
import er.extensions.foundation.ERXArrayUtilities;
import er.extensions.foundation.ERXTimestampUtilities;

public class EditionUniteRechercheCorpsEtGrades extends EditionSangria {

	public static Logger LOG = Logger.getLogger(EditionUniteRechercheCorpsEtGrades.class);

	
	public static CktlAbstractReporter reporter(EOStructure uniteRecherche, IJrxmlReportListener listener) {
		String jasperFileName = "report/UniteRecherche/Personnels/Grades.jasper";
		
        String recordPath = "/unite";
        JrxmlReporterWithXmlDataSource jr = null;
        try {
            StringWriter xmlString = creerXml(uniteRecherche);
            InputStream xmlFileStream = new ByteArrayInputStream(xmlString.toString().getBytes());
            jr = new JrxmlReporterWithXmlDataSource();
            Map<String, Object> parameters = new HashMap<String, Object>();
            jr.printWithThread("Grades des personnels de l'unité de recherche", xmlFileStream, recordPath, pathForJasper(jasperFileName), parameters, CktlAbstractReporter.EXPORT_FORMAT_XLS, true, listener);
        } catch (Throwable e) {
            LOG.error("Une erreur s'est produite durant l'edition des grades de l'unité de recherche", e);
        }
        return jr;
    }

    public static StringWriter creerXml(EOStructure uniteRecherche) throws Exception {
        
    	StringWriter sw = new StringWriter();
        CktlXMLWriter w = new CktlXMLWriter(sw);
        w.setCharsToEscape("&");

        w.startDocument();
        w.startElement("unite");
        
        w.writeElement("libelle", uniteRecherche.llStructure());
        w.writeElement("acronyme", uniteRecherche.lcStructure());

        NSArray<EOGrade> grades = grades(uniteRecherche);
        
        NSArray<EOCorps> corps = (NSArray<EOCorps>) grades.valueForKey(EOGrade.TO_CORPS_KEY);
        corps = ERXArrayUtilities.arrayWithoutDuplicates(corps);
        
        for (EOCorps c : corps) {
        	
        	w.startElement("corps");
        	
        	NSArray<EOGrade> gradesPourCorps = ERXQ.filtered(grades, ERXQ.equals(EOGrade.TO_CORPS_KEY, c));
      	
        	for (EOGrade grade : ERXArrayUtilities.arrayWithoutDuplicates(gradesPourCorps)) {
            		w.startElement("grade");
            		w.writeElement("libellegrade", grade.llGrade());
            		w.writeElement("nbgrade", "" + ERXQ.filtered(gradesPourCorps, EOGrade.C_GRADE.eq(grade.cGrade())).count());
            		w.endElement();
            }

        	w.writeElement("libellecorps", c.llCorps());
        	w.writeElement("nbcorps", "" + gradesPourCorps.count());
    		w.endElement();
        }
        
    	w.startElement("corps");
   		w.writeElement("libellecorps", "NON SPÉCIFIÉ");
   		Integer individusCount = individusUniteSelonSource(uniteRecherche).count();
   		w.writeElement("nbcorps", "" + (individusCount - grades.count()));
   		w.endElement();

        w.writeElement("total", "" + individusCount);
        
        w.endElement();
        w.endDocument();
        w.close();

        return sw;
    }
    
    private static NSArray<EOGrade> grades(EOStructure unite) {
    	EOEditingContext edc = unite.editingContext();

		NSArray<EOIndividu> individus = individusUniteSelonSource(unite);

		IndividusGradesService individusGradesService = IndividusGradesService.creerNouvelleInstance(edc);
		
		/** Récupération des grades **/
		NSDictionary<EOIndividu, EOGrade> gradesPourIndividus = individusGradesService.gradesPourIndividus(individus, ERXTimestampUtilities.today());

		NSArray<EOGrade> grades = gradesPourIndividus.allValues();

		return grades;
	}
    
    private static NSArray<EOIndividu> individusUniteSelonSource(EOStructure unite) {
    	StructureRechercheMembresController structureRechercheMembresController = new StructureRechercheMembresController(unite.editingContext(), null);
    	List<String> sources = structureRechercheMembresController.getSources();
		NSSet<EOIndividu> individus = new NSMutableSet<EOIndividu>();
		
		for (String source : sources) {
			if (StringUtils.equals(source, "RS")) {
				Collection<EORepartStructure> repartStructures = structureRechercheMembresController.getRepartStructures(unite);
				for (EORepartStructure rs : repartStructures) {
					individus.add(rs.toIndividu());
				}
			} else if (StringUtils.equals(source, "RA")) {
		        NSArray<EOAssociation> statutsMembres = (NSArray<EOAssociation>) FactoryAssociation.shared().typesMembresUniteRechercheAssociation(unite.editingContext()).toAssociationReseauPeres().valueForKey(EOAssociationReseau.TO_ASSOCIATION_FILS_KEY);
		        NSArray<EORepartAssociation> repartAssociationsMembres = unite.toRepartAssociationsElts(
		        		EORepartAssociation.TO_ASSOCIATION.in(statutsMembres)
		        		.and(EORepartAssociation.RAS_D_OUVERTURE.before(ERXTimestampUtilities.today()))
		        		.and(
		        				EORepartAssociation.RAS_D_FERMETURE.isNull().or(EORepartAssociation.RAS_D_FERMETURE.after(ERXTimestampUtilities.today()))
		        		)
		        	);
				for (EORepartAssociation ra : repartAssociationsMembres) {
					EOIndividu individu = (EOIndividu) ra.toPersonne();
					individus.add(individu);
				}
			} else if (StringUtils.equals(source, "A")) {
				NSArray<EOAffectation> affectations = structureRechercheMembresController.getAffectations(unite, DateCtrl.getDateJour());
				for (EOAffectation a : affectations) {
					individus.add(a.toIndividu());
				}
			}
		}
		
		return new NSArray<EOIndividu>(individus);
    }

	private static NSArray<EOIndividu> individusUnite(EOStructure unite) {
		EOEditingContext edc = unite.editingContext();
		/** Récupération des REPART_STRUCTURES **/
		NSMutableArray<String> repartStructuresPrefetchsKeyPaths = new NSMutableArray<String>();
		repartStructuresPrefetchsKeyPaths.add(EORepartStructure.TO_REPART_ASSOCIATIONS.key());

		NSArray<EOAssociation> statutsMembres = FactoryAssociation.shared().typesMembresUniteRechercheAssociation(edc).getFils(edc);
		
		NSArray<EORepartStructure> repartStructures = 
				EORepartStructure.fetchAll(
						edc, 
						EORepartStructure.TO_STRUCTURE_GROUPE.eq(unite),
						null, true, true,
						repartStructuresPrefetchsKeyPaths
					);

		
		/** Récupération des REPART_ASOCIATIONS **/
		NSMutableArray<String> repartAssociationsPrefetchsKeyPaths = new NSMutableArray<String>();
		repartAssociationsPrefetchsKeyPaths.add(EORepartAssociation.TO_ASSOCIATION.key());

		NSArray<EOAssociation> statutsMembresAvecAffectatioNsArray = 
				statutsMembres.arrayByAddingObject(FactoryAssociation.shared().affectation(edc));
		
		NSArray<EORepartAssociation> repartAssociations = EORepartAssociation
				.fetchAll(
						edc,
						EORepartAssociation.TO_STRUCTURE.eq(unite)
						.and(EORepartAssociation.TO_ASSOCIATION.in(statutsMembresAvecAffectatioNsArray))
						.and(EORepartAssociation.RAS_D_OUVERTURE.before(ERXTimestampUtilities.today()))
						.and(
							EORepartAssociation.RAS_D_FERMETURE.isNull()
							.or(EORepartAssociation.RAS_D_FERMETURE.after(ERXTimestampUtilities.today()))
						),
						null, true, true,
						repartAssociationsPrefetchsKeyPaths
					);

		/** Filtrage des résultats **/
		NSMutableArray<EORepartStructure> repartStructuresSeules = new NSMutableArray<EORepartStructure>();
		for (EORepartStructure repartStructure : repartStructures) {
			if (repartStructure.toRepartAssociations().isEmpty()) {
				repartStructuresSeules.add(repartStructure);
			}
		}

		NSMutableArray<EORepartAssociation> repartAssociationSeules = new NSMutableArray<EORepartAssociation>();
		for (EORepartAssociation repartAssociation : repartAssociations) {
			Integer nbRepartStructures = ERXQ.filtered(
					repartStructuresSeules,
					EORepartStructure.PERS_ID.eq(repartAssociation.persId())
					.and(EORepartStructure.TO_STRUCTURE_GROUPE.eq(repartAssociation.toStructure())))
					.count();
			if (nbRepartStructures == 0) {
				repartAssociationSeules.add(repartAssociation);
			}

		}

		/** Récupération des persIds **/
		NSMutableArray<Integer> persIdsMembres = new NSMutableArray<Integer>();

		NSArray<Integer> persIdsMembresRepartStructures = (NSArray<Integer>) repartStructuresSeules.valueForKey(EORepartStructure.PERS_ID.key());
		persIdsMembres.addAll(persIdsMembresRepartStructures);

		NSArray<Integer> persIdsMembresRepartAssociations = (NSArray<Integer>) repartAssociationSeules.valueForKey(EORepartAssociation.PERS_ID.key());
		persIdsMembres.addAll(persIdsMembresRepartAssociations);

		/** Recupération des individus **/ 
		NSArray<EOIndividu> individus = EOIndividu.fetchAllValides(edc, EOIndividu.PERS_ID.in(persIdsMembres), null);
		return individus;
	}
	
}
