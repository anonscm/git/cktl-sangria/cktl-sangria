package org.cocktail.sangria.server.reports;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;
import org.cocktail.fwkcktlrecherche.server.metier.EOContratRecherche;
import org.cocktail.fwkcktlrecherche.server.metier.EORepartApportDajCont;
import org.cocktail.fwkcktlrecherche.server.metier.EORepartApportDrvCont;
import org.cocktail.fwkcktlrecherche.server.metier.EORepartTypeResultatsContrat;
import org.cocktail.fwkcktlwebapp.common.util.CktlXMLWriter;
import org.cocktail.reporting.server.CktlAbstractReporter;
import org.cocktail.reporting.server.jrxml.IJrxmlReportListener;
import org.cocktail.reporting.server.jrxml.JrxmlReporterWithBeanCollectionDataSource;
import org.cocktail.reporting.server.jrxml.JrxmlReporterWithXmlDataSource;
import org.cocktail.sangria.server.reports.beans.ContratBean;
import org.cocktail.sangria.server.reports.beans.IndividuBean;
import org.cocktail.sangria.server.reports.beans.LaboratoireBean;
import org.cocktail.sangria.server.reports.beans.PartenaireBean;
import org.cocktail.sangria.server.reports.beans.StructureBean;
import org.cocktail.sangria.server.reports.beans.ValidationBean;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

public class EditionFicheContratBeans extends EditionSangria {

	static public Logger LOG = Logger.getLogger(EditionFicheContratBeans.class);

	public static CktlAbstractReporter reporter(EOContratRecherche contratRecherche, IJrxmlReportListener listener) {
		
		String jasperFileName = "report/ContratRecherche/FicheContrat/FicheContratBeans.jasper";
		
		
        JrxmlReporterWithBeanCollectionDataSource jr = null;
        try {
            jr = new JrxmlReporterWithBeanCollectionDataSource();
            Map<String, Object> parameters = new HashMap<String, Object>();
            List<Object> contrats = new ArrayList<Object>();
            contrats.add(creerBean(contratRecherche));
            jr.printWithThread("FicheContrat", contrats, pathForJasper(jasperFileName), parameters, CktlAbstractReporter.EXPORT_FORMAT_PDF, listener);
        } catch (Throwable e) {
            LOG.error("Une erreur s'est produite durant l'edition de la fiche contrat", e);
        }
        return jr;
		
	}

	

    
    
    public static ContratBean creerBean(EOContratRecherche contratRecherche) throws Exception {
        
        
        ContratBean bean = new ContratBean();

        bean.setExercice(contratRecherche.contrat().exerciceCocktail().exeExercice());
        bean.setIndex(contratRecherche.contrat().conIndex());
        bean.setObjet(contratRecherche.getObjet());
        bean.setType(contratRecherche.contrat().typeContrat().tyconLibelle());
        
        /** LABORATOIRES **/
        NSArray<EOStructure> laboratoires = contratRecherche.structureRechercheConcernees();
        for (EOStructure laboratoire : laboratoires) {
        	LaboratoireBean laboratoireBean = new LaboratoireBean();
        	laboratoireBean.setLibelle(laboratoire.lcStructure());
        	
        	NSArray<EOIndividu> responsables = contratRecherche.responsablesScientifiquesPourLaboratoire(laboratoire);
        	for (EOIndividu responsable : responsables) {
        	  IndividuBean individuBean = new IndividuBean();
        	  individuBean.setNom(responsable.nomAffichage());
        	  individuBean.setPrenom(responsable.prenomAffichage());
        	  laboratoireBean.getResponsablesScientifiques().add(individuBean);
        	}
        	
        	bean.getLaboratoires().add(laboratoireBean);
        }
        
        /** PARTENAIRES **/
        NSArray<IPersonne> partenaires = contratRecherche.partenaires();
        for (IPersonne partenaire : partenaires) {
        	PartenaireBean partenaireBean = new PartenaireBean();
        	partenaireBean.setLibelle(partenaire.getNomCompletAffichage());
        	bean.getPartenaires().add(partenaireBean);
        }
        
        if (contratRecherche.isContratSansIncidenceFinanciere()) {
        	bean.setIsContratSansIncidenceFinanciere(true);
        	bean.setMontant(null);
            bean.setEntrantSortant(null);
            bean.setTauxPrelevement(null); 
        } else {
        	bean.setIsContratSansIncidenceFinanciere(false);
        	bean.setMontant(contratRecherche.contrat().montantHt());
            bean.setEntrantSortant(contratRecherche.getLibelleSensMontantFinancier());
            bean.setTauxPrelevement(contratRecherche.tauxPrelevementEffectue()); 
            
            if (contratRecherche.etablisssementGestionnaireFinancier() != null) {
            	StructureBean structureBean = new StructureBean();
            	structureBean.setLibelle(contratRecherche.etablisssementGestionnaireFinancier().llStructure());
            	bean.setEtablissementGestionnaireFinancier(structureBean);
            }
            
        }  
        
        if (contratRecherche.validationFinanciere().valide()) {
        	ValidationBean validation = new ValidationBean();
        	validation.setType(ValidationBean.FINANCIERE);
        	validation.setCommentaire(contratRecherche.validationFinanciere().commentaire());
        	validation.setDate(contratRecherche.validationFinanciere().dateValidation());

        	if (contratRecherche.validateurFinancier() != null) {
        		IndividuBean validateur = new IndividuBean();
        		validateur.setNom(contratRecherche.validateurFinancier().nomAffichage());
        		validateur.setPrenom(contratRecherche.validateurFinancier().prenomAffichage());
        		validation.setValidateur(validateur);
        	}
        	bean.getValidations().add(validation);
        }
        
        if (contratRecherche.validationJuridique().valide()) {
        	ValidationBean validation = new ValidationBean();
        	validation.setType(ValidationBean.JURIDIQUE);
        	validation.setCommentaire(contratRecherche.validationJuridique().commentaire());
        	validation.setDate(contratRecherche.validationJuridique().dateValidation());

        	if (contratRecherche.getValidateurJuridique() != null) {
        		IndividuBean validateur = new IndividuBean();
        		validateur.setNom(contratRecherche.getValidateurJuridique().nomAffichage());
        		validateur.setPrenom(contratRecherche.getValidateurJuridique().prenomAffichage());
        		validation.setValidateur(validateur);
        	}
        	bean.getValidations().add(validation);
        }
        
        if (contratRecherche.validationValorisation().valide()) {
        	ValidationBean validation = new ValidationBean();
        	validation.setType(ValidationBean.VALORISATION);
        	validation.setCommentaire(contratRecherche.validationValorisation().commentaire());
        	validation.setDate(contratRecherche.validationValorisation().dateValidation());

        	if (contratRecherche.validateurValo() != null) {
        		IndividuBean validateur = new IndividuBean();
        		validateur.setNom(contratRecherche.validateurValo().nomAffichage());
        		validateur.setPrenom(contratRecherche.validateurValo().prenomAffichage());
        		validation.setValidateur(validateur);
        	}
        	bean.getValidations().add(validation);
        }
         
        bean.setRedactionContrat((String) EOContratRecherche.Nomenclatures.typesRedactionContrat().valueForKey(contratRecherche.redactionContrat()));
        bean.setRedactionClausePI((String) EOContratRecherche.Nomenclatures.typesRedactionClausesPi().valueForKey(contratRecherche.redactionClausesPi()));
        
        for (EORepartApportDrvCont apportDrvCont : contratRecherche.toRepartApportDrvContrats()) {
          bean.getApportDirectionRecherche().add((String) EOContratRecherche.Nomenclatures.typesApportDrvContrat().valueForKey(apportDrvCont.apportContDrv()));
        }
      
        for (EORepartApportDajCont apportDajCont : contratRecherche.toRepartApportDajContrats()) {
          bean.getApportDirectionJuridique().add((String) EOContratRecherche.Nomenclatures.typesApportDajContrat().valueForKey(apportDajCont.apportContDaj()));
        }
        
        for(EORepartTypeResultatsContrat typeRes : contratRecherche.toRepartTypeResultatsContrats()) {
          bean.getResultatAttendu().add((String) EOContratRecherche.Nomenclatures.typesDeResultats().valueForKey(typeRes.typeResultatsContrat()));
        }    
        
        bean.setProprieteResultats((String) EOContratRecherche.Nomenclatures.typesProprieteResultats().valueForKey(contratRecherche.proprieteDesResultats()));
        bean.setExploitationResultat((String) EOContratRecherche.Nomenclatures.typesExploitationResultats().valueForKey(contratRecherche.exploitationDesResultats()));
        bean.setSuiviValorisation((String) EOContratRecherche.Nomenclatures.typeSuiviValo().valueForKey(contratRecherche.suiviValo()));

        return bean;
    }
    
    
}
