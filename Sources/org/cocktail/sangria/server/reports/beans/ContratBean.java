package org.cocktail.sangria.server.reports.beans;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


/**
 * Bean pour les infos d'un contrat 
 *
 */
public class ContratBean {

	private Integer exercice;
	private Integer index;
	private String objet;
	private String objetCourt;
	private String type;
	private List<LaboratoireBean> laboratoires = new ArrayList<LaboratoireBean>();
	private List<PartenaireBean> partenaires = new ArrayList<PartenaireBean>();
	private BigDecimal montant;
	private Float tauxPrelevement;
	private StructureBean etablissementGestionnaireFinancier;
	private List<ValidationBean> validations = new ArrayList<ValidationBean>();
	private String redactionContrat;
	private String redactionClausePI;
	private List<String> apportDirectionRecherche = new ArrayList<String>();
	private List<String> apportDirectionJuridique = new ArrayList<String>();
	private List<String> resultatAttendu = new ArrayList<String>();
	private String proprieteResultats;
	private String exploitationResultat;
	private String suiviValorisation;
	private String entrantSortant;
	private Boolean isContratSansIncidenceFinanciere;
	
	public Integer getExercice() {
		return exercice;
	}
	public void setExercice(Integer exercice) {
		this.exercice = exercice;
	}
	public Integer getIndex() {
		return index;
	}
	public void setIndex(Integer index) {
		this.index = index;
	}
	public String getObjet() {
		return objet;
	}
	public void setObjet(String objet) {
		this.objet = objet;
	}
	public String getObjetCourt() {
		return objetCourt;
	}
	public void setObjetCourt(String objetCourt) {
		this.objetCourt = objetCourt;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public List<LaboratoireBean> getLaboratoires() {
		return laboratoires;
	}
	public void setLaboratoires(List<LaboratoireBean> laboratoires) {
		this.laboratoires = laboratoires;
	}

	public List<PartenaireBean> getPartenaires() {
		return partenaires;
	}
	public void setPartenaires(List<PartenaireBean> partenaires) {
		this.partenaires = partenaires;
	}
	public BigDecimal getMontant() {
		return montant;
	}
	public void setMontant(BigDecimal montant) {
		this.montant = montant;
	}
	public Float getTauxPrelevement() {
		return tauxPrelevement;
	}
	public void setTauxPrelevement(Float tauxPrelevement) {
		this.tauxPrelevement = tauxPrelevement;
	}
	public StructureBean getEtablissementGestionnaireFinancier() {
		return etablissementGestionnaireFinancier;
	}
	public void setEtablissementGestionnaireFinancier(StructureBean etablissementGestionnaireFinancier) {
		this.etablissementGestionnaireFinancier = etablissementGestionnaireFinancier;
	}

	public String getRedactionContrat() {
		return redactionContrat;
	}
	public void setRedactionContrat(String redactionContrat) {
		this.redactionContrat = redactionContrat;
	}
	public String getRedactionClausePI() {
		return redactionClausePI;
	}
	public void setRedactionClausePI(String redactionClausePI) {
		this.redactionClausePI = redactionClausePI;
	}
	public String getProprieteResultats() {
		return proprieteResultats;
	}
	public void setProprieteResultats(String proprieteResultats) {
		this.proprieteResultats = proprieteResultats;
	}
	public String getExploitationResultat() {
		return exploitationResultat;
	}
	public void setExploitationResultat(String exploitationResultat) {
		this.exploitationResultat = exploitationResultat;
	}
	public String getSuiviValorisation() {
		return suiviValorisation;
	}
	public void setSuiviValorisation(String suiviValorisation) {
		this.suiviValorisation = suiviValorisation;
	}
	
	public static Collection<ContratBean> getContratBeansList() {
		return new ArrayList<ContratBean>();
	}
	public List<String> getApportDirectionRecherche() {
		return apportDirectionRecherche;
	}
	public void setApportDirectionRecherche(List<String> apportDirectionRecherche) {
		this.apportDirectionRecherche = apportDirectionRecherche;
	}
	public List<String> getApportDirectionJuridique() {
		return apportDirectionJuridique;
	}
	public void setApportDirectionJuridique(List<String> apportDirectionJuridique) {
		this.apportDirectionJuridique = apportDirectionJuridique;
	}
	public List<String> getResultatAttendu() {
		return resultatAttendu;
	}
	public void setResultatAttendu(List<String> resultatAttendu) {
		this.resultatAttendu = resultatAttendu;
	}
	public List<ValidationBean> getValidations() {
		return validations;
	}
	public void setValidations(List<ValidationBean> validations) {
		this.validations = validations;
	}
	public String getEntrantSortant() {
		return entrantSortant;
	}
	public void setEntrantSortant(String entrantSortant) {
		this.entrantSortant = entrantSortant;
	}
	public Boolean getIsContratSansIncidenceFinanciere() {
		return isContratSansIncidenceFinanciere;
	}
	public void setIsContratSansIncidenceFinanciere(
			Boolean isContratSansIncidenceFinanciere) {
		this.isContratSansIncidenceFinanciere = isContratSansIncidenceFinanciere;
	}
	
}
