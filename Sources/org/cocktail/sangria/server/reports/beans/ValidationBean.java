package org.cocktail.sangria.server.reports.beans;

import java.util.Date;

public class ValidationBean {
	
	public static final String FINANCIERE = "financière";
	public static final String JURIDIQUE = "juridique";
	public static final String VALORISATION = "valorisation";
	
	private String type;
	private Date date;

	private IndividuBean validateur;
	private String commentaire;
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public IndividuBean getValidateur() {
		return validateur;
	}
	public void setValidateur(IndividuBean validateur) {
		this.validateur = validateur;
	}
	public String getCommentaire() {
		return commentaire;
	}
	public void setCommentaire(String commentaire) {
		this.commentaire = commentaire;
	}
}
