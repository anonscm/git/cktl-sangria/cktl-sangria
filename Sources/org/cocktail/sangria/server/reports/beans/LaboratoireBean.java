package org.cocktail.sangria.server.reports.beans;

import java.util.ArrayList;
import java.util.List;

public class LaboratoireBean {
	
	private String libelle;
	private List<IndividuBean> responsablesScientifiques = new ArrayList<IndividuBean>();
	
	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	public List<IndividuBean> getResponsablesScientifiques() {
		return responsablesScientifiques;
	}

	public void setResponsablesScientifiques(List<IndividuBean> responsablesScientifiques) {
		this.responsablesScientifiques = responsablesScientifiques;
	}
	
	
}
