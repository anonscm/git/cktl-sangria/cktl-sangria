package org.cocktail.sangria.server.reports.beans;

public class StructureBean {
	private String libelle;

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}
	
}
