package org.cocktail.sangria.server.reports.beans;

import java.util.ArrayList;

import org.cocktail.fwkcktlreportingextended.serveur.services.impression.ReportImpressionData;


/**
 * Bean pour les infos d'une fiche personne
 */
public class FichePersonneBean implements ReportImpressionData {

	private String nomAffichage;
	private String prenomAffichage;
	private ArrayList<FichePersonneRattachementBean> rattachementsActuels = new ArrayList<FichePersonneRattachementBean>();
	private String position;
	private String composante;
	private String etablissement;
	private Boolean isEnseignantChercheur;
	private String codeBap;
	private String categorie;
	private String quotite;
	private String grade;
	private String dateDoctorat;
	private String dateHdr;
	private String equipeRattachement;
	private String employeur;
	private String site;
	private String adressePro;
	private String codeCnu;
	private String typeContrat;
	private ArrayList<FichePersonneRattachementBean> rattachementsPrecedents = new ArrayList<FichePersonneRattachementBean>();
	
	public String getNomAffichage() {
		return nomAffichage;
	}

	public void setNomAffichage(String nomAffichage) {
		this.nomAffichage = nomAffichage;
	}

	public String getPrenomAffichage() {
		return prenomAffichage;
	}

	public void setPrenomAffichage(String prenomAffichage) {
		this.prenomAffichage = prenomAffichage;
	}

	public ArrayList<FichePersonneRattachementBean> getRattachementsActuels() {
	    return rattachementsActuels;
    }

	public void setRattachementsActuels(ArrayList<FichePersonneRattachementBean> rattachementsActuels) {
	    this.rattachementsActuels = rattachementsActuels;
    }
	
	/**
	 * Ajoute un rattachement à la liste des rattachements actuels
	 * @param rattachement le rattachement à ajouter
	 */
	public void addRattachementActuel(FichePersonneRattachementBean rattachement) {
		getRattachementsActuels().add(rattachement);
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getComposante() {
		return composante;
	}

	public void setComposante(String composante) {
		this.composante = composante;
	}

	public String getEtablissement() {
		return etablissement;
	}

	public void setEtablissement(String etablissement) {
		this.etablissement = etablissement;
	}

	public String getCodeBap() {
		return codeBap;
	}

	public void setCodeBap(String codeBap) {
		this.codeBap = codeBap;
	}

	public String getCategorie() {
		return categorie;
	}

	public void setCategorie(String categorie) {
		this.categorie = categorie;
	}

	public String getQuotite() {
		return quotite;
	}

	public void setQuotite(String quotite) {
		this.quotite = quotite;
	}

	public String getGrade() {
		return grade;
	}

	public void setGrade(String grade) {
		this.grade = grade;
	}

	public String getDateDoctorat() {
		return dateDoctorat;
	}

	public void setDateDoctorat(String dateDoctorat) {
		this.dateDoctorat = dateDoctorat;
	}

	public String getDateHdr() {
		return dateHdr;
	}

	public void setDateHdr(String dateHdr) {
		this.dateHdr = dateHdr;
	}

	public String getEquipeRattachement() {
		return equipeRattachement;
	}

	public void setEquipeRattachement(String equipeRattachement) {
		this.equipeRattachement = equipeRattachement;
	}

	public String getEmployeur() {
		return employeur;
	}

	public void setEmployeur(String employeur) {
		this.employeur = employeur;
	}

	public String getSite() {
		return site;
	}

	public void setSite(String site) {
		this.site = site;
	}

	public String getAdressePro() {
		return adressePro;
	}

	public void setAdressePro(String adressePro) {
		this.adressePro = adressePro;
	}

	public String getCodeCnu() {
		return codeCnu;
	}

	public void setCodeCnu(String codeCnu) {
		this.codeCnu = codeCnu;
	}

	public String getTypeContrat() {
		return typeContrat;
	}

	public void setTypeContrat(String typeContrat) {
		this.typeContrat = typeContrat;
	}

	public ArrayList<FichePersonneRattachementBean> getRattachementsPrecedents() {
		return rattachementsPrecedents;
	}

	public void setRattachementsPrecedents(ArrayList<FichePersonneRattachementBean> rattachementsPrecedents) {
		this.rattachementsPrecedents = rattachementsPrecedents;
	}
	
	/**
	 * Ajoute un rattachement à la liste des rattachements précédents
	 * @param rattachement le rattachement à ajouter
	 */
	public void addRattachementPrecedent(FichePersonneRattachementBean rattachement) {
		getRattachementsPrecedents().add(rattachement);
	}
	
	public Boolean getIsEnseignantChercheur() {
	    return isEnseignantChercheur;
    }

	public void setIsEnseignantChercheur(Boolean isEnseignantChercheur) {
	    this.isEnseignantChercheur = isEnseignantChercheur;
    }
	
	/**
	 * Bean pour un statut dans une structure
	 */
	public class FichePersonneStatutBean {
		private String dateDebut;
		private String dateFin;
		private String statut;
		
		public String getDateDebut() {
			return dateDebut;
		}
		
		public void setDateDebut(String dateDebut) {
			this.dateDebut = dateDebut;
		}
		
		public String getDateFin() {
			return dateFin;
		}
		
		public void setDateFin(String dateFin) {
			this.dateFin = dateFin;
		}
		
		public String getStatut() {
			return statut;
		}
		
		public void setStatut(String statut) {
			this.statut = statut;
		}
		
	}
	
	/**
	 * Bean pour la fonction dans une structure
	 */
	public class FichePersonneFonctionBean {
		private String dateDebut;
		private String dateFin;
		private String fonction;
		
		public String getDateDebut() {
			return dateDebut;
		}
		
		public void setDateDebut(String dateDebut) {
			this.dateDebut = dateDebut;
		}
		
		public String getDateFin() {
			return dateFin;
		}
		
		public void setDateFin(String dateFin) {
			this.dateFin = dateFin;
		}
		
		public String getFonction() {
			return fonction;
		}
		
		public void setFonction(String fonction) {
			this.fonction = fonction;
		}
		
	}

	/**
	 * Bean pour les rattachements
	 */
	public class FichePersonneRattachementBean {
		private String structure;
		private ArrayList<FichePersonneStatutBean> statuts = new ArrayList<FichePersonneStatutBean>();
		private ArrayList<FichePersonneFonctionBean> fonctions = new ArrayList<FichePersonneFonctionBean>();
		
		/**
		 * Constructeur
		 */
		public FichePersonneRattachementBean() {
			
		}
		
		public String getStructure() {
			return structure;
		}
		
		public void setStructure(String structure) {
			this.structure = structure;
		}

		public ArrayList<FichePersonneStatutBean> getStatuts() {
	        return statuts;
        }

		public void setStatuts(ArrayList<FichePersonneStatutBean> statuts) {
	        this.statuts = statuts;
        }

		public ArrayList<FichePersonneFonctionBean> getFonctions() {
	        return fonctions;
        }

		public void setFonctions(ArrayList<FichePersonneFonctionBean> fonctions) {
	        this.fonctions = fonctions;
        }
		
		/**
		 * Ajoute un statut à la liste des rattachements 
		 * @param statut le statut à ajouter
		 */
		public void addStatut(FichePersonneStatutBean statut) {
			getStatuts().add(statut);
		}
		
		/**
		 * Ajoute une fonction à la liste des rattachements 
		 * @param fonction la fonction à ajouter
		 */
		public void addFonction(FichePersonneFonctionBean fonction) {
			getFonctions().add(fonction);
		}
		
	}

}
