package org.cocktail.sangria.server.reports;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;
import org.cocktail.fwkcktlrecherche.server.metier.EOContratRecherche;
import org.cocktail.fwkcktlrecherche.server.metier.EORepartApportDajCont;
import org.cocktail.fwkcktlrecherche.server.metier.EORepartApportDrvCont;
import org.cocktail.fwkcktlrecherche.server.metier.EORepartTypeResultatsContrat;
import org.cocktail.fwkcktlwebapp.common.util.CktlXMLWriter;
import org.cocktail.reporting.server.CktlAbstractReporter;
import org.cocktail.reporting.server.jrxml.IJrxmlReportListener;
import org.cocktail.reporting.server.jrxml.JrxmlReporterWithXmlDataSource;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

public class EditionFicheContrat extends EditionSangria {

	static public Logger LOG = Logger.getLogger(EditionFicheContrat.class);

	public static CktlAbstractReporter reporter(EOContratRecherche contratRecherche, IJrxmlReportListener listener) {
		
		String jasperFileName = "report/ContratRecherche/FicheContrat/FicheContrat.jasper";
		
		
        String recordPath = "/contrat";
        JrxmlReporterWithXmlDataSource jr = null;
        try {
            StringWriter xmlString = creerXml(contratRecherche);
            InputStream xmlFileStream = new ByteArrayInputStream(xmlString.toString().getBytes());
            jr = new JrxmlReporterWithXmlDataSource();
            Map<String, Object> parameters = new HashMap<String, Object>();
            jr.printWithThread("Fiche contrat", xmlFileStream, recordPath, pathForJasper(jasperFileName), parameters, CktlAbstractReporter.EXPORT_FORMAT_PDF, true, listener);
        }
        catch (Throwable e) {
            LOG.error("Une erreur s'est produite durant l'edition de la fiche contrat", e);
        }
        return jr;
		
	}

	

    
    
    public static StringWriter creerXml(EOContratRecherche contratRecherche) throws Exception {
        
    	EOEditingContext editingContext = contratRecherche.editingContext();
    	
    	StringWriter sw = new StringWriter();
        CktlXMLWriter w = new CktlXMLWriter(sw);
        w.setCharsToEscape("&");
        
        w.startDocument();
        w.startElement("contrat");
        
        w.writeElement("numero", contratRecherche.contrat().exerciceEtIndex());
        w.writeElement("objet", contratRecherche.getObjet());
        w.writeElement("type", contratRecherche.contrat().typeContrat().tyconLibelle());
        
        
        /** LABORATOIRES **/
        NSArray<EOStructure> laboratoires = contratRecherche.structureRechercheConcernees();
        for(EOStructure laboratoire : laboratoires) {
        	w.startElement("laboratoire");
        	w.writeElement("nom", laboratoire.lcStructure());
        	NSArray<EOIndividu> responsables = contratRecherche.responsablesScientifiquesPourLaboratoire(laboratoire);
        	for(EOIndividu responsable : responsables) {
        	  w.writeElement("responsable", responsable.getNomAndPrenom());
        	}
        	w.endElement();
        }
        
        /** PARTENAIRES **/
        NSArray<IPersonne> partenaires = contratRecherche.partenaires();
        for(IPersonne partenaire : partenaires) {
        	w.writeElement("partenaire", partenaire.getNomCompletAffichage());
        }
        
        if (contratRecherche.isContratSansIncidenceFinanciere() || contratRecherche.contrat().montantHt() == null) {
        	w.writeElement("montant", "");
        } else {
        	w.writeElement("montant", contratRecherche.contrat().montantHt().toString());
        }
        
        if(contratRecherche.tauxPrelevementEffectue() != null) {
          w.writeElement("tauxPrelevement", "" + contratRecherche.tauxPrelevementEffectue());
        } else {
          w.writeElement("tauxPrelevement", "");
        }
        
        if(contratRecherche.etablisssementGestionnaireFinancier() != null) {
          w.writeElement("etablissementGestionnaireFinancier", contratRecherche.etablisssementGestionnaireFinancier().llStructure());
        } else {
          w.writeElement("etablissementGestionnaireFinancier", "");
        }
        
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

        if(contratRecherche.validationFinanciere().valide()) {
          w.startElement("validation");
          w.writeElement("type", "financière");
          w.writeElement("commentaire", contratRecherche.validationFinanciere().commentaire());
          if(contratRecherche.validationFinanciere().dateValidation() != null) {
            NSTimestamp date  = contratRecherche.validationFinanciere().dateValidation();
            w.writeElement("date", sdf.format(date));
          } else {
            w.writeElement("date", "");
          }
          if(contratRecherche.validateurFinancier() != null) {
            w.writeElement("validateur", contratRecherche.validateurFinancier().getNomAndPrenom());
          } else {
            w.writeElement("validateur", "");
          }
          w.endElement();
        }
        
        if(contratRecherche.validationJuridique().valide()) {
          w.startElement("validation");
          w.writeElement("type", "juridique");
          w.writeElement("commentaire", contratRecherche.validationJuridique().commentaire());
          if(contratRecherche.validationJuridique().dateValidation() != null) {
            NSTimestamp date  = contratRecherche.validationJuridique().dateValidation();
            w.writeElement("date", sdf.format(date));
          } else {
            w.writeElement("date", "");
          }
          if(contratRecherche.getValidateurJuridique() != null) {
            w.writeElement("validateur", contratRecherche.getValidateurJuridique().getNomAndPrenom());
          } else {
            w.writeElement("validateur", "");
          }
          w.endElement();
        }
        
        if(contratRecherche.validationValorisation().valide()) {
          w.startElement("validation");
          w.writeElement("type", "valorisation");
          w.writeElement("commentaire", contratRecherche.validationValorisation().commentaire());
          if(contratRecherche.validationValorisation().dateValidation() != null) {
            NSTimestamp date  = contratRecherche.validationValorisation().dateValidation();
            w.writeElement("date", sdf.format(date));
          } else {
            w.writeElement("date", "");
          }
          if(contratRecherche.validateurValo() != null) {
            w.writeElement("validateur", contratRecherche.validateurValo().getNomAndPrenom());
          } else {
            w.writeElement("validateur", "");
          }
          w.endElement();
        }
 
        if(contratRecherche.redactionContrat() != null) {        
          w.writeElement("redactionContrat", (String) EOContratRecherche.Nomenclatures.typesRedactionContrat().valueForKey(contratRecherche.redactionContrat()));
        }
     
        if(contratRecherche.redactionClausesPi() != null) {        
          w.writeElement("redactionClausePI", (String) EOContratRecherche.Nomenclatures.typesRedactionClausesPi().valueForKey(contratRecherche.redactionClausesPi()));
        }
        
        for(EORepartApportDrvCont apportDrvCont : contratRecherche.toRepartApportDrvContrats()) {
          w.writeElement("apportDirectionRecherche", (String) EOContratRecherche.Nomenclatures.typesApportDrvContrat().valueForKey(apportDrvCont.apportContDrv()));
        }
      
        for(EORepartApportDajCont apportDajCont : contratRecherche.toRepartApportDajContrats()) {
          w.writeElement("apportDirectionJuridique", (String) EOContratRecherche.Nomenclatures.typesApportDajContrat().valueForKey(apportDajCont.apportContDaj()));
        }
        
        for(EORepartTypeResultatsContrat typeRes : contratRecherche.toRepartTypeResultatsContrats()) {
          w.writeElement("resultatAttendu", (String) EOContratRecherche.Nomenclatures.typesDeResultats().valueForKey(typeRes.typeResultatsContrat()));
        }    
        
        if(contratRecherche.proprieteDesResultats() != null) {        
          w.writeElement("proprieteResultats", (String) EOContratRecherche.Nomenclatures.typesProprieteResultats().valueForKey(contratRecherche.proprieteDesResultats()));
        }
        
        if(contratRecherche.exploitationDesResultats() != null) {        
          w.writeElement("exploitationResultat", (String) EOContratRecherche.Nomenclatures.typesExploitationResultats().valueForKey(contratRecherche.exploitationDesResultats()));
        }
        
        if(contratRecherche.suiviValo() != null) {        
          w.writeElement("suiviValorisation", (String) EOContratRecherche.Nomenclatures.typeSuiviValo().valueForKey(contratRecherche.suiviValo()));
        }
        
        w.endElement();
        w.endDocument();
        w.close();

        
        return sw;
        
    }
    
    
}
