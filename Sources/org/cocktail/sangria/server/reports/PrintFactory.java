package org.cocktail.sangria.server.reports;

import java.sql.Connection;
import java.util.HashMap;

import org.cocktail.fwkcktlwebapp.server.CktlDataResponse;
import org.cocktail.reporting.server.jrxml.JrxmlReporter;
import org.cocktail.sangria.server.Application;
import org.cocktail.sangria.server.Session;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSData;
import com.webobjects.foundation.NSMutableDictionary;

public class PrintFactory {
	public static final String JASPER_CONTRATS = "contrats.jasper";

	public static NSData printContrats(Session session, NSArray<Integer> con_ordres) throws Exception {
		Application app = ((Application) Application.application());

		String reportFileName = app.getReportsLocation(JASPER_CONTRATS);
		Connection connection = app.getJDBCConnection();

		int i = 0;
		String string_con_ordres = "";
		for(Integer con_ordre : con_ordres) {
			if(i > 0) {
				string_con_ordres += ",";
			}
			string_con_ordres += con_ordre;
			i++;
		}
		
	    NSMutableDictionary parametres = new NSMutableDictionary();
	    parametres.takeValueForKey(string_con_ordres, "CONTRATS");
		HashMap aMap = new HashMap();
		aMap.putAll(parametres.hashtable());
		
		String sqlQuery  = "select * from accords.contrat where exe_ordre = 2011"; 
	    JrxmlReporter reporter = new JrxmlReporter();
	    try {
	    	return reporter.printNow(connection, null, reportFileName, aMap, JrxmlReporter.EXPORT_FORMAT_PDF, false, null);
	    } catch (Throwable e) {
	    	throw new Exception(e); 
	    }
 
	}

	public static WOActionResults afficherPdf(NSData pdfData, String fileName) {
		CktlDataResponse resp = new CktlDataResponse();
		if (pdfData != null) {
			resp.setContent(pdfData, CktlDataResponse.MIME_PDF);
			resp.setFileName(fileName);
		}
		else {
			resp.setContent("");
			resp.setHeader("0", "Content-Length");
		}
		return resp.generateResponse();
	}

}
