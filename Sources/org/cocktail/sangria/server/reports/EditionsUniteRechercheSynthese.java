package org.cocktail.sangria.server.reports;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.cocktail.fwkcktlgrh.common.metier.EOAffectation;
import org.cocktail.fwkcktlpersonne.common.metier.EOAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EOAssociationReseau;
import org.cocktail.fwkcktlpersonne.common.metier.EODomaineScientifique;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlrecherche.server.metier.EODotationAnnuelle;
import org.cocktail.fwkcktlrecherche.server.metier.factory.FactoryAssociation;
import org.cocktail.fwkcktlrecherche.server.metier.service.StatutAffectesService;
import org.cocktail.fwkcktlrecherche.server.util.StructuresRechercheHelper;
import org.cocktail.fwkcktlsangriaguiajax.serveur.components.structures.controller.StructureRechercheMembresController;
import org.cocktail.fwkcktlwebapp.common.util.CktlXMLWriter;
import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;
import org.cocktail.reporting.server.CktlAbstractReporter;
import org.cocktail.reporting.server.jrxml.IJrxmlReportListener;
import org.cocktail.reporting.server.jrxml.JrxmlReporterWithXmlDataSource;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSComparator;
import com.webobjects.foundation.NSDictionary;

import er.extensions.eof.ERXKey;
import er.extensions.eof.ERXQ;
import er.extensions.foundation.ERXArrayUtilities;
import er.extensions.foundation.ERXTimestampUtilities;

public class EditionsUniteRechercheSynthese extends EditionSangria {

	static public Logger LOG = Logger.getLogger(EditionsUniteRechercheSynthese.class);

	public static CktlAbstractReporter reporter(EOStructure uniteRecherche, IJrxmlReportListener listener) {
		
		String jasperFileName = "report/UniteRecherche/Synthese/SyntheseUniteRecherche.jasper";
		
        String recordPath = "/unite";
        JrxmlReporterWithXmlDataSource jr = null;
        try {
            StringWriter xmlString = creerXml(uniteRecherche);
            InputStream xmlFileStream = new ByteArrayInputStream(xmlString.toString().getBytes());
            jr = new JrxmlReporterWithXmlDataSource();
            Map<String, Object> parameters = new HashMap<String, Object>();
            jr.printWithThread("Synthèse de l'unité de recherche", xmlFileStream, recordPath, pathForJasper(jasperFileName), parameters, CktlAbstractReporter.EXPORT_FORMAT_PDF, true, listener);
        } catch (Throwable e) {
            LOG.error("Une erreur s'est produite durant l'edition de la synthèse de l'unité de recherche", e);
        }
        return jr;
		
	}
	
    public static StringWriter creerXml(EOStructure uniteRecherche) throws Exception {
        
    	EOEditingContext editingContext = uniteRecherche.editingContext();
    	
    	StringWriter sw = new StringWriter();
        CktlXMLWriter w = new CktlXMLWriter(sw);
        w.setCharsToEscape("&");

        
        w.startDocument();
        w.startElement("unite");
        
        w.writeElement("libelle", uniteRecherche.llStructure());
        w.writeElement("acronyme", uniteRecherche.lcStructure());
        
        
        /** TUTELLES **/
        NSArray<EOStructure> tutelles = StructuresRechercheHelper.tutellesStructureRecherche(editingContext, uniteRecherche);
        for (EOStructure tutelle : tutelles) {
        	w.startElement("tutelle");
        	w.writeElement("libelle", tutelle.llStructure());
        	w.endElement();
        }
        
        /** ETABLISSEMENTS DE RATTACHEMENT **/
        NSArray<EOStructure> ers = StructuresRechercheHelper.etablissementsRattachementPourStructureRecherche(editingContext, uniteRecherche);
        for (EOStructure er : ers) {
        	w.startElement("er");
        	w.writeElement("libelle", er.llStructure());
        	w.endElement();
        }
        
        /** DOMAINES SCIENTIFIQUES **/
        EODomaineScientifique domainePrincipal = StructuresRechercheHelper.domaineScientifiquePrincipalUniteRecherche(editingContext, uniteRecherche, false);
        w.startElement("ds");
        w.writeElement("libelle", domainePrincipal.dsCodePlusLibelle());
        w.writeElement("principal", "1");
        w.endElement();
        
        NSArray<EODomaineScientifique> domainesSecondaires = StructuresRechercheHelper.domainesScientifiquesSecondaireUniteRecherches(editingContext, uniteRecherche, false);
        for (EODomaineScientifique domaine : domainesSecondaires) {
        	w.startElement("ds");
        	w.writeElement("libelle", domaine.dsCodePlusLibelle());
        	w.writeElement("principal", "0");
        	w.endElement();
        }
        
        /** STATUTS **/
        NSArray<EOAssociation> statutsMembres = (NSArray<EOAssociation>) FactoryAssociation.shared().typesMembresUniteRechercheAssociation(editingContext).toAssociationReseauPeres().valueForKey(EOAssociationReseau.TO_ASSOCIATION_FILS_KEY);
        
        StructureRechercheMembresController structureRechercheMembresController = new StructureRechercheMembresController(editingContext, null);
        List<String> sources = structureRechercheMembresController.getSources();
        Map<Integer, EOAssociation> infoMembres = new HashMap<Integer, EOAssociation>();
        
        for (String source : sources) {
			if (StringUtils.equals(source, "RA")) {
				NSArray<EORepartAssociation> repartAssociationsMembres = uniteRecherche.toRepartAssociationsElts(
		        		EORepartAssociation.TO_ASSOCIATION.in(statutsMembres)
		        		.and(EORepartAssociation.RAS_D_OUVERTURE.before(ERXTimestampUtilities.today()))
		        		.and(
		        				EORepartAssociation.RAS_D_FERMETURE.isNull().or(EORepartAssociation.RAS_D_FERMETURE.after(ERXTimestampUtilities.today()))
		        		)
		        	);
				for (EORepartAssociation ra : repartAssociationsMembres) {
					infoMembres.put(ra.toPersonne().persId(), ra.toAssociation());
				}
			} else if (StringUtils.equals(source, "A")) {
				
				NSArray<EOAffectation> affectations = structureRechercheMembresController.getAffectations(uniteRecherche, DateCtrl.getDateJour());
				NSArray<EOIndividu> individusAffectes = (NSArray<EOIndividu>) affectations.valueForKey(EOAffectation.TO_INDIVIDU_KEY);
				NSDictionary<EOIndividu, EOAssociation> statutsAffectes = 
						StatutAffectesService.creerNouvelleInstance(editingContext).statutsPourIndividusAffectesEtDate(individusAffectes, DateCtrl.getDateJour());
				for (EOAffectation a : affectations) {
					infoMembres.put(a.toIndividu().persId(), statutsAffectes.objectForKey(a.toIndividu()));
				}
			}
        }
        
        NSArray<EOAssociation> associationsDesMembres = new NSArray<EOAssociation>(infoMembres.values());
        
        for (EOAssociation statut : statutsMembres) {
        	w.startElement("statutMembre");
        	w.writeElement("libelle", statut.assLibelle());
        	w.writeElement("nb", "" + ERXQ.filtered(associationsDesMembres, EOAssociation.ASS_LIBELLE.eq(statut.assLibelle())).count());
        	w.endElement();
        }
        
        /** DOTATIONS **/
        
    	NSArray<EODotationAnnuelle> dotationsAnnuelles = EODotationAnnuelle.fetchAll(editingContext, EODotationAnnuelle.STRUCTURE.eq(uniteRecherche), null);
    	NSArray<Integer> annees = (NSArray<Integer>) dotationsAnnuelles.valueForKey(EODotationAnnuelle.ANNEE_KEY);
    
    	annees = ERXArrayUtilities.arrayWithoutDuplicates(annees).sortedArrayUsingComparator(NSComparator.DescendingNumberComparator);
    	for (Integer annee : annees) {
    		NSArray<EODotationAnnuelle> dotationsPourAnnee = ERXQ.filtered(dotationsAnnuelles, EODotationAnnuelle.ANNEE.eq(annee));
    		BigDecimal total = (BigDecimal) dotationsPourAnnee.valueForKey(ERXKey.sum(EODotationAnnuelle.MONTANT).toString());
    		w.startElement("dotation");
        	w.writeElement("annee", "" + annee);
        	w.writeElement("montant", "" + total.intValue());
        	w.endElement();
    	}
        
        w.endElement();
        w.endDocument();
        w.close();

        return sw;
    }
    
}
