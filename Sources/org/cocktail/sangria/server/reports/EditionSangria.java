package org.cocktail.sangria.server.reports;

import java.net.URL;


import er.extensions.appserver.ERXApplication;
import er.extensions.appserver.ERXResourceManager;

public abstract class EditionSangria {
	
    protected static String pathForJasper(String reportPath) {
        ERXResourceManager rsm = (ERXResourceManager) ERXApplication.application().resourceManager();
        URL url = rsm.pathURLForResourceNamed(reportPath, "app", null);
        return url.getFile();
    }
    

}
