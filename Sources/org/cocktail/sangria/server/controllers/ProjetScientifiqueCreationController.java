package org.cocktail.sangria.server.controllers;

import org.cocktail.fwkcktljefyadmin.common.metier.EOExerciceCocktail;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartStructure;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlrecherche.server.metier.EOProjetScientifique;
import org.cocktail.fwkcktlrecherche.server.metier.factory.FactoryProjetScientifique;
import org.cocktail.sangria.server.components.projetscientifique.ProjetScientifiqueCreation2;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSForwardException;
import com.webobjects.foundation.NSValidation.ValidationException;

import er.extensions.eof.ERXEC;
import er.extensions.eof.ERXEOControlUtilities;
import er.extensions.eof.ERXQ;
import er.extensions.foundation.ERXStringUtilities;

public class ProjetScientifiqueCreationController {

	private EOEditingContext edc;
	private EOEditingContext edcDeRecherche;

	private ProjetScientifiqueCreation2 projetScientifiqueCreation;

	private EOProjetScientifique projetScientifique;

	public ProjetScientifiqueCreationController(ProjetScientifiqueCreation2 projetScientifiqueCreation) {
		this.projetScientifiqueCreation = projetScientifiqueCreation;
	}

	public EOEditingContext edc() {
		if (edc == null) {
			edc = ERXEC.newEditingContext();
		}
		return edc;
	}

	public EOEditingContext edcDeRecherche() {
		if (edcDeRecherche == null) {
			edcDeRecherche = ERXEC.newEditingContext(edc());
		}
		return edcDeRecherche;
	}

	public EOProjetScientifique projetScientifique() {
		if (projetScientifique == null) {
			try {
				projetScientifique = FactoryProjetScientifique.creerProjetScientifiqueVide(edc(), null, projetScientifiqueCreation.getUtilisateurPersId());
			} catch (Exception e) {
				throw NSForwardException._runtimeExceptionForThrowable(e);
			}
		}
		return projetScientifique;
	}

	public void setProjetScientifique(EOProjetScientifique projetScientifique) {
		this.projetScientifique = projetScientifique;
	}

	public EOQualifier qualifierPourResponsables() {
		return EOIndividu.TO_REPART_STRUCTURES.dot(EORepartStructure.TO_STRUCTURE_GROUPE).eq(projetScientifiqueCreation.structurePorteuseEditee());
	}

	public void enregistrer() {

		try {
			projetScientifique = FactoryProjetScientifique.creerProjetScientifiqueVide(edc(), null, projetScientifiqueCreation.getUtilisateurPersId());

			if (projetScientifiqueCreation.annee() == null) {
				throw new ValidationException("La saisie de l'année est obligatoire");
			}

			EOExerciceCocktail exercicePourAnnee = EOExerciceCocktail.fetchFirstByQualifier(edc(), ERXQ.equals(EOExerciceCocktail.EXE_EXERCICE_KEY, projetScientifiqueCreation.annee()));

			projetScientifique.setExercice(exercicePourAnnee);

			if (ERXStringUtilities.stringIsNullOrEmpty(projetScientifiqueCreation.titre())) {
				throw new ValidationException("La saisie du titre est obligatoire");
			}

			projetScientifique.setIntitule(projetScientifiqueCreation.titre());

			if (projetScientifiqueCreation.dateDebut() == null) {
				throw new ValidationException("La saisie de la date de début est obligatoire");
			}

			projetScientifique.setDateDebut(projetScientifiqueCreation.dateDebut());

			NSArray<EOStructure> structuresPorteuses = projetScientifiqueCreation.structuresPorteuses();

			if (structuresPorteuses.isEmpty()) {
				throw new ValidationException("Il faut au moins une structure porteuse pour le projet");
			}

			Boolean auMoinsUnResponsableScientifique = false;

			for (EOStructure structurePorteuse : structuresPorteuses) {

				EOStructure _structurePorteuse = ERXEOControlUtilities.localInstanceOfObject(edc(), structurePorteuse);

				try {
					projetScientifique.ajouterStructureRechercheConcernee(_structurePorteuse);
				} catch (Exception e) {
					throw new ValidationException(projetScientifiqueCreation.session().localizer().localizedStringForKey("erreur.ajout.structurePorteuse"));
				}

				NSArray<EOIndividu> responsablesScientifiques = projetScientifiqueCreation.responsablesScientifiques(structurePorteuse);

				for (EOIndividu responsableScientifique : responsablesScientifiques) {

					try {
						projetScientifique.ajouterResponsableScientifiquePourStructureRecherche(ERXEOControlUtilities.localInstanceOfObject(edc(), responsableScientifique), _structurePorteuse);
					} catch (Exception e) {
						throw new ValidationException(projetScientifiqueCreation.session().localizer().localizedStringForKey("erreur.ajout.responsableScientifique"));
					}

					auMoinsUnResponsableScientifique = true;

				}

			}

			if (auMoinsUnResponsableScientifique == false) {
				throw new ValidationException("Il faut au moins un responsable scientifique pour le projet");
			}

			edc().saveChanges();

		} catch (ValidationException e) {
			edc().revert();
			setProjetScientifique(null);
			throw e;
		} catch (Exception e) {
			throw NSForwardException._runtimeExceptionForThrowable(e);
		}

	}

}
