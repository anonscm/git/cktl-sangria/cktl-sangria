package org.cocktail.sangria.server.controllers;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Nonnull;

import org.cocktail.fwkcktlpersonne.common.eospecificites.EOStructureForGroupeSpec;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartStructure;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IStructure;
import org.cocktail.fwkcktlwebapp.common.UserInfo;

import com.google.inject.Inject;
import com.webobjects.eocontrol.EOArrayDataSource;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSValidation.ValidationException;

import er.extensions.appserver.ERXDisplayGroup;
import er.extensions.localization.ERXLocalizer;

/**
 * Cette classe permet l'ajout et la suppression
 * de personnes dans un groupe.
 * Elle gère aussi un displayGroup contenant les personnes
 * qui appartiennent au groupe 
 */
public class GestionPersonnesGroupeController {

	@Inject
	private UserInfo userInfo;
	
	@Nonnull
	private IStructure groupe;
	
	@Nonnull
	private ERXDisplayGroup<IPersonne> displayGroup;
	
	private Boolean afficheStructures = true;
	private Boolean afficheIndividus = true;
	
	/**
	 * @param groupe le groupe à gérer
	 */
	public GestionPersonnesGroupeController(IStructure groupe) {
		this.groupe = groupe;
		displayGroup = new ERXDisplayGroup<IPersonne>();
		displayGroup.setDataSource(getNewDataSource(getPersonnes(groupe)));
		displayGroup.fetch();
	}
	
	/**
	 * @return le displayGroup
	 */
	public ERXDisplayGroup<IPersonne> getDisplayGroup() {
		return displayGroup;
	}
	
	public void setUserInfo(UserInfo userInfo) {
		this.userInfo = userInfo;
	}
	
	protected EOArrayDataSource getNewDataSource(List<IPersonne> personnes) {
		EOArrayDataSource dataSource = new EOArrayDataSource(null, null);
		dataSource.setArray(new NSArray<IPersonne>(personnes));
		return dataSource;
	}
	
	protected EOEditingContext getEditingContext() {
		EOStructure g = (EOStructure) groupe;
		return g.editingContext();
	}
	
	
	public IStructure getGroupe() {
		return groupe;
	}

	
	protected List<IPersonne> getPersonnes(IStructure groupe) {
		List<IPersonne> personnes = new ArrayList<IPersonne>();
		personnes.addAll(getIndividus(groupe));
		personnes.addAll(getStructures(groupe));
		return personnes;
	}
	
	protected List<IPersonne> getStructures(IStructure groupe) {
		if (afficheStructures) {
			EOQualifier qualifier = EOStructure.TO_REPART_STRUCTURES.dot(EORepartStructure.TO_STRUCTURE_GROUPE).eq((EOStructure) groupe);
			return new ArrayList<IPersonne>(EOStructure.fetchAll(getEditingContext(), qualifier));
		} else {
			return new ArrayList<IPersonne>();
		}
	}
	
	
	protected List<IPersonne> getIndividus(IStructure groupe) {
		if (afficheIndividus) {
			EOQualifier qualifier = EOIndividu.TO_REPART_STRUCTURES.dot(EORepartStructure.TO_STRUCTURE_GROUPE).eq((EOStructure) groupe);
			return new ArrayList<IPersonne>(EOIndividu.fetchAll(getEditingContext(), qualifier));
		} else {
			return new ArrayList<IPersonne>();
		}
	}
	public void setAfficheIndividus(@Nonnull Boolean afficheIndividus) {
		this.afficheIndividus = afficheIndividus;
	}
	
	public void setAfficheStructures(@Nonnull Boolean afficheStructures) {
		this.afficheStructures = afficheStructures;
	}
	
	/**
	 * @param personne la personne que l'on souhaite ajouter au groupe
	 * @param persId le perId de l'utilisateur qui modifie
	 * @throws Exception en cas d'erreur
	 */
	public void ajouterPersonne(@Nonnull IPersonne personne, Integer persId) throws Exception {
		
		if (EOStructureForGroupeSpec.isPersonneInGroupe(personne, (EOStructure) groupe)) {
			throw new Exception(ERXLocalizer.currentLocalizer().localizedStringForKey("personneDejaPresente"));
		}
		
		EOStructureForGroupeSpec.affecterPersonneDansGroupe(getEditingContext(), personne, (EOStructure) groupe, persId);
		
		try {
			getEditingContext().saveChanges();
		} catch (ValidationException e) {
			getEditingContext().revert();
			throw e;
		}
		
		displayGroup.dataSource().insertObject(personne);
		displayGroup.updateDisplayedObjects();
	    displayGroup.fetch();

		
	}
	
	/**
	 * @param personne la personne que l'on souhaite supprimer du groupe
	 * @param persIdUtilisateur persId de la personne effectuant la suppression
	 * @throws Exception en cas d'erreur
	 */
	public void supprimerPersonne(@Nonnull IPersonne personne, Integer persIdUtilisateur) throws Exception {
		
		try {
		    EOStructureForGroupeSpec.supprimerPersonneDuGroupe(getEditingContext(), personne, (EOStructure) groupe, persIdUtilisateur);
		    getEditingContext().saveChanges();
		} catch (Exception e) {
			getEditingContext().revert();
			throw e;
		}
		
		displayGroup.dataSource().deleteObject(personne);
	    displayGroup.updateDisplayedObjects();
	    displayGroup.fetch();


	}
	
}
