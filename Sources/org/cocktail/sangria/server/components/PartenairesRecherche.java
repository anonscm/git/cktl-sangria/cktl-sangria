package org.cocktail.sangria.server.components;

import org.cocktail.fwkcktlajaxwebext.serveur.component.CktlAjaxWindow;
import org.cocktail.fwkcktlpersonne.common.eospecificites.EOStructureForGroupeSpec;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;
import org.cocktail.sangria.server.components.commons.ASangriaComponent;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WODisplayGroup;
import com.webobjects.eocontrol.EOArrayDataSource;
import com.webobjects.eocontrol.EOClassDescription;

import er.extensions.appserver.ERXDisplayGroup;
import er.extensions.eof.ERXS;

public class PartenairesRecherche extends ASangriaComponent {
	private static final long serialVersionUID = -3594013913261179728L;
	
	private final static String GROUPE_PARTENAIRES_RECHERCHE_KEY = "GROUPE_PARTENAIRES_RECHERCHE";
    
    private IPersonne currentPartenaireRecherche;
    private ERXDisplayGroup<IPersonne> partenaireRechercheDisplayGroup;
    private EOArrayDataSource partenaireRechercheDataSource;
    private EOStructure groupePartenaireRecherche;
    private IPersonne selectedPartenaireRecherche;
    private IPersonne nouveauPartenaireRecherche;
    
    public PartenairesRecherche(WOContext context) {
        super(context);
    }
    
    
    public String getAjoutPartenaireRechercheWindowId() {
    	return getComponentId() + "_" + "AjoutPartenaireRechercheWindow";
    }
    
    public String getPartenaireRechercheTableViewId() {
    	return getComponentId() + "_" + "PartenaireRechercheTableView";
    }
    
    public String getPartenaireRechercheContainerId() {
    	return getComponentId() + "_" + "PartenaireRechercheContainer";
    }
    
    
    public IPersonne getCurrentPartenaireRecherche() {
    	return currentPartenaireRecherche;
    }

    public void setCurrentPartenaireRecherche(IPersonne currentPartenaireRecherche) {
    	this.currentPartenaireRecherche = currentPartenaireRecherche;
    }

    public ERXDisplayGroup<IPersonne> getPartenaireRechercheDisplayGroup() {
		if(partenaireRechercheDisplayGroup == null) {
			partenaireRechercheDisplayGroup = new ERXDisplayGroup<IPersonne>();
			partenaireRechercheDisplayGroup.setDataSource(getPartenaireRechercheDataSource());
			partenaireRechercheDisplayGroup.setDelegate(new PartenaireRechercheDisplayGroupDelegate());
			partenaireRechercheDisplayGroup.setSortOrderings(ERXS.ascInsensitives(IPersonne.NOM_PRENOM_COMPLET_AFFICHAGE_KEY));
			partenaireRechercheDisplayGroup.fetch();
		}
		
		return partenaireRechercheDisplayGroup;
    }

    public void setPartenaireRechercheDisplayGroup(ERXDisplayGroup<IPersonne> partenaireRechercheDisplayGroup) {
    	this.partenaireRechercheDisplayGroup = partenaireRechercheDisplayGroup;
    }

    public EOArrayDataSource getPartenaireRechercheDataSource() {
    	if(partenaireRechercheDataSource == null) {
    		partenaireRechercheDataSource = new EOArrayDataSource(EOClassDescription.classDescriptionForClass(IPersonne.class), null);
    		partenaireRechercheDataSource.setArray(EOStructureForGroupeSpec.getPersonnesInGroupe(getGroupePartenaireRecherche()));
    	}
    	return partenaireRechercheDataSource;
    }

    public void setPartenaireRechercheDataSource(EOArrayDataSource partenaireRechercheDataSource) {
    	this.partenaireRechercheDataSource = partenaireRechercheDataSource;
    }

    public EOStructure getGroupePartenaireRecherche() {
	if(groupePartenaireRecherche == null) {
	    try {
		groupePartenaireRecherche = EOStructureForGroupeSpec.getGroupeForParamKey(edc(), GROUPE_PARTENAIRES_RECHERCHE_KEY);
	      } catch (Exception e) {
	        session().addSimpleErrorMessage("Erreur", session().localizer().localizedStringForKey("echecRecuperationPartenaireRecherche"));
	      }
	}
	
	return groupePartenaireRecherche;
    }

    public void setGroupePartenaireRecherche(EOStructure groupePartenaireRecherche) {
	this.groupePartenaireRecherche = groupePartenaireRecherche;
    }

    public IPersonne getSelectedPartenaireRecherche() {
	return selectedPartenaireRecherche;
    }

    public void setSelectedPartenaireRecherche(IPersonne selectedPartenaireRecherche) {
	this.selectedPartenaireRecherche = selectedPartenaireRecherche;
    }
    
    public IPersonne getNouveauPartenaireRecherche() {
	return nouveauPartenaireRecherche;
    }

    public void setNouveauPartenaireRecherche(IPersonne nouveauPartenaireRecherche) {
	this.nouveauPartenaireRecherche = nouveauPartenaireRecherche;
    }
    
    public WOActionResults ajouterPartenaireRecherche() {
	
	if(getNouveauPartenaireRecherche() == null) {
	    session().addSimpleErrorMessage("Erreur", session().localizer().localizedStringForKey("gestionPartenaireRecherche.aucunPartenaireRechercheSelectionne"));
	    return doNothing();
	}
	
	EOStructure _groupePartenaireRecherche;
	try {
	    _groupePartenaireRecherche = getGroupePartenaireRecherche();
	} catch (Exception e) {
	    session().addSimpleErrorMessage("Erreur", session().localizer().localizedStringForKey("echecRecuperationPartenaireRecherche")); 
	    return doNothing();
	}
	
	if(EOStructureForGroupeSpec.isPersonneInGroupe(nouveauPartenaireRecherche, _groupePartenaireRecherche)) {
	    session().addSimpleErrorMessage("Erreur", session().localizer().localizedStringForKey("gestionPartenaireRecherche.PartenaireRechercheDejaPresent"));
	    return doNothing();
	}
	
	EOStructureForGroupeSpec.affecterPersonneDansGroupe(
		edc(), 
		getNouveauPartenaireRecherche(), 
		_groupePartenaireRecherche, 
		getUtilisateurPersId());
	
	try {
            edc().saveChanges();
            getPartenaireRechercheDataSource().insertObject(getNouveauPartenaireRecherche());

            
        } catch (ValidationException e) {
            session().addSimpleErrorMessage("Erreur", e);
            edc().revert();
            return doNothing();
        }

	CktlAjaxWindow.close(context(), getAjoutPartenaireRechercheWindowId());
	
	getPartenaireRechercheDisplayGroup().fetch();
	
	return doNothing();
    }
    
    public WOActionResults annulerAjout() {
	
	edc().revert();
	CktlAjaxWindow.close(context(), getAjoutPartenaireRechercheWindowId());
	return doNothing();
    }
    
    public WOActionResults supprimerPartenaireRecherche() {
	
	if(getSelectedPartenaireRecherche() == null) {
	    session().addSimpleErrorMessage("Erreur", session().localizer().localizedStringForKey("gestionPartenaireRecherche.aucunPartenaireRechercheSelectionne"));
	    return doNothing();
	}
    	
	try {
	    EOStructureForGroupeSpec.supprimerPersonneDuGroupe(edc(), getSelectedPartenaireRecherche(), getGroupePartenaireRecherche(), getUtilisateurPersId());
	    edc().saveChanges();
	    getPartenaireRechercheDataSource().deleteObject(getSelectedPartenaireRecherche());
	    getPartenaireRechercheDisplayGroup().fetch();
	} catch (ValidationException e) {
	    session().addSimpleErrorMessage("Erreur", e);
	    edc().revert();
	} catch (Exception e) {
	    session().addSimpleErrorMessage("Erreur", session().localizer().localizedStringForKey("echecRecuperationPartenaireRecherche"));
	    edc().revert();
	}
	
	return doNothing();
    }

    public class PartenaireRechercheDisplayGroupDelegate {
		public void displayGroupDidChangeSelectedObjects(WODisplayGroup group) {
		    ERXDisplayGroup<EOStructure> _groupe = (ERXDisplayGroup<EOStructure>) group;
		    if( _groupe.selectedObject() != null) {
			setSelectedPartenaireRecherche(_groupe.selectedObject());
		    }
		}
    }
}
    
