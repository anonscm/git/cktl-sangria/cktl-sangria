package org.cocktail.sangria.server.components;

import org.cocktail.fwkcktlrecherche.server.ParametresRecherche;

import com.webobjects.appserver.WOContext;

/**
 * Gestion des validateurs juridiques
 */
public class ValidateursJuridiques extends GestionPersonnesGroupe {

	private static final long serialVersionUID = 1L;

	/**
	 * @param context le context
	 */
	public ValidateursJuridiques(WOContext context) {
        super(context);
		if (getGestionPersonnesGroupeController() != null) {
			getGestionPersonnesGroupeController().setAfficheStructures(false);
		}
    }

	@Override
	protected String getGroupeParamKey() {
		return ParametresRecherche.GROUPE_VALIDATEURS_JURIDIQUES_KEY;
	}

	@Override
	public String getEchecRecuperationGroupeMessage() {
		return session().localizer().localizedStringForKey("echecRecuperationValidateursJuridiques");
	}
}