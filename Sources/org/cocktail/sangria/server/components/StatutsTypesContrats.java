package org.cocktail.sangria.server.components;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.foundation.NSArray;

import er.ajax.AjaxUpdateContainer;
import er.extensions.appserver.ERXDisplayGroup;
import er.extensions.eof.ERXDatabaseDataSource;
import er.extensions.eof.ERXFetchSpecification;
import er.extensions.eof.ERXKey;
import er.extensions.foundation.ERXArrayUtilities;
import er.extensions.foundation.ERXTimestampUtilities;
import er.extensions.qualifiers.ERXAndQualifier;
import er.extensions.qualifiers.ERXKeyValueQualifier;

import org.apache.commons.lang3.math.NumberUtils;
import org.cocktail.fwkcktlajaxwebext.serveur.component.CktlAjaxWindow;
import org.cocktail.fwkcktlpersonne.common.metier.EOAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EOAssociationReseau;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypeContratTravail;
import org.cocktail.fwkcktlrecherche.server.metier.EOStatutTypeContrat;
import org.cocktail.fwkcktlrecherche.server.metier.factory.FactoryAssociation;
import org.cocktail.sangria.server.components.commons.ASangriaComponent;

public class StatutsTypesContrats extends ASangriaComponent {
    
	private EOAssociation categorieStatutsMembresUnite = FactoryAssociation.shared().typesMembresUniteRechercheAssociation(edc());
	private EOAssociation categorieStatutsMembresStructureRecherche = FactoryAssociation.shared().typesMembresStructureRechercheAssociation(edc());
	
	private NSArray<EOAssociation> categoriesStatutsMembres = new NSArray<EOAssociation>(categorieStatutsMembresStructureRecherche, categorieStatutsMembresUnite);
	private EOAssociation currentCategorieStatutMembre;
	private EOAssociation selectedCategorieStatutMembre;
	
	private NSArray<EOAssociation> listeStatuts;
	private EOAssociation currentStatut;
	private EOAssociation selectedStatut;
	
	private EOAssociation statutAAjouter;
	
	public final String selectionStatutContainerId = getComponentId() + "_selectionStatutContainer";
	public final String typesContratsContainerId = getComponentId() + "_typesContratsContainer";
	public final String statutSelectWindowId = getComponentId() + "_statutSelectWindow";
	public final String typesContratsDisponiblesTableViewId = getComponentId() + "_typesContratsDisponiblesTableView";
	public final String typesContratsUtilisesTableViewId = getComponentId() + "_typesContratsUtilisesTableView";
	
	private ERXDisplayGroup<EOTypeContratTravail> typesContratsDisponiblesDisplayGroup;
	private ERXDatabaseDataSource typesContratsDisponiblesDataSource;	
	private ERXFetchSpecification<EOTypeContratTravail> typesContratsDisponiblesFetchSpecification;
	private EOTypeContratTravail currentTypeContratDisponible;
	
	private ERXDisplayGroup<EOStatutTypeContrat> typesContratsUtilisesDisplayGroup;
	private ERXDatabaseDataSource typesContratsUtilisesDataSource;
	private ERXFetchSpecification<EOStatutTypeContrat> typesContratsUtilisesFetchSpecification;
	private EOStatutTypeContrat currentTypeContratUtilise;
	
	public StatutsTypesContrats(WOContext context) {
        super(context);
    }

	public EOAssociation getCurrentCategorieStatutMembre() {
		return currentCategorieStatutMembre;
	}

	public void setCurrentCategorieStatutMembre(
			EOAssociation currentCategorieStatutMembre) {
		this.currentCategorieStatutMembre = currentCategorieStatutMembre;
	}

	public NSArray<EOAssociation> getCategoriesStatutsMembres() {
		return categoriesStatutsMembres;
	}

	public void setCategoriesStatutsMembres(NSArray<EOAssociation> categoriesStatutsMembres) {
		this.categoriesStatutsMembres = categoriesStatutsMembres;
	}

	public EOAssociation getSelectedCategorieStatutMembre() {
		if(selectedCategorieStatutMembre == null) {
			selectedCategorieStatutMembre = ERXArrayUtilities.firstObject(getCategoriesStatutsMembres());
		}
		return selectedCategorieStatutMembre;
	}

	public void setSelectedCategorieStatutMembre(EOAssociation selectedCategorieStatutMembre) {
		this.selectedCategorieStatutMembre = selectedCategorieStatutMembre;
		setListeStatuts(getSelectedCategorieStatutMembre().getFils(edc()));
	}

	public NSArray<EOAssociation> getListeStatuts() {
		if (listeStatuts == null) {
			setListeStatuts(getSelectedCategorieStatutMembre().getFils(edc()));
		}
		return listeStatuts;
	}

	public void setListeStatuts(NSArray<EOAssociation> listeStatuts) {
		this.listeStatuts = listeStatuts;
		setSelectedStatut(ERXArrayUtilities.firstObject(listeStatuts));
	}

	public EOAssociation getCurrentStatut() {
		return currentStatut;
	}

	public void setCurrentStatut(EOAssociation currentStatut) {
		this.currentStatut = currentStatut;
	}

	public EOAssociation getSelectedStatut() {
		if(selectedStatut == null) {
			selectedStatut = ERXArrayUtilities.firstObject(getListeStatuts());
		}
		return selectedStatut;
	}

	public void setSelectedStatut(EOAssociation selectedStatut) {
		this.selectedStatut = selectedStatut;
		getTypesContratsDisponiblesFetchSpecification().setQualifier(typesContratsDisponiblesQualifier());	
		getTypesContratsUtilisesFetchSpecification().setQualifier(typesContratsUtilisesQualifier());
		getTypesContratsDisponiblesDisplayGroup().fetch();
		getTypesContratsUtilisesDisplayGroup().fetch();
	}

	public EOAssociation getStatutAAjouter() {
		return statutAAjouter;
	}

	public void setStatutAAjouter(EOAssociation statutAAjouter) {
		this.statutAAjouter = statutAAjouter;
	}
	
	public WOActionResults didSelectStatut() {
		EOAssociationReseau.createEOAssociationReseau(edc(), getStatutAAjouter(), getSelectedCategorieStatutMembre());
		try {
			edc().saveChanges();
			setListeStatuts(getSelectedCategorieStatutMembre().getFils(edc()));
			AjaxUpdateContainer.updateContainerWithID(selectionStatutContainerId, context());
			CktlAjaxWindow.close(context(), statutSelectWindowId);
		} catch (Exception e) {
			edc().revert();
			session().addSimpleErrorMessage("Erreur", e);
		}
		return doNothing();
	}
	
	public WOActionResults supprimerStatutDansCategorie() {
		EOAssociationReseau associationReseau  = EOAssociationReseau.rechercheAssociationReseau(getSelectedStatut(), getSelectedCategorieStatutMembre(), edc());
		associationReseau.delete();
		try {
			edc().saveChanges();
			setListeStatuts(getSelectedCategorieStatutMembre().getFils(edc()));
			AjaxUpdateContainer.updateContainerWithID(selectionStatutContainerId, context());
		} catch (Exception e) {
			edc().revert();
			session().addSimpleErrorMessage("Erreur", e);
		}
		return doNothing();
	}

	public NSArray<EOTypeContratTravail> typesContratsUtilisesPourStatut(EOAssociation statut) {
		NSArray<EOStatutTypeContrat> statutTypeContrats = EOStatutTypeContrat.fetchAll(
				edc(), 
				EOStatutTypeContrat.STATUT.eq(statut)
			);
		return (NSArray<EOTypeContratTravail>) statutTypeContrats.valueForKey(EOStatutTypeContrat.TYPE_CONTRAT_TRAVAIL.key());
	}
	
	public ERXDisplayGroup<EOTypeContratTravail> getTypesContratsDisponiblesDisplayGroup() {
		if(typesContratsDisponiblesDisplayGroup == null) {
			typesContratsDisponiblesDisplayGroup = new ERXDisplayGroup<EOTypeContratTravail>();
			typesContratsDisponiblesDisplayGroup.setDataSource(getTypesContratsDisponiblesDataSource());
			typesContratsDisponiblesDisplayGroup.fetch();
		}
		return typesContratsDisponiblesDisplayGroup;
	}

	public void setTypesContratsDisponiblesDisplayGroup(
			ERXDisplayGroup<EOTypeContratTravail> typesContratsDisponiblesDisplayGroup) {
		this.typesContratsDisponiblesDisplayGroup = typesContratsDisponiblesDisplayGroup;
	}

	public ERXDatabaseDataSource getTypesContratsDisponiblesDataSource() {
		if(typesContratsDisponiblesDataSource == null) {
			typesContratsDisponiblesDataSource = new ERXDatabaseDataSource(edc(), EOTypeContratTravail.ENTITY_NAME);
			typesContratsDisponiblesDataSource.setFetchSpecification(getTypesContratsDisponiblesFetchSpecification());
		}
		return typesContratsDisponiblesDataSource;
	}

	public void setTypesContratsDisponiblesDataSource(
			ERXDatabaseDataSource typesContratsDisponiblesDataSource) {
		this.typesContratsDisponiblesDataSource = typesContratsDisponiblesDataSource;
	}

	public ERXFetchSpecification<EOTypeContratTravail> getTypesContratsDisponiblesFetchSpecification() {
		if(typesContratsDisponiblesFetchSpecification == null) {
			typesContratsDisponiblesFetchSpecification = 
					new ERXFetchSpecification<EOTypeContratTravail>(
							EOTypeContratTravail.ENTITY_NAME,
							typesContratsDisponiblesQualifier(),
							EOTypeContratTravail.LL_TYPE_CONTRAT_TRAV.ascInsensitives()
						);
		}
		return typesContratsDisponiblesFetchSpecification;
	}

	public void setTypesContratsDisponiblesFetchSpecification(
			ERXFetchSpecification<EOTypeContratTravail> typesContratsDisponiblesFetchSpecification) {
		this.typesContratsDisponiblesFetchSpecification = typesContratsDisponiblesFetchSpecification;
	}

	public EOTypeContratTravail getCurrentTypeContratDisponible() {
		return currentTypeContratDisponible;
	}

	public void setCurrentTypeContratDisponible(
			EOTypeContratTravail currentTypeContratDisponible) {
		this.currentTypeContratDisponible = currentTypeContratDisponible;
	}

	public ERXDisplayGroup<EOStatutTypeContrat> getTypesContratsUtilisesDisplayGroup() {
		if(typesContratsUtilisesDisplayGroup == null) {
			typesContratsUtilisesDisplayGroup = new ERXDisplayGroup<EOStatutTypeContrat>();
			typesContratsUtilisesDisplayGroup.setDataSource(getTypesContratsUtilisesDataSource());
			typesContratsUtilisesDisplayGroup.fetch();
		}
		return typesContratsUtilisesDisplayGroup;
	}

	public void setTypesContratsUtilisesDisplayGroup(
			ERXDisplayGroup<EOStatutTypeContrat> typesContratsUtilisesDisplayGroup) {
		this.typesContratsUtilisesDisplayGroup = typesContratsUtilisesDisplayGroup;
	}

	public ERXDatabaseDataSource getTypesContratsUtilisesDataSource() {
		if(typesContratsUtilisesDataSource == null) {
			typesContratsUtilisesDataSource = new ERXDatabaseDataSource(edc(), EOStatutTypeContrat.ENTITY_NAME);
			typesContratsUtilisesDataSource.setFetchSpecification(getTypesContratsUtilisesFetchSpecification());
		}
		return typesContratsUtilisesDataSource;
	}

	public void setTypesContratsUtilisesDataSource(
			ERXDatabaseDataSource typesContratsUtilisesDataSource) {
		this.typesContratsUtilisesDataSource = typesContratsUtilisesDataSource;
	}

	public ERXFetchSpecification<EOStatutTypeContrat> getTypesContratsUtilisesFetchSpecification() {
		if(typesContratsUtilisesFetchSpecification == null) {
			typesContratsUtilisesFetchSpecification = 
					new ERXFetchSpecification<EOStatutTypeContrat>(
							EOStatutTypeContrat.ENTITY_NAME,
							typesContratsUtilisesQualifier(),
							EOStatutTypeContrat.TYPE_CONTRAT_TRAVAIL.dot(EOTypeContratTravail.LL_TYPE_CONTRAT_TRAV).ascInsensitives()
						);
		}
		return typesContratsUtilisesFetchSpecification;
	}

	private ERXKeyValueQualifier typesContratsUtilisesQualifier() {
		return EOStatutTypeContrat.STATUT.eq(getSelectedStatut());
	}

	public void setTypesContratsUtilisesFetchSpecification(
			ERXFetchSpecification<EOStatutTypeContrat> typesContratsUtilisesFetchSpecification) {
		this.typesContratsUtilisesFetchSpecification = typesContratsUtilisesFetchSpecification;
	}

	public EOStatutTypeContrat getCurrentTypeContratUtilise() {
		return currentTypeContratUtilise;
	}

	public void setCurrentTypeContratUtilise(EOStatutTypeContrat currentTypeContratUtilise) {
		this.currentTypeContratUtilise = currentTypeContratUtilise;
	}

	
	public WOActionResults utiliserTypeContrat() {
		EOStatutTypeContrat.createStatutTypeContrat(
				edc(), 
				ERXTimestampUtilities.today(),
				getUtilisateurPersId(),
				getSelectedStatut(),
				getTypesContratsDisponiblesDisplayGroup().selectedObject()
			);
		
		
		try {
			edc().saveChanges();
			AjaxUpdateContainer.updateContainerWithID(typesContratsContainerId, context());
			getTypesContratsDisponiblesFetchSpecification().setQualifier(typesContratsDisponiblesQualifier());	
			getTypesContratsDisponiblesDisplayGroup().fetch();
			getTypesContratsUtilisesDisplayGroup().fetch();
		} catch (Exception e) {
			edc().revert();
			session().addSimpleErrorMessage("Erreur", e);
		}
		return doNothing();

	}

	private ERXAndQualifier typesContratsDisponiblesQualifier() {
		return 
			EOTypeContratTravail.C_TYPE_CONTRAT_TRAV.notIn(
				(NSArray<String>) typesContratsUtilisesPourStatut(getSelectedStatut()).valueForKey(EOTypeContratTravail.C_TYPE_CONTRAT_TRAV_KEY)
			);
	}
	
	public WOActionResults enleverTypeContrat() {
		getTypesContratsUtilisesDisplayGroup().selectedObject().delete();
		try {
			edc().saveChanges();
			setListeStatuts(getSelectedCategorieStatutMembre().getFils(edc()));
			AjaxUpdateContainer.updateContainerWithID(typesContratsContainerId, context());
			getTypesContratsDisponiblesFetchSpecification().setQualifier(typesContratsDisponiblesQualifier());	
			getTypesContratsDisponiblesDisplayGroup().fetch();
			getTypesContratsUtilisesDisplayGroup().fetch();
		} catch (Exception e) {
			edc().revert();
			session().addSimpleErrorMessage("Erreur", e);
		}
		return doNothing();
	}

	
	public Boolean peutUtiliserTypeContrat() {
		return getTypesContratsDisponiblesDisplayGroup().allObjects().isEmpty();
	}
	
	public Boolean peutEnleverTypeContrat() {
		return getTypesContratsUtilisesDisplayGroup().allObjects().isEmpty();
	}
    
	
	public WOActionResults changerCategorie() {
		AjaxUpdateContainer.updateContainerWithID(typesContratsContainerId, context());
		return doNothing();
	}
    
}