package org.cocktail.sangria.server.components;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WODisplayGroup;
import com.webobjects.eocontrol.EOArrayDataSource;
import com.webobjects.eocontrol.EOClassDescription;
import com.webobjects.foundation.NSValidation.ValidationException;

import er.extensions.appserver.ERXDisplayGroup;

import org.cocktail.fwkcktlajaxwebext.serveur.component.CktlAjaxWindow;
import org.cocktail.fwkcktlpersonne.common.eospecificites.EOStructureForGroupeSpec;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;
import org.cocktail.sangria.server.components.commons.ASangriaComponent;

public class PolesCompetitivite extends ASangriaComponent {

    

	private final static String POLE_COMPETITIVITE_KEY = "org.cocktail.sangria.groupes.polecompetitivite";
    
    private IPersonne currentPoleCompetitivite;
    private ERXDisplayGroup<IPersonne> poleCompetitiviteDisplayGroup;
    private EOArrayDataSource poleCompetitiviteDataSource;
    private EOStructure groupePoleCompetitivite;
    private IPersonne selectedPoleCompetitivite;
    private IPersonne nouveauPoleCompetitivite;
    
    public PolesCompetitivite(WOContext context) {
        super(context);
    }
    
    public String getAjoutPoleCompetitiviteWindowId() {
    	return getComponentId() + "_" + "AjoutPoleCompetitiviteWindow";
    }
    
    public String getPoleCompetitiviteTableViewId() {
    	return getComponentId() + "_" + "PoleCompetitiviteTableView";
    }
    
    public String getPoleCompetitiviteContainerId() {
    	return getComponentId() + "_" + "PoleCompetitiviteContainer";
    }
    
    
    public IPersonne getCurrentPoleCompetitivite() {
    	return currentPoleCompetitivite;
    }

    public void setCurrentPoleCompetitivite(IPersonne currentPoleCompetitivite) {
    	this.currentPoleCompetitivite = currentPoleCompetitivite;
    }

    public ERXDisplayGroup<IPersonne> getPoleCompetitiviteDisplayGroup() {
		if(poleCompetitiviteDisplayGroup == null) {
			poleCompetitiviteDisplayGroup = new ERXDisplayGroup<IPersonne>();
			poleCompetitiviteDisplayGroup.setDataSource(getPoleCompetitiviteDataSource());
			poleCompetitiviteDisplayGroup.setDelegate(new PoleCompetitiviteDisplayGroupDelegate());
			poleCompetitiviteDisplayGroup.fetch();
		}
		
		return poleCompetitiviteDisplayGroup;
    }

    public void setPoleCompetitiviteDisplayGroup(ERXDisplayGroup<IPersonne> poleCompetitiviteDisplayGroup) {
    	this.poleCompetitiviteDisplayGroup = poleCompetitiviteDisplayGroup;
    }

    public EOArrayDataSource getPoleCompetitiviteDataSource() {
    	if(poleCompetitiviteDataSource == null) {
    		poleCompetitiviteDataSource = new EOArrayDataSource(EOClassDescription.classDescriptionForClass(IPersonne.class), null);
    		poleCompetitiviteDataSource.setArray(EOStructureForGroupeSpec.getPersonnesInGroupe(getGroupePoleCompetitivite()));
    	}
    	return poleCompetitiviteDataSource;
    }

    public void setPoleCompetitiviteDataSource(EOArrayDataSource poleCompetitiviteDataSource) {
    	this.poleCompetitiviteDataSource = poleCompetitiviteDataSource;
    }

    public EOStructure getGroupePoleCompetitivite() {
	if(groupePoleCompetitivite == null) {
	    try {
		groupePoleCompetitivite = EOStructureForGroupeSpec.getGroupeForParamKey(edc(), POLE_COMPETITIVITE_KEY);
	      } catch (Exception e) {
	        session().addSimpleErrorMessage("Erreur", session().localizer().localizedStringForKey("echecRecuperationPoleCompetitivite"));
	      }
	}
	
	return groupePoleCompetitivite;
    }

    public void setGroupePoleCompetitivite(EOStructure groupePoleCompetitivite) {
	this.groupePoleCompetitivite = groupePoleCompetitivite;
    }

    public IPersonne getSelectedPoleCompetitivite() {
	return selectedPoleCompetitivite;
    }

    public void setSelectedPoleCompetitivite(IPersonne selectedPoleCompetitivite) {
	this.selectedPoleCompetitivite = selectedPoleCompetitivite;
    }
    
    public IPersonne getNouveauPoleCompetitivite() {
	return nouveauPoleCompetitivite;
    }

    public void setNouveauPoleCompetitivite(IPersonne nouveauPoleCompetitivite) {
	this.nouveauPoleCompetitivite = nouveauPoleCompetitivite;
    }
    
    public WOActionResults ajouterPoleCompetitivite() {
	
	if(getNouveauPoleCompetitivite() == null) {
	    session().addSimpleErrorMessage("Erreur", session().localizer().localizedStringForKey("gestionPoleCompetitivite.aucunPoleCompetitiviteSelectionne"));
	    return doNothing();
	}
	
	EOStructure _groupePoleCompetitivite;
	try {
	    _groupePoleCompetitivite = getGroupePoleCompetitivite();
	} catch (Exception e) {
	    session().addSimpleErrorMessage("Erreur", session().localizer().localizedStringForKey("echecRecuperationPoleCompetitivite")); 
	    return doNothing();
	}
	
	if(EOStructureForGroupeSpec.isPersonneInGroupe(nouveauPoleCompetitivite, _groupePoleCompetitivite)) {
	    session().addSimpleErrorMessage("Erreur", session().localizer().localizedStringForKey("gestionPoleCompetitivite.PoleCompetitiviteDejaPresent"));
	    return doNothing();
	}
	
	EOStructureForGroupeSpec.affecterPersonneDansGroupe(
		edc(), 
		getNouveauPoleCompetitivite(), 
		_groupePoleCompetitivite, 
		getUtilisateurPersId());
	
	try {
            edc().saveChanges();
            getPoleCompetitiviteDataSource().insertObject(getNouveauPoleCompetitivite());

            
        } catch (ValidationException e) {
            session().addSimpleErrorMessage("Erreur", e);
            edc().revert();
            return doNothing();
        }

	CktlAjaxWindow.close(context(), getAjoutPoleCompetitiviteWindowId());
	
	getPoleCompetitiviteDisplayGroup().fetch();
	
	return doNothing();
    }
    
    public WOActionResults annulerAjout() {
	
	edc().revert();
	CktlAjaxWindow.close(context(), getAjoutPoleCompetitiviteWindowId());
	return doNothing();
    }
    
    public WOActionResults supprimerPoleCompetitivite() {
	
	if(getSelectedPoleCompetitivite() == null) {
	    session().addSimpleErrorMessage("Erreur", session().localizer().localizedStringForKey("gestionPoleCompetitivite.aucunPoleCompetitiviteSelectionne"));
	    return doNothing();
	}
    	
	try {
	    EOStructureForGroupeSpec.supprimerPersonneDuGroupe(edc(), getSelectedPoleCompetitivite(), getGroupePoleCompetitivite(), getUtilisateurPersId());
	    edc().saveChanges();
	    getPoleCompetitiviteDataSource().deleteObject(getSelectedPoleCompetitivite());
	    getPoleCompetitiviteDisplayGroup().fetch();
	} catch (ValidationException e) {
	    session().addSimpleErrorMessage("Erreur", e);
	    edc().revert();
	} catch (Exception e) {
	    session().addSimpleErrorMessage("Erreur", session().localizer().localizedStringForKey("echecRecuperationPoleCompetitivite"));
	    edc().revert();
	}
	
	return doNothing();
    }

    public class PoleCompetitiviteDisplayGroupDelegate {
		public void displayGroupDidChangeSelectedObjects(WODisplayGroup group) {
		    ERXDisplayGroup<EOStructure> _groupe = (ERXDisplayGroup<EOStructure>) group;
		    if( _groupe.selectedObject() != null) {
			setSelectedPoleCompetitivite(_groupe.selectedObject());
		    }
		}
    }
}