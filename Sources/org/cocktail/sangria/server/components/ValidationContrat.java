package org.cocktail.sangria.server.components;

import com.webobjects.appserver.WOComponent;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;

import er.extensions.eof.ERXEOControlUtilities;

import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IStructure;
import org.cocktail.fwkcktlrecherche.server.metier.IContratRecherche;
import org.cocktail.sangria.server.components.commons.ASangriaComponent;

public class ValidationContrat extends WOComponent {
	private IContratRecherche contrat;
	private String url;
	private IStructure structure;
	private IIndividu individu;

	
    public ValidationContrat(WOContext context) {
        super(context);
    }


	public IContratRecherche getContrat() {
		return contrat;
	}


	public void setContrat(IContratRecherche contrat) {
		this.contrat = contrat;
	}
	
	public NSDictionary<String, String> getParams() {
		NSDictionary<String, String> params = new NSMutableDictionary<String, String>();
		params.put("nextPage", "contratRechercheValidation");
		params.put("contratid", ERXEOControlUtilities.primaryKeyObjectForObject((EOEnterpriseObject) contrat).toString());
		return params;
	}


	public String getUrl() {
		return url;
	}


	public void setUrl(String url) {
		this.url = url;
	}
	

	public IStructure getStructure() {
		return structure;
	}


	public void setStructure(IStructure structure) {
		this.structure = structure;
	}


	public IIndividu getIndividu() {
		return individu;
	}


	public void setIndividu(IIndividu individu) {
		this.individu = individu;
	}
}