/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2010 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/

package org.cocktail.sangria.server.components;

import org.cocktail.fwkcktlajaxwebext.serveur.CktlAjaxWOComponent;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;
import org.cocktail.fwkcktlwebapp.server.components.CktlLoginResponder;
import org.cocktail.sangria.server.DirectAction;
import org.cocktail.sangria.server.DirectAction.DefaultLoginResponder;
import org.cocktail.sangria.server.components.commons.ASangriaComponent;

import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResponse;

import er.ajax.AjaxUtils;
import er.ajax.CktlAjaxUtils;

// TODO: Auto-generated Javadoc
/**
 * The Class Login.
 */
public class Login extends CktlAjaxWOComponent {
	/** Le gestionnaire des evenements de composant de login local. */
	private DefaultLoginResponder loginResponder;

	/** The login. */
	private String login;
	
	/** The password. */
	private String password;
	
	/** The message erreur. */
	private String messageErreur;

	/**
	 * Instantiates a new login.
	 *
	 * @param context the context
	 */
	public Login(WOContext context) {
        super(context);
    }

	/* (non-Javadoc)
	 * @see org.cocktail.pitch.server.components.commons.PitchWOComponent#appendToResponse(com.webobjects.appserver.WOResponse, com.webobjects.appserver.WOContext)
	 */
	@Override
	public void appendToResponse(WOResponse response, WOContext context) {
		super.appendToResponse(response, context);
		AjaxUtils.addScriptResourceInHead(context, response, "prototype.js");
		AjaxUtils.addScriptResourceInHead(context, response, "effects.js");
		AjaxUtils.addScriptResourceInHead(context, response, "wonder.js");
		AjaxUtils.addScriptResourceInHead(context, response, "FwkCktlAjaxWebExt.framework", "scripts/window.js");
		CktlAjaxUtils.addStylesheetResourceInHead(context, response, "FwkCktlAjaxWebExt.framework", "themes/default.css");
		CktlAjaxUtils.addStylesheetResourceInHead(context, response, "FwkCktlAjaxWebExt.framework", "themes/alert.css");
		CktlAjaxUtils.addStylesheetResourceInHead(context, response, "FwkCktlAjaxWebExt.framework", "themes/lighting.css");
	}


	/**
	 * Retourne la reference vers une instance de gestionnaire des evenements
	 * du composant de login local.
	 *
	 * @return the cRI login responder
	 */
	public DefaultLoginResponder loginResponder() {
		return loginResponder;
	}

	/**
	 * Definit le gestionnaire des evenements du composant de login local.
	 *
	 * @param responder the responder
	 */
	public void registerLoginResponder(DefaultLoginResponder responder) {
		loginResponder = responder;
	}

	/**
	 * Login.
	 *
	 * @return the login
	 */
	public String login() {
		return login;
	}

	/**
	 * Sets the login.
	 *
	 * @param login the login to set
	 */
	public void setLogin(String login) {
		this.login = login;
	}

	/**
	 * Password.
	 *
	 * @return the password
	 */
	public String password() {
		return password;
	}

	/**
	 * Sets the password.
	 *
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * Message erreur.
	 *
	 * @return the messageErreur
	 */
	public String messageErreur() {
		return messageErreur;
	}

	/**
	 * Sets the message erreur.
	 *
	 * @param messageErreur the messageErreur to set
	 */
	public void setMessageErreur(String messageErreur) {
		this.messageErreur = messageErreur;
	}

	/**
	 * Checks if is afficher message erreur.
	 *
	 * @return true, if is afficher message erreur
	 */
	public boolean isAfficherMessageErreur() {
		boolean isAfficherMessageErreur = false;
		
		isAfficherMessageErreur = !StringCtrl.isEmpty(messageErreur());
		
		return isAfficherMessageErreur;
	}
}