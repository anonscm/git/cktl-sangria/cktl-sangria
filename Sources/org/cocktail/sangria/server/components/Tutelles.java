package org.cocktail.sangria.server.components;

import org.cocktail.fwkcktlajaxwebext.serveur.component.CktlAjaxWindow;
import org.cocktail.fwkcktlpersonne.common.eospecificites.EOStructureForGroupeSpec;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartStructure;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.sangria.server.components.commons.ASangriaComponent;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WODisplayGroup;
import com.webobjects.eocontrol.EOQualifier;

import er.extensions.appserver.ERXDisplayGroup;
import er.extensions.eof.ERXDatabaseDataSource;
import er.extensions.eof.ERXFetchSpecification;

public class Tutelles extends ASangriaComponent {
	private static final long serialVersionUID = -8316064626815218030L;

	private final static String GROUPE_TUTELLE_KEY = "org.cocktail.sangria.groupes.tutelles";
    
    private EOStructure currentTutelle;
    private ERXDisplayGroup<EOStructure> tutelleDisplayGroup;
    private ERXDatabaseDataSource tutelleDataSource;
    private EOStructure groupeTutelle;
    private EOStructure selectedTutelle;
    private EOStructure nouvelleTutelle;
    
    public Tutelles(WOContext context) {
        super(context);
    }
    
    public String getAjoutTutelleWindowId() {
    	return getComponentId() + "_" + "AjoutTutelleWindow";
    }
    
    public String getTutelleTableViewId() {
    	return getComponentId() + "_" + "TutelleTableView";
    }
    
    public String getTutelleContainerId() {
    	return getComponentId() + "_" + "TutelleContainer";
    }
    
    
    public EOStructure getCurrentTutelle() {
    	return currentTutelle;
    }

    public void setCurrentTutelle(EOStructure currentTutelle) {
    	this.currentTutelle = currentTutelle;
    }

    public ERXDisplayGroup<EOStructure> getTutelleDisplayGroup() {
		if(tutelleDisplayGroup == null) {
			tutelleDisplayGroup = new ERXDisplayGroup<EOStructure>();
			tutelleDisplayGroup.setDataSource(getTutelleDataSource());
			tutelleDisplayGroup.setDelegate(new TutelleDisplayGroupDelegate());
			tutelleDisplayGroup.fetch();
		}
		
		return tutelleDisplayGroup;
    }

    public void setTutelleDisplayGroup(ERXDisplayGroup<EOStructure> tutelleDisplayGroup) {
    	this.tutelleDisplayGroup = tutelleDisplayGroup;
    }

    public ERXDatabaseDataSource getTutelleDataSource() {
	
		if(tutelleDataSource == null) {
			tutelleDataSource = new ERXDatabaseDataSource(edc(), EOStructure.ENTITY_NAME);
		    
	            EOStructure _groupeTutelle;      
	            try {
	            	_groupeTutelle = getGroupeTutelle();
	            }
	            catch (Exception e) {
	            	return null;
	            }
	            
	            EOQualifier qualifier = EOStructure.TO_REPART_STRUCTURES.dot(EORepartStructure.TO_STRUCTURE_GROUPE).eq(_groupeTutelle);
	            ERXFetchSpecification<EOStructure> fetchSpecification = new ERXFetchSpecification<EOStructure>(EOStructure.ENTITY_NAME, qualifier, EOStructure.LL_STRUCTURE.ascs());
	            tutelleDataSource.setFetchSpecification(fetchSpecification);
		}	
		
		return tutelleDataSource;
    }

    public void setTutelleDataSource(ERXDatabaseDataSource tutelleDataSource) {
    	this.tutelleDataSource = tutelleDataSource;
    }

    public EOStructure getGroupeTutelle() throws Exception {
	if(groupeTutelle == null) {
	    try {
		groupeTutelle = EOStructureForGroupeSpec.getGroupeForParamKey(edc(), GROUPE_TUTELLE_KEY);
	      } catch (Exception e) {
	        session().addSimpleErrorMessage("Erreur", session().localizer().localizedStringForKey("echecRecuperationTutelle"));
	        throw e;
	      }
	}
	
	return groupeTutelle;
    }

    public void setGroupeTutelle(EOStructure groupeTutelle) {
	this.groupeTutelle = groupeTutelle;
    }

    public EOStructure getSelectedTutelle() {
	return selectedTutelle;
    }

    public void setSelectedTutelle(EOStructure selectedTutelle) {
	this.selectedTutelle = selectedTutelle;
    }
    
    public EOStructure getNouvelleTutelle() {
	return nouvelleTutelle;
    }

    public void setNouvelleTutelle(EOStructure nouvelleTutelle) {
	this.nouvelleTutelle = nouvelleTutelle;
    }
    
    public WOActionResults ajouterTutelle() {
	
	if(getNouvelleTutelle() == null) {
	    session().addSimpleErrorMessage("Erreur", session().localizer().localizedStringForKey("gestionTutelle.aucuneTutelleSelectionnee"));
	    return doNothing();
	}
	
	EOStructure _groupeTutelle;
	try {
	    _groupeTutelle = getGroupeTutelle();
	} catch (Exception e) {
	    session().addSimpleErrorMessage("Erreur", session().localizer().localizedStringForKey("echecRecuperationTutelle")); 
	    return doNothing();
	}
	
	if(EOStructureForGroupeSpec.isPersonneInGroupe(nouvelleTutelle, _groupeTutelle)) {
	    session().addSimpleErrorMessage("Erreur", session().localizer().localizedStringForKey("gestionTutelle.TutelleDejaPresente"));
	    return doNothing();
	}
	
	EOStructureForGroupeSpec.affecterPersonneDansGroupe(
		edc(), 
		getNouvelleTutelle(), 
		_groupeTutelle, 
		getUtilisateurPersId());
	
	try {
            edc().saveChanges();
        } catch (ValidationException e) {
            session().addSimpleErrorMessage("Erreur", e);
            edc().revert();
            return doNothing();
        }

	CktlAjaxWindow.close(context(), getAjoutTutelleWindowId());
	
	tutelleDisplayGroup.fetch();
	
	return doNothing();
    }
    
    public WOActionResults annulerAjout() {
	
	edc().revert();
	CktlAjaxWindow.close(context(), getAjoutTutelleWindowId());
	return doNothing();
    }
    
    public WOActionResults supprimerTutelle() {
	
	if(getSelectedTutelle() == null) {
	    session().addSimpleErrorMessage("Erreur", session().localizer().localizedStringForKey("gestionTutelle.aucuneTutelleSelectionnee"));
	    return doNothing();
	}
    	
	try {
	    EOStructureForGroupeSpec.supprimerPersonneDuGroupe(edc(), getSelectedTutelle(), getGroupeTutelle(), getUtilisateurPersId());
	    edc().saveChanges();
	    tutelleDisplayGroup.fetch();
	} catch (ValidationException e) {
	    session().addSimpleErrorMessage("Erreur", e);
	    edc().revert();
	} catch (Exception e) {
	    session().addSimpleErrorMessage("Erreur", session().localizer().localizedStringForKey("echecRecuperationTutelle"));
	    edc().revert();
	}
	
	return doNothing();
    }

    public class TutelleDisplayGroupDelegate {
		public void displayGroupDidChangeSelectedObjects(WODisplayGroup group) {
		    ERXDisplayGroup<EOStructure> _groupe = (ERXDisplayGroup<EOStructure>) group;
		    if( _groupe.selectedObject() != null) {
			setSelectedTutelle(_groupe.selectedObject());
		    }
		}
    }
}
