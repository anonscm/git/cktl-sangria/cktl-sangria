package org.cocktail.sangria.server.components;

import com.webobjects.appserver.WOContext;

import org.cocktail.fwkcktlnomenclatures.server.metier.EONomenclature;
import org.cocktail.sangria.server.components.commons.ASangriaComponent;

public class Nomenclatures extends ASangriaComponent {

	private static final long serialVersionUID = 8998888844963254558L;
	
	private EONomenclature selectedNomenclature;
	private Boolean wantRefreshObjet;
	
    public Nomenclatures(WOContext context) {
        super(context);
    }
    
    public String containerObjetsNomenclatureId() {
    	return getComponentId() + "_containerObjetsNomenclature";
    }

	public EONomenclature getSelectedNomenclature() {
		return selectedNomenclature;
	}

	public void setSelectedNomenclature(EONomenclature selectedNomenclature) {
		this.selectedNomenclature = selectedNomenclature;
		setWantRefreshObjet(Boolean.TRUE);
	}

	public Boolean getWantRefreshObjet() {
		return wantRefreshObjet;
	}

	public void setWantRefreshObjet(Boolean wantRefreshObjet) {
		this.wantRefreshObjet = wantRefreshObjet;
	}
	
	public Integer getPersId() {
		return getApplicationUser().getPersId();
	}
}