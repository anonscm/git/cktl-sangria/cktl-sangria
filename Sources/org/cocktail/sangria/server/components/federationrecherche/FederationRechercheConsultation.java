package org.cocktail.sangria.server.components.federationrecherche;

import org.cocktail.fwkcktlpersonne.common.metier.EOAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlrecherche.server.metier.factory.FactoryAssociation;
import org.cocktail.sangria.server.components.commons.ASangriaComponent;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.foundation.NSTimestamp;

public class FederationRechercheConsultation extends ASangriaComponent {
	
	
	private EOStructure federationRecherche;
	
    private Boolean ongletIdentificationSelected = true;

    private Boolean ongletMembresSelected = false;
    
    private Boolean ongletPersonnelsSelected = false;

    private Boolean ongletGestionFinanciereSelected = false;
    
    private Boolean ongletEvaluationSelected = false;

    public FederationRechercheConsultation(WOContext context) {
        super(context);
    }

	public void setFederationRecherche(EOStructure federationRecherche) {
		this.federationRecherche = federationRecherche;
	}

	public EOStructure federationRecherche() {
		return federationRecherche;
	}
	

	public Boolean ongletIdentificationSelected() {
		return ongletIdentificationSelected;
	}
	
	/**
	 * Sets the onglet identification selected.
	 *
	 * @param ongletIdentificationSelected the new onglet identification selected
	 */
	public void setOngletIdentificationSelected(Boolean ongletIdentificationSelected) {
		this.ongletIdentificationSelected = ongletIdentificationSelected;
	}
	
	public void setOngletMembresSelected(Boolean ongletMembresSelected) {
		this.ongletMembresSelected = ongletMembresSelected;
	}

	public Boolean ongletMembresSelected() {
		return ongletMembresSelected;
	}

	public void setOngletPersonnelsSelected(Boolean ongletPersonnelsSelected) {
		this.ongletPersonnelsSelected = ongletPersonnelsSelected;
	}

	public Boolean ongletPersonnelsSelected() {
		return ongletPersonnelsSelected;
	}
	
	public void setOngletGestionFinanciereSelected(
			Boolean ongletGestionFinanciereSelected) {
		this.ongletGestionFinanciereSelected = ongletGestionFinanciereSelected;
	}

	public Boolean ongletGestionFinanciereSelected() {
		return ongletGestionFinanciereSelected;
	}

	public void setOngletEvaluationSelected(Boolean ongletEvaluationSelected) {
		this.ongletEvaluationSelected = ongletEvaluationSelected;
	}

	public Boolean ongletEvaluationSelected() {
		return ongletEvaluationSelected;
	}

	/**
     * Onglets container id.
     *
     * @return the string
     */
    public String ongletsContainerId() {
    	return getComponentId() + "_ongletsContainer";
    }
    
    /**
     * Onglet identification id.
     *
     * @return the string
     */
    public String ongletIdentificationId() {
    	return getComponentId() + "_ongletIdentification";
    }
 
    public String ongletMembresId() {
    	return getComponentId() + "_ongletMembres";
    }
    
    public String ongletPersonnelsId() {
    	return getComponentId() + "_ongletPersonnels";
    }
    
    public String ongletGestionFinanciereId() {
    	return getComponentId() + "_ongletGestionFinanciere";
    }

    public String ongletEvaluationId() {
    	return getComponentId() + "_ongletEvaluation";
    }
    
	public WOActionResults enregistrerLesModications() {
		
		try {
			federationRecherche().setDModification(new NSTimestamp()); 	
			federationRecherche().setPersIdModification(getUtilisateurPersId());
			edc().saveChanges();
			session().addSimpleSuccessMessage("Enregistrement effectué avec succès", null);
		} catch (ValidationException e) {
			session().addSimpleErrorMessage("Erreur", e);
		}
		return doNothing();
	}
	
	public WOActionResults retourFederationsDeRecherche() {
		return pageWithName(FederationsRecherche.class.getName());
	}
	
	public EOAssociation typesMembresStructureRecherche() {
		return FactoryAssociation.shared().typesMembresStructureRechercheAssociation(edc());
	}
	
	
	
}