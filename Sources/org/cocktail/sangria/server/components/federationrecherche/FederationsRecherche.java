package org.cocktail.sangria.server.components.federationrecherche;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;

import er.extensions.eof.ERXEC;

import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlrecherche.server.util.StructuresRechercheHelper;
import org.cocktail.fwkcktlsangriaguiajax.serveur.components.commons.AFwkCktlSangriaComponent;
import org.cocktail.sangria.server.Session;
import org.cocktail.sangria.server.components.commons.ASangriaComponent;

public class FederationsRecherche extends ASangriaComponent {
	
	private EOStructure currentFederationRecherche;
	
    public FederationsRecherche(WOContext context) {
        super(context);
    }
    
    public NSArray<EOStructure> listeFederationRecherche() {
    	return StructuresRechercheHelper.federationsRecherche(edc());
    }

	public void setCurrentFederationRecherche(EOStructure currentFederationRecherche) {
		this.currentFederationRecherche = currentFederationRecherche;
	}

	public EOStructure currentFederationRecherche() {
		return currentFederationRecherche;
	}
	
	public WOActionResults voirCurrentFederation() {
		FederationRechercheConsultation federationRechercheConsultation = (FederationRechercheConsultation) pageWithName(FederationRechercheConsultation.class.getName());
		if(currentFederationRecherche().persIdCreation() == null) {
			currentFederationRecherche().setPersIdCreation(session().connectedUserInfo().persId().intValue());
		}
		currentFederationRecherche().setPersIdModification(session().connectedUserInfo().persId().intValue());
		EOEditingContext e  = ERXEC.newEditingContext();
		federationRechercheConsultation.setFederationRecherche(currentFederationRecherche().localInstanceIn(e));
		federationRechercheConsultation.setEdc(e);
		return federationRechercheConsultation;
	}
	
    
}