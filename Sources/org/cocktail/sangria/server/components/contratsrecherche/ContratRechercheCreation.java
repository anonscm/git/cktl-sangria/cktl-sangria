/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2010 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.cocktail.sangria.server.components.contratsrecherche;

import org.cocktail.fwkcktlrecherche.server.metier.EOContratRecherche;
import org.cocktail.fwkcktlrecherche.server.metier.EOProjetScientifique;
import org.cocktail.fwkcktlrecherche.server.metier.factory.FactoryContratRecherche;
import org.cocktail.sangria.server.components.assistants.IAssistantActionsDelegate;
import org.cocktail.sangria.server.components.commons.ASangriaComponent;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOComponent;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSForwardException;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.appserver.ERXRedirect;
import er.extensions.eof.ERXEC;
import er.extensions.foundation.ERXTimestampUtilities;
import er.extensions.validation.ERXValidationException;
import er.extensions.validation.ERXValidationFactory;

/**
 * The Class ContratRechercheCreation.
 */
public class ContratRechercheCreation extends ContratRechercheComponent implements IAssistantActionsDelegate {
	
	/** The edc. */
	private EOEditingContext editingContext = null;

	/** The modules. */
	private NSArray modules;
	
	/** The etapes. */
	private NSArray<String> etapes;
	
	/** The index module actif. */
	private Integer indexModuleActif = 0;
	
	/** The contrat recherche. */
	private EOContratRecherche contratRecherche;

	private EOProjetScientifique projetScientifique;
	
	private IAssistantActionsDelegate assistantActionsDelegate = null;
	
    /**
     * Instantiates a new contrat recherche creation.
     *
     * @param context the context
     */
    public ContratRechercheCreation(WOContext context) {
        super(context);
    }
    
    
	/**
	 * Gets the contrat recherche.
	 *
	 * @return the contrat recherche
	 */
	public EOContratRecherche contratRecherche() {
		if (contratRecherche == null) {
			
			FactoryContratRecherche factoryContratRecherche = new FactoryContratRecherche(edc(), false);
			try {
				contratRecherche = factoryContratRecherche.creerContratVide("", getUtilisateurPersId());
				contratRecherche.setEtablisssementGestionnaireFinancier(null, getUtilisateurPersId());
				contratRecherche.setPersIdCreation(getUtilisateurPersId());
				contratRecherche.setPersIdModification(getUtilisateurPersId());
				contratRecherche.setDCreation(new NSTimestamp());
				contratRecherche.setDModification(new NSTimestamp());
			} catch (Exception e) {
				throw NSForwardException._runtimeExceptionForThrowable(e);
			}
		}
		
		return contratRecherche;
	}

    
	
	/**
	 * Modules.
	 *
	 * @return the nS array
	 */
	public NSArray modules() {
			modules = new NSArray<String>(new String[]{
					"ModuleContratRechercheIdentification",
					"ModuleContratRecherchePartieAdmin",
					"ModuleContratRecherchePartiePI",
					"ModuleContratRecherchePartieFin"});

		return modules;
	}
	
	
	/**
	 * Etapes.
	 *
	 * @return the nS array
	 */
	public NSArray<String> etapes() {
		etapes = new NSArray<String>(new String[]{
					"Identification",
					"Partie Admin.",
					"Partie PI",
					"Partie Fin."});
		
		return etapes;
	}

	/**
	 * Sets the index module actif.
	 *
	 * @param indexModuleActif the new index module actif
	 */
	public void setIndexModuleActif(Integer indexModuleActif) {
		this.indexModuleActif = indexModuleActif;
	}

	/**
	 * Gets the index module actif.
	 *
	 * @return the index module actif
	 */
	public Integer getIndexModuleActif() {
		return indexModuleActif;
	}
	
	/**
	 * Terminer.
	 *
	 * @return the wO action results
	 * @throws Exception the exception
	 */
	public WOActionResults terminer() {
		try {
			contratRecherche().contrat().setShouldNotValidate(true);
			edc().saveChanges();
		}
		catch(ValidationException e) {
			throw e;
		}
		return doNothing();
	}
	
	public WOActionResults onAnnuler() {
		return assistantActionsDelegate().annuler();
	}
	
	public WOActionResults annuler()  {
		return pageWithName(ContratsRecherche.class.getName());
	}

	
	public WOActionResults onApresTerminer() {
		ERXRedirect redirect = (ERXRedirect) pageWithName(ERXRedirect.class.getName());
		redirect.setComponent((WOComponent) assistantActionsDelegate().apresTerminer()); 
		return redirect;
	}
	
	public WOActionResults apresTerminer() {
		
		ContratRechercheConsultation nextPage = (ContratRechercheConsultation) pageWithName(ContratRechercheConsultation.class.getName());
		EOEditingContext e = ERXEC.newEditingContext();
		nextPage.setEdc(e);
		nextPage.setContratRecherche(contratRecherche().localInstanceIn(e));
		nextPage.setModification(true);
		session().addSimpleSuccessMessage("Création OK", "Le contrat de recherche " + contratRecherche().contrat().exerciceEtIndex() + " a bien été créé");

		return nextPage;
		
	}
	
	public EOProjetScientifique projetScientifique() {
		return projetScientifique;
	}


	public void setProjetScientifique(EOProjetScientifique projetScientifique) {
		this.projetScientifique = projetScientifique;
	}

   
    @Override
    public EOEditingContext edc() {
    	if (editingContext == null) {
    		editingContext = ERXEC.newEditingContext();
    	}
    	return editingContext;
    }


	public void setAssistantActionsDelegate(IAssistantActionsDelegate assistantActionsDelegate) {
		this.assistantActionsDelegate = assistantActionsDelegate;
	}


	public IAssistantActionsDelegate assistantActionsDelegate() {
		if (assistantActionsDelegate == null) {
			assistantActionsDelegate = this;
		}
		return assistantActionsDelegate;
	}	
}