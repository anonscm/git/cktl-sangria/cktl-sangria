/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2010 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.cocktail.sangria.server.components.contratsrecherche;

import org.cocktail.fwkcktlrecherche.server.metier.EOContratRecherche;
import org.cocktail.fwkcktlrecherche.server.metier.factory.FactoryContratRecherche;
import org.cocktail.fwkcktlreportingguiajax.serveur.CktlAbstractReporterAjaxProgress;
import org.cocktail.reporting.server.CktlAbstractReporter;
import org.cocktail.sangria.server.components.uniterecherche.UniteRechercheConsultation.ReporterAjaxProgress;
import org.cocktail.sangria.server.reports.EditionFicheContrat;
import org.cocktail.sangria.server.reports.EditionFicheContratBeans;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSForwardException;
import com.webobjects.foundation.NSNotificationCenter;
import com.webobjects.foundation.NSTimestamp;

import er.ajax.AjaxUpdateContainer;
import er.extensions.appserver.ERXRedirect;

// TODO: Auto-generated Javadoc
/**
 * The Class ContratRechercheConsultation.
 */
public class ContratRechercheConsultation extends ContratRechercheComponent {

  /** The Constant BINDING_contratRecherche. */
  private final static String BINDING_contratRecherche = "contratRecherche";

  /** The Constant NOTIFICATION_contratSupprime. */
  public final static String NOTIFICATION_contratSupprime = "contratSupprime";

  /** The contrat recherche. */
  private EOContratRecherche contratRecherche;

  private EOContratRecherche editingContratRecherche = null;

  private EOEditingContext contratRechercheEditingContext = null;


  private ReporterAjaxProgress reporterProgress;
  private String reportFilename;
  private CktlAbstractReporter reporter;

  
  /**
   * Instantiates a new contrat recherche consultation.
   * 
   * @param context
   *          the context
   */
  public ContratRechercheConsultation(WOContext context) {
    super(context);
  }

  /**
   * Gets the contrat recherche.
   * 
   * @return the contrat recherche
   */
  public EOContratRecherche contratRecherche() {
    return contratRecherche;

  }

  /**
   * Sets the contrat recherche.
   * 
   * @param contratRecherche
   *          the new contrat recherche
   */
  public void setContratRecherche(EOContratRecherche contratRecherche) {
    this.contratRecherche = contratRecherche;
  }

  // GESTION DES ONGLETS

  /** The onglet identification selected. */
  private Boolean ongletIdentificationSelected = true;

  /** The onglet partie admin selected. */
  private Boolean ongletPartieAdminSelected = false;

  /** The onglet partie fin selected. */
  private Boolean ongletPartieFinSelected = false;

  /** The onglet partie pi selected. */
  private Boolean ongletPartiePiSelected = false;

  /** The onglet notes selected. */
  private Boolean ongletNotesSelected = false;

  private Boolean ongletPartieDocumentsSelected = false;

  /** The consultation. */
  private Boolean consultation = false;

  /** The modification. */
  private Boolean modification = false;

  public final String documentsContainerId = getComponentId() + "_documentsContainer";
  
  /**
   * Onglet identification selected.
   * 
   * @return the boolean
   */
  public Boolean ongletIdentificationSelected() {
    return ongletIdentificationSelected;
  }

  public String erreurMessageContainerId() {
    return getComponentId() + "_erreurMessageContainer";
  }

  /**
   * Sets the onglet identification selected.
   * 
   * @param ongletIdentificationSelected
   *          the new onglet identification selected
   */
  public void setOngletIdentificationSelected(Boolean ongletIdentificationSelected) {
    this.ongletIdentificationSelected = ongletIdentificationSelected;
  }

  /**
   * Onglet partie admin selected.
   * 
   * @return the boolean
   */
  public Boolean ongletPartieAdminSelected() {
    return ongletPartieAdminSelected;
  }

  /**
   * Sets the onglet partie admin selected.
   * 
   * @param ongletPartieAdminSelected
   *          the new onglet partie admin selected
   */
  public void setOngletPartieAdminSelected(Boolean ongletPartieAdminSelected) {
    this.ongletPartieAdminSelected = ongletPartieAdminSelected;
  }

  /**
   * Onglet partie fin selected.
   * 
   * @return the boolean
   */
  public Boolean ongletPartieFinSelected() {
    return ongletPartieFinSelected;
  }

  /**
   * Sets the onglet partie fin selected.
   * 
   * @param ongletPartieFinSelected
   *          the new onglet partie fin selected
   */
  public void setOngletPartieFinSelected(Boolean ongletPartieFinSelected) {
    this.ongletPartieFinSelected = ongletPartieFinSelected;
  }

  /**
   * Onglet partie pi selected.
   * 
   * @return the boolean
   */
  public Boolean ongletPartiePiSelected() {
    return ongletPartiePiSelected;
  }

  /**
   * Sets the onglet partie pi selected.
   * 
   * @param ongletPartiePiSelected
   *          the new onglet partie pi selected
   */
  public void setOngletPartiePiSelected(Boolean ongletPartiePiSelected) {
    this.ongletPartiePiSelected = ongletPartiePiSelected;
  }

  /**
   * Onglet notes selected.
   * 
   * @return the boolean
   */
  public Boolean ongletNotesSelected() {
    return ongletNotesSelected;
  }

  /**
   * Sets the onglet notes selected.
   * 
   * @param ongletPartiePiSelected
   *          the new onglet partie pi selected
   */
  public void setOngletNotesSelected(Boolean ongletNotesSelected) {
    this.ongletNotesSelected = ongletNotesSelected;
  }

  // INDENTIFIANTS DES COMPOSANTS GRAPHIQUES

  /**
   * Onglets container id.
   * 
   * @return the string
   */
  public String ongletsContainerId() {
    return getComponentId() + "_ongletsContainer";
  }

  /**
   * Onglet identification id.
   * 
   * @return the string
   */
  public String ongletIdentificationId() {
    return getComponentId() + "_ongletIdentification";
  }

  /**
   * Onglet partie admin id.
   * 
   * @return the string
   */
  public String ongletPartieAdminId() {
    return getComponentId() + "_ongletPartieAdmin";
  }

  /**
   * Onglet partie fin id.
   * 
   * @return the string
   */
  public String ongletPartieFinId() {
    return getComponentId() + "_ongletPartieFin";
  }

  /**
   * Onglet partie pi id.
   * 
   * @return the string
   */
  public String ongletPartiePiId() {
    return getComponentId() + "_ongletPartiePi";
  }

  public String ongletNotesId() {
    return getComponentId() + "_ongletNotes";
  }

  public String ongletPartieDocumentsId() {
    return getComponentId() + "_ongletPartieDocuments";
  }

  /**
   * Consultation.
   * 
   * @return the boolean
   */
  public Boolean consultation() {
    return consultation;
  }

  /**
   * Sets the consultation.
   * 
   * @param consultation
   *          the new consultation
   */
  public void setConsultation(Boolean consultation) {
    this.consultation = consultation;

  }

  /**
   * Modification.
   * 
   * @return the boolean
   */
  public Boolean modification() {
    if (consultation()) {
      return false;
    }
    else {
      return modification;
    }
  }

  /**
   * Sets the modification.
   * 
   * @param modification
   *          the new modification
   */
  public void setModification(Boolean modification) {
    this.modification = modification;
  }

  /**
   * Retour recherche.
   * 
   * @return the wO action results
   */
  public WOActionResults retourRecherche() {
    return session().getSavedPageWithName(ContratRechercheSearch.class.getName());
  }

  /**
   * Enregistrer les modications.
   * 
   * @return the wO action results
   */
  public WOActionResults enregistrerLesModications() {

    try {
      contratRecherche().setDModification(new NSTimestamp());
      contratRecherche().setPersIdModification(getUtilisateurPersId());
      contratRecherche().contrat().setShouldNotValidate(true);
      edc().saveChanges();
      session().addSimpleSuccessMessage(null, "Le contrat a été enregistré avec succès");
      if (ongletPartieDocumentsSelected()) {
    	  AjaxUpdateContainer.updateContainerWithID(documentsContainerId,  context());
      }
    } catch (ValidationException e) {
      session().addSimpleErrorMessage("Erreur", e);
    }
    return doNothing();
  }

  /**
   * Supprimer contrat recherche.
   * 
   * @return the wO action results
   */
  public WOActionResults supprimerContratRecherche() {

    FactoryContratRecherche factory = new FactoryContratRecherche(edc(), false);
    try {
      factory.supprimerContratRecherche(contratRecherche());
      edc().saveChanges();
      session().addSimpleSuccessMessage("Confirmation", "Le contrat a bien été supprimé");
      NSNotificationCenter.defaultCenter().postNotification(NOTIFICATION_contratSupprime, this);
    } catch (ValidationException e) {
      session().addSimpleErrorMessage("Erreur", e);
      return doNothing();
    } catch (Exception e) {
      throw NSForwardException._runtimeExceptionForThrowable(e);
    }

    ERXRedirect redirect = (ERXRedirect) pageWithName(ERXRedirect.class.getName());
    redirect.setComponent(session().getSavedPageWithName(ContratRechercheSearch.class.getName()));
    return redirect;

  }

  public void setOngletPartieDocumentsSelected(Boolean ongletPartieDocumentsSelected) {
    this.ongletPartieDocumentsSelected = ongletPartieDocumentsSelected;
  }

  public Boolean ongletPartieDocumentsSelected() {
    return ongletPartieDocumentsSelected;
  }


  public WOActionResults imprimerFicheContrat() {
    reporterProgress = new ReporterAjaxProgress(100);

    reportFilename = "FicheContrat-" + contratRecherche().contrat().exerciceEtIndex() + ".pdf";
	reporter = EditionFicheContratBeans.reporter(contratRecherche(), reporterProgress);

    return doNothing();
  }


  public CktlAbstractReporter getReporter() {
        return reporter;
    }

  public CktlAbstractReporterAjaxProgress getReporterProgress() {
        return reporterProgress;
    }
  
  public String getReportFilename() {
        return reportFilename;
    }
  
  public void setReportFilename(String reportFilename) {
        this.reportFilename = reportFilename;
    }
  
}