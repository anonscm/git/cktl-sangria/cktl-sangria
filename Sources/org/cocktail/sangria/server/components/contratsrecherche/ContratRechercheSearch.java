/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2010 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.cocktail.sangria.server.components.contratsrecherche;

import java.util.List;

import org.cocktail.cocowork.server.metier.convention.Contrat;
import org.cocktail.fwkcktldroitsutils.common.util.MyDateCtrl;
import org.cocktail.fwkcktlrecherche.server.metier.EOContratRecherche;
import org.cocktail.fwkcktlrecherche.server.metier.EOGdStrategieSangria;
import org.cocktail.fwkcktlrecherche.server.metier.service.GroupeRechercheService;
import org.cocktail.fwkcktlrecherche.server.metier.service.RechercheContratService;
import org.cocktail.fwkcktlrecherche.server.metier.service.RechercheContratService.RechercheContratCriteres;
import org.cocktail.fwkcktlrecherche.server.metier.service.RechercheContratService.RechercheContratResultat;
import org.cocktail.fwkcktlreportingguiajax.serveur.CktlAbstractReporterAjaxProgress;
import org.cocktail.fwkcktlsangriaguiajax.serveur.components.contrats.ContratRechercheExportExcel;
import org.cocktail.fwkcktlsangriaguiajax.serveur.components.contrats.ContratRechercheExportExcelValo;
import org.cocktail.reporting.server.CktlAbstractReporter;
import org.cocktail.sangria.server.components.uniterecherche.UniteRechercheConsultation.ReporterAjaxProgress;
import org.cocktail.sangria.server.reports.EditionFicheContratBeans;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

import er.ajax.AjaxUpdateContainer;
import er.extensions.appserver.ERXDisplayGroup;
import er.extensions.eof.ERXEC;

// TODO: Auto-generated Javadoc
/**
 * The Class ContratRechercheSearch.
 */
public class ContratRechercheSearch extends ContratRechercheComponent {

	public String KEY_ANNEE_CONTRAT = "EXE_ORDRE";
	public String KEY_NUMERO_CONTRAT = "CON_INDEX";
	public String KEY_OBJET_CONTRAT = "CON_OBJET";
	public String KEY_IS_CONTRAT_RECHERCHE = "CONTRAT_RECHERCHE_ID";
	public String KEY_TYPE_CONTRAT = "TYPE_CONTRAT";
	public String KEY_SUIVI_CONTRAT = "SUIVI";

	
	private RechercheContratCriteres criteres = new RechercheContratCriteres();
	
	private RechercheContratResultat contratCourant;
	private RechercheContratResultat selectedResultat;

	private ERXDisplayGroup<RechercheContratResultat> dgContratRecherche;
	private RechercheContratService rechercheContratService;
	
	private ReporterAjaxProgress reporterProgress;
	private String reportFilename;
	private CktlAbstractReporter reporter;

	private Boolean resetStructureRechercheAutoComplete = false;
	
	private GroupeRechercheService groupeRechercheService;
	
	/**
	 * @param context le context pour le composant
	 */
	public ContratRechercheSearch(WOContext context) {
		super(context);
	}
	
	/**
	 * @return le service pour la recherche des contrats
	 */
	public RechercheContratService getRechercheContratService() {
		if (rechercheContratService == null) {
			rechercheContratService = new RechercheContratService(edc());
		}
		return rechercheContratService;
	}
	
	/**
	 * @return un instance de GroupeRechercheService
	 */
	public GroupeRechercheService getGroupeRechercheService() {
		if (groupeRechercheService == null) {
			groupeRechercheService = new GroupeRechercheService(edc());
		}
		return groupeRechercheService;
	}

	public String getFormulaireRechercheAapId() {
		return getComponentId() + "_formulaireRechercheAap";
	}

	public String getContratDisplayContainerId() {
		return getComponentId() + "_contratDisplayContainer";
	}

	public String getContratTableViewId() {
		return getComponentId() + "_contratTableView";
	}

	public String getContratTableViewContainerId() {
		return getComponentId() + "_contratTableViewContainer";
	}


	/**
	 * Action pour la remise à réro du formulaire
	 * @return rien
	 */
	public WOActionResults razRecherche() {
		raz();
		// MISE A JOUR DE L'INTERFACE
		AjaxUpdateContainer.updateContainerWithID(getContratTableViewContainerId(), context());
		AjaxUpdateContainer.updateContainerWithID(getContratDisplayContainerId(), context());

		return doNothing();
	}

	protected void raz() {
		setCriteres(new RechercheContratCriteres());
		setResetStructureRechercheAutoComplete(true);
		// RESET DU DG
		dgContratRecherche().setObjectArray(null);
		dgContratRecherche().updateDisplayedObjects();
	}

	public RechercheContratCriteres getCriteres() {
		return criteres;
	}

	public void setCriteres(RechercheContratCriteres criteres) {
		this.criteres = criteres;
	}

	public void setContratCourant(RechercheContratResultat contratCourant) {
		this.contratCourant = contratCourant;
	}

	public RechercheContratResultat getSelectedResultat() {
		return dgContratRecherche().selectedObject();
	}

	public RechercheContratResultat getContratCourant() {
		return contratCourant;
	}

	/**
	 * Effectue la recherche avec les critères choisis
	 * @return rien
	 */
	public WOActionResults lancerRecherche() {

		EOQualifier qualifierPerimetresDeDonnees = getPerimetreProvider().getAllQualifiers(getStrategies(),
				session().connectedUserInfo());
		criteres.setPerimetresQualifier(qualifierPerimetresDeDonnees);
		
		List<RechercheContratResultat> resultats = getRechercheContratService().rechercher(getCriteres());
		dgContratRecherche().setObjectArray(new NSArray<RechercheContratResultat>(resultats));
		
		if (qualifierPerimetresDeDonnees == null) {
			session().addSimpleInfoMessage("Remarque", "Vous ne disposez pas de droits suffisants pour afficher les contrats de recherche.");
		}
		if (resultats.isEmpty()) {
			session().addSimpleLocalizedInfoMessage("aucunResultatDeRecherche");
		}

		AjaxUpdateContainer.updateContainerWithID(getContratDisplayContainerId(), context());
		
		return doNothing();
	}
	
	private NSArray<EOGdStrategieSangria> getStrategies() {
		NSArray<EOGdStrategieSangria> strategies = new NSMutableArray<EOGdStrategieSangria>();
		strategies.add(EOGdStrategieSangria.strategieAccesConsultationContratsStructure(edc()));
		strategies.add(EOGdStrategieSangria.strategieAccesConsultationTousContratsStructure(edc()));
		return strategies;
	}

	public Boolean getHasResultatsDeRecherche() {
		return true;
	}

	/**
	 * @return le contrat "réél" correspondant au résultat
	 */
	public EOContratRecherche selectedContratRecherche() {
		return getRechercheContratService().getSelectedContratRecherchePourResultat(getSelectedResultat());
	}

	/**
	 * @return le contrat "réél" correspondant au résultat
	 */
	public Contrat getSelectedContrat() {
		return getRechercheContratService().getSelectedContratPourResultat(getSelectedResultat());
	}

	public Boolean isContratRecherche() {
		return getSelectedResultat().isTypeRecherche();
	}


	public ERXDisplayGroup<RechercheContratResultat> dgContratRecherche() {

		if (dgContratRecherche == null) {
			dgContratRecherche = new ERXDisplayGroup<RechercheContratResultat>();
		}
		return dgContratRecherche;

	}



	public WOActionResults voirContratEnModification() {
		ContratRechercheConsultation nextPage = (ContratRechercheConsultation) pageWithName(ContratRechercheConsultation.class.getName());
		EOEditingContext edc = ERXEC.newEditingContext();
		nextPage.setEdc(edc);
		nextPage.setContratRecherche(selectedContratRecherche().localInstanceIn(edc));
		nextPage.setModification(true);
		return nextPage;
	}

	public WOActionResults voirContratEnConsultation() {
		ContratRechercheConsultation nextPage = (ContratRechercheConsultation) pageWithName(ContratRechercheConsultation.class.getName());
		nextPage.setContratRecherche(selectedContratRecherche());
		nextPage.setConsultation(true);
		return nextPage;
	}

	public WOActionResults creerContratRecherche() {
		EOEditingContext e = ERXEC.newEditingContext();
		EOContratRecherche contratRecherche = EOContratRecherche.creerInstance(e);
		
		if (getSelectedContrat() != null) {
			contratRecherche.setContratRelationship(getSelectedContrat().localInstanceIn(e));
		}
		
		contratRecherche.setSensMontantFinancier(EOContratRecherche.CODE_SENS_MONTANT_FINANCIER_ENTRANT);

		contratRecherche.setPersIdCreation(getUtilisateurPersId());
		contratRecherche.setPersIdModification(getUtilisateurPersId());

		contratRecherche.setDCreation(MyDateCtrl.getDateJour());
		contratRecherche.setDModification(MyDateCtrl.getDateJour());

		contratRecherche.setContratSupprime(false);

		ContratRechercheConsultation nextPage = (ContratRechercheConsultation) pageWithName(ContratRechercheConsultation.class.getName());

		nextPage.setEdc(e);
		nextPage.setContratRecherche(contratRecherche);
		nextPage.setModification(true);

		return nextPage;

	}

	public WOActionResults exportExcel() {
		ContratRechercheExportExcel contratRechercheExportExcel = (ContratRechercheExportExcel) pageWithName(ContratRechercheExportExcel.class.getName());
		NSArray<EOContratRecherche> contrats = new NSArray<EOContratRecherche>(getRechercheContratService().getContratsPourResultats(dgContratRecherche().allObjects()));
		contratRechercheExportExcel.setContrats(contrats);
		return contratRechercheExportExcel;
	}

	public WOActionResults exportExcelValo() {
		ContratRechercheExportExcelValo contratRechercheExportExcel = (ContratRechercheExportExcelValo) pageWithName(ContratRechercheExportExcelValo.class.getName());
		NSArray<EOContratRecherche> contrats = new NSArray<EOContratRecherche>(getRechercheContratService().getContratsPourResultats(dgContratRecherche().allObjects()));
		contratRechercheExportExcel.setContrats(contrats);
		return contratRechercheExportExcel;
	}

	public WOActionResults imprimerFicheContrat() {
		reporterProgress = new ReporterAjaxProgress(100);

		reportFilename = "FicheContrat-" + selectedContratRecherche().contrat().exerciceEtIndex() + ".pdf";
		reporter = EditionFicheContratBeans.reporter(selectedContratRecherche(), reporterProgress);

		return doNothing();
	}

	public CktlAbstractReporter getReporter() {
		return reporter;
	}

	public CktlAbstractReporterAjaxProgress getReporterProgress() {
		return reporterProgress;
	}

	public String getReportFilename() {
		return reportFilename;
	}

	public void setReportFilename(String reportFilename) {
		this.reportFilename = reportFilename;
	}

	public Boolean getResetStructureRechercheAutoComplete() {
		return resetStructureRechercheAutoComplete;
	}

	public void setResetStructureRechercheAutoComplete(Boolean resetStructureRechercheAutoComplete) {
		this.resetStructureRechercheAutoComplete = resetStructureRechercheAutoComplete;
	}

}