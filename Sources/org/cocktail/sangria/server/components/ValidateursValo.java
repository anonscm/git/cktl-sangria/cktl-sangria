package org.cocktail.sangria.server.components;

import org.cocktail.fwkcktlrecherche.server.ParametresRecherche;

import com.webobjects.appserver.WOContext;

/**
 * Gestion des validateurs valo
 */
public class ValidateursValo extends GestionPersonnesGroupe {

	private static final long serialVersionUID = -2073730406080192355L;

	/**
	 * @param context le context
	 */
	public ValidateursValo(WOContext context) {
        super(context);
		if (getGestionPersonnesGroupeController() != null) {
			getGestionPersonnesGroupeController().setAfficheStructures(false);
		}
    }

	@Override
	protected String getGroupeParamKey() {
		return ParametresRecherche.GROUPE_VALIDATEURS_VALO_KEY;
	}

	@Override
	public String getEchecRecuperationGroupeMessage() {
		return session().localizer().localizedStringForKey("echecRecuperationValidateursValo");
	}
}