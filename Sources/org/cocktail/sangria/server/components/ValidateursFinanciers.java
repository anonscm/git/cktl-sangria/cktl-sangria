package org.cocktail.sangria.server.components;

import org.cocktail.fwkcktlrecherche.server.ParametresRecherche;

import com.webobjects.appserver.WOContext;

/**
 * Gestion des validateurs financiers des conventions
 *
 */
public class ValidateursFinanciers extends GestionPersonnesGroupe {

	private static final long serialVersionUID = -9014071974652182384L;

	/**
	 * @param context le context
	 */
	public ValidateursFinanciers(WOContext context) {
        super(context);
		if (getGestionPersonnesGroupeController() != null) {
			getGestionPersonnesGroupeController().setAfficheStructures(false);
		}
    }

	@Override
	public String getEchecRecuperationGroupeMessage() {
		return session().localizer().localizedStringForKey("echecRecuperationValidateursFinanciers");
	}

	@Override
	protected String getGroupeParamKey() {
		return ParametresRecherche.GROUPE_VALIDATEURS_FINANCIERS_KEY;
	}
	
}