package org.cocktail.sangria.server.components;

import org.cocktail.fwkcktlajaxwebext.serveur.component.CktlAjaxWindow;
import org.cocktail.fwkcktlpersonne.common.eospecificites.EOStructureForGroupeSpec;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartStructure;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.sangria.server.components.commons.ASangriaComponent;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WODisplayGroup;
import com.webobjects.eocontrol.EOQualifier;

import er.extensions.appserver.ERXDisplayGroup;
import er.extensions.eof.ERXDatabaseDataSource;
import er.extensions.eof.ERXFetchSpecification;

public class CoordinateursProjets extends ASangriaComponent {
	
	private static final long serialVersionUID = -1270269438948072998L;

	private final static String GROUPE_COORDINATEURS_PROJETS_KEY = "org.cocktail.sangria.groupes.coordinateursprojets";
    
    private EOStructure currentCoordinateurProjet;
    private ERXDisplayGroup<EOStructure> coordinateurProjetDisplayGroup;
    private ERXDatabaseDataSource coordinateurProjetDataSource;
    private EOStructure groupeCoordinateurProjet;
    private EOStructure selectedCoordinateurProjet;
    private EOStructure nouveauCoordinateurProjet;

	public CoordinateursProjets(WOContext context) {
        super(context);
    }
    
    public String getAjoutCoordinateurProjetWindowId() {
    	return getComponentId() + "_" + "AjoutCoordinateurProjetWindow";
    }
    
    public String getCoordinateurProjetTableViewId() {
    	return getComponentId() + "_" + "CoordinateurProjetTableView";
    }
    
    public String getCoordinateurProjetContainerId() {
    	return getComponentId() + "_" + "CoordinateurProjetContainer";
    }
    
    
    public EOStructure getCurrentCoordinateurProjet() {
    	return currentCoordinateurProjet;
    }

    public void setCurrentCoordinateurProjet(EOStructure currentCoordinateurProjet) {
    	this.currentCoordinateurProjet = currentCoordinateurProjet;
    }

    public ERXDisplayGroup<EOStructure> getCoordinateurProjetDisplayGroup() {
		if(coordinateurProjetDisplayGroup == null) {
			coordinateurProjetDisplayGroup = new ERXDisplayGroup<EOStructure>();
			coordinateurProjetDisplayGroup.setDataSource(getCoordinateurProjetDataSource());
			coordinateurProjetDisplayGroup.setDelegate(new CoordinateurProjetDisplayGroupDelegate());
			coordinateurProjetDisplayGroup.fetch();
		}
		
		return coordinateurProjetDisplayGroup;
    }

    public void setCoordinateurProjetDisplayGroup(ERXDisplayGroup<EOStructure> coordinateurProjetDisplayGroup) {
    	this.coordinateurProjetDisplayGroup = coordinateurProjetDisplayGroup;
    }

    public ERXDatabaseDataSource getCoordinateurProjetDataSource() {
	
		if(coordinateurProjetDataSource == null) {
			coordinateurProjetDataSource = new ERXDatabaseDataSource(edc(), EOStructure.ENTITY_NAME);
		    
	            EOStructure _groupeCoordinateurProjet;      
	            try {
	            	_groupeCoordinateurProjet = getGroupeCoordinateurProjet();
	            }
	            catch (Exception e) {
	            	return null;
	            }
	            
	            EOQualifier qualifier = EOStructure.TO_REPART_STRUCTURES.dot(EORepartStructure.TO_STRUCTURE_GROUPE).eq(_groupeCoordinateurProjet);
	            ERXFetchSpecification<EOStructure> fetchSpecification = new ERXFetchSpecification<EOStructure>(EOStructure.ENTITY_NAME, qualifier, EOStructure.LL_STRUCTURE.ascs());
	            coordinateurProjetDataSource.setFetchSpecification(fetchSpecification);
		}	
		
		return coordinateurProjetDataSource;
    }

    public void setCoordinateurProjetDataSource(ERXDatabaseDataSource coordinateurProjetDataSource) {
    	this.coordinateurProjetDataSource = coordinateurProjetDataSource;
    }

    public EOStructure getGroupeCoordinateurProjet() throws Exception {
    	if(groupeCoordinateurProjet == null) {
		    try {
			groupeCoordinateurProjet = EOStructureForGroupeSpec.getGroupeForParamKey(edc(), GROUPE_COORDINATEURS_PROJETS_KEY);
		      } catch (Exception e) {
		        session().addSimpleErrorMessage("Erreur", session().localizer().localizedStringForKey("echecRecuperationCoordinateurProjet"));
		        throw e;
		      }
    	}
		
		return groupeCoordinateurProjet;
    }

    public void setGroupeCoordinateurProjet(EOStructure groupeCoordinateurProjet) {
    	this.groupeCoordinateurProjet = groupeCoordinateurProjet;
    }

    public EOStructure getSelectedCoordinateurProjet() {
    	return selectedCoordinateurProjet;
    }

    public void setSelectedCoordinateurProjet(EOStructure selectedCoordinateurProjet) {
    	this.selectedCoordinateurProjet = selectedCoordinateurProjet;
    }
    
    public EOStructure getNouveauCoordinateurProjet() {
    	return nouveauCoordinateurProjet;
    }

    public void setNouveauCoordinateurProjet(EOStructure nouveauCoordinateurProjet) {
    	this.nouveauCoordinateurProjet = nouveauCoordinateurProjet;
    }
    
    public WOActionResults ajouterCoordinateurProjet() {
	
		if(getNouveauCoordinateurProjet() == null) {
		    session().addSimpleErrorMessage("Erreur", session().localizer().localizedStringForKey("gestionCoordinateurProjet.aucunCoordinateurProjetSelectionne"));
		    return doNothing();
		}
		
		EOStructure _groupeCoordinateurProjet;
		try {
		    _groupeCoordinateurProjet = getGroupeCoordinateurProjet();
		} catch (Exception e) {
		    session().addSimpleErrorMessage("Erreur", session().localizer().localizedStringForKey("echecRecuperationCoordinateurProjet")); 
		    return doNothing();
		}
		
		if(EOStructureForGroupeSpec.isPersonneInGroupe(getNouveauCoordinateurProjet(), _groupeCoordinateurProjet)) {
		    session().addSimpleErrorMessage("Erreur", session().localizer().localizedStringForKey("gestionCoordinateurProjet.CoordinateurProjetDejaPresent"));
		    return doNothing();
		}
		
		EOStructureForGroupeSpec.affecterPersonneDansGroupe(
			edc(), 
			getNouveauCoordinateurProjet(), 
			_groupeCoordinateurProjet, 
			getUtilisateurPersId());
		
		try {
	            edc().saveChanges();
	        } catch (ValidationException e) {
	            session().addSimpleErrorMessage("Erreur", e);
	            edc().revert();
	            return doNothing();
	        }
	
		CktlAjaxWindow.close(context(), getAjoutCoordinateurProjetWindowId());
		
		coordinateurProjetDisplayGroup.fetch();
		
		return doNothing();
    }
    
    public WOActionResults annulerAjout() {
	
		edc().revert();
		CktlAjaxWindow.close(context(), getAjoutCoordinateurProjetWindowId());
		return doNothing();
    }
    
    public WOActionResults supprimerCoordinateurProjet() {
		if(getSelectedCoordinateurProjet() == null) {
			session().addSimpleErrorMessage("Erreur", session().localizer().localizedStringForKey("gestionCoordinateurProjet.aucunCoordinateurProjetSelectionne"));
			return doNothing();
		}
		try {
		    EOStructureForGroupeSpec.supprimerPersonneDuGroupe(edc(), getSelectedCoordinateurProjet(), getGroupeCoordinateurProjet(), getUtilisateurPersId());
		    edc().saveChanges();
		    coordinateurProjetDisplayGroup.fetch();
		} catch (ValidationException e) {
		    session().addSimpleErrorMessage("Erreur", e);
		    edc().revert();
		} catch (Exception e) {
		    session().addSimpleErrorMessage("Erreur", session().localizer().localizedStringForKey("echecRecuperationCoordinateurProjet"));
		    edc().revert();
		}
		
		return doNothing();
    }

    public class CoordinateurProjetDisplayGroupDelegate {
		public void displayGroupDidChangeSelectedObjects(WODisplayGroup group) {
		    ERXDisplayGroup<EOStructure> _groupe = (ERXDisplayGroup<EOStructure>) group;
		    if( _groupe.selectedObject() != null) {
			setSelectedCoordinateurProjet(_groupe.selectedObject());
		    }
		}
    }
}
