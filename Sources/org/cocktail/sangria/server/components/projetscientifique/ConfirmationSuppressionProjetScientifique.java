package org.cocktail.sangria.server.components.projetscientifique;

import org.cocktail.fwkcktlajaxwebext.serveur.CocktailAjaxSession;
import org.cocktail.sangria.server.components.Accueil;
import org.cocktail.sangria.server.components.commons.ASangriaComponent;
import org.cocktail.sangria.server.components.contratsrecherche.ContratRechercheSearch;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;

public class ConfirmationSuppressionProjetScientifique extends ASangriaComponent {
    public ConfirmationSuppressionProjetScientifique(WOContext context) {
        super(context);
    }
    
    /**
     * Retour recherche en cours.
     *
     * @return the wO action results
     */
    public WOActionResults retourRechercheEnCours() {
    	CocktailAjaxSession sess = (CocktailAjaxSession) session(); 
		return sess.getSavedPageWithName(ProjetScientifiqueSearch.class.getName());
    }

	/**
	 * Retour nouvelle recherche.
	 *
	 * @return the wO action results
	 */
	public WOActionResults retourNouvelleRecherche() {
		CocktailAjaxSession sess = (CocktailAjaxSession) session(); 
		ProjetScientifiqueSearch nextPage = (ProjetScientifiqueSearch) sess.getSavedPageWithName(ProjetScientifiqueSearch.class.getName());
		nextPage.raz();
		return nextPage;
	}

	/**
	 * Aller acceuil.
	 *
	 * @return the wO action results
	 */
	public WOActionResults allerAcceuil() {
		return pageWithName(Accueil.class.getName());
	}
	
}