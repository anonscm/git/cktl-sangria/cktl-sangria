package org.cocktail.sangria.server.components.projetscientifique;

import org.cocktail.sangria.server.components.commons.ASangriaComponent;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;

public class ProjetScientifiqueComponent extends ASangriaComponent {

	public ProjetScientifiqueComponent(WOContext context) {
		super(context);
	}

	public WOActionResults retourProjetsScientifiques() {
		return pageWithName(ProjetScientifique.class.getName());
	}
	
}
