package org.cocktail.sangria.server.components.projetscientifique;

import org.cocktail.cocowork.server.metier.convention.ProjetContrat;
import org.cocktail.cocowork.server.metier.convention.factory.FactoryProjet;
import org.cocktail.fwkcktlajaxwebext.serveur.component.CktlAjaxWindow;
import org.cocktail.fwkcktlrecherche.server.metier.EOAap;
import org.cocktail.fwkcktlrecherche.server.metier.EOContratRecherche;
import org.cocktail.fwkcktlsangriaguiajax.serveur.components.commons.ICktlAjaxWindowWrapperActionsDelegate;
import org.cocktail.fwkcktlsangriaguiajax.serveur.components.contrats.ContratsRechercheSearchWinForProjet;
import org.cocktail.fwkcktlsangriaguiajax.serveur.components.projetscientifique.ProjetScientifiqueComponentUI;
import org.cocktail.sangria.server.components.aap.AapSearchWinForProjet;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WODisplayGroup;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSForwardException;

import er.extensions.appserver.ERXDisplayGroup;
import er.extensions.eof.ERXFetchSpecification;
import er.extensions.eof.ERXQ;

public class ProjetScientifiqueContratsConventionsUIM extends ProjetScientifiqueComponentUI {
	
	private final String BINDING_creationContratRechercheAction = "creationContratRechercheAction";
	private final String BINDING_creationAapAction = "creationAapAction";

	
	private EOAap aapAAjouter;
	private EOAap selectedAap;
	private EOContratRecherche contratRechercheAAjouter;
	private EOContratRecherche selectedContratRecherche;
	
	
	
    private ERXDisplayGroup<EOAap> dgAap = null;
    private ERXDisplayGroup<EOContratRecherche> dgContratRecherche = null;

    
    private ContratsRechercheSearchWinForProjet contratsRechercheSearchWinForProjet = null;
    private AapSearchWinForProjet aapSearchWinForProjet = null;
    
    
    public ProjetScientifiqueContratsConventionsUIM(WOContext context) {
        super(context);
    }

	public ERXDisplayGroup<EOAap> dgAap() {
		if (dgAap == null) {
			dgAap = new ERXDisplayGroup<EOAap>();
			dgAap.setDelegate(new DgAapDelegate());
			dgAap.setObjectArray(listeAap());
		}
		return dgAap;
	}
	
	public class DgAapDelegate {
		public void displayGroupDidChangeSelection(WODisplayGroup group) {
			ERXDisplayGroup<EOAap> localGroup = (ERXDisplayGroup<EOAap>) group;
			if (group.selectedObject() != null) {
				setSelectedAap(localGroup.selectedObject());
			}
		}
	}
	
	public ERXDisplayGroup<EOContratRecherche> dgContratsRecherche() {
		if (dgContratRecherche == null) {
			dgContratRecherche = new ERXDisplayGroup<EOContratRecherche>();
			dgContratRecherche.setDelegate(new DgContratRechercheDelegate());
			dgContratRecherche.setObjectArray(listeContratsRecherche());
		}
		return dgContratRecherche;
	}
	
	public class DgContratRechercheDelegate {
		public void displayGroupDidChangeSelection(WODisplayGroup group) {
			ERXDisplayGroup<EOContratRecherche> localGroup = (ERXDisplayGroup<EOContratRecherche>) group;
			if (group.selectedObject() != null) {
				setSelectedContratRecherche(localGroup.selectedObject());
			}
		}
	}

    
    

	public void setAapAAjouter(EOAap aapAAjouter) {
		this.aapAAjouter = aapAAjouter;
	}
	
	public EOAap aapAAjouter() {
		return aapAAjouter;
	}

	
	public void setSelectedAap(EOAap selectedAap) {
		this.selectedAap = selectedAap;
	}

	public EOAap selectedAap() {
		return selectedAap;
	}





	public NSArray<EOAap> listeAap() {
		return projetScientifique().aapsDuProjet();
	}
	
	public void setContratRechercheAAjouter(EOContratRecherche contratRechercheAAjouter) {
		this.contratRechercheAAjouter = contratRechercheAAjouter;
	}

	public EOContratRecherche contratRechercheAAjouter() {
		return contratRechercheAAjouter;
	}

	public void setSelectedContratRecherche(EOContratRecherche selectedContratRecherche) {
		this.selectedContratRecherche = selectedContratRecherche;
	}

	public EOContratRecherche selectedContratRecherche() {
		return selectedContratRecherche;
	}

	public NSArray<EOContratRecherche> listeContratsRecherche() {
		return projetScientifique().contratsRecherche(); 
	}
	
	
	public void onAjoutAap() {
		FactoryProjet factoryProjet = new FactoryProjet(edc());
		try {
			factoryProjet.ajouterContrat(projetScientifique().projet(), aapAAjouter().contrat());
		} catch (Exception e) {
			throw NSForwardException._runtimeExceptionForThrowable(e);
		}	
	}
	
	public void onSupprimerAap() {
		EOQualifier quaflierFoContratProjet = ERXQ.and(
				ERXQ.equals("projet", projetScientifique().projet()),
				ERXQ.equals("contrat", selectedAap().contrat())
		);
		ERXFetchSpecification<ProjetContrat> fetchSpecification = new ERXFetchSpecification<ProjetContrat>(ProjetContrat.ENTITY_NAME, quaflierFoContratProjet, null);
		fetchSpecification.setIncludeEditingContextChanges(true);
		NSArray<ProjetContrat> resultats =  edc().objectsWithFetchSpecification(fetchSpecification);
		ProjetContrat projetContrat = resultats.get(0);
		selectedAap().contrat().removeFromProjetContratRelationship(projetContrat);
		projetScientifique().projet().removeFromProjetContratsRelationship(projetContrat);
		projetContrat.setContratRelationship(null);
		projetContrat.setProjetRelationship(null);
		edc().deleteObject(projetContrat);

	}
	
	public void onAjoutContratRecherche() {
		FactoryProjet factoryProjet = new FactoryProjet(edc());
		try {
			factoryProjet.ajouterContrat(projetScientifique().projet(), contratRechercheAAjouter().contrat());
		} catch (Exception e) {
			throw NSForwardException._runtimeExceptionForThrowable(e);
		}
	}
	
	public void onSupprimerContratRecherche() {
		EOQualifier quaflierForContratProjet = ERXQ.and(
				ERXQ.equals("projet", projetScientifique().projet()),
				ERXQ.equals("contrat", selectedContratRecherche())
		);
		ERXFetchSpecification<ProjetContrat> fetchSpecification = new ERXFetchSpecification<ProjetContrat>(ProjetContrat.ENTITY_NAME, quaflierForContratProjet, null);
		fetchSpecification.setIncludeEditingContextChanges(true);
		NSArray<ProjetContrat> resultats =  edc().objectsWithFetchSpecification(fetchSpecification);
		ProjetContrat projetContrat = resultats.get(0);
		selectedContratRecherche().contrat().removeFromProjetContratRelationship(projetContrat);
		projetScientifique().projet().removeFromProjetContratsRelationship(projetContrat);
		projetContrat.setContratRelationship(null);
		projetContrat.setProjetRelationship(null);
		edc().deleteObject(projetContrat);
	}




	public String ddsContainerId() {
		return getComponentId() + "_ddsContainer";
	}
	
	public String ddsSearchWindowId() {
		return getComponentId() + "_ddsSearchWindow";
	}
	
	
	public WOActionResults onAjoutDds() {
		FactoryProjet factoryProjet = new FactoryProjet(edc());
		try {
			factoryProjet.ajouterContrat(projetScientifique().projet(), aapAAjouter().contrat());
		}  catch (Exception e) {
			throw NSForwardException._runtimeExceptionForThrowable(e);
		}
		return doNothing();
	}
	
	public WOActionResults onSuppressionDds() {
		EOQualifier quaflierFoContratProjet = ERXQ.and(
				ERXQ.equals("projet", projetScientifique().projet()),
				ERXQ.equals("contrat", selectedAap().contrat())
		);
		ERXFetchSpecification<ProjetContrat> fetchSpecification = new ERXFetchSpecification<ProjetContrat>(ProjetContrat.ENTITY_NAME, quaflierFoContratProjet, null);
		fetchSpecification.setIncludeEditingContextChanges(true);
		NSArray<ProjetContrat> resultats =  edc().objectsWithFetchSpecification(fetchSpecification);
		ProjetContrat projetContrat = resultats.get(0);
		selectedAap().contrat().removeFromProjetContratRelationship(projetContrat);
		projetScientifique().projet().removeFromProjetContratsRelationship(projetContrat);
		projetContrat.setContratRelationship(null);
		projetContrat.setProjetRelationship(null);
		edc().deleteObject(projetContrat);
		dgAap.setObjectArray(listeAap());
		session().addSimpleSuccessMessage(null, "La demande de subvention " + selectedAap().numero() + " a été retirée du projet");
		return doNothing();
	}
	

	/*******************************************/
	/*** Gestion des contrats de recherche ***/

	

	public String contratsContainerId() {
		return getComponentId() + "_contratsContainer";
	}
	
	public String contratSearchWindowId() {
		return getComponentId() + "_contratSearchWindow";
	}
	
	public WOActionResults onSuppressionContrat() {
		EOQualifier quaflierFoContratProjet = ERXQ.and(
				ERXQ.equals("projet", projetScientifique().projet()),
				ERXQ.equals("contrat", selectedContratRecherche().contrat())
		);
		ERXFetchSpecification<ProjetContrat> fetchSpecification = new ERXFetchSpecification<ProjetContrat>(ProjetContrat.ENTITY_NAME, quaflierFoContratProjet, null);
		fetchSpecification.setIncludeEditingContextChanges(true);
		NSArray<ProjetContrat> resultats =  edc().objectsWithFetchSpecification(fetchSpecification);
		ProjetContrat projetContrat = resultats.get(0);
		selectedContratRecherche().contrat().removeFromProjetContratRelationship(projetContrat);
		projetScientifique().projet().removeFromProjetContratsRelationship(projetContrat);
		projetContrat.setContratRelationship(null);
		projetContrat.setProjetRelationship(null);
		edc().deleteObject(projetContrat);
		dgContratRecherche.setObjectArray(listeContratsRecherche());
		session().addSimpleSuccessMessage(null, "Le contrat " + selectedContratRecherche().contrat().exerciceEtIndex() + " a été retiré du projet");
		return doNothing();
	}


	public ContratsRechercheSearchWinForProjet contratsRechercheSearchWinForProjet() {
		if (contratsRechercheSearchWinForProjet == null) {
			contratsRechercheSearchWinForProjet = (ContratsRechercheSearchWinForProjet) pageWithName(ContratsRechercheSearchWinForProjet.class.getName());
			WOActionResults creationContratRechercheAction = (WOActionResults) valueForBinding(BINDING_creationContratRechercheAction);
			contratsRechercheSearchWinForProjet.setCreerContratRechercheAction(creationContratRechercheAction);
			contratsRechercheSearchWinForProjet.setRechercheContratActionDelegate(new RechercheContratActionDelegate());
			contratsRechercheSearchWinForProjet.setEdc(edc());
		}
		return contratsRechercheSearchWinForProjet;
	}
	
	class RechercheContratActionDelegate implements ICktlAjaxWindowWrapperActionsDelegate {

		public WOActionResults ok() {
			if (contratsRechercheSearchWinForProjet().selection() == null) {
				session().addSimpleErrorMessage("Erreur", "Vous  devez sélectionner un contrat ou en créer un nouveau");
			}
			FactoryProjet factoryProjet = new FactoryProjet(edc());
			try {
				contratsRechercheSearchWinForProjet().selection().contrat().setShouldNotValidate(true);
				factoryProjet.ajouterContrat(projetScientifique().projet(), contratsRechercheSearchWinForProjet().selection().contrat());
				CktlAjaxWindow.close(context());
				dgContratRecherche.setObjectArray(listeContratsRecherche());
			} catch (Exception e) {
				throw NSForwardException._runtimeExceptionForThrowable(e);
			}
			return doNothing();

		}

		public WOActionResults annuler() {
			contratsRechercheSearchWinForProjet = null;
			CktlAjaxWindow.close(context());
			return null;
		}
		
	}
	
	

	public AapSearchWinForProjet aapSearchWinForProjet() {
		if (aapSearchWinForProjet == null) {
			aapSearchWinForProjet = (AapSearchWinForProjet) pageWithName(AapSearchWinForProjet.class.getName());
			WOActionResults creationAapAction = (WOActionResults) valueForBinding(BINDING_creationAapAction);
			aapSearchWinForProjet.setCreerAapAction(creationAapAction);
			aapSearchWinForProjet.setRechercheAapActionDelegate(new RechercheAapActionDelegate());
			aapSearchWinForProjet.setEdc(edc());
		}
		return aapSearchWinForProjet;
	}
	
	class RechercheAapActionDelegate implements ICktlAjaxWindowWrapperActionsDelegate {

		public WOActionResults ok() {
			if(aapSearchWinForProjet().selection() == null) {
				session().addSimpleErrorMessage("Erreur", "Vous  devez sélectionner un appel à projet ou en créer un nouveau");
			}
			FactoryProjet factoryProjet = new FactoryProjet(edc());
			try {
				factoryProjet.ajouterContrat(projetScientifique().projet(), aapSearchWinForProjet().selection().contrat());
				CktlAjaxWindow.close(context());
				dgAap.setObjectArray(listeAap());
			} catch (Exception e) {
				throw NSForwardException._runtimeExceptionForThrowable(e);
			}
			return doNothing();

		}

		public WOActionResults annuler() {
			aapSearchWinForProjet = null;
			CktlAjaxWindow.close(context());
			return null;
		}
		
	}
	
}