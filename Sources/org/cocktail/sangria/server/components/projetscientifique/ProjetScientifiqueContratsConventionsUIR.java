package org.cocktail.sangria.server.components.projetscientifique;

import org.cocktail.fwkcktlrecherche.server.metier.EOAap;
import org.cocktail.fwkcktlrecherche.server.metier.EOContratRecherche;
import org.cocktail.fwkcktlsangriaguiajax.serveur.components.projetscientifique.ProjetScientifiqueComponentUI;

import com.webobjects.appserver.WOContext;
import com.webobjects.foundation.NSArray;

public class ProjetScientifiqueContratsConventionsUIR extends ProjetScientifiqueComponentUI {
	
	private EOAap aapCourant;
	private EOContratRecherche contratRechercheCourant;
	
    public ProjetScientifiqueContratsConventionsUIR(WOContext context) {
        super(context);
    }
    
	public NSArray<EOAap> listeAap() {
		return projetScientifique().aapsDuProjet();
	}
	
	public NSArray<EOContratRecherche> listeContratsRecherche() {
		return projetScientifique().contratsRecherche();
	}

	public void setAapCourant(EOAap aapCourant) {
		this.aapCourant = aapCourant;
	}

	public EOAap getAapCourant() {
		return aapCourant;
	}

	public void setContratRechercheCourant(EOContratRecherche contratRechercheCourant) {
		this.contratRechercheCourant = contratRechercheCourant;
	}

	public EOContratRecherche getContratRechercheCourant() {
		return contratRechercheCourant;
	}
	
}