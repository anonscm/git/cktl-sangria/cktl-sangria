/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2010 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.cocktail.sangria.server.components.projetscientifique;

import java.util.List;

import org.cocktail.cocowork.common.exception.ExceptionFinder;
import org.cocktail.cocowork.server.metier.convention.Contrat;
import org.cocktail.fwkcktlajaxwebext.serveur.component.tableview.column.CktlAjaxTableViewColumn;
import org.cocktail.fwkcktlajaxwebext.serveur.component.tableview.column.CktlAjaxTableViewColumnAssociation;
import org.cocktail.fwkcktldroitsutils.common.util.MyStringCtrl;
import org.cocktail.fwkcktlrecherche.server.metier.EOAap;
import org.cocktail.fwkcktlrecherche.server.metier.EOProjetScientifique;
import org.cocktail.fwkcktlrecherche.server.metier.finder.FinderProjetScientifique;
import org.cocktail.fwkcktlrecherche.server.metier.service.RechercheProjetService;
import org.cocktail.fwkcktlrecherche.server.metier.service.RechercheProjetService.RechercheProjetCriteres;
import org.cocktail.sangria.server.components.commons.ASangriaComponent;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WODisplayGroup;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSData;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSPropertyListSerialization;

import er.ajax.AjaxUpdateContainer;
import er.extensions.appserver.ERXDisplayGroup;
import er.extensions.eof.ERXEC;
import er.extensions.foundation.ERXArrayUtilities;

// TODO: Auto-generated Javadoc
/**
 * The Class ProjetScientifiqueSearch.
 */
public class ProjetScientifiqueSearch extends ProjetScientifiqueComponent {

    /** The exercice contrat. */
    private String exerciceContrat;

    /** The numero contrat. */
    private String numeroContrat;

    /** The erreurs messages. */
    private NSArray<String> erreursMessages;

    private NSMutableDictionary dicoTableViewProjetScientifique;

    private Boolean hasSearchedForProjet = false;

    private EOProjetScientifique selectedProjetScientifique;
    private EOProjetScientifique currentProjetScientifique;


    private RechercheProjetService rechercheProjetService;
    private RechercheProjetCriteres criteres = new RechercheProjetCriteres();
    
    private ERXDisplayGroup<EOProjetScientifique> dgProjetScientifique;

	private Boolean resetStructureRechercheAutoComplete;

    /**
     * Instantiates a new projet scientifique search.
     * 
     * @param context
     *            the context
     */
    public ProjetScientifiqueSearch(WOContext context) {
        super(context);
    }
    
    /**
     * 
     * @return le service pour lancer les recherches de projet
     */
    public RechercheProjetService getRechercheProjetService() {
    	if (rechercheProjetService == null) {
    		rechercheProjetService = new RechercheProjetService(edc());
    	}
		return rechercheProjetService;
	}
    
    public RechercheProjetCriteres getCriteres() {
		return criteres;
	}
    
    public void setErreursMessages(NSArray<String> erreursMessages) {
        this.erreursMessages = erreursMessages;
    }

    public NSArray<String> getErreursMessages() {
        return erreursMessages;
    }

    /**
     * Raz recherche.
     * 
     * @return rien
     */
    public WOActionResults razRecherche() {
        raz();
        AjaxUpdateContainer.updateContainerWithID(getProjetTableViewContainerId(), context());
        AjaxUpdateContainer.updateContainerWithID(getProjetDisplayContainerId(), context());
        return doNothing();
    }

    /**
     * Raz.
     */
    public void raz() {
    	criteres = new RechercheProjetCriteres();
        setSelectedProjetScientifique(null);
        setResetStructureRechercheAutoComplete(true);
        
        dgProjetScientifique = null;
        hasSearchedForProjet = false;
    }

    public String getFormulaireRechercheProjetScientifiqueId() {
        return getComponentId() + "_formulaireRechercheProjetScientifique";
    }

    public String getProjetScientifiqueTableViewId() {
        return getComponentId() + "_projetScientifiqueTableView";
    }

    public String getProjetDisplayContainerId() {
        return getComponentId() + "_projetDisplayContainer";
    }

    public String getProjetTableViewContainerId() {
        return getComponentId() + "_projetTableViewContainer";
    }



    /**
     * Si besoin le displayGroup utilis� par la tableView est initialis�
     * Il d'agit d'un ERXDisplayGroup. Le delegate pour ce displayGroup est
     * d�fini juste apr�s
     * 
     * @return le displayGroup
     */
    public WODisplayGroup dgProjetScientifique() {

        if (dgProjetScientifique == null) {
            dgProjetScientifique = new ERXDisplayGroup<EOProjetScientifique>();
            dgProjetScientifique.setDelegate(new DgProjetScientifiqueDelegate());
            dgProjetScientifique.setSelectsFirstObjectAfterFetch(true);
        }
        return dgProjetScientifique;

    }

    public void setSelectedProjetScientifique(EOProjetScientifique selectedProjetScientifique) {
        this.selectedProjetScientifique = selectedProjetScientifique;
    }

    public EOProjetScientifique getSelectedProjetScientifique() {
        return selectedProjetScientifique;
    }

    public void setCurrentProjetScientifique(EOProjetScientifique currentProjetScientifique) {
        this.currentProjetScientifique = currentProjetScientifique;
    }

    public EOProjetScientifique getCurrentProjetScientifique() {
        return currentProjetScientifique;
    }

    /**
     * Dans cette classe, on définit le comportement lorsque
     * l'utilisateur change la sélection dans la tableView
     * Il s'agit simplement de changer la variable qui stocke
     * le projet sélectionné.
     */
    public class DgProjetScientifiqueDelegate {

        /**
         * Display group did change selection.
         * 
         * @param group
         *            the group
         */
        public void displayGroupDidChangeSelection(WODisplayGroup group) {
            if (group.selectedObject() != null) {
                setSelectedProjetScientifique((EOProjetScientifique) group.selectedObject());
            }
        }
    }


    public Boolean hasSelectedProjet() {
        return (selectedProjetScientifique != null);
    }

    public Boolean hasSearchedForProjet() {
        return hasSearchedForProjet;
    }

    public WOActionResults lancerRecherche() {
    	List<EOProjetScientifique> resultats = getRechercheProjetService().rechercher(getCriteres());
    	dgProjetScientifique().setObjectArray(new NSArray<EOProjetScientifique>(resultats));
        if (resultats.isEmpty()) {
        	selectedProjetScientifique = null;
        }

        hasSearchedForProjet = true;

        AjaxUpdateContainer.updateContainerWithID(getProjetDisplayContainerId(), context());
        
        return doNothing();
    }

    public WOActionResults voirProjetEnModification() {
        EOEditingContext e = ERXEC.newEditingContext();
        ProjetScientifiqueConsultation nextPage = (ProjetScientifiqueConsultation) pageWithName(ProjetScientifiqueConsultation.class.getName());
        nextPage.setProjetScientifique(getSelectedProjetScientifique().localInstanceIn(e));
        nextPage.setEdc(e);
        nextPage.setModification(true);

        return nextPage;
    }

    /**
     * Voir contrat en consultation.
     * 
     * @return the wO action results
     */
    public WOActionResults voirProjetEnConsultation() {
        ProjetScientifiqueConsultation nextPage = (ProjetScientifiqueConsultation) pageWithName(ProjetScientifiqueConsultation.class.getName());
        nextPage.setProjetScientifique(getSelectedProjetScientifique());
        nextPage.setConsultation(true);
        return nextPage;
    }
    
    
    public Boolean getResetStructureRechercheAutoComplete() {
    	return resetStructureRechercheAutoComplete;
    }


    public void setResetStructureRechercheAutoComplete(
    		Boolean resetStructureRechercheAutoComplete) {
    	this.resetStructureRechercheAutoComplete = resetStructureRechercheAutoComplete;
    }

}