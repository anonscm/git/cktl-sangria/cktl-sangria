package org.cocktail.sangria.server.components.projetscientifique;

import org.cocktail.fwkcktlajaxwebext.serveur.component.CktlAjaxWindow;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.sangria.server.components.commons.ASangriaComponent;
import org.cocktail.sangria.server.controllers.ProjetScientifiqueCreationController;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.appserver.ERXRedirect;
import er.extensions.eof.ERXEC;

public class ProjetScientifiqueCreation2 extends ASangriaComponent {
  
  
  private ProjetScientifiqueCreationController controller;
  
  private static final String ANNEE_KEY = "annee"; 
  private Integer annee;
  
  private static final String TITRE_KEY = "titre"; 
  private String titre;
  
  private static final String DATE_DEBUT_KEY = "dateDebut";
  private NSTimestamp dateDebut;
  
  
  private NSMutableArray<EOStructure> structuresPorteuses;
  private EOStructure structurePorteuse;
  private EOStructure structurePorteuseSelectionnee;
  private EOStructure structurePorteuseEditee;
  
  private NSMutableDictionary<EOStructure, NSArray<EOIndividu>> responsablesScientifiquesDesStructures = new NSMutableDictionary<EOStructure, NSArray<EOIndividu>>();
  private EOIndividu responsableScientifique;
  private EOIndividu responsableScientifiqueSelectionnne;
  
  public final String structuresPorteusesContainerId = getComponentId() + "_structuresPorteusesContainer";
  public final String ajouterStructurePorteuseWindowId = getComponentId() + "_ajouterStructurePorteuseWindow";
  public final String ajouterResponsableScientifiqueWindowId = getComponentId() + "_ajouterResponsableScientifiqueWindow";

  
  public ProjetScientifiqueCreation2(WOContext context) {
    super(context);
  }
    
  public ProjetScientifiqueCreationController controller() {
    if (controller == null) {
      controller = new ProjetScientifiqueCreationController(this);
    }
    return controller;
  }   
    
  public WOActionResults retourProjetsScientifiques() {
    return pageWithName(ProjetScientifique.class.getName());
  }

  public Integer annee() {
    return annee;
  }

  public void setAnnee(Integer annee) {
    this.annee = annee;
  }

  public String titre() {
    return titre;
  }

  public void setTitre(String titre) {
    this.titre = titre;
  }

  public NSTimestamp dateDebut() {
    return dateDebut;
  }

  public void setDateDebut(NSTimestamp dateDebut) {
    this.dateDebut = dateDebut;
  }

  public NSMutableArray<EOStructure> structuresPorteuses() {
    if (structuresPorteuses == null) {
      structuresPorteuses = new NSMutableArray<EOStructure>();
    }
    return structuresPorteuses;
  }

  public void setStructuresPorteuses(NSMutableArray<EOStructure> structuresPorteuses) {
    this.structuresPorteuses = structuresPorteuses;
  }

  public EOStructure structurePorteuse() {
    return structurePorteuse;
  }

  public void setStructurePorteuse(EOStructure structurePorteuse) {
    this.structurePorteuse = structurePorteuse;
  }

  public EOStructure structurePorteuseSelectionnee() {
    return structurePorteuseSelectionnee;
  }

  public void setStructurePorteuseSelectionnee(EOStructure structurePorteuseSelectionnee) {
    this.structurePorteuseSelectionnee = structurePorteuseSelectionnee;
  }
  
  public EOStructure structurePorteuseEditee() {
    return structurePorteuseEditee;
  }

  public void setStructurePorteuseEditee(EOStructure structurePorteuseEditee) {
    this.structurePorteuseEditee = structurePorteuseEditee;
  }

  public NSArray<EOIndividu> responsablesScientifiques() {
    return responsablesScientifiques(structurePorteuse());
  }
  
  public NSArray<EOIndividu> responsablesScientifiques(EOStructure structurePorteuse) {
    if(responsablesScientifiquesDesStructures.containsKey(structurePorteuse) == false) {
      responsablesScientifiquesDesStructures.setObjectForKey(new NSMutableArray<EOIndividu>(), structurePorteuse);
    } 
    return responsablesScientifiquesDesStructures.objectForKey(structurePorteuse); 
  }
  
  public EOIndividu responsableScientifique() {
    return responsableScientifique;
  }

  public void setResponsableScientifique(EOIndividu responsableScientifique) {
    this.responsableScientifique = responsableScientifique;
  }
  
  public EOIndividu responsableScientifiqueSelectionnne() {
    return responsableScientifiqueSelectionnne;
  }

  public void setResponsableScientifiqueSelectionnne(EOIndividu responsableScientifiqueSelectionnne) {
    this.responsableScientifiqueSelectionnne = responsableScientifiqueSelectionnne;
  }

  public WOActionResults ajouterResponsableScientifique() {
    setStructurePorteuseEditee(structurePorteuse());
    return doNothing();
  }
  
  public WOActionResults validerAjoutResponsableScientifique() {
    if (responsableScientifiqueSelectionnne() == null) {
      session().addSimpleErrorMessage("Erreur", "Veuillez sélectionner un individu comme responsable scientifique");
      return doNothing();
    }
    responsablesScientifiques(structurePorteuseEditee()).add(responsableScientifiqueSelectionnne());
    CktlAjaxWindow.close(context(), ajouterResponsableScientifiqueWindowId);
    return doNothing();
  }
  
  public WOActionResults annulerAjoutResponsableScientifique() {
    CktlAjaxWindow.close(context(), ajouterResponsableScientifiqueWindowId);
    return doNothing();
  }
  
  public WOActionResults supprimerResponsableScientifique() {
    responsablesScientifiques().remove(responsableScientifique());
    return doNothing();
  }
  
  
  public WOActionResults validerAjoutStructurePorteuse() {
    if (structurePorteuseSelectionnee() == null) {
      session().addSimpleErrorMessage("Erreur", "Veuillez sélectionner une structure de recherche");
    }
    structuresPorteuses().add(structurePorteuseSelectionnee());
    CktlAjaxWindow.close(context(), ajouterStructurePorteuseWindowId);      
    return doNothing();
  }
  
  public WOActionResults annulerSelectionnerStructurePorteuse() {
    CktlAjaxWindow.close(context(), ajouterStructurePorteuseWindowId);
    return doNothing();
  }
  
  public WOActionResults supprimerStructurePorteuse() {
    structuresPorteuses().remove(structurePorteuse());
    return doNothing();
  }

  public WOActionResults enregistrer() {
    try {
      controller().enregistrer();
      ProjetScientifiqueConsultation nextPage = (ProjetScientifiqueConsultation) pageWithName(ProjetScientifiqueConsultation.class.getName());
      EOEditingContext e = ERXEC.newEditingContext();
      nextPage.setEdc(e);
      nextPage.setProjetScientifique(controller().projetScientifique().localInstanceIn(e));
      nextPage.setModification(true);
      session().addSimpleSuccessMessage("Création OK", "Le projet scientifique " + controller().projetScientifique().contratProjet().exerciceEtIndex() + " a bien été créé");
      ERXRedirect redirect = (ERXRedirect) pageWithName(ERXRedirect.class.getName());
      redirect.setComponent(nextPage);
      return redirect;

    } catch (ValidationException e) {
      session().addSimpleErrorMessage("Erreur", e);
    }
    return doNothing();
  }
  
}