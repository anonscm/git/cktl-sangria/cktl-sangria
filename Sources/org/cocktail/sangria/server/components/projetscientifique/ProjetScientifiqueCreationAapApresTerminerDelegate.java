package org.cocktail.sangria.server.components.projetscientifique;

import org.cocktail.fwkcktlrecherche.server.metier.EOAap;
import org.cocktail.fwkcktlrecherche.server.metier.EOProjetScientifique;
import org.cocktail.fwkcktlsangriaguiajax.serveur.components.commons.AFwkCktlSangriaComponent;
import org.cocktail.sangria.server.components.assistants.IAssistantActionsDelegate;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOEditingContext;

import er.extensions.eof.ERXEC;

public class ProjetScientifiqueCreationAapApresTerminerDelegate extends
		AFwkCktlSangriaComponent implements IAssistantActionsDelegate {

	public ProjetScientifiqueCreationAapApresTerminerDelegate(WOContext context) {
		super(context);
	}

	private EOProjetScientifique projetScientifique;
	private EOAap aap;
	
	public WOActionResults apresTerminer() {
		ProjetScientifiqueConsultation nextPage = (ProjetScientifiqueConsultation) pageWithName(ProjetScientifiqueConsultation.class.getName());
		EOEditingContext e = ERXEC.newEditingContext();
		nextPage.setEdc(e);
		nextPage.setProjetScientifique(projetScientifique());
		nextPage.setModification(true);
		nextPage.setOngletContratsConventionsSelected(true);
		nextPage.setOngletIdentificationSelected(false);
		session().addSimpleSuccessMessage("Création OK", "La demande de subvention " + aap().contrat().exerciceEtIndex() + " a bien été créée et a bien été ajoutée au projet");
		return nextPage;
	}

	public WOActionResults annuler() {
		ProjetScientifiqueConsultation projetScientifiqueConsultation = (ProjetScientifiqueConsultation) pageWithName(ProjetScientifiqueConsultation.class.getName());
		EOEditingContext e = ERXEC.newEditingContext();
		projetScientifiqueConsultation.setEdc(e);
		projetScientifiqueConsultation.setProjetScientifique(projetScientifique().localInstanceIn(e));
		projetScientifiqueConsultation.setOngletIdentificationSelected(false);
		projetScientifiqueConsultation.setOngletContratsConventionsSelected(true);
		projetScientifiqueConsultation.setOngletProductionScientifiqueSelected(false);
		projetScientifiqueConsultation.setConsultation(false);
		projetScientifiqueConsultation.setModification(true);
		return projetScientifiqueConsultation;		
	}

	public void setProjetScientifique(EOProjetScientifique projetScientifique) {
		this.projetScientifique = projetScientifique;
	}

	public EOProjetScientifique projetScientifique() {
		return projetScientifique;
	}

	public void setAap(EOAap aap) {
		this.aap = aap;
	}

	public EOAap aap() {
		return aap;
	}

}
