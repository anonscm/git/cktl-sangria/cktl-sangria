/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2010 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.cocktail.sangria.server.components.projetscientifique;

import org.cocktail.fwkcktlrecherche.server.metier.EOProjetScientifique;
import org.cocktail.fwkcktlrecherche.server.metier.factory.FactoryProjetScientifique;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOComponent;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSForwardException;

import er.extensions.appserver.ERXRedirect;
import er.extensions.eof.ERXEC;

// TODO: Auto-generated Javadoc
/**
 * The Class ProjetScientifiqueCreation.
 */
public class ProjetScientifiqueCreation extends ProjetScientifiqueComponent {
	
	/** The modules. */
	private NSArray modules;
	
	/** The etapes. */
	private NSArray<String> etapes;
	
	/** The projet scientifique. */
	EOProjetScientifique projetScientifique;
	
	/** The index module actif. */
	private Integer indexModuleActif = 0;
	
	
    /**
     * Instantiates a new projet scientifique creation.
     *
     * @param context the context
     */
    public ProjetScientifiqueCreation(WOContext context) {
        super(context);
    }
    
    
	/**
	 * Gets the projet scientifique.
	 *
	 * @return the projet scientifique
	 */
	public EOProjetScientifique getProjetScientifique() {
		if (projetScientifique == null) {
			try {
				projetScientifique = FactoryProjetScientifique.creerProjetScientifiqueVide(edc(), "", getUtilisateurPersId());
			} catch (Exception e) {
				throw NSForwardException._runtimeExceptionForThrowable(e);
			}
		}
		
		return projetScientifique;
	}

    
	
	/**
	 * Modules.
	 *
	 * @return the nS array
	 */
	public NSArray modules() {
			modules = new NSArray<String>(new String[]{
					"ModuleProjetScientifiqueIdentification",
					//"ModuleProjetScientifiqueProductionScientifique",
					});

		return modules;
	}
	
	
	/**
	 * Etapes.
	 *
	 * @return the nS array
	 */
	public NSArray<String> etapes() {
		etapes = new NSArray<String>(new String[]{
					"Identification",
					//"Prod. Scient."
		});
		
		return etapes;
	}

	/**
	 * Sets the index module actif.
	 *
	 * @param indexModuleActif the new index module actif
	 */
	public void setIndexModuleActif(Integer indexModuleActif) {
		this.indexModuleActif = indexModuleActif;
	}

	/**
	 * Gets the index module actif.
	 *
	 * @return the index module actif
	 */
	public Integer getIndexModuleActif() {
		return indexModuleActif;
	}
	
	/**
	 * Terminer.
	 *
	 * @return the wO action results
	 * @throws Exception the exception
	 */
	public WOActionResults terminer() {
		try {
			edc().saveChanges();
		} catch (ValidationException e) {
			throw e;
		}
		return doNothing();
	}
	
	/**
	 * Annuler.
	 *
	 * @return the wO action results
	 * @throws Exception the exception
	 */
	public WOActionResults annuler()  {
		edc().revert();
		
		return pageWithName(ProjetScientifique.class.getName());
	}
	
	public WOActionResults apresTerminer() {
		ProjetScientifiqueConsultation nextPage  = (ProjetScientifiqueConsultation) pageWithName(ProjetScientifiqueConsultation.class.getName());
		EOEditingContext e = ERXEC.newEditingContext();
		nextPage.setEdc(e);
		nextPage.setProjetScientifique(getProjetScientifique().localInstanceIn(e));
		nextPage.setModification(true);
		session().addSimpleSuccessMessage("Création OK", "Le projet scientifique " + getProjetScientifique().contratProjet().exerciceEtIndex() + " a bien été créé");
		ERXRedirect redirect = (ERXRedirect) pageWithName(ERXRedirect.class.getName());
		redirect.setComponent((WOComponent) nextPage); 
		return redirect;
	}
}