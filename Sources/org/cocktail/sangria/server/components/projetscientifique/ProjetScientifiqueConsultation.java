/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2010 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.cocktail.sangria.server.components.projetscientifique;

import org.cocktail.cocowork.server.metier.convention.factory.FactoryProjet;
import org.cocktail.fwkcktlajaxwebext.serveur.CocktailAjaxSession;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlrecherche.server.metier.EOAap;
import org.cocktail.fwkcktlrecherche.server.metier.EOContratRecherche;
import org.cocktail.fwkcktlrecherche.server.metier.EOProjetScientifique;
import org.cocktail.sangria.server.components.aap.AapCreation;
import org.cocktail.sangria.server.components.assistants.IAssistantActionsDelegate;
import org.cocktail.sangria.server.components.contratsrecherche.ContratRechercheCreation;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSForwardException;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.appserver.ERXRedirect;
import er.extensions.eof.ERXEC;

// TODO: Auto-generated Javadoc
/**
 * The Class ProjetScientifiqueConsultation.
 */
public class ProjetScientifiqueConsultation extends ProjetScientifiqueComponent {
    
	/** The projet scientifique. */
	private EOProjetScientifique projetScientifique; 

    /** The onglet identification selected. */
    private Boolean ongletIdentificationSelected = true;
    private Boolean ongletContratsConventionsSelected = false;
    private Boolean ongletProductionScientifiqueSelected = false;
    private Boolean ongletValoSelected = false;
	
    private EOContratRecherche nouveauContrat;
    private EOAap nouvelAap;
    
    public WOActionResults creerContratPourProjetAction() {
    	ContratRechercheCreation contratRechercheCreation = (ContratRechercheCreation) pageWithName(ContratRechercheCreation.class.getName());
    	EOEditingContext contratRechercheCreationEditingContext = contratRechercheCreation.edc();
    	nouveauContrat = contratRechercheCreation.contratRecherche();
    	nouveauContrat.contrat().setConObjet(projetScientifique().contratProjet().conObjet());
    	nouveauContrat.contrat().setConObjetCourt(projetScientifique().contratProjet().conObjetCourt());
		for (EOStructure labo : projetScientifique().getStructuresRechercheConcernees()) {
			EOStructure localLabo = labo.localInstanceIn(contratRechercheCreationEditingContext);
			try {
				nouveauContrat.ajouterStructureConcernee(localLabo, getUtilisateurPersId());
			} catch (Exception e) {
				session().addSimpleLocalizedErrorMessage("attention", "erreur.ajout.structurePorteuse");		
			}
			for (EOIndividu responsable : projetScientifique().getResponsablesScientifiquesForStructureRecherche(localLabo)) {
				EOIndividu localResponsable = responsable.localInstanceIn(contratRechercheCreationEditingContext);
				try {
					nouveauContrat.ajouterResponsableScientifiquePourStructureRecherche(localResponsable, localLabo);
				} catch (Exception e) {
					session().addSimpleLocalizedErrorMessage("attention", "erreur.ajout.responsableScientifique");		
				}
			}
		}
		contratRechercheCreation.setAssistantActionsDelegate(new CreationContratRecherchePourProjetAcionsDelegate());
		return contratRechercheCreation;
    }
    
    public WOActionResults creerAapPourProjetAction() {
    	AapCreation aapCreation = (AapCreation) pageWithName(AapCreation.class.getName());
    	EOEditingContext aapCreationEditingContext = aapCreation.edc();
    	nouvelAap = aapCreation.aap();
    	nouvelAap.contrat().setConObjet(projetScientifique().contratProjet().conObjet());
    	nouvelAap.contrat().setConObjetCourt(projetScientifique().contratProjet().conObjetCourt());
		for (EOStructure labo : projetScientifique().getStructuresRechercheConcernees()) {
			EOStructure localLabo = labo.localInstanceIn(aapCreationEditingContext);
			try {
				nouvelAap.ajouterStructureRechercheConcernee(localLabo);
			} catch (Exception e1) {
				session().addSimpleErrorMessage("Attention", "Une errreur est survenue lors de la suppression de la structure de recherche");
			}
			for (EOIndividu responsable : projetScientifique().getResponsablesScientifiquesForStructureRecherche(localLabo)) {
				EOIndividu localResponsable = responsable.localInstanceIn(aapCreationEditingContext);
				try {
					nouvelAap.ajouterResponsableScientifiquePourStructureRecherche(localResponsable, localLabo);
				} catch (Exception e) {
					session().addSimpleErrorMessage("Attention", "Une errreur est survenue lors de l'ajout du responsable scientifique");
				}
			}
		}
		aapCreation.setAssistantActionsDelegate(new CreationAapPourProjetAcionsDelegate());
		return aapCreation;
    }
    
	
	/**
	 * Instantiates a new projet scientifique consultation.
	 *
	 * @param context the context
	 */
	public ProjetScientifiqueConsultation(WOContext context) {
        super(context);
    }
    
	/**
	 * Projet scientifique.
	 *
	 * @return the eO projet scientifique
	 */
	public EOProjetScientifique projetScientifique() {
		return projetScientifique;
		
	}
	
	/**
	 * Sets the projet scientifique.
	 *
	 * @param projetScientifique the new projet scientifique
	 */
	public void setProjetScientifique(EOProjetScientifique projetScientifique) {
		this.projetScientifique = projetScientifique;
	}
    
	/**
	 * Retour recherche.
	 *
	 * @return the wO action results
	 */
	public WOActionResults retourRecherche() {
		edc().revert();
		CocktailAjaxSession sess = (CocktailAjaxSession) session(); 
		return sess.getSavedPageWithName(ProjetScientifiqueSearch.class.getName());
		
	}
	
	/** The consultation. */
	private Boolean consultation = false;
	
	/** The modification. */
	private Boolean modification = false;
    
	/**
	 * Onglet identification selected.
	 *
	 * @return the boolean
	 */
	public Boolean ongletIdentificationSelected() {
		return ongletIdentificationSelected;
	}
	
	/**
	 * Sets the onglet identification selected.
	 *
	 * @param ongletIdentificationSelected the new onglet identification selected
	 */
	public void setOngletIdentificationSelected(Boolean ongletIdentificationSelected) {
		this.ongletIdentificationSelected = ongletIdentificationSelected;
	}
	
    // INDENTIFIANTS DES COMPOSANTS GRAPHIQUES
    
    public void setOngletProductionScientifiqueSelected(
			Boolean ongletProductionScientifiqueSelected) {
		this.ongletProductionScientifiqueSelected = ongletProductionScientifiqueSelected;
	}

	public Boolean getOngletProductionScientifiqueSelected() {
		return ongletProductionScientifiqueSelected;
	}

	/**
     * Onglets container id.
     *
     * @return the string
     */
    public String ongletsContainerId() {
    	return getComponentId() + "_ongletsContainer";
    }
    
    /**
     * Onglet identification id.
     *
     * @return the string
     */
    public String ongletIdentificationId() {
    	return getComponentId() + "_ongletIdentification";
    }
    
    public String ongletContratsConventionsId() {
    	return getComponentId() + "_ongletContratsConventions";
    }
    
    public String ongletProductionScientifiqueId() {
    	return getComponentId() + "_ongletProductionScientifique";
    }
    
    public String ongletValoId() {
    	return getComponentId() + "_ongletValo";
    }
    
    /**
     * Consultation.
     *
     * @return the boolean
     */
    public Boolean consultation() {
    	return consultation;
    }
    
    /**
     * Sets the consultation.
     *
     * @param consultation the new consultation
     */
    public void setConsultation(Boolean consultation) {
    	this.consultation = consultation;
    	
    	
    }
    
    /**
     * Modification.
     *
     * @return the boolean
     */
    public Boolean modification() {
    	if (consultation()) {
    		return false;
    	} else {
    		return modification;
    	}
    }
    
    /**
     * Sets the modification.
     *
     * @param modification the new modification
     */
    public void setModification(Boolean modification) {
    	this.modification = modification;
    }
    
	
	/**
	 * Enregistrer les modications.
	 *
	 * @return the wO action results
	 */
	public WOActionResults enregistrerLesModications() {
		
		try {
			projetScientifique().setDModification(new NSTimestamp());
			projetScientifique().contratProjet().setShouldNotValidate(true);
			edc().saveChanges();
			mySession().addSimpleSuccessMessage("Enregistrement OK", "Les modifications apportées au projet scientifique ont bien été enregistrées");
		} catch (ValidationException e) {
			mySession().addSimpleErrorMessage("Erreur", e);
		}
		return doNothing();
	}



	/**
	 * Supprimer contrat recherche.
	 *
	 * @return the wO action results
	 */
	public WOActionResults supprimerProjetScientifique() {
		

		try {
			projetScientifique().setProjetSupprime(true);
			edc().saveChanges();		
			//NSNotificationCenter.defaultCenter().postNotification(NOTIFICATION_aapSupprime, this);
		} catch (ValidationException e) {
			session().addSimpleErrorMessage("Erreur", "Une erreur est survenue lors de la suppression du contrat de recherche");
			return doNothing();
		}
		
		ERXRedirect redirect = (ERXRedirect) pageWithName(ERXRedirect.class.getName());
		redirect.setComponent(session().getSavedPageWithName(ProjetScientifiqueSearch.class.getName())); 
		return redirect;
		
		
	}

	/**
	 * Apres supprimer.
	 *
	 * @return the wO action results
	 */
	public WOActionResults apresSupprimer() {
		ConfirmationSuppressionProjetScientifique nextPage = (ConfirmationSuppressionProjetScientifique) pageWithName(ConfirmationSuppressionProjetScientifique.class.getName());
		return nextPage;
	}

	public void setOngletContratsConventionsSelected(
			Boolean ongletContratsConventionsSelected) {
		this.ongletContratsConventionsSelected = ongletContratsConventionsSelected;
	}

	public Boolean ongletContratsConventionsSelected() {
		return ongletContratsConventionsSelected;
	}

	public void setOngletValoSelected(Boolean ongletValoSelected) {
		this.ongletValoSelected = ongletValoSelected;
	}

	public Boolean ongletValoSelected() {
		return ongletValoSelected;
	}
	
    
	class CreationContratRecherchePourProjetAcionsDelegate implements IAssistantActionsDelegate {

		public WOActionResults apresTerminer() {
			
			EOEditingContext ajoutContratEditingContext = ERXEC.newEditingContext();
			FactoryProjet f = new FactoryProjet(ajoutContratEditingContext);
			EOProjetScientifique _projetScientifique = projetScientifique().localInstanceIn(ajoutContratEditingContext);
			EOContratRecherche _nouveauContrat = nouveauContrat.localInstanceIn(ajoutContratEditingContext);
			try {
				f.ajouterContrat(_projetScientifique.projet(), _nouveauContrat.contrat());
				ajoutContratEditingContext.saveChanges();
				nouveauContrat = null;
				session().addSimpleSuccessMessage("OK", "Le contrat de recherche été créé et a été ajouté au projet");
			} catch (ValidationException e) {
				session().addSimpleErrorMessage("Erreur", e);
				return doNothing();
			} catch (Exception e) {
				throw NSForwardException._runtimeExceptionForThrowable(e);
			}

			
			ProjetScientifiqueConsultation nextPage = (ProjetScientifiqueConsultation) pageWithName(ProjetScientifiqueConsultation.class.getName());
			EOEditingContext e = ERXEC.newEditingContext();
			nextPage.setEdc(e);
			nextPage.setProjetScientifique(projetScientifique());
			nextPage.setModification(true);
			nextPage.setOngletContratsConventionsSelected(true);
			nextPage.setOngletIdentificationSelected(false);
			

			return nextPage;
		}

		public WOActionResults annuler() {
			ProjetScientifiqueConsultation nextPage = (ProjetScientifiqueConsultation) pageWithName(ProjetScientifiqueConsultation.class.getName());
			EOEditingContext e = ERXEC.newEditingContext();
			nextPage.setEdc(e);
			nextPage.setProjetScientifique(projetScientifique());
			nextPage.setModification(true);
			nextPage.setOngletContratsConventionsSelected(true);
			nextPage.setOngletIdentificationSelected(false);
			session().addSimpleInfoMessage("Annulation", "La création du contrat a été annulée");
			
			nouveauContrat = null;
			
			return nextPage;
		}
		
	}
	
	class CreationAapPourProjetAcionsDelegate implements IAssistantActionsDelegate {

		public WOActionResults apresTerminer() {
			
			EOEditingContext ajoutAapEditingContext = ERXEC.newEditingContext();
			FactoryProjet f = new FactoryProjet(ajoutAapEditingContext);
			EOProjetScientifique _projetScientifique = projetScientifique().localInstanceIn(ajoutAapEditingContext);
			EOAap _nouvelAap = nouvelAap.localInstanceIn(ajoutAapEditingContext);
			try {
				f.ajouterContrat(_projetScientifique.projet(), _nouvelAap.contrat());
				ajoutAapEditingContext.saveChanges();
				nouveauContrat = null;
				session().addSimpleSuccessMessage("OK", "L'appel à projet été créé et a été ajouté au projet");
			} catch (ValidationException e) {
				session().addSimpleErrorMessage("Erreur", e);
			} catch (Exception e) {
				throw NSForwardException._runtimeExceptionForThrowable(e);
			}

			
			ProjetScientifiqueConsultation nextPage = (ProjetScientifiqueConsultation) pageWithName(ProjetScientifiqueConsultation.class.getName());
			EOEditingContext e = ERXEC.newEditingContext();
			nextPage.setEdc(e);
			nextPage.setProjetScientifique(projetScientifique());
			nextPage.setModification(true);
			nextPage.setOngletContratsConventionsSelected(true);
			nextPage.setOngletIdentificationSelected(false);
			

			return nextPage;
		}

		public WOActionResults annuler() {
			ProjetScientifiqueConsultation nextPage = (ProjetScientifiqueConsultation) pageWithName(ProjetScientifiqueConsultation.class.getName());
			EOEditingContext e = ERXEC.newEditingContext();
			nextPage.setEdc(e);
			nextPage.setProjetScientifique(projetScientifique());
			nextPage.setModification(true);
			nextPage.setOngletContratsConventionsSelected(true);
			nextPage.setOngletIdentificationSelected(false);
			session().addSimpleInfoMessage("Annulation", "La création de l'appel à projet a été annulée");
			
			nouveauContrat = null;
			
			return nextPage;
		}
		
	}
	
}