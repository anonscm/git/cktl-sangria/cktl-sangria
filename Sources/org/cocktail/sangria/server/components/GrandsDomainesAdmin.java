package org.cocktail.sangria.server.components;

import org.cocktail.fwkcktlajaxwebext.serveur.component.CktlAjaxWindow;
import org.cocktail.fwkcktlpersonne.common.metier.EODomaineScientifique;
import org.cocktail.fwkcktlrecherche.server.metier.EOGrandSecteurDisciplinaire;
import org.cocktail.fwkcktlrecherche.server.metier.EORepartGrandSecteurDisciplinaireDomaineScientifique;
import org.cocktail.sangria.server.components.commons.ASangriaComponent;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WODisplayGroup;
import com.webobjects.eocontrol.EOArrayDataSource;
import com.webobjects.eocontrol.EOClassDescription;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;

import er.ajax.AjaxUpdateContainer;
import er.extensions.appserver.ERXDisplayGroup;
import er.extensions.eof.ERXEC;
import er.extensions.foundation.ERXArrayUtilities;
import er.extensions.foundation.ERXStringUtilities;

public class GrandsDomainesAdmin extends ASangriaComponent {

	private static final long serialVersionUID = 6607941056383940647L;

	private EOEditingContext edc = null;

	private NSArray<EOGrandSecteurDisciplinaire> grandsSecteursDisciplinaires = null;
	private EOArrayDataSource grandsDomainesDataSource = null;
	private ERXDisplayGroup<EOGrandSecteurDisciplinaire> grandsDomainesDisplayGroup = null;

	private NSArray<EODomaineScientifique> domainesScientifiques = null;
	private EOArrayDataSource domainesScientifiquesDataSource = null;
	private ERXDisplayGroup<EODomaineScientifique> domainesScientifiquesDisplayGroup = null;

	private EOGrandSecteurDisciplinaire selectedGrandSecteurDisciplinaire;
	private EOGrandSecteurDisciplinaire currentGrandSecteurDisciplinaire;

	private EODomaineScientifique selectedDomaineScientifique;
	private EODomaineScientifique currentDomaineScientifique;
	private EODomaineScientifique domaineScientifiqueAAjouter;
	
	private String editingGrandSecteurDisplinaireLibelle;

	
	
	public GrandsDomainesAdmin(WOContext context) {
		super(context);
	}

	@Override
	public String updateContainerID() {
		return getComponentId() + "_updateContainer";
	}

	public String getGrandsDomainesTableViewId() {
		return getComponentId() + "_grandsDomainesTableView";
	}

	public String domainesScientifiquesTableViewId() {
		return getComponentId() + "_domainesScientifiquesTableView";
	}

	public String nouveauGrandSecteurDisplinaireWindowId() {
		return getComponentId() + "_nouveauGrandSecteurDisplinaireWindow";
	}

	public String editionGrandSecteurDisplinaireWindowId() {
		return getComponentId() + "_editionGrandSecteurDisplinaireWindow";
	}

	public String ajouterDomaineScientifiqueWindowId() {
		return getComponentId() + "_ajouterDomaineScientifiqueWindow";
	}

	public String deplacerDomaineScientifiqueWindowId() {
		return getComponentId() + "_deplacerDomaineScientifiqueWindow";
	}

	@Override
	public EOEditingContext edc() {
		if (edc == null) {
			edc = ERXEC.newEditingContext();
		}
		return edc;
	}

	public NSArray<EOGrandSecteurDisciplinaire> grandsSecteursDisciplinaires() {
		if (grandsSecteursDisciplinaires == null) {
			grandsSecteursDisciplinaires = EOGrandSecteurDisciplinaire.fetchAll(edc());
		}
		return grandsSecteursDisciplinaires;
	}

	public EOArrayDataSource grandsDomainesDataSource() {
		if (grandsDomainesDataSource == null) {
			EOClassDescription classDescription = EOClassDescription.classDescriptionForEntityName(EOGrandSecteurDisciplinaire.ENTITY_NAME);
			grandsDomainesDataSource = new EOArrayDataSource(classDescription, edc());
			grandsDomainesDataSource.setArray(grandsSecteursDisciplinaires());
		}
		return grandsDomainesDataSource;
	}

	public ERXDisplayGroup<EOGrandSecteurDisciplinaire> grandsDomainesDisplayGroup() {
		if (grandsDomainesDisplayGroup == null) {
			grandsDomainesDisplayGroup = new ERXDisplayGroup<EOGrandSecteurDisciplinaire>();
			grandsDomainesDisplayGroup.setDataSource(grandsDomainesDataSource());
			grandsDomainesDisplayGroup.setSelectsFirstObjectAfterFetch(true);
			grandsDomainesDisplayGroup.setSortOrderings(EOGrandSecteurDisciplinaire.GSD_LIBELLE.ascs());
			grandsDomainesDisplayGroup.setDelegate(new GrandsDomainesDisplayGroupDelegate());
			grandsDomainesDisplayGroup.fetch();
		}
		return grandsDomainesDisplayGroup;
	}

	public EOGrandSecteurDisciplinaire selectedGrandSecteurDisciplinaire() {
		return selectedGrandSecteurDisciplinaire;
	}

	public void setSelectedGrandSecteurDisciplinaire(EOGrandSecteurDisciplinaire selectedGrandSecteurDisciplinaire) {
		this.selectedGrandSecteurDisciplinaire = selectedGrandSecteurDisciplinaire;
	}

	public EOGrandSecteurDisciplinaire currentGrandSecteurDisciplinaire() {
		return currentGrandSecteurDisciplinaire;
	}

	public void setCurrentGrandSecteurDisciplinaire(EOGrandSecteurDisciplinaire currentGrandSecteurDisciplinaire) {
		this.currentGrandSecteurDisciplinaire = currentGrandSecteurDisciplinaire;
	}

	public NSArray<EODomaineScientifique> domainesScientifiques() {
		if (domainesScientifiques == null && selectedGrandSecteurDisciplinaire() != null) {
			domainesScientifiques = selectedGrandSecteurDisciplinaire().domainesScientifiques();
		}
		return domainesScientifiques;
	}

	public EOArrayDataSource domainesScientifiquesDataSource() {
		if (domainesScientifiquesDataSource == null) {
			EOClassDescription classDescription = EOClassDescription.classDescriptionForEntityName(EODomaineScientifique.ENTITY_NAME);
			domainesScientifiquesDataSource = new EOArrayDataSource(classDescription, edc());
			domainesScientifiquesDataSource.setArray(domainesScientifiques());
		}
		return domainesScientifiquesDataSource;
	}

	public ERXDisplayGroup<EODomaineScientifique> domainesScientifiquesDisplayGroup() {
		if (domainesScientifiquesDisplayGroup == null) {
			domainesScientifiquesDisplayGroup = new ERXDisplayGroup<EODomaineScientifique>();
			domainesScientifiquesDisplayGroup.setDataSource(domainesScientifiquesDataSource());
			domainesScientifiquesDisplayGroup.setSelectsFirstObjectAfterFetch(true);
			domainesScientifiquesDisplayGroup.setDelegate(new DomainesScientifiquesDisplayGroupDelegate());
			domainesScientifiquesDisplayGroup.setSortOrderings(EODomaineScientifique.DS_CODE.ascs());
			domainesScientifiquesDisplayGroup.fetch();
		}
		return domainesScientifiquesDisplayGroup;
	}

	public EODomaineScientifique selectedDomaineScientifique() {
		return selectedDomaineScientifique;
	}

	public void setSelectedDomaineScientifique(EODomaineScientifique selectedDomaineScientifique) {
		this.selectedDomaineScientifique = selectedDomaineScientifique;
	}

	public EODomaineScientifique getDomaineScientifiqueAAjouter() {
		return domaineScientifiqueAAjouter;
	}

	public void setDomaineScientifiqueAAjouter(EODomaineScientifique domaineScientifiqueAAjouter) {
		this.domaineScientifiqueAAjouter = domaineScientifiqueAAjouter;
		ajouterDomaineScientifique();
	}

	public EODomaineScientifique currentDomaineScientifique() {
		return currentDomaineScientifique;
	}

	public void setCurrentDomaineScientifique(EODomaineScientifique currentDomaineScientifique) {
		this.currentDomaineScientifique = currentDomaineScientifique;
	}

	public WOActionResults ajouterGrandSecteurDisplinaire() {
		setEditingGrandSecteurDisplinaireLibelle(null);
		return doNothing();
	}

	public WOActionResults validerAjouterGrandSecteurDisplinaire() {
		if (ERXStringUtilities.stringIsNullOrEmpty(editingGrandSecteurDisplinaireLibelle())) {
			session().addSimpleErrorMessage("Erreur", "Vous devez spécifier un libéllé pour le nouveau grand secteur.");
			return doNothing();
		}
		EOGrandSecteurDisciplinaire nouveauGrandSecteurDisciplinaire = EOGrandSecteurDisciplinaire.creerInstance(edc());
		nouveauGrandSecteurDisciplinaire.setGsdLibelle(editingGrandSecteurDisplinaireLibelle());
		try {
			edc().saveChanges();

			/** RESET */
			grandsSecteursDisciplinaires = null;
			grandsDomainesDataSource = null;
			grandsDomainesDisplayGroup = null;

			CktlAjaxWindow.close(context(), nouveauGrandSecteurDisplinaireWindowId());
			AjaxUpdateContainer.updateContainerWithID(updateContainerID(), context());

		} catch (ValidationException e) {
			session().addSimpleErrorMessage("Erreur", e);
			edc().revert();
		}

		return doNothing();

	}

	public WOActionResults annulerAjouterGrandSecteurDisplinaire() {
		edc().revert();
		CktlAjaxWindow.close(context(), nouveauGrandSecteurDisplinaireWindowId());

		return doNothing();
	}

	public String editingGrandSecteurDisplinaireLibelle() {
		return editingGrandSecteurDisplinaireLibelle;
	}

	public void setEditingGrandSecteurDisplinaireLibelle(String editingGrandSecteurDisplinaireLibelle) {
		this.editingGrandSecteurDisplinaireLibelle = editingGrandSecteurDisplinaireLibelle;
	}

	public WOActionResults editerGrandSecteurDisplinaire() {
		setEditingGrandSecteurDisplinaireLibelle(selectedGrandSecteurDisciplinaire().gsdLibelle());
		return doNothing();
	}

	public WOActionResults validerEditerGrandSecteurDisplinaire() {
		if (ERXStringUtilities.stringIsNullOrEmpty(editingGrandSecteurDisplinaireLibelle())) {
			session().addSimpleErrorMessage("Erreur", "Vous devez spécifier un libéllé pour le grand secteur.");
			return doNothing();
		}
		selectedGrandSecteurDisciplinaire.setGsdLibelle(editingGrandSecteurDisplinaireLibelle());
		try {
			edc().saveChanges();

			CktlAjaxWindow.close(context(), editionGrandSecteurDisplinaireWindowId());
			AjaxUpdateContainer.updateContainerWithID(updateContainerID(), context());

		} catch (ValidationException e) {
			session().addSimpleErrorMessage("Erreur", e);
			edc().revert();
		}
		return doNothing();
	}

	public WOActionResults annulerEditerGrandSecteurDisplinaire() {
		edc().revert();
		CktlAjaxWindow.close(context(), editionGrandSecteurDisplinaireWindowId());
		return doNothing();
	}

	public WOActionResults supprimerGrandSecteurDisplinaire() {
		edc().deleteObject(selectedGrandSecteurDisciplinaire());
		try {
			edc().saveChanges();
		} catch (ValidationException e) {
			session().addSimpleErrorMessage("Erreur", e);
			edc().revert();
		}
		return doNothing();
	}

	public WOActionResults deplacerDomaineScientifique() {
		EORepartGrandSecteurDisciplinaireDomaineScientifique repartGrandSecteurDisciplinaireDomaineScientifique = EORepartGrandSecteurDisciplinaireDomaineScientifique.fetchFirstByQualifier(edc(),
				EORepartGrandSecteurDisciplinaireDomaineScientifique.DOMAINE_SCIENTIFIQUE.eq(selectedDomaineScientifique()).and(EORepartGrandSecteurDisciplinaireDomaineScientifique.GRAND_SECTEUR_DISCIPLINAIRE.eq(selectedGrandSecteurDisciplinaire())));
		edc().deleteObject(repartGrandSecteurDisciplinaireDomaineScientifique);
		EORepartGrandSecteurDisciplinaireDomaineScientifique nouveauRepartGrandSecteurDisciplinaireDomaineScientifique = EORepartGrandSecteurDisciplinaireDomaineScientifique.creerInstance(edc());
		nouveauRepartGrandSecteurDisciplinaireDomaineScientifique.setDomaineScientifiqueRelationship(selectedDomaineScientifique());
		nouveauRepartGrandSecteurDisciplinaireDomaineScientifique.setGrandSecteurDisciplinaireRelationship(currentGrandSecteurDisciplinaire());
		try {
			edc().saveChanges();
			domainesScientifiques = null;
			domainesScientifiquesDataSource = null;
			domainesScientifiquesDisplayGroup = null;
			AjaxUpdateContainer.updateContainerWithID(updateContainerID(), context());
			CktlAjaxWindow.close(context(), deplacerDomaineScientifiqueWindowId());
		} catch (ValidationException e) {
			edc().revert();
			session().addSimpleErrorMessage("Erreur", e);
		}
		return doNothing();
	}

	public WOActionResults ajouterDomaineScientifique() {
		if(getDomaineScientifiqueAAjouter() == null) {
			return doNothing();
		}
		NSArray<EORepartGrandSecteurDisciplinaireDomaineScientifique> repartsExistants = EORepartGrandSecteurDisciplinaireDomaineScientifique.fetchAll(edc(), EORepartGrandSecteurDisciplinaireDomaineScientifique.DOMAINE_SCIENTIFIQUE.eq(getDomaineScientifiqueAAjouter()));
		if(repartsExistants.count() != 0) {
			session().addSimpleErrorMessage("Le domaine scientifique est déjà utlisé");
			AjaxUpdateContainer.updateContainerWithID(updateContainerID(), context());
			return doNothing();
		}
		EORepartGrandSecteurDisciplinaireDomaineScientifique nouveauRepartGrandSecteurDisciplinaireDomaineScientifique = EORepartGrandSecteurDisciplinaireDomaineScientifique.creerInstance(edc());
		nouveauRepartGrandSecteurDisciplinaireDomaineScientifique.setDomaineScientifiqueRelationship(getDomaineScientifiqueAAjouter());
		nouveauRepartGrandSecteurDisciplinaireDomaineScientifique.setGrandSecteurDisciplinaireRelationship(selectedGrandSecteurDisciplinaire());
		try {
			edc().saveChanges();
			domainesScientifiques = null;
			domainesScientifiquesDataSource = null;
			domainesScientifiquesDisplayGroup = null;
			setDomaineScientifiqueAAjouter(null);
			// CktlAjaxWindow.close(context(), ajouterDomaineScientifiqueWindowId());
		} catch (ValidationException e) {
			edc().revert();
			session().addSimpleErrorMessage("Erreur", e);
		}			
		AjaxUpdateContainer.updateContainerWithID(updateContainerID(), context());
		return doNothing();
	}

	public WOActionResults supprimerDomaineScientifique() {
		EORepartGrandSecteurDisciplinaireDomaineScientifique repartGrandSecteurDisciplinaireDomaineScientifique = 
				EORepartGrandSecteurDisciplinaireDomaineScientifique.fetchFirstByQualifier(
						edc(),
						EORepartGrandSecteurDisciplinaireDomaineScientifique.DOMAINE_SCIENTIFIQUE.eq(selectedDomaineScientifique())
						.and(EORepartGrandSecteurDisciplinaireDomaineScientifique.GRAND_SECTEUR_DISCIPLINAIRE.eq(selectedGrandSecteurDisciplinaire()
					)
				)
			);
		
		edc().deleteObject(repartGrandSecteurDisciplinaireDomaineScientifique);
		
		try {
			edc().saveChanges();
			domainesScientifiques = null;
			domainesScientifiquesDataSource = null;
			domainesScientifiquesDisplayGroup = null;
			AjaxUpdateContainer.updateContainerWithID(updateContainerID(), context());
		} catch (ValidationException e) {
			edc().revert();
			session().addSimpleErrorMessage("Erreur", e);
		}
		return doNothing();
	}

	public NSArray<EOGrandSecteurDisciplinaire> grandsSecteursPossibles() {
		return ERXArrayUtilities.arrayMinusObject(grandsSecteursDisciplinaires(), selectedGrandSecteurDisciplinaire());
	}

	public NSArray<EODomaineScientifique> domainesScientifiquesPossibles() {
		return ERXArrayUtilities.arrayMinusArray(EODomaineScientifique.fetchAll(edc()),
				(NSArray<EODomaineScientifique>) EORepartGrandSecteurDisciplinaireDomaineScientifique.fetchAll(edc()).valueForKey(EORepartGrandSecteurDisciplinaireDomaineScientifique.DOMAINE_SCIENTIFIQUE_KEY));
	}

	public class GrandsDomainesDisplayGroupDelegate {
		public void displayGroupDidChangeSelection(WODisplayGroup group) {
			ERXDisplayGroup<EOGrandSecteurDisciplinaire> grandsDomainesDisplayGroup = (ERXDisplayGroup<EOGrandSecteurDisciplinaire>) group;
			if (grandsDomainesDisplayGroup.selectedObject() != null) {
				setSelectedGrandSecteurDisciplinaire(grandsDomainesDisplayGroup.selectedObject());
				domainesScientifiques = null;
				domainesScientifiquesDataSource = null;
				domainesScientifiquesDisplayGroup = null;
			}
		}
	}

	public class DomainesScientifiquesDisplayGroupDelegate {
		public void displayGroupDidChangeSelection(WODisplayGroup group) {
			ERXDisplayGroup<EODomaineScientifique> domainesScientifiquesDisplayGroup = (ERXDisplayGroup<EODomaineScientifique>) group;
			if (domainesScientifiquesDisplayGroup.selectedObject() != null) {
				setSelectedDomaineScientifique(domainesScientifiquesDisplayGroup.selectedObject());
			}
		}
	}

}