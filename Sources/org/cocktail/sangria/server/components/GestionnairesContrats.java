package org.cocktail.sangria.server.components;

import org.cocktail.fwkcktlrecherche.server.ParametresRecherche;

import com.webobjects.appserver.WOContext;

/**
 * Gestion des gestionnaires de contrats 
 */
public class GestionnairesContrats extends GestionPersonnesGroupe {

	private static final long serialVersionUID = -2447650729229310508L;

	/**
	 * @param context le context
	 */
	public GestionnairesContrats(WOContext context) {
        super(context);
		if (getGestionPersonnesGroupeController() != null) {
			getGestionPersonnesGroupeController().setAfficheStructures(false);
		}
    }

	@Override
	protected String getGroupeParamKey() {
		return ParametresRecherche.GROUPE_GESTIONNAIRES_CONTRATS_KEY;
	}

	@Override
	public String getEchecRecuperationGroupeMessage() {
		return session().localizer().localizedStringForKey("echecRecuperationGestionnairesContrat");
	}
}