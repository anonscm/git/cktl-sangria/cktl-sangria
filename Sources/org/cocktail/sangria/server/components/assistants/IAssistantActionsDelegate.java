package org.cocktail.sangria.server.components.assistants;

import com.webobjects.appserver.WOActionResults;

public interface IAssistantActionsDelegate {
	public WOActionResults apresTerminer();
	public WOActionResults annuler();
}
