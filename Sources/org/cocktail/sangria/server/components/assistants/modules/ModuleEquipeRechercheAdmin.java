package org.cocktail.sangria.server.components.assistants.modules;

import com.webobjects.appserver.WOContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

import org.cocktail.fwkcktldroitsutils.common.util.MyStringCtrl;
import org.cocktail.fwkcktlsangriaguiajax.serveur.components.commons.ModuleEquipeRecherche;

public class ModuleEquipeRechercheAdmin extends ModuleEquipeRecherche {
	
	
    public ModuleEquipeRechercheAdmin(WOContext context) {
        super(context);
    }

	@Override
	public boolean valider() {

		Boolean hasError = false;
		
		if(equipeRecherche().llStructure() == null) {
			session().addSimpleErrorMessage("Erreur", "Le nom de l'équipe de recherche doit être renseigné");
			hasError = true;					
		}
		if(equipeRecherche().lcStructure() == null) {
			session().addSimpleErrorMessage("Erreur", "L'acronyme de l'équipe de recherche doit être renseigné");
			hasError = true;
		}		

		return !hasError;
	}
	
	@Override
	public boolean isTerminerDisabled() {
		return true;
	}
	
}
