/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2010 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.cocktail.sangria.server.components.assistants.modules;

import org.cocktail.fwkcktldroitsutils.common.util.MyDateCtrl;
import org.cocktail.fwkcktldroitsutils.common.util.MyStringCtrl;
import org.cocktail.fwkcktlsangriaguiajax.serveur.components.commons.ModuleAap;

import com.webobjects.appserver.WOContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

// TODO: Auto-generated Javadoc
/**
 * The Class ModuleAapIdentification.
 */
public class ModuleAapIdentification extends ModuleAap {
    
    /**
     * Instantiates a new module aap identification.
     *
     * @param context the context
     */
    public ModuleAapIdentification(WOContext context) {
        super(context);
    }



	/* (non-Javadoc)
	 * @see org.cocktail.fwkcktlrechercheguiajax.serveur.components.commons.ModuleFwkCktlRecherche#valider()
	 */
	@Override
	public boolean valider() {
		
		Boolean hasError = false;
		
		if(aap().contrat().exerciceCocktail().exeExercice() == null) {
			session().addSimpleErrorMessage("Erreur", "L'année doit être renseignée");
			hasError = true;					
		}
		if(MyStringCtrl.isEmpty(aap().objet())) {
			session().addSimpleErrorMessage("Erreur", "L'objet est obligatoire");
			hasError = true;
		}		
		if(aap().getStructuresRecherchesConcernees().count() == 0) {
			session().addSimpleErrorMessage("Erreur", "Il doit y avoir au moins un laboratoire concerné");
			hasError = true;
		}
		/*
		if(aap().responsables().count() == 0) {
			session().addSimpleErrorMessage("Erreur", "Il doit y avoir au moins un responsable scientifique");
			hasError = true;
		}
		*/		
		
		return !hasError;
		
	}
    

	@Override
	public boolean isTerminerDisabled() {
		return true;
	}
    
    
}