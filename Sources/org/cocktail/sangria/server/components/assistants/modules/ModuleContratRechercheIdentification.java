/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2010 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.cocktail.sangria.server.components.assistants.modules;

import org.cocktail.fwkcktldroitsutils.common.util.MyStringCtrl;
import org.cocktail.fwkcktlsangriaguiajax.serveur.components.commons.ModuleContratRecherche;

import com.webobjects.appserver.WOContext;

public class ModuleContratRechercheIdentification extends ModuleContratRecherche {
    
    /**
     * Instantiates a new module contrat recherche identification.
     *
     * @param context the context
     */
    public ModuleContratRechercheIdentification(WOContext context) {
        super(context);
    }

	/* (non-Javadoc)
	 * @see org.cocktail.fwkcktlrechercheguiajax.serveur.components.commons.ModuleFwkCktlRecherche#valider()
	 */
	@Override
	public boolean valider() {
		
		Boolean hasError = false;
		
		if(contratRecherche().contrat().exerciceCocktail().exeExercice() == null) {
			session().addSimpleErrorMessage("Erreur", "L'année doit être renseignée");
			hasError = true;					
		}
		if(MyStringCtrl.isEmpty(contratRecherche().contrat().conObjet())) {
			session().addSimpleErrorMessage("Erreur","L'objet est obligatoire");
			hasError = true;
		}	
		/*
		if(contratRecherche().contrat().avenantZero().avtDateDeb() == null) {
			messages.add("La date de début est obligatoire");
			hasError = true;
		}
		if(contratRecherche().contrat().avenantZero().avtDateFin() == null) {
			messages.add("La date de fin est obligatoire");
			hasError = true;
		}
		*/
		if(contratRecherche().structureRechercheConcernees().count() == 0) {
			session().addSimpleErrorMessage("Erreur","Il doit y avoir au moins un laboratoire concerné");
			hasError = true;
		}
		
		if(contratRecherche().partenaires().count() == 0) {
			session().addSimpleErrorMessage("Erreur", "Il doit y avoir au moins un partenaire");
		}
		
		if(contratRecherche().contrat().typeContrat() == null) {
			session().addSimpleErrorMessage("Erreur","Le type de contrat droit être renseigné");
			hasError = true;
		}
		/*
		if(contratRecherche().getResponsablesScientifiques().count() == 0) {
			messages.add("Il doit y avoir au moins un responsable scientifique");
			hasError = true;
		}	
		*/	
		/*
		if(contratRecherche().getFinanceur() == null) {
			messages.add("Le financeur est obligatoire");
			hasError = true;
		}
		*/
		
		
		return !hasError;
	}
    
	/* (non-Javadoc)
	 * @see org.cocktail.fwkcktlrechercheguiajax.serveur.components.commons.ModuleFwkCktlRecherche#isTerminerDisabled()
	 */
	@Override
	public boolean isTerminerDisabled() {
		return true;
	}
}