package org.cocktail.sangria.server.components.assistants.modules;

import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

import er.extensions.eof.ERXFetchSpecification;
import er.extensions.eof.ERXQ;

import org.cocktail.fwkcktlpersonne.common.metier.EOAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartAssociation;
import org.cocktail.fwkcktlrecherche.server.metier.factory.FactoryAssociation;
import org.cocktail.fwkcktlsangriaguiajax.serveur.components.commons.ModuleEquipeRecherche;

public class ModuleEquipeRechercheRattachementUnite extends ModuleEquipeRecherche {
	
    public ModuleEquipeRechercheRattachementUnite(WOContext context) {
        super(context);
    }

	@Override
	public boolean valider() {
		
		Boolean hasError = false;
		
		EOAssociation equipeDeRechercheAssociation = FactoryAssociation.shared().equipeDeRechercheAssociation(edc());
		
		EOQualifier qualifier = ERXQ.and(
				ERXQ.equals(EORepartAssociation.TO_ASSOCIATION_KEY, equipeDeRechercheAssociation),
				ERXQ.equals(EORepartAssociation.PERS_ID_KEY, equipeRecherche().persId())
				);
		
		ERXFetchSpecification<EORepartAssociation> fetchSpecification = new ERXFetchSpecification<EORepartAssociation>(EORepartAssociation.ENTITY_NAME, qualifier, null);
		fetchSpecification.setIncludeEditingContextChanges(true);
		NSArray<EORepartAssociation> repartAssociations = fetchSpecification.fetchObjects(edc());
		
		if(repartAssociations == null) {
			session().addSimpleErrorMessage("Erreur", "Vous devez choisir une unité de rattachement");
			hasError=true;
		}
		else if(repartAssociations.isEmpty()) {
			hasError=true;
		}

		return !hasError;
	}
	
}