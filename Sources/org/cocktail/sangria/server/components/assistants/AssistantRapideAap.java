package org.cocktail.sangria.server.components.assistants;

import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSForwardException;

import er.extensions.eof.ERXEC;

import org.cocktail.cocowork.common.exception.ExceptionUtilisateur;
import org.cocktail.fwkcktlrecherche.server.metier.EOAap;
import org.cocktail.fwkcktlrecherche.server.metier.factory.FactoryAap;
import org.cocktail.fwkcktlsangriaguiajax.serveur.components.commons.AFwkCktlSangriaComponent;
import org.cocktail.sangria.server.components.commons.ASangriaComponent;

public class AssistantRapideAap extends ASangriaComponent {
    
	private EOEditingContext creationAapEdc= null;
	
	private EOAap nouvelAap = null;
	
	public AssistantRapideAap(WOContext context) {
        super(context);
    }
    
	
	public EOEditingContext creationAapEdc() {
		if(creationAapEdc == null) {
			creationAapEdc = ERXEC.newEditingContext(edc());
		}
		return creationAapEdc;
	}
	
	
	public EOAap nouvelAap() {
		
		FactoryAap factoryAap = new FactoryAap(edc(), false);
		
		if(nouvelAap == null) {
			try {
				nouvelAap = factoryAap.creerAapVide("", getUtilisateurPersId());
			} catch (Exception e) {
				throw NSForwardException._runtimeExceptionForThrowable(e);
			}
		}
		
		return nouvelAap;
	}



	
	
    
    
    
    
}