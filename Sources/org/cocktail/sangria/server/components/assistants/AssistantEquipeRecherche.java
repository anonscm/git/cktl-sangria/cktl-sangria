package org.cocktail.sangria.server.components.assistants;

import com.webobjects.appserver.WOContext;
import com.webobjects.foundation.NSArray;

import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlsangriaguiajax.serveur.components.commons.Assistant;

public class AssistantEquipeRecherche extends Assistant {
	
	public final static String BINDING_equipeRecherche = "equipeRecherche";
	
    public AssistantEquipeRecherche(WOContext context) {
        super(context);
    }
    
    public EOStructure equipeRecherche() {
    	return (EOStructure) valueForBinding(BINDING_equipeRecherche);
    }
    
}