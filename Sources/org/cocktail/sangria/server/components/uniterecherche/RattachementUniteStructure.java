package org.cocktail.sangria.server.components.uniterecherche;

import org.cocktail.fwkcktldroitsutils.common.util.MyDateCtrl;
import org.cocktail.fwkcktlpersonne.common.metier.EODomaineScientifique;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypeGroupe;
import org.cocktail.fwkcktlrecherche.server.metier.EORepartStructureRechercheDomaineScientifique;
import org.cocktail.sangria.server.components.commons.ASangriaComponent;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.eof.ERXQ;

public class RattachementUniteStructure extends ASangriaComponent {
	
	private EOStructure selectedUniteDeRecherche;
	
	private EODomaineScientifique currentDomaineScientifique;
	private EODomaineScientifique selectedDomaineScientifique;
	
    public RattachementUniteStructure(WOContext context) {
        super(context);
    }
    
    public String selectionDomaineScientifiqueContainerId() {
    	return getComponentId() + "_selectionDomaineScientifiqueContainer";
    }
    
    public String creationUniteBoutonFormContainerId() {
    	return getComponentId() + "_creationUniteBoutonFormContainer";
    }
    
    public EOQualifier qualifierForUniteRecherche() {
    	return ERXQ.equals(EOStructure.TO_REPART_TYPE_GROUPES_KEY + "." + EOTypeGroupe.TGRP_CODE_KEY, EOTypeGroupe.TGRP_CODE_LA);
    }
	
    public EOStructure getSelectedUniteDeRecherche() {
		return selectedUniteDeRecherche;
	}


	public void setSelectedUniteDeRecherche(EOStructure selectedUniteDeRecherche) {
		this.selectedUniteDeRecherche = selectedUniteDeRecherche;
	}
	
	public NSArray<EODomaineScientifique> domainesScientifiques() {
		return EODomaineScientifique.fetchAll(edc());
	}

	public void setSelectedDomaineScientifique(EODomaineScientifique selectedDomaineScientifique) {
		this.selectedDomaineScientifique = selectedDomaineScientifique;
	}

	public EODomaineScientifique selectedDomaineScientifique() {
		return selectedDomaineScientifique;
	}

	public void setCurrentDomaineScientifique(EODomaineScientifique currentDomaineScientifique) {
		this.currentDomaineScientifique = currentDomaineScientifique;
	}

	public EODomaineScientifique currentDomaineScientifique() {
		return currentDomaineScientifique;
	}
	
	public String onSelectionnerUniteSuccess() {
		return "function() { " + selectionDomaineScientifiqueContainerId() + "Update(); }" ;
	}
	
	public WOActionResults creerUniteAPartirDeLaStructureSelectionnee() {
		if (selectedDomaineScientifique() != null) {
			EORepartStructureRechercheDomaineScientifique repartUniteRechercheDomaineScientifique = EORepartStructureRechercheDomaineScientifique.creerInstance(edc());
			repartUniteRechercheDomaineScientifique.setDCreation(MyDateCtrl.getDateJour());
			repartUniteRechercheDomaineScientifique.setDModification(MyDateCtrl.getDateJour());
			repartUniteRechercheDomaineScientifique.setPersIdCreation(getUtilisateurPersId());
			repartUniteRechercheDomaineScientifique.setPersIdModification(getUtilisateurPersId());
			repartUniteRechercheDomaineScientifique.setDomaineScientifiqueRelationship(selectedDomaineScientifique()); 
			repartUniteRechercheDomaineScientifique.setUniteRechercheRelationship(getSelectedUniteDeRecherche());
			repartUniteRechercheDomaineScientifique.setPrincipal(true);
			edc().saveChanges();
			return pageWithName(UniteRecherche.class.getName());
		} else {
			session().addSimpleLocalizedErrorMessage("erreur", "dsNonSelectionne");
			return doNothing();
		}
	}
    
}