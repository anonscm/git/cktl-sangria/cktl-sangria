package org.cocktail.sangria.server.components.uniterecherche;

import org.cocktail.fwkcktlpersonne.common.metier.EOAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlrecherche.server.metier.factory.FactoryAssociation;
import org.cocktail.fwkcktlreportingguiajax.serveur.CktlAbstractReporterAjaxProgress;
import org.cocktail.reporting.server.CktlAbstractReporter;
import org.cocktail.reporting.server.jrxml.IJrxmlReportListener;
import org.cocktail.sangria.server.components.commons.ASangriaComponent;
import org.cocktail.sangria.server.reports.EditionUniteRechercheCorpsEtGrades;
import org.cocktail.sangria.server.reports.EditionUniteRechercheDotations;
import org.cocktail.sangria.server.reports.EditionsUniteRechercheSynthese;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.foundation.NSTimestamp;

public class UniteRechercheConsultation extends ASangriaComponent {
    
	private EOStructure uniteRecherche;
	
	private ReporterAjaxProgress reporterProgress;
	private String reportFilename;
	private CktlAbstractReporter reporter;

	
	public UniteRechercheConsultation(WOContext context) {
        super(context);
    }

	public void setUniteRecherche(EOStructure uniteRecherche) {
		this.uniteRecherche = uniteRecherche;
	}

	public EOStructure uniteRecherche() {
		return uniteRecherche;
	}
	
	
	
// GESTION DES ONGLETS
    
    /** The onglet identification selected. */
    private Boolean ongletIdentificationSelected = true;

    private Boolean ongletEquipesSelected = false;
    
    private Boolean ongletPersonnelsSelected = false;
    
    private Boolean ongletGestionFinanciereSelected = false;
  
    private Boolean ongletEvaluationSelected = false;
    
    private Boolean ongletEditionSelected = false;

    
	/** The consultation. */
	private Boolean consultation = false;
	
	/** The modification. */
	private Boolean modification = false;
    
	/**
	 * Onglet identification selected.
	 *
	 * @return the boolean
	 */
	public Boolean ongletIdentificationSelected() {
		return ongletIdentificationSelected;
	}
	
	/**
	 * Sets the onglet identification selected.
	 *
	 * @param ongletIdentificationSelected the new onglet identification selected
	 */
	public void setOngletIdentificationSelected(Boolean ongletIdentificationSelected) {
		this.ongletIdentificationSelected = ongletIdentificationSelected;
	}
	
	public void setOngletEquipesSelected(Boolean ongletEquipesSelected) {
		this.ongletEquipesSelected = ongletEquipesSelected;
	}

	public Boolean ongletEquipesSelected() {
		return ongletEquipesSelected;
	}

	public void setOngletPersonnelsSelected(Boolean ongletPersonnelsSelected) {
		this.ongletPersonnelsSelected = ongletPersonnelsSelected;
	}

	public Boolean ongletPersonnelsSelected() {
		return ongletPersonnelsSelected;
	}

	public void setOngletGestionFinanciereSelected(
			Boolean ongletGestionFinanciereSelected) {
		this.ongletGestionFinanciereSelected = ongletGestionFinanciereSelected;
	}

	public Boolean ongletGestionFinanciereSelected() {
		return ongletGestionFinanciereSelected;
	}

	public void setOngletEvaluationSelected(Boolean ongletEvaluationSelected) {
		this.ongletEvaluationSelected = ongletEvaluationSelected;
	}

	public Boolean ongletEvaluationSelected() {
		return ongletEvaluationSelected;
	}

	/**
     * Onglets container id.
     *
     * @return the string
     */
    public String ongletsContainerId() {
    	return getComponentId() + "_ongletsContainer";
    }
    
    /**
     * Onglet identification id.
     *
     * @return the string
     */
    public String ongletIdentificationId() {
    	return getComponentId() + "_ongletIdentification";
    }
 
    public String ongletEquipesId() {
    	return getComponentId() + "_ongletEquipes";
    }
    
    public String ongletPersonnelsId() {
    	return getComponentId() + "_ongletPersonnels";
    }
        
    public String ongletGestionFinanciereId() {
    	return getComponentId() + "_ongletGestionFinanciere";
    }

    public String ongletEvaluationId() {
    	return getComponentId() + "_ongletEvaluation";
    }
    
    public String ongletEditionId() {
    	return getComponentId() + "_ongletEdition";
    }
    
    /**
     * Consultation.
     *
     * @return the boolean
     */
    public Boolean consultation() {
    	return consultation;
    }
    
    /**
     * Sets the consultation.
     *
     * @param consultation the new consultation
     */
    public void setConsultation(Boolean consultation) {
    	this.consultation = consultation;
    	
    	
    }
    
    /**
     * Modification.
     *
     * @return the boolean
     */
    public Boolean modification() {
    	if (consultation()) {
    		return false;
    	} else {
    		return modification;
    	}
    }
    
    /**
     * Sets the modification.
     *
     * @param modification the new modification
     */
    public void setModification(Boolean modification) {
    	this.modification = modification;
    }

	/**
	 * Retour recherche.
	 *
	 * @return the wO action results
	 */
	public WOActionResults retourRecherche() {
		return pageWithName(UniteRecherche.class.getName());
		
	}
	
	/**
	 * Enregistrer les modications.
	 *
	 * @return the wO action results
	 */
	public WOActionResults enregistrerLesModications() {
		
		try {
			uniteRecherche().setDModification(new NSTimestamp()); 	
			edc().saveChanges();
			session().addSimpleSuccessMessage("Enregistrement effectué avec succés", null);

		} catch (Exception e) {
			session().addSimpleErrorMessage("Erreur", e);
		}
		return doNothing();
	}

	

	public static class ReporterAjaxProgress extends CktlAbstractReporterAjaxProgress implements IJrxmlReportListener {

        public ReporterAjaxProgress(int maximum) {
            super(maximum);
        }

			    
	}

	public WOActionResults imprimerSynthese() {
		reporterProgress = new ReporterAjaxProgress(100);
		
		reportFilename = "FicheSyntheseUniteRecherche.pdf";
		reporter = EditionsUniteRechercheSynthese.reporter(uniteRecherche(), reporterProgress);
		

		return doNothing();
	}
	
	public WOActionResults imprimerGrades() {
		reporterProgress = new ReporterAjaxProgress(100);

		reportFilename = "GradesUniteRecherche.xls";
		reporter = EditionUniteRechercheCorpsEtGrades.reporter(uniteRecherche(), reporterProgress);

		return doNothing();
	}
	
	public WOActionResults imprimerDotations() {
		reporterProgress = new ReporterAjaxProgress(100);

		reportFilename = "DotationsUniteRecherche.pdf";
		reporter = EditionUniteRechercheDotations.reporter(uniteRecherche(), reporterProgress);

		return doNothing();
	}
	

	public CktlAbstractReporter getReporter() {
        return reporter;
    }

	public CktlAbstractReporterAjaxProgress getReporterProgress() {
        return reporterProgress;
    }
	
	public String getReportFilename() {
        return reportFilename;
    }
	
	public void setReportFilename(String reportFilename) {
        this.reportFilename = reportFilename;
    }

	public Boolean ongletEditionSelected() {
		return ongletEditionSelected;
	}

	public void setOngletEditionSelected(Boolean ongletEditionSelected) {
		this.ongletEditionSelected = ongletEditionSelected;
	}

	public EOAssociation typesMembresUniteRecherche() {
		return FactoryAssociation.shared().typesMembresUniteRechercheAssociation(edc());
	}
	

}