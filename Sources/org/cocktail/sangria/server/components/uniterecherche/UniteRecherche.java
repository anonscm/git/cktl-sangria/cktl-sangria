package org.cocktail.sangria.server.components.uniterecherche;

import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSSocketUtilities;

import er.extensions.eof.ERXEC;
import er.extensions.eof.ERXQ;

import org.cocktail.fwkcktlpersonne.common.metier.EORepartTypeGroupe;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypeGroupe;
import org.cocktail.fwkcktlrecherche.server.metier.EOGrandSecteurDisciplinaire;
import org.cocktail.fwkcktlrecherche.server.metier.EORepartStructureRechercheDomaineScientifique;
import org.cocktail.fwkcktlrecherche.server.util.StructuresRechercheHelper;
import org.cocktail.fwkcktlsangriaguiajax.serveur.components.commons.AFwkCktlSangriaComponent;
import org.cocktail.sangria.server.components.commons.ASangriaComponent;

import com.webobjects.appserver.WOActionResults;

public class UniteRecherche extends ASangriaComponent {

	public final static Integer largeurLigneGrandsSecteurs = 3;

	EOGrandSecteurDisciplinaire currentGrandSecteurDisciplinaire;
	EOStructure currentUniteRecherche;
	Integer grandsSecteursDisciplinairesCount;

	public UniteRecherche(WOContext context) {
		super(context);
	}

	public NSArray<EOGrandSecteurDisciplinaire> grandsSecteursDisciplinaires() {
		EOSortOrdering sortOrdering = EOSortOrdering.sortOrderingWithKey(EOGrandSecteurDisciplinaire.GSD_LIBELLE_KEY, EOSortOrdering.CompareAscending);
		return EOGrandSecteurDisciplinaire.fetchAll(edc(), new NSArray<EOSortOrdering>(sortOrdering));
	}

	public EOGrandSecteurDisciplinaire currentGrandSecteurDisciplinaire() {
		return currentGrandSecteurDisciplinaire;
	}

	public void setCurrentGrandSecteurDisciplinaire(EOGrandSecteurDisciplinaire currentGrandSecteurDisciplinaire) {
		this.currentGrandSecteurDisciplinaire = currentGrandSecteurDisciplinaire;
	}

	public Integer grandsSecteursDisciplinairesCount() {
		return grandsSecteursDisciplinairesCount;
	}

	public void setGrandsSecteursDisciplinairesCount(Integer grandsSecteursDisciplinairesCount) {
		this.grandsSecteursDisciplinairesCount = grandsSecteursDisciplinairesCount;
	}

	public Boolean nouvelleLigneGrandSecteurs() {
		return ((grandsSecteursDisciplinairesCount() + 1) % largeurLigneGrandsSecteurs) == 0;
	}

	public NSArray<EOStructure> unitesPourCurrentGrandSecteur() {
		return StructuresRechercheHelper.unitesRecherchePourGrandSecteurDisciplinaire(edc(), currentGrandSecteurDisciplinaire(), false);
	}

	public EOStructure currentUniteRecherche() {
		return currentUniteRecherche;
	}

	public void setCurrentUniteRecherche(EOStructure currentUniteRecherche) {
		this.currentUniteRecherche = currentUniteRecherche;
	}

	public WOActionResults ouvrirUniteRechercheConsutation() {
		EOEditingContext e = ERXEC.newEditingContext();
		UniteRechercheConsultation uniteRechercheConsultation = (UniteRechercheConsultation) pageWithName(UniteRechercheConsultation.class.getName());
		uniteRechercheConsultation.setUniteRecherche(EOStructure.fetchFirstByQualifier(e, EOStructure.C_STRUCTURE.eq(currentUniteRecherche().cStructure())));
		uniteRechercheConsultation.setModification(true);
		uniteRechercheConsultation.setConsultation(false);
		uniteRechercheConsultation.setEdc(e);
		return uniteRechercheConsultation;
	}

	public NSArray<EOStructure> unitesSansDomaine() {
		NSArray<EOStructure> _uniteSansDomaines = ERXQ.filtered(
				StructuresRechercheHelper.unitesDeRecherche(edc()),
				EOStructure.C_STRUCTURE.notIn((NSArray<String>) EORepartStructureRechercheDomaineScientifique.fetchAll(edc()).valueForKeyPath(EORepartStructureRechercheDomaineScientifique.UNITE_RECHERCHE_KEY + "." + EOStructure.C_STRUCTURE_KEY)));
		return _uniteSansDomaines;
	}

	public WOActionResults attacherUnDomaineScientifique() {
		EOEditingContext e = ERXEC.newEditingContext();
		RattachementUniteStructure rattachementUniteStructure = (RattachementUniteStructure) pageWithName(RattachementUniteStructure.class.getName());
		rattachementUniteStructure.setSelectedUniteDeRecherche(currentUniteRecherche().localInstanceIn(e));
		rattachementUniteStructure.setEdc(e);
		return rattachementUniteStructure;
	}

}