package org.cocktail.sangria.server.components.commons;

import org.cocktail.cocowork.server.metier.convention.Avenant;
import org.cocktail.cocowork.server.metier.convention.AvenantDocument;
import org.cocktail.cocowork.server.metier.convention.Contrat;
import org.cocktail.cocowork.server.metier.convention.factory.FactoryAvenant;
import org.cocktail.fwkcktlajaxwebext.serveur.component.CktlAjaxWindow;
import org.cocktail.fwkcktldroitsutils.common.ApplicationUser;
import org.cocktail.fwkcktlged.serveur.metier.Document;
import org.cocktail.fwkcktlged.serveur.metier.EODocument;
import org.cocktail.fwkcktlged.serveur.metier.EOTypeDocument;
import org.cocktail.fwkcktlged.serveur.metier.service.DocumentService;
import org.cocktail.fwkcktlged.serveur.metier.service.DocumentServiceImpl;
import org.cocktail.fwkcktlpersonne.common.metier.EOGrhumParametres;
import org.cocktail.fwkcktlrecherche.server.metier.EOAap;
import org.cocktail.sangria.server.Application;
import org.cocktail.sangria.server.components.aap.AapComponentUI;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOArrayDataSource;
import com.webobjects.eocontrol.EOClassDescription;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSData;
import com.webobjects.foundation.NSForwardException;
import com.webobjects.foundation.NSMutableArray;

import er.extensions.appserver.ERXDisplayGroup;
import er.extensions.appserver.ERXRedirect;
import er.extensions.eof.ERXEC;
import er.extensions.eof.ERXEOAccessUtilities;
import er.extensions.eof.ERXEOControlUtilities;
import er.extensions.eof.ERXQ;

public class DocumentsUIM extends AapComponentUI {
	
	public final static String BINDING_contrat = "contrat";
	public final static String BINDING_readOnly = "readOnly";

	private AvenantDocument selectedAvenantDocument;
	private EODocument nouveauDocument;
	
	private ERXDisplayGroup<AvenantDocument> dgDocuments;	
	private EOArrayDataSource documentDataSource = null;
	private FactoryAvenant factoryAvenant;
	private Contrat contrat;
	
	private EOEditingContext _editingContext = null;
	
	
	private NSData documentData;
	private String documentObjet;
	private String documentCommentaire;
	private String documentFileName;
	private String documentMotsClefs;
	private EOTypeDocument documentType; 
	private NSArray<EOTypeDocument> types;
	
	private Boolean ajoutEnCours = false;
	

	private DocumentService documentService;
	
    public DocumentsUIM(WOContext context) {
        super(context);
    }

    @Override
    public EOEditingContext edc() {
    	if (_editingContext == null) {
    		_editingContext = ERXEC.newEditingContext((EOEditingContext) valueForBinding(BINDING_editingContext));
    	}
    	return _editingContext;
    }
	    
    public String ajouterDocumentWindowId() {
    	return getComponentId() + "_ajouterDocumentWindow";
    }
   
    public String documentsContainerId() {
    	return getComponentId() + "_documentsContainer";
    }
    
    public String getBoutonsAjoutDocumentContainerId() {
    	return getComponentId() + "_boutonsAjoutDocumentContainer";
    }
    
    
		
	public void setContrat(Contrat contrat) {
		this.contrat = contrat;
	}

	public Contrat contrat() {
    	if(hasBinding(BINDING_contrat)) {
    		Contrat _contrat = (Contrat) valueForBinding(BINDING_contrat);
    		contrat  = _contrat.localInstanceIn(edc());
    		refreshAvenantZeroPourBugDeDateSignature();
    		contrat.setShouldNotValidate(true);
    	}
    	return contrat;	
    }

	private void refreshAvenantZeroPourBugDeDateSignature() {
		edc().refaultObject(contrat.avenantZero());
	}

	public void setFactoryAvenant(FactoryAvenant factoryAvenant) {
		this.factoryAvenant = factoryAvenant;
	}

	public FactoryAvenant factoryAvenant() {
		if(factoryAvenant == null) {
			factoryAvenant = new FactoryAvenant(edc(), false);
		}
		return factoryAvenant;
	}
	
	
	public EOArrayDataSource documentDataSource() {
		if(documentDataSource == null) {
			documentDataSource = new EOArrayDataSource(
					EOClassDescription.classDescriptionForClass(AvenantDocument.class),
					edc()
				);
		}
		return documentDataSource;
	}
	
	
	public ERXDisplayGroup<AvenantDocument> dgDocuments() {
		contrat();
 		if (dgDocuments == null) {
 			
 				dgDocuments = new ERXDisplayGroup<AvenantDocument>();

		        NSMutableArray<AvenantDocument> avenantDocuments = new NSMutableArray<AvenantDocument>();
		        for(Avenant avenant :(NSArray<Avenant>) contrat().avenantsDontInitialNonSupprimes()) {
					avenantDocuments.addObjectsFromArray(avenant.avenantDocuments());
				}
		        documentDataSource().setArray(avenantDocuments);
		        dgDocuments.setDataSource(documentDataSource());
		        dgDocuments.fetch();

 		}
	
		return dgDocuments;
	}


	public WOActionResults validerAjout() {
			ApplicationUser applicationUser = new ApplicationUser(edc(), getUtilisateurPersId());
	        AvenantDocument _avenantDocument = factoryAvenant().ajouterDocument(edc(), applicationUser, contrat().avenantZero(), nouveauDocument());
	        try {
	            edc().saveChanges();
	            documentDataSource().insertObject(_avenantDocument);
	            dgDocuments().fetch();
	            CktlAjaxWindow.close(context());
	        } catch (ValidationException e) {
				session().addSimpleErrorMessage("Erreur", e);
			} catch (Exception e) {
				throw NSForwardException._runtimeExceptionForThrowable(e);
			}
	        return doNothing();
	}
	
	public WOActionResults annulerAjout() {
		edc().revert();
		CktlAjaxWindow.close(context());
		return doNothing();
	}

	public boolean isSupprimerUnDocumentEnabled() {
		return !isReadOnly() && dgDocuments().selectedObject() != null;
	}
	
	public WOActionResults supprimerUnDocument() {
		AvenantDocument leDocumentAArchiver = (AvenantDocument) dgDocuments().selectedObject();

		if (leDocumentAArchiver != null) {
			try {
				ApplicationUser applicationUser = new ApplicationUser(edc(), getUtilisateurPersId());
				documentDataSource().deleteObject(leDocumentAArchiver);
				
				factoryAvenant().supprimerDocument(edc(), leDocumentAArchiver, applicationUser, null);
				edc().saveChanges();
			} catch (ValidationException e) {
				edc().revert();
				session().addSimpleErrorMessage("Erreur", e);
			} catch (Exception e) {
				throw NSForwardException._runtimeExceptionForThrowable(e);
			}
		}
		
		return null;
	}

	
	public AvenantDocument selectedAvenantDocument() {
		return selectedAvenantDocument;
	}

	public void setSelectedAvenantDocument(AvenantDocument selectedAvenantDocument) {
		this.selectedAvenantDocument = selectedAvenantDocument;
	}

	public EODocument nouveauDocument() {
		return nouveauDocument;
	}

	public void setNouveauDocument(EODocument nouveauDocument) {
		this.nouveauDocument = nouveauDocument;
	}

	public Boolean isReadOnly() {
		return booleanValueForBinding(BINDING_readOnly, false);
	}

	public String getDocumentObjet() {
		return documentObjet;
	}

	public void setDocumentObjet(String documentObjet) {
		this.documentObjet = documentObjet;
	}

	public NSData getDocumentData() {
		return documentData;
	}

	public void setDocumentData(NSData documentData) {
		this.documentData = documentData;
	}

	public String getDocumentCommentaire() {
		return documentCommentaire;
	}

	public void setDocumentCommentaire(String documentCommentaire) {
		this.documentCommentaire = documentCommentaire;
	}

	public String getDocumentFileName() {
		return documentFileName;
	}

	public void setDocumentFileName(String documentFileName) {
		this.documentFileName = documentFileName;
	}

	public String getDocumentMotsClefs() {
		return documentMotsClefs;
	}

	public void setDocumentMotsClefs(String documentMotsClefs) {
		this.documentMotsClefs = documentMotsClefs;
	}

	public EOTypeDocument getDocumentType() {
		return documentType;
	}

	public void setDocumentType(EOTypeDocument documentType) {
		this.documentType = documentType;
	}

	public NSArray<EOTypeDocument> getTypes() {
		if (types == null) {
			EOQualifier qualifier = ERXQ.contains(EOTypeDocument.TD_STR_ID_KEY, "gfc.acte");
			types = EOTypeDocument.fetchAll(edc(), qualifier, null);
		}
		return types;
	}

	public DocumentService getDocumentService() {
		if (documentService == null) {
			Application application = (Application) application();
			documentService = new DocumentServiceImpl(edc(), application.getConfigurationGedConventions());
		}
		return documentService;
	}


	public WOActionResults validerAjoutDocument() {
		Document document = getDocumentService().creerFileDocument(getDocumentType().tdStrId(), getDocumentFileName(), getDocumentData().bytes(), getUtilisateurPersId());
		
		if (document == null) {
			session().addSimpleErrorMessage("Erreur", "Impossible de charger le nouveau document");
			edc().revert();
			return doNothing();
		}
		
		document.setObjet(getDocumentObjet());
		document.setCommentaire(getDocumentCommentaire());
		document.setMotsClefs(getDocumentMotsClefs());
		
		ApplicationUser applicationUser = new ApplicationUser(edc(), getUtilisateurPersId());
        AvenantDocument _avenantDocument = factoryAvenant().ajouterDocument(edc(), applicationUser, contrat().avenantZero(), (EODocument) document);
        
        try {
            edc().saveChanges();
            documentDataSource().insertObject(_avenantDocument);
            dgDocuments().fetch();
            setAjoutEnCours(false);
            clear();
        } catch (ValidationException e) {
			session().addSimpleErrorMessage("Erreur", e);
			edc().revert();
		} catch (Exception e) {
			throw NSForwardException._runtimeExceptionForThrowable(e);
		}
        
        return doNothing();
				
	}
	
	private void clear() {
		setDocumentObjet(null);
		setDocumentCommentaire(null);
		setDocumentMotsClefs(null);
		setDocumentData(null);
		setDocumentType(null);
		setDocumentFileName(null);
	}

	public WOActionResults ajouterDocument() {
		setAjoutEnCours(true);
		return doNothing();
	}

	public Boolean getAjoutEnCours() {
		return ajoutEnCours;
	}

	public void setAjoutEnCours(Boolean ajoutEnCours) {
		this.ajoutEnCours = ajoutEnCours;
	}

	public WOActionResults annulerAjoutDocument() {
		setAjoutEnCours(false);
		return null;
	}

	public Boolean getNePeutValiderAjout() {
		return getDocumentObjet() == null || getDocumentData() == null || getDocumentType() == null;
	}
	
    
    

}