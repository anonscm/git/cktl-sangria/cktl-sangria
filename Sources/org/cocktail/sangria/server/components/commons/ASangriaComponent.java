/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2010 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/

package org.cocktail.sangria.server.components.commons;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Nullable;

import org.cocktail.fwkcktlrecherche.server.droits.PerimetreSangriaProvider;
import org.cocktail.fwkcktlrecherche.server.metier.EOGdPerimetreSangria;
import org.cocktail.fwkcktlsangriaguiajax.serveur.components.commons.AFwkCktlSangriaComponent;
import org.cocktail.fwkcktlwebapp.common.UserInfo;
import org.cocktail.sangria.server.Application;
import org.cocktail.sangria.server.Session;
import org.cocktail.sangria.server.components.Accueil;

import com.google.inject.Inject;
import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResponse;

import er.ajax.AjaxUtils;
import er.extensions.eof.ERXEOControlUtilities;

/**
 * 
 * @author jlafourc
 * 
 */
public abstract class ASangriaComponent extends AFwkCktlSangriaComponent {

	private static final long serialVersionUID = -6604422253602670758L;

	private static final String BINDING_READ_ONLY = "readOnly";
	
	private Boolean readOnly = false;
	private PerimetreSangriaProvider perimetreProvider;
	
	@Inject @Nullable
	private UserInfo userInfo;

	/**
	 * @param context Context
	 */
	public ASangriaComponent(WOContext context) {
		super(context);
	}

	public void setUserInfo(UserInfo userInfo) {
		this.userInfo = userInfo;
	}

	public UserInfo getUserInfo() {
		return userInfo;
	}

	/**
	 * L'état du composant
	 * @return renvoi en priorité la valeur du binding
	 * et la valeur du champ sinon
	 */
	public Boolean isReadOnly() {
		return booleanValueForBinding(BINDING_READ_ONLY, readOnly);
	}
	
	public void setReadOnly(Boolean readOnly) {
		this.readOnly = readOnly;
	}
	
	public Boolean isNotReadOnly() {
		return !isReadOnly();
	}
	
	@Override
	public void appendStyleSheets(WOResponse response, WOContext context) {
		super.appendStyleSheets(response, context);
		AjaxUtils.addStylesheetResourceInHead(context, response, "app", "styles/sangria.css");
	}
	
	@Override
	public void appendToResponse(WOResponse response, WOContext context) {
		super.appendToResponse(response, context);
	}

	@Override
	public Application application() {
		return (Application) super.application();
	}

	@Override
	public Session session() {
		return (Session) super.session();
	}

	@Override
	public Integer getUtilisateurPersId() {
		return userInfo.persId().intValue();
	}
		
	/**
	 * @return la page d'accueil
	 */
	public WOActionResults retourAccueil() {
		return pageWithName(Accueil.class.getName());
	}

	/**
	 * @return la page une fois qu'on est déconnecté
	 */
	public WOActionResults quitter() {
		return session().logout();
	}
	
	/**
	 * @return le fournisseur de perimetre de donnees pour sangria
	 */
	public PerimetreSangriaProvider getPerimetreProvider() {
		if (perimetreProvider == null) {
			perimetreProvider = new PerimetreSangriaProvider();
			perimetreProvider.setPerimetres(getPerimetresUser());
		}

		return perimetreProvider;
	}

	/**
	 * @return les perimetres de donnees pour Sangria et pour le user courant
	 */
	public List<EOGdPerimetreSangria> getPerimetresUser() {

		List<EOGdPerimetreSangria> perimetres = new ArrayList<EOGdPerimetreSangria>();
		for (Object perimetre : ERXEOControlUtilities.objectsForGlobalIDs(edc(), session().rechercheApplicationAutorisationCache().getPerimetresAutorises())) {
			perimetres.add((EOGdPerimetreSangria) perimetre);
		}

		return perimetres;
	}

}
