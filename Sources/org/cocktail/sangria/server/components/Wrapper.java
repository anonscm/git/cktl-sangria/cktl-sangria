/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2010 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/

package org.cocktail.sangria.server.components;

import java.util.GregorianCalendar;

import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;
import org.cocktail.sangria.server.Application;
import org.cocktail.sangria.server.VersionMe;
import org.cocktail.sangria.server.components.commons.ASangriaComponent;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResponse;
import com.webobjects.eocontrol.EOEditingContext;

import er.ajax.AjaxUtils;
import er.extensions.eof.ERXEC;

public class Wrapper extends ASangriaComponent {

	private static final String BINDING_confirmerRetourAccueil = "confirmerRetourAccueil";
	private EOEditingContext edc = null;
	private String titre;
	
	public Wrapper(WOContext context) {
        super(context);
    }

	
	
	@Override
	public EOEditingContext edc() {
		if(edc == null) {
			edc = ERXEC.newEditingContext();
		}
		return edc;
	}
	
	@Override
	public void appendToResponse(WOResponse response, WOContext context) {

		super.appendToResponse(response, context);

		
    AjaxUtils.addStylesheetResourceInHead(context, response, "app", "styles/application.css");

		
		AjaxUtils.addStylesheetResourceInHead(context, response, "FwkCktlThemes.framework", "themes/default.css");
		AjaxUtils.addStylesheetResourceInHead(context, response, "FwkCktlThemes.framework", "themes/alert.css");
		AjaxUtils.addStylesheetResourceInHead(context, response, "FwkCktlThemes.framework", "themes/lighting.css");
		AjaxUtils.addStylesheetResourceInHead(context, response, "FwkCktlThemes.framework", "css/CktlCommon.css");
		AjaxUtils.addStylesheetResourceInHead(context, response, "FwkCktlThemes.framework", "css/CktlCommonBleu.css");

		
		
		AjaxUtils.addStylesheetResourceInHead(context, response, "app", "styles/sangria.css");
		AjaxUtils.addStylesheetResourceInHead(context, response, "app", "styles/application.css");

		
		
		
		AjaxUtils.addScriptResourceInHead(context, response, "prototype.js");
		AjaxUtils.addScriptResourceInHead(context, response, "FwkCktlThemes.framework", "scripts/window.js");
		AjaxUtils.addScriptResourceInHead(context, response, "app", "scripts/strings.js");
		AjaxUtils.addScriptResourceInHead(context, response, "app", "scripts/formatteurs.js");
		AjaxUtils.addScriptResourceInHead(context, response, "app", "scripts/pitch.js");

		AjaxUtils.addStylesheetResourceInHead(context, response, "FwkCktlAjaxWebExt.framework", "themes/fixedheadertable/defaultTheme.css");

	}

	public String copyright() {
		return "(c) "+DateCtrl.nowDay().get(GregorianCalendar.YEAR)+" Association Cocktail";
	}

	public String version() {
		return VersionMe.htmlAppliVersion();
	}
	
	public String instance() {
	  return Application.serverBDId();
	}

	public Boolean confirmerRetourAccueil() {
		return booleanValueForBinding(BINDING_confirmerRetourAccueil, true);
	}
	
	public WOActionResults retourAccueil() {
		return pageWithName(Accueil.class.getName());
	}
	
	public String onClickRetourAccueil() {
		if(confirmerRetourAccueil()) {
			return "return confirm(&quot;Voulez-vous vraiment revenir à la page d'accueil ? Toutes les informations non sauvegardées seront perdues.&quot;);";
		}
		return "";
	}
	
	/**
	 * @return the titre
	 */
	public String titre() {
		if (titre == null) {
			titre = "Sangria";
		}
		return titre;
	}

}
