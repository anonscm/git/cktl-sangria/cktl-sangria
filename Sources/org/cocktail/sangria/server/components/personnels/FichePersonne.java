package org.cocktail.sangria.server.components.personnels;

import java.text.SimpleDateFormat;
import java.util.Locale;

import org.cocktail.fwkcktlgrh.common.metier.EOAbsence;
import org.cocktail.fwkcktlgrh.common.metier.EOAffectation;
import org.cocktail.fwkcktlgrh.common.metier.EOCarriereSpecialisations;
import org.cocktail.fwkcktlgrh.common.metier.EOChangementPosition;
import org.cocktail.fwkcktlgrh.common.metier.EOContrat;
import org.cocktail.fwkcktlgrh.common.metier.EOElements;
import org.cocktail.fwkcktlgrh.common.metier.EOIndividuDiplome;
import org.cocktail.fwkcktlgrh.common.metier.EOModalitesService;
import org.cocktail.fwkcktlgrh.common.metier.services.CarriereSpecialisationsService;
import org.cocktail.fwkcktlgrh.common.metier.services.IndividusGradesService;
import org.cocktail.fwkcktlpersonne.common.metier.EOAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EODiplome;
import org.cocktail.fwkcktlpersonne.common.metier.EOGrade;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartPersonneAdresse;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypeAdresse;
import org.cocktail.fwkcktlrecherche.server.metier.factory.FactoryAssociation;
import org.cocktail.fwkcktlrecherche.server.util.PersonnelRechercheUtilities;
import org.cocktail.fwkcktlrecherche.server.util.StructuresRechercheHelper;
import org.cocktail.sangria.server.components.commons.ASangriaComponent;
import org.cocktail.sangria.server.reports.beans.FichePersonneBean;
import org.cocktail.sangria.server.reports.beans.FichePersonneBean.FichePersonneFonctionBean;
import org.cocktail.sangria.server.reports.beans.FichePersonneBean.FichePersonneStatutBean;
import org.cocktail.sangria.server.reports.beans.FichePersonneBean.FichePersonneRattachementBean;
import org.cocktail.sangria.server.service.ImpressionService;

import com.google.inject.Inject;
import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.eof.ERXQ;

/**
 * Fiche avec les donnees d'une personne
 */
public class FichePersonne extends ASangriaComponent {
    
    private static final long serialVersionUID = 7039188332261450014L;
    
    @Inject
	private ImpressionService impressionService;
    
    private EOIndividu personnel;
    private EORepartAssociation currentRepartAssociation;
    private EORepartAssociation currentRepartAssociationActuel;
    private EORepartAssociation currentRepartAssociationPrecedent;
    private NSArray<EORepartAssociation> repartsAssociations;
    private NSArray<EORepartAssociation> repartsHistoriqueAssociations;
    private EOCarriereSpecialisations currentCarriereSpecialisation;
    private NSArray<EORepartAssociation> repartsAssociationsFonction;
    private NSArray<EORepartAssociation> repartsHistoriqueAssociationsFonction;
    
    private EOStructure currentRepartStructure;
    private NSArray<EOStructure> structures;
    
    private NSArray<EOContrat> contrats;

	private static final String CODE_DIPLOME_HDR = "0731000";
	private static final String CODE_DIPLOME_DOCTORAT_ETAT = "1000000";
	private NSArray<String> codesDiplomesHdr = new NSArray<String>(CODE_DIPLOME_HDR, CODE_DIPLOME_DOCTORAT_ETAT);

	private static final String CODE_DIPLOME_DOCTORAT_UNIVERSITE = "0990000";
	private static final String CODE_DIPLOME_DOCTORAT_3_EME_CYCLE = "1010000";
	private NSArray<String> codesDiplomesDoctorat = new NSArray<String>(CODE_DIPLOME_DOCTORAT_UNIVERSITE, CODE_DIPLOME_DOCTORAT_3_EME_CYCLE);

	private static final String CODE_DIRECTEUR_RECHERCHE_2_CLASSE = "H041";
	private static final String CODE_DIRECTEUR_RECHERCHE_1_CLASSE = "H042";
	private static final String CODE_DIRECTEUR_RECHERCHE_CLASSE_EXCEPT = "H043";
	private static final String CODE_CHARGE_RECHERCHE_2_CLASSE = "H051";
	private static final String CODE_CHARGE_RECHERCHE_1_CLASSE = "H052";
	private static final String CODE_DIRECTEUR_CNRS_2_CLASSE = "DC 2";
	private static final String CODE_DIRECTEUR_CNRS_1_CLASSE = "DC 1";
	
	private NSArray<String> gradesChercheur = new NSArray<String>(CODE_DIRECTEUR_RECHERCHE_2_CLASSE, CODE_DIRECTEUR_RECHERCHE_1_CLASSE, CODE_DIRECTEUR_RECHERCHE_CLASSE_EXCEPT, 
			CODE_CHARGE_RECHERCHE_2_CLASSE, CODE_CHARGE_RECHERCHE_1_CLASSE, CODE_DIRECTEUR_CNRS_2_CLASSE, CODE_DIRECTEUR_CNRS_1_CLASSE);
	
    /**
     * Constructeur
     * @param context contexte
     */
	public FichePersonne(WOContext context) {
        super(context);
    }

	public EOIndividu getPersonnel() {
	    return personnel;
    }

	public void setPersonnel(EOIndividu personnel) {
	    this.personnel = personnel;
    }
	
	public EORepartAssociation getCurrentRepartAssociation() {
	    return currentRepartAssociation;
    }

	public void setCurrentRepartAssociation(EORepartAssociation currentRepartAssociation) {
	    this.currentRepartAssociation = currentRepartAssociation;
    }
	
	public EORepartAssociation getCurrentRepartAssociationActuel() {
	    return currentRepartAssociationActuel;
    }

	public void setCurrentRepartAssociationActuel(EORepartAssociation currentRepartAssociationActuel) {
	    this.currentRepartAssociationActuel = currentRepartAssociationActuel;
    }
	
	public EORepartAssociation getCurrentRepartAssociationPrecedent() {
	    return currentRepartAssociationPrecedent;
    }

	public void setCurrentRepartAssociationPrecedent(EORepartAssociation currentRepartAssociationPrecedent) {
	    this.currentRepartAssociationPrecedent = currentRepartAssociationPrecedent;
    }

	public EOStructure getCurrentRepartStructure() {
	    return currentRepartStructure;
    }

	public void setCurrentRepartStructure(EOStructure currentRepartStructure) {
	    this.currentRepartStructure = currentRepartStructure;
    }
	
	public NSArray<EOContrat> getContratsActuels() {
    	if (contrats == null) {
    		contrats = EOContrat.contratsActuelsForIndividu(edc(), personnel);
    	}
		return contrats;
	}

	public void setContrats(NSArray<EOContrat> contrats) {
		this.contrats = contrats;
	}
	
	private EOCarriereSpecialisations getCurrentCarriereSpecialisation() {
		if (currentCarriereSpecialisation == null) {
			CarriereSpecialisationsService carriereSpecialisationService = new CarriereSpecialisationsService(edc());
			currentCarriereSpecialisation = carriereSpecialisationService.getCurrentCarriereSpecialisation(getPersonnel());
		}
		return currentCarriereSpecialisation;
	}
	
	public NSArray<EORepartAssociation> lesRepartsActuels() {
		NSMutableArray<EORepartAssociation> lesReparts = new NSMutableArray<EORepartAssociation>();
		
		lesReparts.addAll(repartsAssociationsStructureRechercheStatut());
		
		for (EORepartAssociation raFonction : repartsAssociationsStructureRechercheFonction()) {
			boolean trouve = false;
			for (EORepartAssociation raStatut : repartsAssociationsStructureRechercheStatut()) {
				if (raFonction.toStructure().equals(raStatut.toStructure())) {
					trouve = true;
				}
			}
			if (!trouve) {
				lesReparts.add(raFonction);
			}
		}
		
		return lesReparts;
	}
	
	public NSArray<EORepartAssociation> repartsAssociationsStructureRechercheStatutPourUneStructure() {
		NSArray<EORepartAssociation> rasrg = new NSMutableArray<EORepartAssociation>();
		
		for (EORepartAssociation ra : repartsAssociationsStructureRechercheStatut()) {
			if (getCurrentRepartAssociationActuel() != null) {
				if (getCurrentRepartAssociationActuel().toStructure().equals(ra.toStructure())) {
					rasrg.add(ra);
				}
			}
		}
		return rasrg;
	}

	public NSArray<EORepartAssociation> repartsAssociationsHistoriqueStructureRechercheStatutPourUneStructure() {
		NSArray<EORepartAssociation> rasrg = new NSMutableArray<EORepartAssociation>();
		
		for (EORepartAssociation ra : repartsAssociationsHistoriqueStructureRechercheStatut()) {
			if (getCurrentRepartAssociationActuel() != null) {
				if (getCurrentRepartAssociationActuel().toStructure().equals(ra.toStructure())) {
					rasrg.add(ra);
				}
			}
		}
		return rasrg;
	}
	
	public NSArray<EORepartAssociation> repartsAssociationsStructureRechercheStatut() {
		if (repartsAssociations == null) {
			NSMutableArray<EORepartAssociation> ras = (NSMutableArray<EORepartAssociation>) StructuresRechercheHelper.repartsAssociationsStructureRechercheActuellePourIndividu(edc(), getPersonnel());
			repartsAssociations = associationsMembreUniteRecherche(ras);
		}
		return repartsAssociations;
	}
	
	public NSArray<EORepartAssociation> repartsAssociationsStructureRechercheFonction() {
		if (repartsAssociationsFonction == null) {
			NSMutableArray<EORepartAssociation> ras = (NSMutableArray<EORepartAssociation>) StructuresRechercheHelper.repartsAssociationsStructureRechercheActuellePourIndividu(edc(), getPersonnel());
			repartsAssociationsFonction = associationsNonMembreUniteRecherche(ras);
		}
		return repartsAssociationsFonction;
	}
	
	public NSArray<EORepartAssociation> repartsAssociationsFonctionStructureRecherchePourUneStructure() {
		NSArray<EORepartAssociation> rasrg = new NSMutableArray<EORepartAssociation>();
		
		for (EORepartAssociation ra : repartsAssociationsStructureRechercheFonction()) {
			if (getCurrentRepartAssociationActuel().toStructure().equals(ra.toStructure())) {
				rasrg.add(ra);
			}
		}
		return rasrg;
		
	}
	
	public NSArray<EORepartAssociation> repartsAssociationsHistoriqueStructureRechercheFonctionPourUneStructure() {
		NSArray<EORepartAssociation> rasrg = new NSMutableArray<EORepartAssociation>();
		
		for (EORepartAssociation ra : repartsAssociationsHistoriqueStructureRechercheFonction()) {
			if (getCurrentRepartAssociationActuel().toStructure().equals(ra.toStructure())) {
				rasrg.add(ra);
			}
		}
		return rasrg;
		
	}
	
	public NSArray<EORepartAssociation> lesRepartsPrecedents() {
		NSMutableArray<EORepartAssociation> lesReparts = new NSMutableArray<EORepartAssociation>();
		
		lesReparts.addAll(repartsAssociationsHistoriqueStructureRechercheStatut());
		
		for (EORepartAssociation raFonction : repartsAssociationsHistoriqueStructureRechercheFonction()) {
			boolean trouve = false;
			for (EORepartAssociation raStatut : repartsAssociationsHistoriqueStructureRechercheStatut()) {
				if (raFonction.toStructure().equals(raStatut.toStructure())) {
					trouve = true;
				}
			}
			if (!trouve) {
				lesReparts.add(raFonction);
			}
		}
		
		return lesReparts;
	}
	
	public NSArray<EORepartAssociation> repartsAssociationsHistoriqueStructureRechercheStatut() {
		if (repartsHistoriqueAssociations == null) {
			NSMutableArray<EORepartAssociation> ras = (NSMutableArray<EORepartAssociation>) StructuresRechercheHelper.repartsAssociationsStructureRecherchePasseesPourIndividu(edc(), getPersonnel());
			repartsHistoriqueAssociations = associationsMembreUniteRecherche(ras);
		}
		return repartsHistoriqueAssociations;
	}
	
	public NSArray<EORepartAssociation> repartsAssociationsHistoriqueStructureRechercheFonction() {
		if (repartsHistoriqueAssociationsFonction == null) {
			NSMutableArray<EORepartAssociation> ras = (NSMutableArray<EORepartAssociation>) StructuresRechercheHelper.repartsAssociationsStructureRecherchePasseesPourIndividu(edc(), getPersonnel());
			repartsHistoriqueAssociationsFonction = associationsNonMembreUniteRecherche(ras);
		}
		return repartsHistoriqueAssociationsFonction;
	}
	
	public NSArray<EOStructure> structuresRechercheNonRepartAssociationPourIndividu() {
		if (structures == null) {
			structures = StructuresRechercheHelper.repartsStructuresRecherchePourIndividu(edc(), getPersonnel());
			// supprime les structures deja présentes ds la liste des structures actuelles et ds l'historique
			for (EORepartAssociation ra : repartsAssociationsStructureRechercheStatut()) {
				structures.remove(ra.toStructure());
			}
			for (EORepartAssociation ra : repartsAssociationsHistoriqueStructureRechercheStatut()) {
				structures.remove(ra.toStructure());
			}
		}
		return structures;
	}
	
	public String getPositionPersonnel() {
		if (isRetraite()) {
			return "Retraité(e)";
		} else {
			String abs = getAbsenceActuelle();
			if (abs.equals("")) {
				return getPositionActuelle();
			}
			return abs;
		}
	}
	
	/**
	 * Indique si la personne est retraitée ou pas
	 * Est retraitée une personne qui n'a pas d'element ni d'avenant à son contrat
	 * @return boolean
	 */
	public Boolean isRetraite() {
		EOElements element = EOElements.elementCourant(edc(), getPersonnel());
		
		boolean hasAvenant = false;
		boolean hasContrat = false;
		for (EOContrat contrat : getContratsActuels()) {
			hasContrat = true;
			if (contrat.avenantActuel() != null) {
				hasAvenant = true;
			}
		}
		
		return (element == null && !hasAvenant && hasContrat);
	}
	
	public String getAbsenceActuelle() {
		EOAbsence absence = EOAbsence.absenceValideActuelleForIndividu(edc(), personnel);
		
		if (absence == null) {
			return "";
		}
		
		if (absence.dateFin() == null) {
			return absence.typeAbsence().libelle() + " depuis le " + new SimpleDateFormat("d MMMMM yyyy", new Locale("fr", "FR")).format(absence.dateDebut());
		}
		
		return absence.typeAbsence().libelle() + " du " + new SimpleDateFormat("d MMMMM yyyy", new Locale("fr", "FR")).format(absence.dateDebut()) + " au " 
		+ new SimpleDateFormat("d MMMMM yyyy", new Locale("fr", "FR")).format(absence.dateFin());
		
	}
	
	public String getPositionActuelle() {
		EOChangementPosition position = EOChangementPosition.dernierePositionValideCouranteForIndividu(edc(), personnel);
		if (position == null) {
			return "Inconnue";
		}
		return position.libellePosition();
	}
	
	public String getRattachementComposante() {
		EOAffectation affectation = EOAffectation.affectationCourante(edc(), getPersonnel());
		if (affectation != null && affectation.toStructure() != null && affectation.toStructure().toComposante() != null) {
			return affectation.toStructure().toComposante().llStructure();
		} else {
			return "Non renseignée";
		}
	}
	
	public String etablissementsAffectation() {
		String liste = "";
		NSArray<EOStructure> lesEtablissements = getPersonnel().getEtablissementsAffectation(null);
		for (EOStructure etablissement : lesEtablissements) {
			if (!liste.equals("")) {
				liste += ", ";
			}
			liste += etablissement.llStructure();
		}
		return liste;
	}
	
	public boolean isEnseignantChercheur() {
		
		if (getGradePourIndividu() != null) {
			if (gradesChercheur.contains(getGradePourIndividu().cGrade())) {
				return true;
			}
		}
		
		if (getPersonnel().estTitulaire()) {
			EOElements elementCourant = EOElements.elementCourant(edc(), getPersonnel());
			if (elementCourant != null) {
				return "O".equals(elementCourant.toCorps().toTypePopulation().temEnseignant());
			}
		}
		return false;
	}
	
	public EOGrade getGradePourIndividu() {
		IndividusGradesService individusGradesService = IndividusGradesService.creerNouvelleInstance(edc());
		return individusGradesService.gradePourIndividu(getPersonnel(), new NSTimestamp());
	}	
	
	public NSTimestamp getDateDoctorat() {
		
		EOQualifier qualifierDoctorat = 
				ERXQ.and(
					EOIndividuDiplome.TO_INDIVIDU.eq(personnel),
					ERXQ.in(ERXQ.keyPath(EOIndividuDiplome.TO_DIPLOME_KEY, EODiplome.C_DIPLOME_KEY), codesDiplomesDoctorat)
				);
		
		EOIndividuDiplome individuDiplomeDoctorat = EOIndividuDiplome.fetchFirstByQualifier(edc(), qualifierDoctorat);
		
		if (individuDiplomeDoctorat == null) {
			return null;
		}
		
		return individuDiplomeDoctorat.dDiplome();
		
	}
	
	public NSTimestamp getDateHdr() {
		
		EOQualifier qualifierHdr = 
				ERXQ.and(
					EOIndividuDiplome.TO_INDIVIDU.eq(personnel),
					ERXQ.in(ERXQ.keyPath(EOIndividuDiplome.TO_DIPLOME_KEY, EODiplome.C_DIPLOME_KEY), codesDiplomesHdr)
				);
		
		EOIndividuDiplome individuDiplomeHdr = EOIndividuDiplome.fetchFirstByQualifier(edc(), qualifierHdr);
		
		if (individuDiplomeHdr == null) {
			return null;
		}
		
		return individuDiplomeHdr.dDiplome();
		
	}
	
	public String equipesDeRecherche() {
		
		String equipesDeRecherche = "";
		NSMutableArray<EOStructure> labos = new NSMutableArray<EOStructure>();
		
		for (EORepartAssociation ra : repartsAssociationsStructureRechercheStatut()) {
			if (!labos.contains(ra.toStructure())) {
				labos.add(ra.toStructure());
				NSArray<EOStructure> equipes = StructuresRechercheHelper.equipesDeRecherchePourUniteRecherche(edc(), ra.toStructure());
				
				for (EOStructure equipe : equipes) {
					if (!"".equals(equipesDeRecherche)) {
						equipesDeRecherche += ", ";
					}
					equipesDeRecherche += equipe.llStructure();
				}
			}
		}
		
		return equipesDeRecherche;
	}

	
	public String getEmployeur() {
    	return PersonnelRechercheUtilities.employeurPourPersonnel(edc(), getPersonnel());
    }
	
	public String getAdressePro() {
		
		EOQualifier qualifier = ERXQ.and(
				EORepartPersonneAdresse.TADR_CODE.eq(EOTypeAdresse.TADR_CODE_PRO),
				EORepartPersonneAdresse.TO_INDIVIDUS.eq(getPersonnel()), 
				EORepartPersonneAdresse.RPA_VALIDE.eq(EORepartPersonneAdresse.RPA_VALIDE_OUI)
				);
		
		EORepartPersonneAdresse.fetchAll(edc(), qualifier);
		
		EORepartPersonneAdresse rpa = EORepartPersonneAdresse.fetchFirstByQualifier(edc(), qualifier);
		
		if (rpa == null || rpa.toAdresse() == null) {
			return "";
		}
		
		String adresse = "";
		if (!"".equals(rpa.toAdresse().adrAdresse1()) && rpa.toAdresse().adrAdresse1() != null) {
			adresse += rpa.toAdresse().adrAdresse1();
		}
		
		if (!"".equals(rpa.toAdresse().adrAdresse2()) && rpa.toAdresse().adrAdresse2() != null) {
			adresse += "<br/>" + rpa.toAdresse().adrAdresse2();
		}
		
		if (!"".equals(rpa.toAdresse().adrAdresse3()) && rpa.toAdresse().adrAdresse3() != null) {
			adresse += "<br/>" + rpa.toAdresse().adrAdresse3();
		}
		
		if (!"".equals(rpa.toAdresse().adrAdresse4()) && rpa.toAdresse().adrAdresse4() != null) {
			adresse += "<br/>" + rpa.toAdresse().adrAdresse4();
		}
		
		if (!"".equals(rpa.toAdresse().getCPCache()) && rpa.toAdresse().getCPCache() != null) {
			adresse += "<br/>" + rpa.toAdresse().getCPCache();
		}
		
		if (!"".equals(rpa.toAdresse().ville()) && rpa.toAdresse().ville() != null) {
			adresse += " " + rpa.toAdresse().ville();
		}
		
		return adresse;
	}	
    
	public String getCodeCNU() {
		if (getCurrentCarriereSpecialisation() != null && getCurrentCarriereSpecialisation().toCnu() != null) {
			return getCurrentCarriereSpecialisation().toCnu().cSectionCnu() + " - " + getCurrentCarriereSpecialisation().toCnu().llSectionCnu();
		}
		return "";
	}
	
	public String getTypeContratTravail() {
		
		if (getContratsActuels().count() == 0 && getCurrentCarriereSpecialisation() == null) {
			return "";
		}
		
		if (getCurrentCarriereSpecialisation() != null && getCurrentCarriereSpecialisation().toCarriere() != null 
				&& getCurrentCarriereSpecialisation().toCarriere().toTypePopulation() != null) {
			return getCurrentCarriereSpecialisation().toCarriere().toTypePopulation().llTypePopulation();
		}
		
		if (getContratsActuels().get(0).toTypeContratTravail() != null) {
			return getContratsActuels().get(0).toTypeContratTravail().llTypeContratTrav();
		}
		
		return "";
	}
	
	public String getCodeBAP() {
		if (getCurrentCarriereSpecialisation() != null && getCurrentCarriereSpecialisation().toBap() != null) {
			return getCurrentCarriereSpecialisation().toBap().lcBap() + " - " + getCurrentCarriereSpecialisation().toBap().llBap();
		}

		if (getCurrentCarriereSpecialisation() != null && getCurrentCarriereSpecialisation().toReferensEmplois() != null) {
			return getCurrentCarriereSpecialisation().toReferensEmplois().intitulEmploi();
		}
		
		for (EOContrat contrat : getContratsActuels()) {
			if (contrat.avenantActuel() != null && contrat.avenantActuel().toBap() != null) {
				return contrat.avenantActuel().toBap().lcBap() + " - " + contrat.avenantActuel().toBap().llBap();
			}
		}
		
		return "";
	}
	
	public String getCategorie() {
		if (getGradePourIndividu() != null && getGradePourIndividu().toCategorie() != null) {
			return getGradePourIndividu().toCategorie().lcCategorie();
		}
		for (EOContrat contrat : getContratsActuels()) {
			if (contrat.avenantActuel() != null && contrat.avenantActuel().toCategorie() != null) {
				return contrat.avenantActuel().toCategorie().lcCategorie();
			}
		}
		return "";
	}
	
	public String getQuotite() {
		EOModalitesService modaliteService = EOModalitesService.rechercherModaliteServiceCourantePourIndividu(edc(), getPersonnel());
		if (modaliteService != null) {
			return modaliteService.quotite() + " %";
		}
		return "100 %";
	}
	
	public boolean hasHistorique() {
		return ((repartsAssociationsHistoriqueStructureRechercheStatut().count() + repartsAssociationsHistoriqueStructureRechercheFonction().count()) > 0);
	}
	
	private NSArray<EORepartAssociation> associationsMembreUniteRecherche(NSArray<EORepartAssociation> rasATrier) {
		
		if (rasATrier == null) {
			return null;
		}
		
		NSArray<EORepartAssociation> raTriees = new NSMutableArray<EORepartAssociation>();
		
		EOAssociation associationMembresUniteRecherche = FactoryAssociation.shared().typesMembresUniteRechercheAssociation(edc());
		
		for (EORepartAssociation ra : rasATrier) {
			if (ra.toAssociation() != null) {
				NSArray<EOAssociation> associationsPere = ra.toAssociation().getPeres(edc());
				if (associationsPere != null && associationsPere.contains(associationMembresUniteRecherche)) {
					raTriees.add(ra);
				}
			}
		}
		
		return raTriees;
	}
	
	public EOAssociation responsableAdministratifAssociation() {
		return getFactoryAssociation().secretraireAdministrativeAssociation(edc());
	}

	public EOAssociation directionAssociation() {
		return getFactoryAssociation().directionAssociation(edc());
	}

	public EOAssociation directionAdjointeAssociation() {
		return getFactoryAssociation().directeurAdjointAssociation(edc());
	}
	
	private NSArray<EORepartAssociation> associationsNonMembreUniteRecherche(NSArray<EORepartAssociation> rasATrier) {
		
		if (rasATrier == null) {
			return null;
		}
		
		NSArray<EORepartAssociation> raTriees = new NSMutableArray<EORepartAssociation>();
		
		for (EORepartAssociation ra : rasATrier) {
			if (ra.toAssociation() != null) {
				
				if (ra.toAssociation().equals(responsableAdministratifAssociation()) 
						|| ra.toAssociation().equals(directionAssociation()) 
						|| ra.toAssociation().equals(directionAdjointeAssociation())) {
					
					raTriees.add(ra);
					
				}
			}
		}
		
		return raTriees;
	}
	
	public ImpressionService getImpressionService() {
		return impressionService;
	}
	
	public void setImpressionService(ImpressionService impressionService) {
		this.impressionService = impressionService;
	}
	
	/**
	 * Lance l'impression de la fiche personne
	 * @return null
	 */
	public WOActionResults imprimer() {
		try {
			impressionService.imprimerFichePersonne(fichePersonneBean());
		} catch (Exception e) {
			e.printStackTrace();
			session().addSimpleErrorMessage(e);
		}
	    return doNothing();
	  }

	private FichePersonneBean fichePersonneBean() {
		FichePersonneBean fichePersonneBean = new FichePersonneBean();

		fichePersonneBean.setNomAffichage(getPersonnel().nomAffichage());
		fichePersonneBean.setPrenomAffichage(getPersonnel().prenomAffichage());
		
		for (EORepartAssociation raGlobaux : lesRepartsActuels()) {
			FichePersonneRattachementBean rattachement = fichePersonneBean.new FichePersonneRattachementBean();
			
			rattachement.setStructure(raGlobaux.toStructure().llStructure());
			
			NSArray<EORepartAssociation> rasrg = new NSMutableArray<EORepartAssociation>();
			
			for (EORepartAssociation ra : repartsAssociationsStructureRechercheStatut()) {
				if (raGlobaux != null) {
					if (raGlobaux.toStructure().equals(ra.toStructure())) {
						rasrg.add(ra);
					}
				}
			}
			
			for (EORepartAssociation raStatut : rasrg) {
				FichePersonneStatutBean statut = fichePersonneBean.new FichePersonneStatutBean();
				statut.setStatut(raStatut.toAssociation().assLibelle());
				if (raStatut.rasDOuverture() != null) {
					statut.setDateDebut(new SimpleDateFormat("dd/MM/yyyy", new Locale("fr", "FR")).format(raStatut.rasDOuverture()));
				} else {
					statut.setDateDebut("null");
				}
				
				if (raStatut.rasDFermeture() != null) {
					statut.setDateFin(new SimpleDateFormat("dd/MM/yyyy", new Locale("fr", "FR")).format(raStatut.rasDFermeture()));
				} else {
					statut.setDateFin("null");
				}
				rattachement.addStatut(statut);
			}
			
			NSArray<EORepartAssociation> rasrgf = new NSMutableArray<EORepartAssociation>();
			
			for (EORepartAssociation ra : repartsAssociationsStructureRechercheFonction()) {
				if (raGlobaux != null) {
					if (raGlobaux.toStructure().equals(ra.toStructure())) {
						rasrgf.add(ra);
					}
				}
			}
			
			for (EORepartAssociation raFonction : rasrgf) {
				FichePersonneFonctionBean fonction = fichePersonneBean.new FichePersonneFonctionBean();
				fonction.setFonction(raFonction.toAssociation().assLibelle());
				if (raFonction.rasDOuverture() != null) {
					fonction.setDateDebut(new SimpleDateFormat("dd/MM/yyyy", new Locale("fr", "FR")).format(raFonction.rasDOuverture()));
				} else {
					fonction.setDateDebut("null");
				}
				
				if (raFonction.rasDFermeture() != null) {
					fonction.setDateFin(new SimpleDateFormat("dd/MM/yyyy", new Locale("fr", "FR")).format(raFonction.rasDFermeture()));
				} else {
					fonction.setDateFin("null");
				}
				rattachement.addFonction(fonction);
			}
			
			fichePersonneBean.addRattachementActuel(rattachement);
		}
		
		for (EOStructure str : structuresRechercheNonRepartAssociationPourIndividu()) {
			FichePersonneRattachementBean rattachement = fichePersonneBean.new FichePersonneRattachementBean();
			
			rattachement.setStructure(str.llStructure());
			
			fichePersonneBean.addRattachementActuel(rattachement);
		}
		
		fichePersonneBean.setPosition(getPositionPersonnel());
		fichePersonneBean.setComposante(getRattachementComposante());
		fichePersonneBean.setEtablissement(etablissementsAffectation());
		
		fichePersonneBean.setIsEnseignantChercheur(isEnseignantChercheur());
		
		if (isEnseignantChercheur()) {
			
			fichePersonneBean.setGrade(getGradePourIndividu().llGrade());
			if (getDateDoctorat() != null) {
				fichePersonneBean.setDateDoctorat(new SimpleDateFormat("yyyy", new Locale("fr", "FR")).format(getDateDoctorat()));
			} else {
				fichePersonneBean.setDateDoctorat("--");
			}
			if (getDateHdr() != null) {
				fichePersonneBean.setDateHdr(new SimpleDateFormat("yyyy", new Locale("fr", "FR")).format(getDateHdr()));
			} else {
				fichePersonneBean.setDateHdr("--");
			}
			if (!"".equals(equipesDeRecherche())) {
				fichePersonneBean.setEquipeRattachement(equipesDeRecherche());
			} else {
				fichePersonneBean.setEquipeRattachement("--");
			}
			fichePersonneBean.setEmployeur(getEmployeur());
			if (!"".equals(getAdressePro())) {
				fichePersonneBean.setAdressePro(getAdressePro());
			} else {
				fichePersonneBean.setAdressePro("--");
			}
			fichePersonneBean.setCodeCnu(getCodeCNU());
			fichePersonneBean.setTypeContrat(getTypeContratTravail());
			
		} else {
			EOGrade grade = getGradePourIndividu();
			if (grade != null) {
				fichePersonneBean.setCodeBap(grade.llGrade());
			}
			fichePersonneBean.setCategorie(getCategorie());
			fichePersonneBean.setQuotite(getQuotite());
		}
		
		boolean hasHistorique = false;
		
		for (EORepartAssociation raGlobaux : lesRepartsPrecedents()) {
			hasHistorique = true;
			FichePersonneRattachementBean rattachement = fichePersonneBean.new FichePersonneRattachementBean();
			
			rattachement.setStructure(raGlobaux.toStructure().llStructure());
			
			NSArray<EORepartAssociation> rasrg = new NSMutableArray<EORepartAssociation>();
			
			for (EORepartAssociation ra : repartsAssociationsHistoriqueStructureRechercheStatut()) {
				if (raGlobaux != null) {
					if (raGlobaux.toStructure().equals(ra.toStructure())) {
						rasrg.add(ra);
					}
				}
			}
			
			for (EORepartAssociation raStatut : rasrg) {
				FichePersonneStatutBean statut = fichePersonneBean.new FichePersonneStatutBean();
				statut.setStatut(raStatut.toAssociation().assLibelle());
				if (raStatut.rasDOuverture() != null) {
					statut.setDateDebut(new SimpleDateFormat("dd/MM/yyyy", new Locale("fr", "FR")).format(raStatut.rasDOuverture()));
				} else {
					statut.setDateDebut("null");
				}
				
				if (raStatut.rasDFermeture() != null) {
					statut.setDateFin(new SimpleDateFormat("dd/MM/yyyy", new Locale("fr", "FR")).format(raStatut.rasDFermeture()));
				} else {
					statut.setDateFin("null");
				}
				rattachement.addStatut(statut);
			}
			
			NSArray<EORepartAssociation> rasrgf = new NSMutableArray<EORepartAssociation>();
			
			for (EORepartAssociation ra : repartsAssociationsHistoriqueStructureRechercheFonction()) {
				if (raGlobaux != null) {
					if (raGlobaux.toStructure().equals(ra.toStructure())) {
						rasrgf.add(ra);
					}
				}
			}
			
			for (EORepartAssociation raFonction : rasrgf) {
				FichePersonneFonctionBean fonction = fichePersonneBean.new FichePersonneFonctionBean();
				fonction.setFonction(raFonction.toAssociation().assLibelle());
				if (raFonction.rasDOuverture() != null) {
					fonction.setDateDebut(new SimpleDateFormat("dd/MM/yyyy", new Locale("fr", "FR")).format(raFonction.rasDOuverture()));
				} else {
					fonction.setDateDebut("null");
				}
				
				if (raFonction.rasDFermeture() != null) {
					fonction.setDateFin(new SimpleDateFormat("dd/MM/yyyy", new Locale("fr", "FR")).format(raFonction.rasDFermeture()));
				} else {
					fonction.setDateFin("null");
				}
				rattachement.addFonction(fonction);
			}
			
			fichePersonneBean.addRattachementPrecedent(rattachement);
		}
		
		if (!hasHistorique) {
			FichePersonneRattachementBean rattachement = fichePersonneBean.new FichePersonneRattachementBean();
			rattachement.setStructure("--");
			fichePersonneBean.addRattachementPrecedent(rattachement);
		}
		
		return fichePersonneBean;
	}
}