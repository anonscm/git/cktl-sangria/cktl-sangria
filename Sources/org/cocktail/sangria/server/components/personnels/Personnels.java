package org.cocktail.sangria.server.components.personnels;

import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartStructure;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartTypeGroupe;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypeGroupe;
import org.cocktail.fwkcktlrecherche.server.util.StructuresRechercheHelper;
import org.cocktail.sangria.server.components.commons.ASangriaComponent;
import org.cocktail.sangria.server.components.ecoledoctorale.EcoleDoctoraleConsultation;
import org.cocktail.sangria.server.components.federationrecherche.FederationRechercheConsultation;
import org.cocktail.sangria.server.components.uniterecherche.UniteRechercheConsultation;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

import er.extensions.eof.ERXEC;
import er.extensions.eof.ERXEOControlUtilities;
import er.extensions.eof.ERXQ;

public class Personnels extends ASangriaComponent {
	
	private EOIndividu selectedPersonnel;
	private EOStructure currentStructureRecherche;
	
	private EOEditingContext editingContext;
	
    public Personnels(WOContext context) {
        super(context);
    }
    
    public EOEditingContext editingContext() {
    	if (editingContext == null) {
    		editingContext = ERXEC.newEditingContext();
    	}
    	return editingContext;
    }
    
    public String detailsPersonnelContainerId() {
    	return getComponentId() + "detailsPersonnelContainer";
    }
	public void setSelectedPersonnel(EOIndividu selectedPersonnel) {
		this.selectedPersonnel = selectedPersonnel;
	}

	public EOIndividu selectedPersonnel() {
		return selectedPersonnel;
	}
	
	public EOQualifier qualifierPourIndividusRecherche() {
		return 
			ERXQ.or(
				EOIndividu.TO_REPART_STRUCTURES
					.dot(EORepartStructure.TO_STRUCTURE_GROUPE)
					.dot(EOStructure.TO_REPART_TYPE_GROUPES)
					.dot(EORepartTypeGroupe.TGRP_CODE)
						.eq(EOTypeGroupe.TGRP_CODE_SR),
				EOIndividu.TO_REPART_ASSOCIATIONS
					.dot(EORepartAssociation.TO_STRUCTURE)
					.dot(EOStructure.TO_REPART_TYPE_GROUPES)
					.dot(EORepartTypeGroupe.TGRP_CODE)
						.eq(EOTypeGroupe.TGRP_CODE_SR)
			);
	}
	
	
	public NSArray<EOStructure> structuresDeRecherche() {
		return StructuresRechercheHelper.structuresRecherchePourIndividu(edc(), selectedPersonnel());
	}

	public EOStructure getCurrentStructureRecherche() {
		return currentStructureRecherche;
	}

	public void setCurrentStructureRecherche(EOStructure currentStructureRecherche) {
		this.currentStructureRecherche = currentStructureRecherche;
	}
	
	public WOActionResults ouvrirCurrentStructureRecherche() {
		if (StructuresRechercheHelper.isUniteDeRecherche(getCurrentStructureRecherche())) {
			EOEditingContext e = ERXEC.newEditingContext();
			UniteRechercheConsultation uniteRechercheConsultation = (UniteRechercheConsultation) pageWithName(UniteRechercheConsultation.class.getName());
			uniteRechercheConsultation.setUniteRecherche(ERXEOControlUtilities.localInstanceOfObject(e, getCurrentStructureRecherche()));
			uniteRechercheConsultation.setModification(true);
			uniteRechercheConsultation.setConsultation(false);	
			uniteRechercheConsultation.setEdc(e);
			return uniteRechercheConsultation;
		} else if (StructuresRechercheHelper.isFederationDeRecherche(getCurrentStructureRecherche())) {
			EOEditingContext e = ERXEC.newEditingContext();
			FederationRechercheConsultation federationDeRechercheConsultation = (FederationRechercheConsultation) pageWithName(FederationRechercheConsultation.class.getName());
			federationDeRechercheConsultation.setFederationRecherche(ERXEOControlUtilities.localInstanceOfObject(e, getCurrentStructureRecherche()));
			federationDeRechercheConsultation.setEdc(e);
			return federationDeRechercheConsultation;
		} else if (StructuresRechercheHelper.isEcoleDoctorale(getCurrentStructureRecherche())) {
			EOEditingContext e = ERXEC.newEditingContext();
			EcoleDoctoraleConsultation ecoleDoctoraleConsultation = (EcoleDoctoraleConsultation) pageWithName(EcoleDoctoraleConsultation.class.getName());
			ecoleDoctoraleConsultation.setEcoleDoctorale(ERXEOControlUtilities.localInstanceOfObject(e, getCurrentStructureRecherche()));
			ecoleDoctoraleConsultation.setEdc(e);
			return ecoleDoctoraleConsultation;
		}
		return doNothing();
	}
	
	public Boolean nePeutVoirCurrentStructureRecherche() {
		
		if (StructuresRechercheHelper.isUniteDeRecherche(getCurrentStructureRecherche())) {
			return !session().rechercheApplicationAutorisationCache().hasDroitUtilisationUniteRecherche();
		} else if (StructuresRechercheHelper.isFederationDeRecherche(getCurrentStructureRecherche())) {
			return !session().rechercheApplicationAutorisationCache().hasDroitUtilisationFederationsRecherche();
		} else if (StructuresRechercheHelper.isEcoleDoctorale(getCurrentStructureRecherche())) {
			return !session().rechercheApplicationAutorisationCache().hasDroitUtilisationEcolesDoctorales();
		}
		return true;
	}
	
	/**
	 * @return la page avec les informations de la personne selectionnee
	 */
	public WOActionResults ouvrirFichePersonne() {
		FichePersonne page = (FichePersonne) pageWithName(FichePersonne.class.getName());
		page.setEdc(edc());
		page.setPersonnel(selectedPersonnel());
		return page;
	}
	
}