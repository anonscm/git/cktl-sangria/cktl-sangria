package org.cocktail.sangria.server.components;

import java.util.GregorianCalendar;

import org.cocktail.fwkcktlajaxwebext.serveur.component.CktlAjaxWindow;
import org.cocktail.fwkcktlpersonne.common.eospecificites.EOStructureForGroupeSpec;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartStructure;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlrecherche.server.metier.EOProgramme;
import org.cocktail.fwkcktlrecherche.server.metier.EOProgrammeAnnee;
import org.cocktail.sangria.server.components.commons.ASangriaComponent;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WODisplayGroup;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

import er.ajax.AjaxUpdateContainer;
import er.extensions.appserver.ERXDisplayGroup;
import er.extensions.eof.ERXDatabaseDataSource;
import er.extensions.eof.ERXFetchSpecification;
import er.extensions.eof.ERXQ;
import er.extensions.foundation.ERXArrayUtilities;
import er.extensions.foundation.ERXTimestampUtilities;

public class FinanceursEtProgrammes extends ASangriaComponent {

  private static final long serialVersionUID = 5308756550157327923L;
  private final static String GROUPE_FINANCEURS_AAP_KEY = "GROUPE_FINANCEURS_AAP";

  private static Integer SUPPRESSION_PROGRAMME_ETAPE_CHOIX = 1;
  private static Integer SUPPRESSION_PROGRAMME_ETAPE_VALIDATION = 2;

  private static Integer AJOUT_ANNEE_ETAPE_SAISIE = 1;
  private static Integer AJOUT_ANNEE_ETAPE_CHOIX = 2;
  private static Integer AJOUT_ANNEE_ETAPE_NOUVEAU_PROGRAMME = 3;
  private static Integer AJOUT_ANNEE_ETAPE_REPORT = 4;
  private EOStructure groupeDesFinanceurs;

  private EOStructure currentFinanceur;
  private EOStructure nouveauFinanceur;
  private EOStructure selectedFinanceur;

  private ERXDisplayGroup<EOStructure> financeursDisplayGroup;
  private ERXDatabaseDataSource financeursDataSource;

  private ERXDisplayGroup<EOProgramme> programmesDisplayGroup;
  private ERXDatabaseDataSource programmesDataSource;
  private ERXFetchSpecification<EOProgramme> programmesFetchSpecification;

  private ERXDisplayGroup<EOProgramme> sousProgrammesDisplayGroup;
  private ERXDatabaseDataSource sousProgrammesDataSource;
  private ERXFetchSpecification<EOProgramme> sousProgrammesFetchSpecification;

  private EOProgramme currentProgramme;
  private EOProgramme selectedProgramme;

  private EOProgramme currentSousProgramme;
  private EOProgramme selectedSousProgramme;

  private Integer currentAnnee;
  private Integer selectedAnnee;

  private String editingCodeProgramme;
  private String editingLibelleProgramme;

  private Boolean choixSupprimerAnneeSelectionnee;
  private Boolean choixSupprimerToutesLesAnnees;

  private Integer suppressionProgrammeEtape;

  private Integer ajoutAnneeEtape;

  private Integer editingAnnee;

  private NSArray<EOProgramme> selectionDesProgrammesAReporter = new NSMutableArray<EOProgramme>();

  public FinanceursEtProgrammes(WOContext context) {
    super(context);
  }

  public String financeursTableViewId() {
    return getComponentId() + "_financeursTableView";
  }

  public String financeursTableViewContainerId() {
    return getComponentId() + "_financeursTableViewContainer";
  }

  public String ajoutFinanceurWindowId() {
    return getComponentId() + "_ajoutFinanceurWindow";
  }

  public String programmesContainerId() {
    return getComponentId() + "°programmesContainer";
  }

  public String programmesTableViewId() {
    return getComponentId() + "_programmesTableView";
  }

  public String ajoutProgrammeWindowId() {
    return getComponentId() + "_ajoutProgrammeWindow";
  }

  public String suppressionProgrammeWindowId() {
    return getComponentId() + "_suppressionProgrammeWindow";
  }

  public String suppressionProgrammeWindowContentContainerId() {
    return getComponentId() + "_suppressionProgrammeWindowContentContainer";
  }

  public String sousProgrammesContainerId() {
    return getComponentId() + "_sousProgrammesContainer";
  }

  public String sousProgrammesTableViewId() {
    return getComponentId() + "_sousProgrammesTableView";
  }

  public String ajoutSousProgrammeWindowId() {
    return getComponentId() + "_ajoutSousProgrammeWindow";
  }

  public String suppressionSousProgrammeWindowId() {
    return getComponentId() + "_suppressionSousProgrammeWindow";
  }

  public String suppressionSousProgrammeWindowContentContainerId() {
    return getComponentId() + "_suppressionSousProgrammeWindowContentContainer";
  }

  public String ajoutAnneeWindowId() {
    return getComponentId() + "_ajoutAnneeWindow";
  }

  public String ajoutAnneeWindowContentContainerId() {
    return getComponentId() + "_ajoutAnneeWindowContentContainer";
  }

  public EOStructure getCurrentFinanceur() {
    return currentFinanceur;
  }

  public void setCurrentFinanceur(EOStructure currentFinanceur) {
    this.currentFinanceur = currentFinanceur;
  }

  public EOStructure getGroupeDesFinanceurs() throws Exception {
    if (groupeDesFinanceurs == null) {
      try {
        groupeDesFinanceurs = EOStructureForGroupeSpec.getGroupeForParamKey(edc(), GROUPE_FINANCEURS_AAP_KEY);
      }
      catch (Exception e) {
        session().addSimpleErrorMessage("Erreur", session().localizer().localizedStringForKey("echecRecuperationFinanceurs"));
        throw e;
      }
    }
    return groupeDesFinanceurs;
  }

  public void setGroupeDesFinanceurs(EOStructure groupeDesFinanceurs) {
    this.groupeDesFinanceurs = groupeDesFinanceurs;
  }

  public ERXDatabaseDataSource financeursDataSource() {

    if (financeursDataSource == null) {
      financeursDataSource = new ERXDatabaseDataSource(edc(), EOStructure.ENTITY_NAME);

      EOStructure _groupeDesFinanceurs;
      try {
        _groupeDesFinanceurs = getGroupeDesFinanceurs();
      }
      catch (Exception e) {
        return null;
      }

      EOQualifier qualifier = EOStructure.TO_REPART_STRUCTURES.dot(EORepartStructure.TO_STRUCTURE_GROUPE).eq(_groupeDesFinanceurs);
      ERXFetchSpecification<EOStructure> fetchSpecification = new ERXFetchSpecification<EOStructure>(EOStructure.ENTITY_NAME, qualifier, EOStructure.LL_STRUCTURE.ascs());
      financeursDataSource.setFetchSpecification(fetchSpecification);

    }

    return financeursDataSource;

  }

  public ERXDisplayGroup<EOStructure> financeursDisplayGroup() {
    if (financeursDisplayGroup == null) {
      financeursDisplayGroup = new ERXDisplayGroup<EOStructure>();
      financeursDisplayGroup.setDataSource(financeursDataSource());
      financeursDisplayGroup.setDelegate(new FinanceursDisplayGroupDelegate());
      financeursDisplayGroup.fetch();
    }

    return financeursDisplayGroup;
  }

  public EOStructure getNouveauFinanceur() {
    return nouveauFinanceur;
  }

  public void setNouveauFinanceur(EOStructure nouveauFinanceur) {
    this.nouveauFinanceur = nouveauFinanceur;
  }

  public EOStructure getSelectedFinanceur() {
    return selectedFinanceur;
  }

  public void setSelectedFinanceur(EOStructure selectedFinanceur) {
    this.selectedFinanceur = selectedFinanceur;
    selectedAnnee = null;
    updateProgrammes();
    //updateSousProgrammes();
  }

  public WOActionResults validerAjoutFinanceur() {

    if (getNouveauFinanceur() == null) {
      session().addSimpleErrorMessage("Erreur", session().localizer().localizedStringForKey("gestionFinanceursEtProgrammes.nouveauFinanceur.aucunFinanceurSelectionne"));
      return doNothing();
    }

    EOStructure _groupeDesFinanceurs;
    try {
      _groupeDesFinanceurs = getGroupeDesFinanceurs();
    }
    catch (Exception e) {
      return doNothing();
    }

    if (EOStructureForGroupeSpec.isPersonneInGroupe(getNouveauFinanceur(), _groupeDesFinanceurs)) {
      session().addSimpleErrorMessage("Erreur", session().localizer().localizedStringForKey("gestionFinanceursEtProgrammes.nouveauFinanceur.financeurDejaPresent"));
      return doNothing();
    }

    EOStructureForGroupeSpec.affecterPersonneDansGroupe(edc(), getNouveauFinanceur(), _groupeDesFinanceurs, getUtilisateurPersId());

    try {
      edc().saveChanges();
    }
    catch (ValidationException e) {
      session().addSimpleErrorMessage("Erreur", e);
      edc().revert();
      return doNothing();
    }

    CktlAjaxWindow.close(context(), ajoutFinanceurWindowId());

    financeursDisplayGroup().fetch();

    return doNothing();
  }

  public WOActionResults annulerAjoutFinanceur() {
    edc().revert();
    CktlAjaxWindow.close(context(), ajoutFinanceurWindowId());
    return doNothing();
  }

  public WOActionResults supprimerFinanceur() {

    if (getSelectedFinanceur() == null) {
      session().addSimpleErrorMessage("Erreur", session().localizer().localizedStringForKey("gestionFinanceursEtProgrammes.nouveauFinanceur.aucunFinanceurSelectionne"));
      return doNothing();
    }

    try {
      EOStructureForGroupeSpec.supprimerPersonneDuGroupe(edc(), getSelectedFinanceur(), getGroupeDesFinanceurs(), getUtilisateurPersId());
      edc().saveChanges();
      financeursDisplayGroup().fetch();
      financeursDisplayGroup().updateDisplayedObjects();
    }
    catch (ValidationException e) {
      session().addSimpleErrorMessage("Erreur", e);
      edc().revert();
    }
    catch (Exception e) {
      edc().revert();
    }
    return doNothing();
  }

  public Boolean selectedFinanceurAAuMoinsUnProgrammeSurUneAnnee() {
    return EOProgrammeAnnee.anneesPourStructure(edc(), getSelectedFinanceur()).isEmpty() == false;
  }

  public NSArray<Integer> anneesPourFinanceur() {
    return EOProgrammeAnnee.anneesPourStructure(edc(), getSelectedFinanceur());
  }

  public Integer getCurrentAnnee() {
    return currentAnnee;
  }

  public void setCurrentAnnee(Integer currentAnnee) {
    this.currentAnnee = currentAnnee;
  }

  public Integer getSelectedAnnee() {
    if (selectedAnnee == null) {
      GregorianCalendar gc = new GregorianCalendar();
      gc.setTime(ERXTimestampUtilities.today());
      selectedAnnee = gc.get(GregorianCalendar.YEAR);
      if(anneesPourFinanceur().contains(selectedAnnee) == false) {
        if(anneesPourFinanceur().isEmpty()) {
          selectedAnnee = null;
        } else {
          selectedAnnee = anneesPourFinanceur().get(0);
        }
      }
    }
    return selectedAnnee;
  }

  public void setSelectedAnnee(Integer selectedAnnee) {
    if (selectedAnnee.equals(getSelectedAnnee()) == false) {
      this.selectedAnnee = selectedAnnee;
      updateProgrammes();
    }
  }

  public ERXDisplayGroup<EOProgramme> getProgrammesDisplayGroup() {
    if (programmesDisplayGroup == null) {
      programmesDisplayGroup = new ERXDisplayGroup<EOProgramme>();
      programmesDisplayGroup.setDataSource(getProgrammesDataSource());
      programmesDisplayGroup.setDelegate(new ProgrammesDisplayGroupDelegate());
      programmesDisplayGroup.setQualifier(EOProgramme.NIVEAU.eq(EOProgramme.NIVEAU_PROGRAMME));
      programmesDisplayGroup.fetch();
    }

    return programmesDisplayGroup;
  }

  public void setProgrammesDisplayGroup(ERXDisplayGroup<EOProgramme> programmesDisplayGroup) {
    this.programmesDisplayGroup = programmesDisplayGroup;
  }

  public ERXDatabaseDataSource getProgrammesDataSource() {
    if (programmesDataSource == null) {
      programmesDataSource = new ERXDatabaseDataSource(edc(), EOProgramme.ENTITY_NAME);

      programmesDataSource.setFetchSpecification(getProgrammesFetchSpecification());

    }

    return programmesDataSource;
  }

  public void setProgrammesDataSource(ERXDatabaseDataSource programmesDataSource) {
    this.programmesDataSource = programmesDataSource;
  }

  public ERXFetchSpecification<EOProgramme> getProgrammesFetchSpecification() {
    if (programmesFetchSpecification == null) {
      programmesFetchSpecification = new ERXFetchSpecification<EOProgramme>(EOProgramme.ENTITY_NAME, programmesQualifier(), EOProgramme.CODE.ascs());
    }
    return programmesFetchSpecification;
  }

  public void setProgrammesFetchSpecification(ERXFetchSpecification<EOProgramme> programmesFetchSpecification) {
    this.programmesFetchSpecification = programmesFetchSpecification;
  }

  public void updateProgrammesFetchSpecification() {
    getProgrammesFetchSpecification().setQualifier(programmesQualifier());
  }

  public EOQualifier programmesQualifier() {
    EOQualifier qualifier = EOProgramme.ANNEES.dot(EOProgrammeAnnee.ANNEE).eq(getSelectedAnnee()).and(EOProgramme.FINANCEUR.eq(getSelectedFinanceur()));
    return qualifier;
  }

  public EOProgramme getCurrentProgramme() {
    return currentProgramme;
  }

  public void setCurrentProgramme(EOProgramme currentProgramme) {
    this.currentProgramme = currentProgramme;
  }

  public EOProgramme getSelectedProgramme() {
    return selectedProgramme;
  }

  public void setSelectedProgramme(EOProgramme selectedProgramme) {
    this.selectedProgramme = selectedProgramme;
    //if (selectedProgramme != null) {
      updateSousProgrammes();
    //}
  }
  
  public void updateProgrammes() {
    getProgrammesFetchSpecification().setQualifier(programmesQualifier());
    //getProgrammesDataSource().setFetchSpecification(getSousProgrammesFetchSpecification());
    getProgrammesDisplayGroup().fetch();
  }

  public ERXDisplayGroup<EOProgramme> getSousProgrammesDisplayGroup() {
    if (sousProgrammesDisplayGroup == null) {
      sousProgrammesDisplayGroup = new ERXDisplayGroup<EOProgramme>();
      sousProgrammesDisplayGroup.setDataSource(getSousProgrammesDataSource());
      sousProgrammesDisplayGroup.setDelegate(new SousProgrammesDisplayGroupDelegate());
      sousProgrammesDisplayGroup.setQualifier(EOProgramme.NIVEAU.eq(EOProgramme.NIVEAU_SOUS_PROGRAMME));
      sousProgrammesDisplayGroup.fetch();
    }

    return sousProgrammesDisplayGroup;
  }

  public void setSousProgrammesDisplayGroup(ERXDisplayGroup<EOProgramme> sousProgrammesDisplayGroup) {
    this.sousProgrammesDisplayGroup = sousProgrammesDisplayGroup;
  }

  public ERXDatabaseDataSource getSousProgrammesDataSource() {
    if (sousProgrammesDataSource == null) {
      sousProgrammesDataSource = new ERXDatabaseDataSource(edc(), EOProgramme.ENTITY_NAME);

      EOQualifier qualifier = EOProgramme.ANNEES.dot(EOProgrammeAnnee.ANNEE).eq(getSelectedAnnee()).and(EOProgramme.FINANCEUR.eq(getSelectedFinanceur()));
      sousProgrammesDataSource.setFetchSpecification(getSousProgrammesFetchSpecification());

    }

    return sousProgrammesDataSource;
  }

  public void setSousProgrammesDataSource(ERXDatabaseDataSource sousProgrammesDataSource) {
    this.sousProgrammesDataSource = sousProgrammesDataSource;
  }

  public ERXFetchSpecification<EOProgramme> getSousProgrammesFetchSpecification() {
    if (sousProgrammesFetchSpecification == null) {
      sousProgrammesFetchSpecification = new ERXFetchSpecification<EOProgramme>(EOProgramme.ENTITY_NAME, sousProgrammesQualifier(), EOProgramme.CODE.ascs());
    }
    return sousProgrammesFetchSpecification;
  }

  public void setSousProgrammesFetchSpecification(ERXFetchSpecification<EOProgramme> sousProgrammesFetchSpecification) {
    this.sousProgrammesFetchSpecification = sousProgrammesFetchSpecification;
  }

  public void updateSousProgrammes() {
    getSousProgrammesFetchSpecification().setQualifier(sousProgrammesQualifier());
    //getSousProgrammesDataSource().setFetchSpecification(getSousProgrammesFetchSpecification());
    getSousProgrammesDisplayGroup().fetch();
    //getSousProgrammesDisplayGroup().updateDisplayedObjects();
  }

  public EOQualifier sousProgrammesQualifier() {
    EOQualifier qualifier = EOProgramme.ANNEES.dot(EOProgrammeAnnee.ANNEE).eq(getSelectedAnnee()).and(EOProgramme.FINANCEUR.eq(getSelectedFinanceur()));
    if(getSelectedProgramme() == null) {
      qualifier = ERXQ.and(qualifier, EOProgramme.CODE.isNull());
    } else {
      qualifier = ERXQ.and(qualifier, EOProgramme.CODE.startsWith(getSelectedProgramme().code()));
    }
    return qualifier;
  }

  public EOProgramme getCurrentSousProgramme() {
    return currentSousProgramme;
  }

  public void setCurrentSousProgramme(EOProgramme currentSousProgramme) {
    this.currentSousProgramme = currentSousProgramme;
  }

  public EOProgramme getSelectedSousProgramme() {
    return selectedSousProgramme;
  }

  public void setSelectedSousProgramme(EOProgramme selectedSousProgramme) {
    this.selectedSousProgramme = selectedSousProgramme;
  }

  public String getEditingCodeProgramme() {
    return editingCodeProgramme;
  }

  public void setEditingCodeProgramme(String editingCodeProgramme) {
    this.editingCodeProgramme = editingCodeProgramme;
  }

  public String getEditingLibelleProgramme() {
    return editingLibelleProgramme;
  }

  public void setEditingLibelleProgramme(String editingLibelleProgramme) {
    this.editingLibelleProgramme = editingLibelleProgramme;
  }

  public WOActionResults ajouterProgramme() {
    setEditingCodeProgramme(EOProgramme.propositionDeCodePourProgramme(edc(), getSelectedFinanceur()));
    setEditingLibelleProgramme(null);
    setEditingAnnee(getSelectedAnnee());
    return doNothing();
  }

  public WOActionResults validerAjoutProgramme() {

    EOProgramme nouveauProgramme = EOProgramme.creerInstance(edc());
    nouveauProgramme.setCode(getEditingCodeProgramme());
    nouveauProgramme.setLibelle(getEditingLibelleProgramme());
    nouveauProgramme.setFinanceurRelationship(getSelectedFinanceur());
    nouveauProgramme.setModifiable(true);
    nouveauProgramme.setPersIdCreation(getUtilisateurPersId());
    nouveauProgramme.setPersIdModification(getUtilisateurPersId());
    nouveauProgramme.setDCreation(ERXTimestampUtilities.today());
    nouveauProgramme.setDModification(ERXTimestampUtilities.today());
    nouveauProgramme.affecterUneAnnee(edc(), getEditingAnnee(), getUtilisateurPersId());

    if (nouveauProgramme.niveau() != EOProgramme.NIVEAU_PROGRAMME) {
      session().addSimpleErrorMessage("Erreur", session().localizer().localizedStringForKey("gestionFinanceursEtProgrammes.nouveauProgramme.codeNonConformeNiveau1"));
    }

    try {
      edc().saveChanges();
      updateProgrammes();
      CktlAjaxWindow.close(context());
    }
    catch (ValidationException e) {
      session().addSimpleErrorMessage("Erreur", e);
      edc().revert();
    }

    return doNothing();

  }

  public WOActionResults ajouterSousProgramme() {
    setEditingCodeProgramme(EOProgramme.propositionDeCodePourProgramme(edc(), getSelectedProgramme()));
    setEditingLibelleProgramme(null);
    return doNothing();
  }

  public Boolean ajouterSousProgrammeDisabled() {
    return getSelectedProgramme() == null || getSelectedProgramme().modifiable() == false;
  }

  public WOActionResults validerAjoutSousProgramme() {

    EOProgramme nouveauProgramme = EOProgramme.creerInstance(edc());
    nouveauProgramme.setCode(getEditingCodeProgramme());
    nouveauProgramme.setLibelle(getEditingLibelleProgramme());
    nouveauProgramme.setFinanceurRelationship(getSelectedFinanceur());
    nouveauProgramme.setModifiable(true);
    nouveauProgramme.setPersIdCreation(getUtilisateurPersId());
    nouveauProgramme.setPersIdModification(getUtilisateurPersId());
    nouveauProgramme.setDCreation(ERXTimestampUtilities.today());
    nouveauProgramme.setDModification(ERXTimestampUtilities.today());
    nouveauProgramme.affecterUneAnnee(edc(), getSelectedAnnee(), getUtilisateurPersId());

    if (nouveauProgramme.niveau() != EOProgramme.NIVEAU_SOUS_PROGRAMME) {
      session().addSimpleErrorMessage("Erreur", session().localizer().localizedStringForKey("gestionFinanceursEtProgrammes.nouveauProgramme.codeNonConformeNiveau2"));
    }

    try {
      edc().saveChanges();
      updateSousProgrammes();
      CktlAjaxWindow.close(context(), ajoutSousProgrammeWindowId());
    }
    catch (ValidationException e) {
      session().addSimpleErrorMessage("Erreur", e);
      edc().revert();
    }

    return doNothing();

  }

  public WOActionResults supprimerProgramme() {
    setChoixSupprimerAnneeSelectionnee(true);
    setChoixSupprimerToutesLesAnnees(false);
    setSuppressionProgrammeEtape(SUPPRESSION_PROGRAMME_ETAPE_CHOIX);
    return doNothing();
  }

  public Boolean supprimerProgrammeDisabled() {
    return getSelectedProgramme() == null || getSelectedProgramme().modifiable() == false;
  }

  public WOActionResults validerSuppressionProgramme() {
    setSuppressionProgrammeEtape(SUPPRESSION_PROGRAMME_ETAPE_VALIDATION);
    return doNothing();
  }

  public WOActionResults retourSuppressionProgrammeEtapeChoix() {
    setSuppressionProgrammeEtape(SUPPRESSION_PROGRAMME_ETAPE_CHOIX);
    return doNothing();
  }

  public WOActionResults annulerSuppressionProgramme() {
    edc().revert();
    CktlAjaxWindow.close(context(), suppressionProgrammeWindowId());
    return doNothing();
  }

  public WOActionResults confirmerSuppressionProgramme() {
    if (getChoixSupprimerAnneeSelectionnee()) {
      NSArray<EOProgrammeAnnee> _annees = getSelectedProgramme().annees(EOProgrammeAnnee.ANNEE.eq(getSelectedAnnee()));
      EOProgrammeAnnee _annee = ERXArrayUtilities.firstObject(_annees);
      _annee.delete();
    }
    else if (getChoixSupprimerToutesLesAnnees()) {
      getSelectedProgramme().delete();
    }

    try {
      edc().saveChanges();
      CktlAjaxWindow.close(context(), suppressionProgrammeWindowId());
      updateProgrammesFetchSpecification();
      getProgrammesDisplayGroup().fetch();
    }
    catch (ValidationException e) {
      session().addSimpleErrorMessage("Erreur", e);
      edc().revert();
    }

    return doNothing();
  }

  public String apercuDeLaSuppressionDuProgramme() {
    return _apercuDeLaSuppressionDuProgramme(getSelectedProgramme());
  }

  public String apercuDeLaSuppressionDuSousProgramme() {
    return _apercuDeLaSuppressionDuProgramme(getSelectedSousProgramme());
  }

  private String _apercuDeLaSuppressionDuProgramme(EOProgramme programme) {
    String _apercu = "Résumé de la suppression: \n\n";
    if (getChoixSupprimerToutesLesAnnees()) {
      NSArray<EOProgrammeAnnee> _annees = programme.annees(null, EOProgrammeAnnee.ANNEE.descs(), false);
      for (EOProgrammeAnnee _annee : _annees) {
        _apercu += "" + _annee.annee() + "\n";
        _apercu += "\t" + programme + "\n";
        NSArray<EOProgramme> _programmeFils = programme.programmesFils(_annee.annee());
        for (EOProgramme _programme : _programmeFils) {
          _apercu += "\t" + _programme + "\n";
        }
        _apercu += "\n";
      }
    }
    else if (getChoixSupprimerAnneeSelectionnee()) {
      _apercu += "" + getSelectedAnnee() + "\n";
      _apercu += "\t" + programme + "\n";
      NSArray<EOProgramme> _programmeFils = programme.programmesFils(getSelectedAnnee());
      for (EOProgramme _programme : _programmeFils) {
        _apercu += "\t" + _programme + "\n";
      }
      _apercu += "\n";
    }
    return _apercu;
  }

  public Boolean getChoixSupprimerAnneeSelectionnee() {
    return choixSupprimerAnneeSelectionnee;
  }

  public void setChoixSupprimerAnneeSelectionnee(Boolean choixSupprimerAnneeSelectionnee) {
    this.choixSupprimerAnneeSelectionnee = choixSupprimerAnneeSelectionnee;
  }

  public Boolean getChoixSupprimerToutesLesAnnees() {
    return choixSupprimerToutesLesAnnees;
  }

  public void setChoixSupprimerToutesLesAnnees(Boolean choixSupprimerToutesLesAnnees) {
    this.choixSupprimerToutesLesAnnees = choixSupprimerToutesLesAnnees;
  }

  public Integer getSuppressionProgrammeEtape() {
    return suppressionProgrammeEtape;
  }

  public void setSuppressionProgrammeEtape(Integer suppressionProgrammeEtape) {
    this.suppressionProgrammeEtape = suppressionProgrammeEtape;
  }

  public Boolean isSuppressionProgrammeEtapeChoix() {
    return getSuppressionProgrammeEtape().equals(SUPPRESSION_PROGRAMME_ETAPE_CHOIX);
  }

  public Boolean isSuppressionProgrammeEtapeValidation() {
    return getSuppressionProgrammeEtape().equals(SUPPRESSION_PROGRAMME_ETAPE_VALIDATION);
  }

  public WOActionResults supprimerSousProgramme() {
    setChoixSupprimerAnneeSelectionnee(true);
    setChoixSupprimerToutesLesAnnees(false);
    setSuppressionProgrammeEtape(SUPPRESSION_PROGRAMME_ETAPE_CHOIX);
    return doNothing();
  }

  public Boolean supprimerSousProgrammeDisabled() {
    return getSelectedSousProgramme() == null || getSelectedSousProgramme().modifiable() == false;
  }

  public WOActionResults validerSuppressionSousProgramme() {
    setSuppressionProgrammeEtape(SUPPRESSION_PROGRAMME_ETAPE_VALIDATION);
    return doNothing();
  }

  public WOActionResults retourSuppressionSousProgrammeEtapeChoix() {
    setSuppressionProgrammeEtape(SUPPRESSION_PROGRAMME_ETAPE_CHOIX);
    return doNothing();
  }

  public WOActionResults annulerSuppressionSousProgramme() {
    edc().revert();
    CktlAjaxWindow.close(context(), suppressionSousProgrammeWindowId());
    return doNothing();
  }

  public WOActionResults confirmerSuppressionSousProgramme() {
    if (getChoixSupprimerAnneeSelectionnee()) {
      NSArray<EOProgrammeAnnee> _annees = getSelectedSousProgramme().annees(EOProgrammeAnnee.ANNEE.eq(getSelectedAnnee()));
      EOProgrammeAnnee _annee = ERXArrayUtilities.firstObject(_annees);
      _annee.delete();
    }
    else if (getChoixSupprimerToutesLesAnnees()) {
      getSelectedSousProgramme().delete();
    }

    try {
      edc().saveChanges();
      CktlAjaxWindow.close(context(), suppressionSousProgrammeWindowId());
      updateSousProgrammes();
    }
    catch (ValidationException e) {
      session().addSimpleErrorMessage("Erreur", e);
      edc().revert();
    }

    return doNothing();
  }

  public Integer getAjoutAnneeEtape() {
    return ajoutAnneeEtape;
  }

  public void setAjoutAnneeEtape(Integer ajoutAnneeEtape) {
    this.ajoutAnneeEtape = ajoutAnneeEtape;
  }

  public Boolean isAjoutAnneeEtapeSaisie() {
    return getAjoutAnneeEtape().equals(AJOUT_ANNEE_ETAPE_SAISIE);
  }

  public Boolean isAjoutAnneeEtapeChoix() {
    return getAjoutAnneeEtape().equals(AJOUT_ANNEE_ETAPE_CHOIX);
  }

  public Boolean isAjoutAnneeEtapeNoueauProgramme() {
    return getAjoutAnneeEtape().equals(AJOUT_ANNEE_ETAPE_NOUVEAU_PROGRAMME);
  }

  public Boolean isAjoutAnneeEtapeReport() {
    return getAjoutAnneeEtape().equals(AJOUT_ANNEE_ETAPE_REPORT);
  }

  public Integer getEditingAnnee() {
    return editingAnnee;
  }

  public void setEditingAnnee(Integer editingAnnee) {
    this.editingAnnee = editingAnnee;
  }

  public WOActionResults ajouterAnnee() {
    setAjoutAnneeEtape(AJOUT_ANNEE_ETAPE_SAISIE);
    return doNothing();
  }

  public WOActionResults avancerVersAjoutAnneeEtapeChoix() {
    if (getEditingAnnee() == null) {
      session().addSimpleErrorMessage("Erreur", session().localizer().localizedStringForKey("gestionFinanceursEtProgrammes.ajoutAnnee.anneeNonSaisie"));
      return doNothing();
    }

    setAjoutAnneeEtape(AJOUT_ANNEE_ETAPE_CHOIX);

    return doNothing();
  }

  public WOActionResults retournerVersAjoutAnneeEtapeSaisie() {
    setAjoutAnneeEtape(AJOUT_ANNEE_ETAPE_SAISIE);
    return doNothing();
  }

  public WOActionResults avancerVersAjoutAnneeEtapeNouveauProgramme() {
    setEditingCodeProgramme(EOProgramme.propositionDeCodePourProgramme(edc(), getSelectedFinanceur()));
    setEditingLibelleProgramme(null);
    setAjoutAnneeEtape(AJOUT_ANNEE_ETAPE_NOUVEAU_PROGRAMME);
    return doNothing();
  }

  public WOActionResults avancerVersAjoutAnneeEtapeReport() {
    getSelectionDesProgrammesAReporter().clear();
    setAjoutAnneeEtape(AJOUT_ANNEE_ETAPE_REPORT);
    return doNothing();
  }

  public WOActionResults retournerVersAjoutAnneeEtapeChoix() {
    setAjoutAnneeEtape(AJOUT_ANNEE_ETAPE_CHOIX);
    return doNothing();
  }

  public WOActionResults annulerAjoutAnnee() {
    CktlAjaxWindow.close(context(), ajoutAnneeWindowId());
    return doNothing();
  }

  public NSArray<EOProgramme> getSelectionDesProgrammesAReporter() {
    return selectionDesProgrammesAReporter;
  }

  public void setSelectionDesProgrammesAReporter(NSArray<EOProgramme> selectionDesProgrammesAReporter) {
    this.selectionDesProgrammesAReporter = selectionDesProgrammesAReporter;
  }

  public WOActionResults validerReportDesProgrammes() {
    EOProgramme.reporterSelectionDeProgrammesSurAnnees(edc(), getSelectionDesProgrammesAReporter(), getEditingAnnee(), getUtilisateurPersId());
    try {
      edc().saveChanges();
      CktlAjaxWindow.close(context());
      updateProgrammesFetchSpecification();
      getProgrammesDisplayGroup().fetch();
    } catch (ValidationException e) {
      session().addSimpleErrorMessage("Erreur", e);
      edc().revert();
    }
    return doNothing();
  }

  public class FinanceursDisplayGroupDelegate {
    public void displayGroupDidChangeSelection(WODisplayGroup group) {
      ERXDisplayGroup<EOStructure> _displayGroup = (ERXDisplayGroup<EOStructure>) group;
      //if (_displayGroup.selectedObject() != null) {
        setSelectedFinanceur(_displayGroup.selectedObject());
      //}
    }
  }

  public class ProgrammesDisplayGroupDelegate {
    public void displayGroupDidChangeSelection(WODisplayGroup group) {
      ERXDisplayGroup<EOProgramme> _displayGroup = (ERXDisplayGroup<EOProgramme>) group;
      // if (_displayGroup.selectedObject() != null) {
      setSelectedProgramme(_displayGroup.selectedObject());
      // }
    }
  }

  public class SousProgrammesDisplayGroupDelegate {
    public void displayGroupDidChangeSelection(WODisplayGroup group) {
      ERXDisplayGroup<EOProgramme> _displayGroup = (ERXDisplayGroup<EOProgramme>) group;
      if (_displayGroup.selectedObject() != null) {
        setSelectedSousProgramme(_displayGroup.selectedObject());
      }
    }
  }

}