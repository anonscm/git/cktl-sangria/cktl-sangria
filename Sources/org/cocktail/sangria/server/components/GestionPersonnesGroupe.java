package org.cocktail.sangria.server.components;

import java.util.Date;

import org.cocktail.fwkcktlajaxwebext.serveur.component.CktlAjaxWindow;
import org.cocktail.fwkcktlpersonne.common.eospecificites.EOStructureForGroupeSpec;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IStructure;
import org.cocktail.fwkcktlrecherche.server.ParametresRecherche;
import org.cocktail.sangria.server.components.commons.ASangriaComponent;
import org.cocktail.sangria.server.controllers.GestionPersonnesGroupeController;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;

public abstract class GestionPersonnesGroupe extends ASangriaComponent {

	private static final long serialVersionUID = -4244920712314341906L;
	private IPersonne currentPersonne;
	private IStructure groupe;
	private IPersonne nouvellePersonne;
	private GestionPersonnesGroupeController gestionPersonnesGroupeController;

	/**
	 * @param context le context
	 */
	public GestionPersonnesGroupe(WOContext context) {
		super(context);
		
		try {
			setGroupe(EOStructureForGroupeSpec.getGroupeForParamKey(edc(), getGroupeParamKey()));
			setGestionPersonnesGroupeController(new GestionPersonnesGroupeController(getGroupe()));
		} catch (Exception e) {
			session().addSimpleErrorMessage("Erreur", getEchecRecuperationGroupeMessage());
		}
	}


	protected abstract String getGroupeParamKey();
	
	protected abstract String getEchecRecuperationGroupeMessage();
	
	public String getAjoutPersonneWindowId() {
		return getComponentId() + "_" + "AjoutPersonneWindow";
	}

	public String getPersonnesTableViewId() {
		return getComponentId() + "_" + "EtablissementsGestionnaireFinancierTableView";
	}

	public String getPersonneContainerId() {
		return getComponentId() + "_" + "EtablissementsGestionnaireFinancierContainer";
	}

	public IPersonne getCurrentPersonne() {
		return currentPersonne;
	}

	public void setCurrentPersonne(IPersonne currentPersonne) {
		this.currentPersonne = currentPersonne;
	}

	protected IStructure getGroupe() {
		return groupe;
	}
	
	public void setGroupe(IStructure groupe) {
		this.groupe = groupe;
	}

	public IPersonne getSelectedPersonne() {
		return gestionPersonnesGroupeController.getDisplayGroup().selectedObject();
	}

	public IPersonne getNouvellePersonne() {
		return nouvellePersonne;
	}

	public void setNouvellePersonne(IPersonne nouvellPersonne) {
		this.nouvellePersonne = nouvellPersonne;
	}

	/**
	 * Action qui ajoute la personne
	 * @return rien
	 */
	public WOActionResults ajouterPersonne() {
	
		if (getNouvellePersonne() == null) {
			session().addSimpleErrorMessage("Erreur", getAucunePersonneSelectionneeMessage());
			return doNothing();
		}
	
		try {
			gestionPersonnesGroupeController.ajouterPersonne(getNouvellePersonne(), getUtilisateurPersId());
		} catch (Exception e) {
			session().addSimpleErrorMessage("Erreur", e);
		}
	
		CktlAjaxWindow.close(context(), getAjoutPersonneWindowId());
	
	
		return doNothing();
	}

	/**
	 * @return Rien
	 */
	public WOActionResults annulerAjout() {
		CktlAjaxWindow.close(context(), getAjoutPersonneWindowId());
		return doNothing();
	}

	/**
	 * Action supprime l'établissement du groupe
	 * @return Rien
	 */
	public WOActionResults supprimerPersonne() {
	
		if (getSelectedPersonne() == null) {
			session().addSimpleErrorMessage("Erreur", getAucunePersonneSelectionneeMessage());
			return doNothing();
		}
	
		try {
			gestionPersonnesGroupeController.supprimerPersonne(getSelectedPersonne(), getUtilisateurPersId());
		} catch (Exception e) {
			session().addSimpleErrorMessage("Erreur", e);
		}
	
		return doNothing();
	}

	public GestionPersonnesGroupeController getGestionPersonnesGroupeController() {
		return gestionPersonnesGroupeController;
	}

	public void setGestionPersonnesGroupeController(GestionPersonnesGroupeController gestionPersonnesGroupeController) {
		this.gestionPersonnesGroupeController = gestionPersonnesGroupeController;
	}


	public String getAucunePersonneSelectionneeMessage() {
		return session().localizer().localizedStringForKey("aucunePersonneSelectionnee");
	}
	
	public String getCurrentNom() {
		if (currentPersonne instanceof IIndividu && getCurrentPersonne() != null) {
			return ((IIndividu) getCurrentPersonne()).nomUsuel();
		}
		return "-";
	}
	
	public String getCurrentPrenom() {
		if (currentPersonne instanceof IIndividu && getCurrentPersonne() != null) {
			return ((IIndividu) getCurrentPersonne()).prenom();
		}
		return "-";
	}
	
	public Date getCurrentDateNaissance() {
		if (getCurrentPersonne() instanceof IIndividu && getCurrentPersonne() != null) {
			return ((IIndividu) getCurrentPersonne()).dNaissance();
		}
		return null;
	}
	
}