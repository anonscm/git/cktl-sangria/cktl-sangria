package org.cocktail.sangria.server.components;

import org.cocktail.fwkcktlrecherche.server.ParametresRecherche;

import com.webobjects.appserver.WOContext;

/**
 * Gestion du groupe contenant les établissements gestionnaires financiers
 */
public class EtablissementGestionnaireFinancier extends GestionPersonnesGroupe {
	
	private static final long serialVersionUID = 1690684408296535317L;

	/**
	 * @param context le context
	 */
	public EtablissementGestionnaireFinancier(WOContext context) {
		super(context);
		getGestionPersonnesGroupeController().setAfficheIndividus(false);
	}

	@Override
	public String getEchecRecuperationGroupeMessage() {
		return session().localizer().localizedStringForKey("echecRecuperationGestionnaireFinancier");
	}
	
	@Override
	protected String getGroupeParamKey() {
		return ParametresRecherche.GROUPE_ETABLISSEMENTS_GESTIONNAIRE_FINANCIER_KEY;
	}



}