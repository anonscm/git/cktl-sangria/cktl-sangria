package org.cocktail.sangria.server.components.ecoledoctorale;

import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlrecherche.server.util.StructuresRechercheHelper;
import org.cocktail.sangria.server.Session;
import org.cocktail.sangria.server.components.commons.ASangriaComponent;
import org.cocktail.sangria.server.components.federationrecherche.FederationRechercheConsultation;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;

import er.extensions.eof.ERXEC;

public class EcolesDoctorales extends ASangriaComponent {
	private EOStructure currentEcoleDoctorale;
	
    public EcolesDoctorales(WOContext context) {
        super(context);
    }
    
    public NSArray<EOStructure> listeEcolesDoctorales() {
    	return StructuresRechercheHelper.ecolesDoctorale(edc());
    }


	public WOActionResults voirCurrentEcoleDoctorale() {
		EOEditingContext e = ERXEC.newEditingContext();
		EcoleDoctoraleConsultation ecoleDoctoraleConsultation = (EcoleDoctoraleConsultation) pageWithName(EcoleDoctoraleConsultation.class.getName());
		ecoleDoctoraleConsultation.setEdc(e);
		ecoleDoctoraleConsultation.setEcoleDoctorale(currentEcoleDoctorale().localInstanceIn(e));
		return ecoleDoctoraleConsultation;
	}

	public void setCurrentEcoleDoctorale(EOStructure currentEcoleDoctorale) {
		this.currentEcoleDoctorale = currentEcoleDoctorale;
	}

	public EOStructure currentEcoleDoctorale() {
		return currentEcoleDoctorale;
	}
}