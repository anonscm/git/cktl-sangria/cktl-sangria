package org.cocktail.sangria.server.components.ecoledoctorale;

import org.cocktail.fwkcktlpersonne.common.metier.EOAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlrecherche.server.metier.factory.FactoryAssociation;
import org.cocktail.sangria.server.components.commons.ASangriaComponent;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.validation.ERXValidationException;

public class EcoleDoctoraleConsultation extends ASangriaComponent {

	
	private EOStructure ecoleDoctorale;
	
    private Boolean ongletIdentificationSelected = true;

    private Boolean ongletPersonnelsSelected = false;
    
    private Boolean ongletDoctorantsSelected = false;

    private Boolean ongletGestionFinanciereSelected = false;
    private Boolean ongletMembresSelected = false;

    private Boolean ongletEvaluationSelected = false;

    public EcoleDoctoraleConsultation(WOContext context) {
        super(context);
    }


	

	public Boolean ongletIdentificationSelected() {
		return ongletIdentificationSelected;
	}
	
	/**
	 * Sets the onglet identification selected.
	 *
	 * @param ongletIdentificationSelected the new onglet identification selected
	 */
	public void setOngletIdentificationSelected(Boolean ongletIdentificationSelected) {
		this.ongletIdentificationSelected = ongletIdentificationSelected;
	}
	
	public void setOngletPersonnelsSelected(Boolean ongletPersonnelsSelected) {
		this.ongletPersonnelsSelected = ongletPersonnelsSelected;
	}

	public Boolean ongletPersonnelsSelected() {
		return ongletPersonnelsSelected;
	}
	
	public Boolean ongletDoctorantsSelected() {
		return ongletDoctorantsSelected;
	}




	public void setOngletDoctorantsSelected(Boolean ongletDoctorantsSelected) {
		this.ongletDoctorantsSelected = ongletDoctorantsSelected;
	}




	public void setOngletGestionFinanciereSelected(
			Boolean ongletGestionFinanciereSelected) {
		this.ongletGestionFinanciereSelected = ongletGestionFinanciereSelected;
	}

	public Boolean ongletGestionFinanciereSelected() {
		return ongletGestionFinanciereSelected;
	}

	public void setOngletEvaluationSelected(Boolean ongletEvaluationSelected) {
		this.ongletEvaluationSelected = ongletEvaluationSelected;
	}

	public Boolean ongletEvaluationSelected() {
		return ongletEvaluationSelected;
	}

	/**
     * Onglets container id.
     *
     * @return the string
     */
    public String ongletsContainerId() {
    	return getComponentId() + "_ongletsContainer";
    }
    
    /**
     * Onglet identification id.
     *
     * @return the string
     */
    public String ongletIdentificationId() {
    	return getComponentId() + "_ongletIdentification";
    }
 
    public String ongletPersonnelsId() {
    	return getComponentId() + "_ongletPersonnels";
    }
    
    public String ongletDoctorantsId() {
    	return getComponentId() + "_ongletDoctorants";
    }
    
    public String ongletMembresId() {
    	return getComponentId() + "_ongletMembres";
    }
    public String ongletGestionFinanciereId() {
    	return getComponentId() + "_ongletGestionFinanciere";
    }

    public void setOngletMembresSelected(Boolean ongletMembresSelected) {
		this.ongletMembresSelected = ongletMembresSelected;
	}




	public Boolean ongletMembresSelected() {
		return ongletMembresSelected;
	}




	public String ongletEvaluationId() {
    	return getComponentId() + "_ongletEvaluation";
    }
    
	public WOActionResults enregistrerLesModications() {
		
		try {
			ecoleDoctorale().setDModification(new NSTimestamp()); 	
			ecoleDoctorale().setPersIdModification(getUtilisateurPersId());
			edc().saveChanges();
			session().addSimpleSuccessMessage("Enregistrement effectué avec succés", null);

		} catch (ValidationException e) {
			session().addSimpleErrorMessage("Erreur", e);
		}
		return doNothing();
	}




	public void setEcoleDoctorale(EOStructure ecoleDoctorale) {
		this.ecoleDoctorale = ecoleDoctorale;
	}




	public EOStructure ecoleDoctorale() {
		return ecoleDoctorale;
	}
	
	
	public WOActionResults retourEcolesDoctorales() {
		return pageWithName(EcolesDoctorales.class.getName());
	}
	
	public EOAssociation typesMembresStructureRecherche() {
		return FactoryAssociation.shared().typesMembresStructureRechercheAssociation(edc());
	}
	
}