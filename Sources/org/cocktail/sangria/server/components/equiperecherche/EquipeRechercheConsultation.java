package org.cocktail.sangria.server.components.equiperecherche;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.foundation.NSTimestamp;

import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlsangriaguiajax.serveur.components.commons.AFwkCktlSangriaComponent;
import org.cocktail.sangria.server.components.commons.ASangriaComponent;

public class EquipeRechercheConsultation extends ASangriaComponent {
    
	private EOStructure equipeDeRecherche;
	
    private Boolean ongletIdentificationSelected = true;
	private Boolean ongletPersonnelsSelected = false;

	/** The consultation. */
	private Boolean consultation = false;
	
	/** The modification. */
	private Boolean modification = false;

	private Boolean isEditing;

    
    
	
	public EquipeRechercheConsultation(WOContext context) {
        super(context);
    }
	
	public WOActionResults retourListeDesEquipes() {
		
		return pageWithName(EquipesRechercheListe.class.getName());
		
	}


	
	public Boolean ongletIdentificationSelected() {
		return ongletIdentificationSelected;
	}
	
	public void setOngletIdentificationSelected(Boolean ongletIdentificationSelected) {
		this.ongletIdentificationSelected = ongletIdentificationSelected;
	}
	
	public Boolean ongletPersonnelsSelected() {
		return ongletPersonnelsSelected;
	}
	
	public void setOngletPersonnelsSelected(Boolean ongletPersonnelsSelected) {
		this.ongletPersonnelsSelected = ongletPersonnelsSelected;
	}
	
	/**
     * Onglets container id.
     *
     * @return the string
     */
    public String ongletsContainerId() {
    	return getComponentId() + "_ongletsContainer";
    }
    
    /**
     * Onglet identification id.
     *
     * @return the string
     */
    public String ongletIdentificationId() {
    	return getComponentId() + "_ongletIdentification";
    }

    public String ongletPersonnelsId() {
    	return getComponentId() + "_ongletPersonnels";
    }
    
	public void setEquipeDeRecherche(EOStructure equipeDeRecherche) {
		this.equipeDeRecherche = equipeDeRecherche;
	}


	public EOStructure equipeDeRecherche() {
		return equipeDeRecherche;
	}
    
	/**
     * Consultation.
     *
     * @return the boolean
     */
    public Boolean consultation() {
    	return consultation;
    }
    
    /**
     * Sets the consultation.
     *
     * @param consultation the new consultation
     */
    public void setConsultation(Boolean consultation) {
    	this.consultation = consultation;
    	
    	
    }
    
    /**
     * Modification.
     *
     * @return the boolean
     */
    public Boolean modification() {
    	if (consultation()) {
    		return false;
    	} else {
    		return modification;
    	}
    }
    
    /**
     * Sets the modification.
     *
     * @param modification the new modification
     */
    public void setModification(Boolean modification) {
    	this.modification = modification;
    }
    
	public WOActionResults enregistrerLesModications() {
		
		try {
			equipeDeRecherche().setDModification(new NSTimestamp()); 	
			edc().saveChanges();
			session().addSimpleSuccessMessage("Enregistrement OK", "Les modifications apportées à l'équipe de recherche ont été enregistrées");
		} catch (ValidationException e) {
			session().addSimpleErrorMessage("Erreur", e);
		}
		return doNothing();
	}


	public void setIsEditing(Boolean isEditing) {
		this.isEditing = isEditing;
	}

	public Boolean isEditing() {
		return isEditing;
	}
	
}