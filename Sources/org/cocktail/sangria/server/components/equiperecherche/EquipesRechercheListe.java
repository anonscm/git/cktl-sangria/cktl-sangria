package org.cocktail.sangria.server.components.equiperecherche;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;

import er.extensions.eof.ERXQ;

import org.cocktail.fwkcktlpersonne.common.eospecificites.EOStructureForGroupeSpec;
import org.cocktail.fwkcktlpersonne.common.metier.EOAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlrecherche.server.metier.EOGrandSecteurDisciplinaire;
import org.cocktail.fwkcktlrecherche.server.metier.factory.FactoryAssociation;
import org.cocktail.fwkcktlrecherche.server.util.StructuresRechercheHelper;
import org.cocktail.fwkcktlsangriaguiajax.serveur.components.commons.AFwkCktlSangriaComponent;
import org.cocktail.sangria.server.components.commons.ASangriaComponent;
import org.cocktail.sangria.server.components.uniterecherche.UniteRechercheConsultation;

public class EquipesRechercheListe extends ASangriaComponent {
	
    public final static Integer largeurLigneUnitesRecherche = 3;
	
	EOGrandSecteurDisciplinaire currentGrandSecteurDisciplinaire;
	EOStructure currentUniteRecherche;
	private Integer unitesRechercheCount;
	private EOStructure currentEquipeRecherche;
	private NSDictionary<EOGrandSecteurDisciplinaire, NSArray<EOStructure>> unitesPourGrandsSecteurs;
	private NSDictionary<EOStructure, NSArray<EOStructure>> equipesPourUnites;
	
    public EquipesRechercheListe(WOContext context) {
        super(context);
        unitesPourGrandsSecteurs = new NSMutableDictionary<EOGrandSecteurDisciplinaire, NSArray<EOStructure>>();
        equipesPourUnites = new NSMutableDictionary<EOStructure, NSArray<EOStructure>>();
    }

    
	public NSArray<EOGrandSecteurDisciplinaire> grandsSecteursDisciplinaires() {
		EOSortOrdering sortOrdering = EOSortOrdering.sortOrderingWithKey(EOGrandSecteurDisciplinaire.GSD_LIBELLE_KEY, EOSortOrdering.CompareAscending);
		return EOGrandSecteurDisciplinaire.fetchAll(edc(), new NSArray<EOSortOrdering>(sortOrdering));
	}

	public EOGrandSecteurDisciplinaire currentGrandSecteurDisciplinaire() {
		return currentGrandSecteurDisciplinaire;
	}

	public void setCurrentGrandSecteurDisciplinaire(
			EOGrandSecteurDisciplinaire currentGrandSecteurDisciplinaire) {
		this.currentGrandSecteurDisciplinaire = currentGrandSecteurDisciplinaire;
	}
	


	
	public Boolean nouvelleLigneUnitesRecherche() {
		return ((getUnitesRechercheCount() + 1) % largeurLigneUnitesRecherche) == 0;
	}
	
	public Integer largeurColonneUnite() {
		
		if(unitesPourGrandsSecteurs.get(currentGrandSecteurDisciplinaire).count() < largeurLigneUnitesRecherche) {
			return 100 / unitesPourGrandsSecteurs.get(currentGrandSecteurDisciplinaire).count();
		}
		else {
			return 100 / largeurLigneUnitesRecherche;
		}

	}
	
	public NSArray<EOStructure> unitesPourCurrentGrandSecteur() {
		NSArray<EOStructure> unites = null;
		if(unitesPourGrandsSecteurs.get(currentGrandSecteurDisciplinaire()) == null) {
			unites = StructuresRechercheHelper.unitesRecherchePourGrandSecteurDisciplinaire(edc(), currentGrandSecteurDisciplinaire(), true);
			unitesPourGrandsSecteurs.put(currentGrandSecteurDisciplinaire(), unites);
		}
		return unitesPourGrandsSecteurs.get(currentGrandSecteurDisciplinaire());
	}

	public EOStructure currentUniteRecherche() {
		return currentUniteRecherche;
	}

	public void setCurrentUniteRecherche(EOStructure currentUniteRecherche) {
		this.currentUniteRecherche = currentUniteRecherche;
	}

	public WOActionResults ouvrirEquipeRechercheConsutation() {
		EquipeRechercheConsultation equipeRechercheConsultation = (EquipeRechercheConsultation) pageWithName(EquipeRechercheConsultation.class.getName());
		equipeRechercheConsultation.setEquipeDeRecherche(currentEquipeRecherche());
		equipeRechercheConsultation.setModification(true);
		equipeRechercheConsultation.setConsultation(false);	
		return equipeRechercheConsultation;
	}


	public void setUnitesRechercheCount(Integer unitesRechercheCount) {
		this.unitesRechercheCount = unitesRechercheCount;
	}


	public Integer getUnitesRechercheCount() {
		return unitesRechercheCount;
	}
	
	public NSArray<EOStructure> equipesPourUnite() {
		if(equipesPourUnites.containsKey(currentUniteRecherche) == false) {
			equipesPourUnites.put(currentUniteRecherche, EOStructureForGroupeSpec.getPersonnesInGroupe(currentUniteRecherche(), FactoryAssociation.shared().equipeDeRechercheAssociation(edc())));
		}
		return equipesPourUnites.get(currentUniteRecherche);
	}


	public void setCurrentEquipeRecherche(EOStructure currentEquipeRecherche) {
		this.currentEquipeRecherche = currentEquipeRecherche;
	}


	public EOStructure currentEquipeRecherche() {
		return currentEquipeRecherche;
	}

	
}