package org.cocktail.sangria.server.components.equiperecherche;

import org.cocktail.fwkcktlpersonne.common.eospecificites.EOStructureForGroupeSpec;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypeGroupe;
import org.cocktail.sangria.server.components.commons.ASangriaComponent;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSForwardException;

import er.extensions.eof.ERXEC;
import er.extensions.eof.ERXQ;

public class EquipeRechercheCreation extends ASangriaComponent {
	
	/** The modules. */
	private NSArray modules;
	
	/** The etapes. */
	private NSArray<String> etapes;
	
	/** The index module actif. */
	private Integer indexModuleActif = 0;
	
	public EOStructure equipeRecherche;

	private EOEditingContext editingContext;
	
    public EquipeRechercheCreation(WOContext context) {
        super(context);
    }
    
    @Override
    public EOEditingContext edc() {
    	if (editingContext == null) {
    		editingContext = ERXEC.newEditingContext();
    	}
    	return editingContext;
    }
    
    public EOStructure equipeRecherche() {
    	if (equipeRecherche == null) {
    		EOStructure groupeDesEquipesRecherche;
			try {
				groupeDesEquipesRecherche = EOStructureForGroupeSpec.getGroupeForParamKey(edc(), "GROUPE_EQUIPES_RECHERCHE");
				NSArray<EOTypeGroupe> groupes = EOTypeGroupe.fetchAll(edc(), ERXQ.in(EOTypeGroupe.TGRP_CODE_KEY, new NSArray<String>("ER", "O", "G")));
				equipeRecherche = EOStructureForGroupeSpec.creerGroupe(edc(), getUtilisateurPersId(), null, null, groupes, groupeDesEquipesRecherche);
			} catch (Exception e) {
				throw NSForwardException._runtimeExceptionForThrowable(e);
			}
    		
    	}
    	return equipeRecherche;
    }
    
    
    
    public NSArray modules() {
		modules = new NSArray<String>(new String[]{
				"ModuleEquipeRechercheAdmin",
				"ModuleEquipeRechercheRattachementUnite"});

		return modules;
	}
	
    
    
	
	/**
	 * Etapes.
	 *
	 * @return the nS array
	 */
	public NSArray<String> etapes() {
		etapes = new NSArray<String>(new String[]{
					"Identification",
					"Rattachement"
				});
		
		return etapes;
	}
	
	/**
	 * Sets the index module actif.
	 *
	 * @param indexModuleActif the new index module actif
	 */
	public void setIndexModuleActif(Integer indexModuleActif) {
		this.indexModuleActif = indexModuleActif;
	}
	
	/**
	 * Gets the index module actif.
	 *
	 * @return the index module actif
	 */
	public Integer getIndexModuleActif() {
		return indexModuleActif;
	}


	
	public WOActionResults terminer() {
		try {
			edc().saveChanges();
			mySession().addSimpleSuccessMessage("Confirmation", "La nouvelle équipe est créée");
		} catch (ValidationException e) {
			session().addSimpleErrorMessage("Erreur", e);
		}
		return doNothing();
	}
	
	public WOActionResults annuler() throws Exception {
		edc().revert();

		return pageWithName("EquipesRecherche");
	}
	

}