package org.cocktail.sangria.server.components.aap;

import org.cocktail.fwkcktlrecherche.server.metier.EOAap;
import org.cocktail.fwkcktlsangriaguiajax.serveur.components.commons.AFwkCktlSangriaComponent;
import org.cocktail.fwkcktlsangriaguiajax.serveur.components.commons.ICktlAjaxWindowWrapperActionsDelegate;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;

public class AapSearchWinForProjet extends AFwkCktlSangriaComponent {

    
	private WOActionResults creerAapAction;
	private ICktlAjaxWindowWrapperActionsDelegate aapActionDelegate;
	

	private EOAap selection;

	
	
    public AapSearchWinForProjet(WOContext context) {
        super(context);
    }
	
	
	public WOActionResults creerNouvelAap() {
		return creerAapAction();
	}
	
	public void setSelection(EOAap selection) {
		this.selection = selection;
	}

	public EOAap selection() {
		return selection;
	}

	public void setCreerAapAction(WOActionResults creerAapAction) {
		this.creerAapAction = creerAapAction;
	}

	public WOActionResults creerAapAction() {
		return creerAapAction;
	}

	public void setRechercheAapActionDelegate(ICktlAjaxWindowWrapperActionsDelegate rechercheAapActionDelegate) {
		this.aapActionDelegate = rechercheAapActionDelegate;
	}

	public ICktlAjaxWindowWrapperActionsDelegate rechercheAapActionDelegate() {
		return aapActionDelegate;
	}

}