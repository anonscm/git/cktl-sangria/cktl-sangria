package org.cocktail.sangria.server.components.aap;

import org.cocktail.cocowork.server.metier.convention.AvenantDomaineScientifique;
import org.cocktail.fwkcktlajaxwebext.serveur.component.CktlAjaxWindow;
import org.cocktail.fwkcktljefyadmin.common.metier.EOExercice;
import org.cocktail.fwkcktljefyadmin.common.metier.EOExerciceCocktail;
import org.cocktail.fwkcktlpersonne.common.metier.EODomaineScientifique;
import org.cocktail.fwkcktlpersonne.common.metier.EOGrhumParametres;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartStructure;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypeGroupe;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;
import org.cocktail.fwkcktlrecherche.server.metier.EOAap;
import org.cocktail.fwkcktlrecherche.server.metier.service.GroupeRechercheService;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;

import er.extensions.eof.ERXEOControlUtilities;
import er.extensions.eof.ERXQ;

public class AapIdentificationUI extends AapComponentUI {


	private final String KEY_ANNEE = "currentExercice";
	private final String KEY_TITRE_PROJET = "aap.contrat.conObjet";

	public static final String BINDING_AAP = "aap";
	public static final String COORDINATION_INTERNE = "interne";
	public static final String COORDINATION_EXTERNE = "externe";

	private String currentCoordination;

	private EOIndividu responsableScientifiqueAAjouter;
	private IPersonne partenaireAAjouter;
	private EOStructure currentLaboratoirePorteur;
	private EOStructure currentLaboratoire;
	private EOStructure selectedLaboratoire;
	private EOIndividu currentResponsableScientifique;
	private IPersonne currentPartenaire;
	private EOStructure currentPoleDeCompetitivite;
	private EOStructure poleDeCompetitiviteAAjouter;

	private EODomaineScientifique domaineScientifique;

	private String coordination;

	private Boolean resetPartenairesSearch = false;
	private Boolean resetPoleDeCompetitiviteSearch = false;

	private NSArray<AvenantDomaineScientifique> listeAvenantDomainesScientifiques;
	private AvenantDomaineScientifique currentAvenantDomaineScientifique;

	private EODomaineScientifique selectedDomaineScientifique;

	private NSArray<EODomaineScientifique> listeDomainesScientifiquesNonSelectionnes;

	private GroupeRechercheService groupeRechercheService;
	
	
	public String getSelectionnerCoordinationInterneWindowId() {
		return getComponentId() + "_selectionnerCoordinationInterneWindow";
	}
	
	/**
	 * @param context Context
	 */
	public AapIdentificationUI(WOContext context) {
		super(context);
	}
	/**
	 * @return une instance de GroupeRechercheService
	 */
	public GroupeRechercheService getGroupeRechercheService() {
		if (groupeRechercheService == null) {
			groupeRechercheService = new GroupeRechercheService(edc());
		}
		return groupeRechercheService;
	}
	
	public EOAap getAap() {
		return (EOAap) valueForBinding(BINDING_AAP);
	}

	public String getCoordinationContainerId() {
		return getComponentId() + "_coordinationContainer";
	}

	public EOQualifier getQualifierForGroupes() {
		return ERXQ.equals(EOStructure.TO_REPART_TYPE_GROUPES_KEY + "." + EOTypeGroupe.TGRP_CODE_KEY, EOTypeGroupe.TGRP_CODE_LA);
	}



	public NSArray<EODomaineScientifique> domainesScientifiques() {
		listeDomainesScientifiquesNonSelectionnes = EODomaineScientifique.fetchAll(edc());

		int nbNonSelectionnes = listeDomainesScientifiquesNonSelectionnes.size();
		int nbSelectionnes = listeAvenantDomainesScientifiques().size();

		NSMutableArray<EODomaineScientifique> listeTemp = listeDomainesScientifiquesNonSelectionnes.mutableClone();

		for (int i = 0; i < nbNonSelectionnes; i++) {
			for (int j = 0; j < nbSelectionnes; j++) {
				if (listeDomainesScientifiquesNonSelectionnes.get(i).dsCode().equals(listeAvenantDomainesScientifiques().get(j).domaineScientifique().dsCode())) {
					listeTemp.remove(listeDomainesScientifiquesNonSelectionnes.get(i));
				}
			}
		}

		return listeTemp.immutableClone();
	}

	public void setDomaineScientifique(EODomaineScientifique domaineScientifique) {
		this.domaineScientifique = domaineScientifique;
	}

	public EODomaineScientifique domaineScientifique() {
		return domaineScientifique;
	}

	public NSArray<AvenantDomaineScientifique> listeAvenantDomainesScientifiques() {
		if (listeAvenantDomainesScientifiques == null) {
			listeAvenantDomainesScientifiques = getAap().localInstanceIn(edc()).contrat().avenantZero().avenantDomaineScientifiques().mutableClone();
		}

		return listeAvenantDomainesScientifiques;
	}

	@Override
	public void validationFailedWithException(Throwable exception, Object value, String keyPath) {
		if (keyPath.equalsIgnoreCase(KEY_ANNEE)) {
			session().addSimpleErrorMessage("Attention", "L'année n'a pas été correctement saisie");
		}
		if (keyPath.equalsIgnoreCase(KEY_TITRE_PROJET)) {
			session().addSimpleErrorMessage("Attention", "Le titre du projet n'a pas été correctement saisi");
		}
	}

	public String containerDomaineScientifiqueId() {
		return getComponentId() + "_containerDomaineScientifique";
	}

	public WOActionResults boutonAjouter() {
		AvenantDomaineScientifique ads = AvenantDomaineScientifique.create(edc(), getAap().contrat().avenantZero().localInstanceIn(edc()), getSelectedDomaineScientifique());
		listeAvenantDomainesScientifiques.add(ads);
		return doNothing();
	}

	public WOActionResults boutonSupprimer() {
		listeAvenantDomainesScientifiques.remove(currentAvenantDomaineScientifique);
		currentAvenantDomaineScientifique.setDomaineScientifiqueRelationship(null);
		currentAvenantDomaineScientifique.setAvenantRelationship(null);
		edc().deleteObject(currentAvenantDomaineScientifique);
		return doNothing();
	}

	public EODomaineScientifique getSelectedDomaineScientifique() {
		return selectedDomaineScientifique;
	}

	public void setSelectedDomaineScientifique(EODomaineScientifique selectedDomaineScientifique) {
		this.selectedDomaineScientifique = selectedDomaineScientifique;
	}

	public AvenantDomaineScientifique currentAvenantDomaineScientifique() {
		return currentAvenantDomaineScientifique;
	}

	public void setCurrentAvenantDomaineScientifique(AvenantDomaineScientifique currentAvenantDomaineScientifique) {
		this.currentAvenantDomaineScientifique = currentAvenantDomaineScientifique;
	}

	/*******************************************/
	/*** Gestion des laboratoires porteurs ***/

	public NSArray<EOStructure> listeDesLaboratoires() {
		EOQualifier qualifierForLabos = ERXQ.equals(EOStructure.TO_REPART_TYPE_GROUPES_KEY + "." + EOTypeGroupe.TGRP_CODE_KEY, EOTypeGroupe.TGRP_CODE_LA);
		EOSortOrdering order = EOSortOrdering.sortOrderingWithKey(EOStructure.LL_STRUCTURE_KEY, EOSortOrdering.CompareCaseInsensitiveAscending);
		return EOStructure.fetchAll(edc(), qualifierForLabos, new NSArray<EOSortOrdering>(order));
	}

	public void setCurrentLaboratoirePorteur(EOStructure currentLaboratoirePorteur) {
		this.currentLaboratoirePorteur = currentLaboratoirePorteur;
	}

	public EOStructure currentLaboratoirePorteur() {
		return this.currentLaboratoirePorteur;
	}

	public EOStructure selectedLaboratoire() {
		return selectedLaboratoire;
	}

	public void setSelectedLaboratoire(EOStructure selectedLaboratoire) {
		this.selectedLaboratoire = selectedLaboratoire;
	}

	public String laboratoiresPorteursContainerId() {
		return getComponentId() + "_laboratoiresPorteursContainer";
	}

	public String laboratoirePorteurSearchWindowId() {
		return getComponentId() + "_laboratoirePorteurSearchWindow";
	}

	public WOActionResults onSuppressionLaboratoirePorteur() {
		try {
			getAap().supprimerStructureRechercheConcernee(currentLaboratoirePorteur(), getUtilisateurPersId());
		} catch (Exception e) {
			session().addSimpleErrorMessage("Attention", "Une errreur est survenue lors de la suppression de la structure de recherche");
		}
		return doNothing();
	}

	public void setCurrentLaboratoire(EOStructure currentLaboratoire) {
		this.currentLaboratoire = currentLaboratoire;
	}

	public EOStructure currentLaboratoire() {
		return currentLaboratoire;
	}

	public WOActionResults selectionnerLaboratoire() {
		try {
			getAap().ajouterStructureRechercheConcernee(selectedLaboratoire());
			CktlAjaxWindow.close(context(), laboratoirePorteurSearchWindowId());
		} catch (Exception e) {
			session().addSimpleErrorMessage("Attention", "Une errreur est survenue lors de la sélection de la structure de recherche");
		}

		return doNothing();

	}

	public WOActionResults annulerAjoutLaboratoire() {
		CktlAjaxWindow.close(context(), laboratoirePorteurSearchWindowId());
		return doNothing();
	}
	
	public String getLaboratoirePrincipalId() {
		return "laboratoirePrincipalId_" + currentLaboratoirePorteur().getNumero();
	}
	
	public Boolean isLaboratoirePrincipal() {
		return ERXEOControlUtilities.eoEquals(currentLaboratoirePorteur(), getAap().contrat().centreResponsabilite());
	}

	/**
	 * Affectation du nouveau laboratoire principal
	 * @param principal boolean
	 */
	public void setLaboratoirePrincipal(Boolean principal) {
		if (principal) {
			getAap().contrat().setCentreResponsabiliteRelationship(currentLaboratoirePorteur(), getUtilisateurPersId());
		}
	}

	/**********************************************/
	/*** Gestion des responsables scientifiques ***/

	public void setCurrentResponsableScientifique(EOIndividu currentResponsableScientifique) {
		this.currentResponsableScientifique = currentResponsableScientifique;
	}

	public EOIndividu currentResponsableScientifique() {
		return currentResponsableScientifique;
	}

	public String responsablesScientifiquesContainerId() {
		return getComponentId() + "_responsablesScientifiquesContainer_" + currentLaboratoirePorteur().persId();
	}

	public String responsableScientifiqueSearchWindowId() {
		return getComponentId() + "_responsableScientifiqueSearchWindow_" + currentLaboratoirePorteur().persId();
	}

	public WOActionResults onAjoutResponsableScientifique() {
		try {
			getAap().ajouterResponsableScientifiquePourStructureRecherche(responsableScientifiqueAAjouter(), currentLaboratoirePorteur());
			CktlAjaxWindow.close(context(), responsableScientifiqueSearchWindowId());
		} catch (Exception e) {
			session().addSimpleErrorMessage("Attention", "Une errreur est survenue lors de l'ajout du responsable scientifique");
		}
		return doNothing();
	}

	public WOActionResults annulerAjoutResponsableScientifique() {
		CktlAjaxWindow.close(context(), responsableScientifiqueSearchWindowId());
		return doNothing();
	}

	public WOActionResults onSuppressionResponsableScientifique() {
		try {
			getAap().supprimerResponsableScientifiquePourStructureRecherche(currentResponsableScientifique(), currentLaboratoirePorteur(), getUtilisateurPersId());
		} catch (Exception e) {
			session().addSimpleErrorMessage("Attention", "Une errreur est survenue lors de la suppression du responsable scientifique");
		}
		return doNothing();
	}

	public EOIndividu responsableScientifiqueAAjouter() {
		return responsableScientifiqueAAjouter;
	}

	public void setResponsableScientifiqueAAjouter(EOIndividu responsableScientifiqueAAjouter) {
		this.responsableScientifiqueAAjouter = responsableScientifiqueAAjouter;
	}

	public NSArray<EOIndividu> responsablesScientifiques() {
		return getAap().responsablesScientifiquesPourStructureRecherche(currentLaboratoirePorteur());
	}

    public EOQualifier qualifierForResponsables() {
    	if(currentLaboratoirePorteur() == null) {
    		return null;
    	}
    	NSArray<EOStructure> filles = currentLaboratoirePorteur().getStructuresFilles();
    	return EOIndividu.TO_REPART_STRUCTURES.dot(EORepartStructure.TO_STRUCTURE_GROUPE).eq(currentLaboratoirePorteur())
    		.or(EOIndividu.TO_REPART_STRUCTURES.dot(EORepartStructure.TO_STRUCTURE_GROUPE).in(filles));
    }

	/*******************************************/
	/*** Gestion des partenaires ***/

	public void setCurrentPartenaire(IPersonne currentPartenaire) {
		this.currentPartenaire = currentPartenaire;
	}

	public IPersonne currentPartenaire() {
		return currentPartenaire;
	}

	public IPersonne partenaireAAjouter() {
		return partenaireAAjouter;
	}

	public void setPartenaireAAjouter(IPersonne partenaireAAjouter) {
		this.partenaireAAjouter = partenaireAAjouter;
	}

	public String partenairesContainerId() {
		return getComponentId() + "_partenairesContainer";
	}

	public String partenaireSearchWindowId() {
		return getComponentId() + "_partenaireSearchWindow";
	}

	public WOActionResults onAjoutPartenaire() {

		try {
			getAap().ajouterPartenaire(partenaireAAjouter());
			CktlAjaxWindow.close(context(), partenaireSearchWindowId());
			setResetPartenairesSearch(true);
		} catch (Exception e) {
			session().addSimpleErrorMessage("Attention", "Une errreur est survenue lors de l'ajout du partenaire");
		}
		return doNothing();

	}

	public WOActionResults annulerAjoutPartenaire() {
		CktlAjaxWindow.close(context(), partenaireSearchWindowId());
		setResetPartenairesSearch(true);
		return doNothing();
	}

	public WOActionResults onSuppressionPartenaire() {
		try {
			getAap().supprimerPartenaire(currentPartenaire(), getUtilisateurPersId());
		} catch (Exception e) {
			session().addSimpleErrorMessage("Attention", "Une errreur est survenue lors de la suppression du partenaire");
		}
		return doNothing();
	}

	/*******************************************/
	/*** Gestion des polesDeCompetitivites ***/

	public void setCurrentPoleDeCompetitivite(EOStructure currentPoleDeCompetitivite) {
		this.currentPoleDeCompetitivite = currentPoleDeCompetitivite;
	}

	public EOStructure currentPoleDeCompetitivite() {
		return currentPoleDeCompetitivite;
	}

	public EOStructure poleDeCompetitiviteAAjouter() {
		return poleDeCompetitiviteAAjouter;
	}

	public void setPoleDeCompetitiviteAAjouter(EOStructure poleDeCompetitiviteAAjouter) {
		this.poleDeCompetitiviteAAjouter = poleDeCompetitiviteAAjouter;
	}

	public String polesDeCompetitivitesContainerId() {
		return getComponentId() + "_polesDeCompetitivitesContainer";
	}

	public String polesDeCompetitiviteSearchWindowId() {
		return getComponentId() + "_polesDeCompetitiviteSearchWindow";
	}

	public WOActionResults onAjoutPoleDeCompetitivite() {
		getAap().ajouterPoleDeCompetitivite(poleDeCompetitiviteAAjouter(), getUtilisateurPersId());
		CktlAjaxWindow.close(context(), polesDeCompetitiviteSearchWindowId());
		setResetPoleDeCompetitiviteSearch(true);
		return doNothing();
	}

	public WOActionResults annulerAjoutPoleDeCompetitivite() {
		CktlAjaxWindow.close(context(), polesDeCompetitiviteSearchWindowId());
		setResetPoleDeCompetitiviteSearch(true);
		return doNothing();
	}

	public WOActionResults onSuppressionPoleDeCompetitivite() {
		getAap().supprimerPoleDeCompetitivite(currentPoleDeCompetitivite(), getUtilisateurPersId());
		return doNothing();
	}

	public void setCurrentExercice(Integer currentExercice) {
		getAap().contrat().setExerciceCocktailRelationship(EOExerciceCocktail.fetchFirstByQualifier(edc(), ERXQ.equals(EOExerciceCocktail.EXE_EXERCICE_KEY, EOExercice.getExercice(edc(), currentExercice))));
	}

	public Integer currentExercice() {
		return getAap().contrat().exerciceCocktail().exeExercice();
	}

	public void setCurrentCoordination(String currentCoordination) {
		this.currentCoordination = currentCoordination;
	}

	public String getCurrentCoordination() {
		return currentCoordination;
	}

	public String coordination() {
		if (coordination == null) {
			if (getAap().getCoordinationInterne() != null) {
				coordination = COORDINATION_INTERNE;
				return coordination;
			}
			if (getAap().getCoordinationExterne() != null) {
				coordination = COORDINATION_EXTERNE;
				return coordination;
			}
			coordination = COORDINATION_INTERNE;
		}
		return coordination;
	}

	public void setCoordination(String coordination) {

		this.coordination = coordination;

	}

	public String coordinationInterneKey() {
		return COORDINATION_INTERNE;
	}

	public String coordinationExterneKey() {
		return COORDINATION_EXTERNE;
	}

	public Boolean showCoordinationInterne() {
		return coordination().equalsIgnoreCase(COORDINATION_INTERNE);
	}

	public Boolean showCoordinationExterne() {
		return coordination().equalsIgnoreCase(COORDINATION_EXTERNE);
	}

	public NSDictionary<String, String> getTypesCoordinations() {
		NSDictionary<String, String> typesCoordinations = new NSMutableDictionary<String, String>();
		typesCoordinations.takeValueForKey(" Coordination interne ", COORDINATION_INTERNE);
		typesCoordinations.takeValueForKey(" Coordination externe ", COORDINATION_EXTERNE);
		return typesCoordinations;
	}

	public String coordinationDisplayString() {
		return (String) getTypesCoordinations().valueForKey(currentCoordination);
	}

	/********************************************/
	/*** Coordination interne **/

	public void setCoordinationInterne(EOStructure coordinationInterne) {
		getAap().setCoordinationInterne(coordinationInterne, getUtilisateurPersId());
	}

	public EOStructure coordinationInterne() {
		return getAap().getCoordinationInterne();
	}

	/********************************************/
	/*** Coordination externe **/

	public void setCoordinationExterne(EOStructure coordinationExterne) {
		getAap().setCoordinationExterne(coordinationExterne, getUtilisateurPersId());
	}

	public EOStructure coordinationExterne() {
		return getAap().getCoordinationExterne();
	}

	/********************************************/
	/*** Coordinateur interne **/

	public void setCoordinateurInterne(EOIndividu coordinateurInterne) {
		getAap().setCoordinateurInterne(coordinateurInterne, getUtilisateurPersId());
	}

	public EOQualifier qualifierForCoordinateurInterne() {
		if (coordinationInterne() != null) {
			return EOIndividu.TO_REPART_STRUCTURES.dot(EORepartStructure.TO_STRUCTURE_GROUPE).eq(coordinationInterne());
		} else {
			return null;
		}
	}

	public EOIndividu coordinateurInterne() {
		return getAap().getCoordinateurInterne();
	}

	/********************************************/
	/*** Coordinateur externe **/

	public void setCoordinateurExterne(EOIndividu coordinateurExterne) {
		getAap().setCoordinateurExterne(coordinateurExterne, getUtilisateurPersId());
	}

	public EOIndividu coordinateurExterne() {
		return getAap().getCoordinateurExterne();
	}

	public Boolean resetPartenairesSearch() {
		return resetPartenairesSearch;
	}

	public Boolean resetPoleDeCompetitiviteSearch() {
		return resetPoleDeCompetitiviteSearch;
	}

	public void setResetPartenairesSearch(Boolean resetPartenairesSearch) {
		this.resetPartenairesSearch = resetPartenairesSearch;
	}

	public void setResetPoleDeCompetitiviteSearch(Boolean resetPoleDeCompetitiviteSearch) {
		this.resetPoleDeCompetitiviteSearch = resetPoleDeCompetitiviteSearch;
	}

	public EOQualifier qualifierForGroupeCoordinateurs() {
		String codeStructureGroupeTutelles = EOGrhumParametres.parametrePourCle(edc(), "org.cocktail.sangria.groupes.coordinateursprojets");
		if (codeStructureGroupeTutelles != null) {
			return EOStructure.C_STRUCTURE.eq(codeStructureGroupeTutelles);
		} else {
			return null;
		}

	}

	public WOActionResults annulerSelectionnerStructureRecherche() {
		CktlAjaxWindow.close(context(), getSelectionnerCoordinationInterneWindowId());
		return doNothing();
	}

	public WOActionResults validerSelectionnerStructureRecherche() {
		CktlAjaxWindow.close(context(), getSelectionnerCoordinationInterneWindowId());
		return doNothing();
	}

}