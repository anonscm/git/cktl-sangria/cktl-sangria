package org.cocktail.sangria.server.components.aap;

import java.math.BigDecimal;

import org.cocktail.cocowork.server.metier.convention.AvenantEvenement;
import org.cocktail.cocowork.server.metier.convention.ModeGestion;
import org.cocktail.fwkcktlevenement.serveur.modele.EOEvenement;
import org.cocktail.fwkcktlevenement.serveur.modele.EOEvenementType;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;
import org.cocktail.fwkcktlrecherche.server.metier.EOAap;
import org.cocktail.fwkcktlrecherche.server.metier.EOContratRecherche;
import org.cocktail.fwkcktlrecherche.server.metier.EORepartTypeFinancementAap;
import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;

import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSBundle;
import com.webobjects.foundation.NSDictionary;

import er.extensions.components.ERXComponent;
import er.extensions.eof.ERXEOControlUtilities;
import er.extensions.eof.ERXKey;
import er.extensions.eof.ERXQ;
import er.extensions.eof.ERXS;
import er.extensions.foundation.ERXArrayUtilities;
import er.extensions.foundation.ERXDictionaryUtilities;
import er.extensions.foundation.ERXStringUtilities;

public class AapExportExcel extends ERXComponent {

    
	private static final String NOTE_EVT_ID_INTERNE = "NOTE";

	
	private NSArray<EOAap> aaps;
	private EOAap currentAap;
	
	private NSArray<IPersonne> partenaires;
	private IPersonne currentPartenaire;
	
	private NSArray<EOStructure> labos;
	private EOStructure currentLabo;
	
    public AapExportExcel(WOContext context) {
        super(context);
    }

    public NSDictionary<?,?> excelStylesData() {
    	NSDictionary<?,?> dict = ERXDictionaryUtilities.dictionaryFromPropertyList("ExcelStyles", NSBundle.mainBundle());
    	return dict;
    }
    
    public String fileName() {
    	return "recap_aaps_" + DateCtrl.currentDateHeureVerbeux() + ".xls";
    }
    
	public NSArray<EOAap> aaps() {
		return aaps;
	}

	public void setAaps(NSArray<EOAap> aaps) {
		this.aaps = aaps;
	}

	public EOAap currentAap() {
		return currentAap;
	}

	public void setCurrentAap(EOAap currentAap) {
		this.currentAap = currentAap;
	}

	public NSArray<IPersonne> partenaires() {
		partenaires = currentAap().getPartenaires();
		return partenaires;
	}

	public void setPartenaires(NSArray<IPersonne> partenaires) {
		this.partenaires = partenaires;
	}

	public IPersonne currentPartenaire() {
		return currentPartenaire;
	}

	public void setCurrentPartenaire(IPersonne currentPartenaire) {
		this.currentPartenaire = currentPartenaire;
	}

	public NSArray<EOStructure> labos() {
		labos = currentAap().getStructuresRecherchesConcernees();
		return labos;
	}

	public void setLabos(NSArray<EOStructure> labos) {
		this.labos = labos;
	}

	public EOStructure currentLabo() {
		return currentLabo;
	}

	public void setCurrentLabo(EOStructure currentLabo) {
		this.currentLabo = currentLabo;
	}
	
	public String laboratoirePrincipal() {
		for (EOStructure laboratoire : labos()) {
			if (ERXEOControlUtilities.eoEquals(laboratoire, currentAap().contrat().centreResponsabilite())) {
				return laboratoire.lcStructure();
			}
		}
		return "";
	}
	
	public String listeLaboratoires() {
		NSArray<String> libellesLaboratoires = (NSArray<String>) labos().valueForKey(EOStructure.LC_STRUCTURE_KEY);
		return libellesLaboratoires.componentsJoinedByString(" / ");
	}
	
	public String listePartenaires() {
		NSArray<String> libellesPartenaires = (NSArray<String>) partenaires().valueForKey(IPersonne.NOM_PRENOM_AFFICHAGE_KEY);
		return libellesPartenaires.componentsJoinedByString(" / ");
	}
	
	public String responsablesScientifiques() {
		NSArray<String> nomsPrenoms = (NSArray<String>) currentAap().getResponsablesScientifiques().valueForKey(EOIndividu.NOM_PRENOM_AFFICHAGE_KEY);
		return nomsPrenoms.componentsJoinedByString(" / ");
	}
	
	public String coordination() {
		String coordination = "";
		if(currentAap().getCoordinationInterne() != null) {
			coordination += currentAap().getCoordinationInterne().llStructure();
			if(currentAap().getCoordinateurInterne() != null) {
				coordination += " - " + currentAap().getCoordinateurInterne().persLibelleAffichage();
			}
		} else if(currentAap().getCoordinationExterne() != null) {
			coordination += currentAap().getCoordinationExterne().llStructure();
			if(currentAap().getCoordinateurExterne() != null) {
				coordination += " - " + currentAap().getCoordinateurExterne().persLibelleAffichage();
			}
		}
		return coordination;
	}
	
	public Double totalObtenu() {
		NSArray<EORepartTypeFinancementAap> repartTypeFinancements = currentAap().typesFinancementAap();
		NSArray<Float> montantObtenus = (NSArray<Float>) repartTypeFinancements.valueForKey(EORepartTypeFinancementAap.MONTANT_OBTENU.key());
		montantObtenus = ERXArrayUtilities.removeNullValues(montantObtenus);
		BigDecimal totalMontants = (BigDecimal) montantObtenus.valueForKey(ERXKey.sum().key());
		return totalMontants.doubleValue();
	}
	
	public String derniereNote() {
		String evtTypeIdKeyPath = ERXQ.keyPath(AvenantEvenement.EVENEMENT_KEY, EOEvenement.TYPE_KEY, EOEvenementType.ID_INTERNE_KEY);
		EOQualifier qualifier = ERXQ.equals(evtTypeIdKeyPath, NOTE_EVT_ID_INTERNE);
		
		NSArray<EOEvenement> notes = (NSArray<EOEvenement>) currentAap().contrat().avenantZero().avenantEvenements(qualifier, ERXS.descs(AvenantEvenement.EVENEMENT_KEY + "." + EOEvenement.DATE_CREATION_KEY), false)
				.valueForKey(AvenantEvenement.EVENEMENT_KEY);
		
		if(notes.isEmpty()) {
			return "";
		} else {
			return notes.get(0).objet();
		}

	}
	
	public String avisPassageCs() {
		if(currentAap().avisPassageCs() != null) {
			return (String) EOAap.Nomenclatures.getListeDesAvis().valueForKey(currentAap().avisPassageCs());
		} else {
			return null;
		}
	}

	public String etat() {
		if(currentAap().etatAcceptation() != null) {
			return (String) EOAap.Nomenclatures.getTypesEtatsAcceptation().valueForKey(currentAap().etatAcceptation());
		} else {
			return null;
		}
	}
}