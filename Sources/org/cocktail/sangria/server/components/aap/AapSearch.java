/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2010 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.cocktail.sangria.server.components.aap;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktldroitsutils.common.util.MyStringCtrl;
import org.cocktail.fwkcktlpersonne.common.eospecificites.EOStructureForGroupeSpec;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypeGroupe;
import org.cocktail.fwkcktlrecherche.server.metier.EOAap;
import org.cocktail.fwkcktlrecherche.server.metier.factory.FactoryAap;
import org.cocktail.fwkcktlrecherche.server.metier.finder.FinderAap;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WODisplayGroup;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSData;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSForwardException;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSNotification;
import com.webobjects.foundation.NSNotificationCenter;
import com.webobjects.foundation.NSPropertyListSerialization;
import com.webobjects.foundation.NSSelector;

import er.extensions.appserver.ERXDisplayGroup;
import er.extensions.eof.ERXQ;
import er.extensions.foundation.ERXArrayUtilities;

// TODO: Auto-generated Javadoc
/**
 * The Class AapSearch.
 */
public class AapSearch extends AapComponent {

	private static Logger logger = Logger.getLogger(AapSearch.class);
	
	/** The exercice aap. */
	private String exerciceAap;

	/** The numero aap. */
	private String numeroAap;

	/** The laboratoire aap. */
	private EOStructure laboratoireAap;

	/** The financeur aap. */
	private EOStructure financeurAap;

	/** The current financeur. */
	private EOStructure currentFinanceur;

	/** The responsable scientifique aap. */
	private EOIndividu responsableScientifiqueAap;

	/** The partenaire aap. */
	private EOStructure partenaireAap;

	/** The aap courant. */
	private EOAap aapCourant;

	/** The selected aap. */
	private EOAap selectedAap;

	/** The dico table view aap. */
	private NSMutableDictionary dicoTableViewAap;

	/** The dg aap. */
	private WODisplayGroup dgAap;

	/** The erreurs messages. */
	private NSArray<String> erreursMessages;

	/** The has searched aap. */
	private Boolean hasSearchedAap = false;

	/** The finder aap. */
	private FinderAap finderAap;

	private EOEditingContext edc = null;

	/**
	 * Instantiates a new aap search.
	 * 
	 * @param context
	 *            the context
	 */
	public AapSearch(WOContext context) {
		super(context);
	}

	/**
	 * Raz.
	 */
	public void raz() {
		setExerciceAap(null);
		setNumeroAap(null);
		setLaboratoireAap(null);
		setResponsableScientifiqueAap(null);
		setFinanceurAap(null);
		setPartenaireAap(null);
		setCurrentFinanceur(null);
		setAapCourant(null);
		setSelectedAap(null);
		dgAap = null;
		hasSearchedAap = false;
		finderAap = null;
	}

	/**
	 * @return Label du bouton modifier en fonction de l'état de l'AAP
	 */
	public String getLabelBoutonModifier() {
		if (getSelectedAap().isAccepte()) {
			return "Générer le contrat";
		} else {
			return "Modifier";
		}
	}
	
	/**
	 * Raz recherche.
	 * 
	 * @return the wO action results
	 */
	public WOActionResults razRecherche() {
		raz();
		return doNothing();
	}

	/**
	 * Gets the exercice aap.
	 * 
	 * @return the exercice aap
	 */
	public String getExerciceAap() {
		return exerciceAap;
	}

	/**
	 * Sets the exercice aap.
	 * 
	 * @param exerciceAap
	 *            the new exercice aap
	 */
	public void setExerciceAap(String exerciceAap) {
		this.exerciceAap = exerciceAap;
	}

	/**
	 * Gets the numero aap.
	 * 
	 * @return the numero aap
	 */
	public String getNumeroAap() {
		return numeroAap;
	}

	/**
	 * Sets the numero aap.
	 * 
	 * @param numeroAap
	 *            the new numero aap
	 */
	public void setNumeroAap(String numeroAap) {
		this.numeroAap = numeroAap;
	}

	/**
	 * Gets the aap courant.
	 * 
	 * @return the aap courant
	 */
	public EOAap getAapCourant() {
		return aapCourant;
	}

	/**
	 * Sets the aap courant.
	 * 
	 * @param aapCourant
	 *            the new aap courant
	 */
	public void setAapCourant(EOAap aapCourant) {
		this.aapCourant = aapCourant;
	}

	/**
	 * Sets the erreurs messages.
	 * 
	 * @param erreursMessages
	 *            the new erreurs messages
	 */
	public void setErreursMessages(NSArray<String> erreursMessages) {
		this.erreursMessages = erreursMessages;
	}

	/**
	 * Gets the erreurs messages.
	 * 
	 * @return the erreurs messages
	 */
	public NSArray<String> getErreursMessages() {
		return erreursMessages;
	}

	/**
	 * Sets the selected aap.
	 * 
	 * @param selectedAap
	 *            the new selected aap
	 */
	public void setSelectedAap(EOAap selectedAap) {
		this.selectedAap = selectedAap;
	}

	/**
	 * Gets the selected aap.
	 * 
	 * @return the selected aap
	 */
	public EOAap getSelectedAap() {
		return selectedAap;
	}

	/**
	 * Checks for selected aap.
	 * 
	 * @return the boolean
	 */
	public Boolean hasSelectedAap() {
		return (selectedAap != null);
	}

	/**
	 * Checks for searched for aap.
	 * 
	 * @return the boolean
	 */
	public Boolean hasSearchedForAap() {
		return hasSearchedAap;
	}

	/**
	 * Finder aap.
	 * 
	 * @return the finder aap
	 */
	private FinderAap finderAap() {
		if (finderAap == null)
			finderAap = new FinderAap(edc());
		return finderAap;
	}

	/**
	 * Dico table view aap.
	 * 
	 * @return the nS mutable dictionary
	 */
	public NSMutableDictionary dicoTableViewAap() {
		if (dicoTableViewAap == null) {
			NSData data = new NSData(application().resourceManager().bytesForResourceNamed("TableViewAap.plist", frameworkName(), NSArray.EmptyArray));
			dicoTableViewAap = new NSMutableDictionary((NSDictionary) NSPropertyListSerialization.propertyListFromData(data, "UTF-8"));
		}
		return dicoTableViewAap;
	}

	/**
	 * Dg aap.
	 * 
	 * @return the wO display group
	 */
	public WODisplayGroup dgAap() {

		if (dgAap == null) {
			dgAap = new ERXDisplayGroup<EOAap>();
			dgAap.setDelegate(new DgAapDelegate());
			// dgAap.setNumberOfObjectsPerBatch(10);
		}
		return dgAap;

	}

	/**
	 * The Class DgAapDelegate.
	 */
	public class DgAapDelegate {

		/**
		 * Display group did change selection.
		 * 
		 * @param group
		 *            the group
		 */
		public void displayGroupDidChangeSelection(WODisplayGroup group) {
			if (group.selectedObject() != null) {
				// edc().revert();
				setSelectedAap((EOAap) group.selectedObject());
				// edc().saveChanges();
			}
		}
	}

	/**
	 * Qualifier for labos.
	 * 
	 * @return the eO qualifier
	 */
	public EOQualifier qualifierForLabos() {
		return ERXQ.equals(EOStructure.TO_REPART_TYPE_GROUPES_KEY + "." + EOTypeGroupe.TGRP_CODE_KEY, EOTypeGroupe.TGRP_CODE_LA);
	}

	/**
	 * Formulaire recherche aap id.
	 * 
	 * @return the string
	 */
	public String formulaireRechercheAapId() {
		return getComponentId() + "_formulaireRechercheAap";
	}

	/**
	 * Aap display container id.
	 * 
	 * @return the string
	 */
	public String aapDisplayContainerId() {
		return getComponentId() + "_aapDisplayContainer";
	}

	/**
	 * Aap table view id.
	 * 
	 * @return the string
	 */
	public String aapTableViewId() {
		return getComponentId() + "_aapTableView";
	}

	/**
	 * Aap table view container id.
	 * 
	 * @return the string
	 */
	public String aapTableViewContainerId() {
		return getComponentId() + "_aapTableViewContainer";
	}

	/**
	 * On selectionner personne.
	 * 
	 * @return the wO action results
	 */
	public WOActionResults onSelectionnerPersonne() {
		return null;
	}

	//

	/**
	 * Lancer recherche.
	 * 
	 * @return the wO action results
	 */
	public WOActionResults lancerRecherche() {
		
		finderAap().clear();
		try {
			if (!MyStringCtrl.isEmpty(exerciceAap)) {
				finderAap().setExercice(Integer.parseInt(exerciceAap));
			}
		} catch (NumberFormatException nfe) {
			session().addSimpleErrorMessage("Erreur", "La saisie de l'année est incorrecte");
		}
		try {
			if (!MyStringCtrl.isEmpty(numeroAap))
				finderAap().setOrdre(Integer.parseInt(numeroAap));
		} catch (NumberFormatException nfe) {
			session().addSimpleErrorMessage("Erreur", "La saisie du numéro est incorrecte");
		}

		finderAap().setLaboratoire(laboratoireAap);
		finderAap().setResponsableScientifique(responsableScientifiqueAap);
		finderAap().setFinanceur(financeurAap);
		finderAap().setPartenaire(partenaireAap);

		try {
			NSArray<EOAap> resultats = finderAap().find();
			dgAap().setObjectArray(resultats);
			if (resultats.isEmpty())
				selectedAap = null;
		} catch (Exception e) {
			session().addSimpleErrorMessage("Erreur", e);
		}

		hasSearchedAap = true;

		return doNothing();
	}

	/**
	 * Sets the laboratoire aap.
	 * 
	 * @param laboratoireAap
	 *            the new laboratoire aap
	 */
	public void setLaboratoireAap(EOStructure laboratoireAap) {
		this.laboratoireAap = laboratoireAap;
	}

	/**
	 * Gets the laboratoire aap.
	 * 
	 * @return the laboratoire aap
	 */
	public EOStructure getLaboratoireAap() {
		return laboratoireAap;
	}

	/**
	 * Sets the partenaire aap.
	 * 
	 * @param partenaireAap
	 *            the new partenaire aap
	 */
	public void setPartenaireAap(EOStructure partenaireAap) {
		this.partenaireAap = partenaireAap;
	}

	/**
	 * Gets the partenaire aap.
	 * 
	 * @return the partenaire aap
	 */
	public EOStructure getPartenaireAap() {
		return partenaireAap;
	}

	/**
	 * Liste des financeurs.
	 * 
	 * @return the nS array
	 */
	  public NSArray<EOStructure> listeDesFinanceurs() {
		    
		    try {
		    	EOStructure groupeFinanceurs = EOStructureForGroupeSpec.getGroupeForParamKey(edc(), "GROUPE_FINANCEURS_AAP");
		    	return
		    			EOStructure.LL_STRUCTURE.ascInsensitives().sorted(
		    					ERXArrayUtilities.arrayBySelectingInstancesOfClass(
		    							EOStructureForGroupeSpec.getPersonnesInGroupe(groupeFinanceurs), 
		    							EOStructure.class
		    						)
		    					);
		    }
		    catch (Exception e) {
		    	logger.warn("Attention le groupe des financeurs n'est pas défini");
		    	return new NSArray<EOStructure>();
		    }
		    
		    
	}

	/**
	 * Sets the financeur aap.
	 * 
	 * @param financeurAap
	 *            the new financeur aap
	 */
	public void setFinanceurAap(EOStructure financeurAap) {
		this.financeurAap = financeurAap;
	}

	/**
	 * Gets the financeur aap.
	 * 
	 * @return the financeur aap
	 */
	public EOStructure getFinanceurAap() {
		return financeurAap;
	}

	/**
	 * Sets the responsable scientifique aap.
	 * 
	 * @param responsableScientifiqueAap
	 *            the new responsable scientifique aap
	 */
	public void setResponsableScientifiqueAap(EOIndividu responsableScientifiqueAap) {
		this.responsableScientifiqueAap = responsableScientifiqueAap;
	}

	/**
	 * Gets the responsable scientifique aap.
	 * 
	 * @return the responsable scientifique aap
	 */
	public EOIndividu getResponsableScientifiqueAap() {
		return responsableScientifiqueAap;
	}

	/**
	 * Sets the current financeur.
	 * 
	 * @param currentFinanceur
	 *            the new current financeur
	 */
	public void setCurrentFinanceur(EOStructure currentFinanceur) {
		this.currentFinanceur = currentFinanceur;
	}

	/**
	 * Gets the current financeur.
	 * 
	 * @return the current financeur
	 */
	public EOStructure getCurrentFinanceur() {
		return currentFinanceur;
	}

	/**
	 * Supprimer selected aap.
	 * 
	 * @return the wO action results
	 */
	public WOActionResults supprimerSelectedAap() {
		FactoryAap factory = new FactoryAap(edc(), false);
		try {
			factory.supprimerAap(selectedAap);
			edc().saveChanges();
		} catch (ValidationException e) {
			session().addSimpleErrorMessage("Erreur", e);
			return doNothing();
		} catch (Exception e) {
			throw NSForwardException._runtimeExceptionForThrowable(e);
		}
		dgAap().delete();
		return doNothing();
	}

	/*
	 * Action qui retourne la page permettant de visualiser l'AAP selectionne
	 */
	/**
	 * Voir aap selectionne.
	 * 
	 * @return the wO action results
	 */
	public WOActionResults voirAapSelectionne() {
		AapConsultation result = (AapConsultation) pageWithName(AapConsultation.class.getName());
		result.setAap(getSelectedAap());
		return result;
	}

	/**
	 * Voir aap en modification.
	 * 
	 * @return the wO action results
	 */
	public WOActionResults voirAapEnModification() {
		AapConsultation nextPage = (AapConsultation) pageWithName(AapConsultation.class.getName());
		nextPage.setEdc(edc());
		nextPage.setAap(getSelectedAap());

		if(getSelectedAap().isAccepte()) {
			nextPage.setOngletIdentificationSelected(false);
			nextPage.setOngletPartieAdminSelected(true);
		}
		
		NSNotificationCenter.defaultCenter().addObserver(this, new NSSelector("rechargerLesContrats", new Class[] { NSNotification.class }), AapConsultation.NOTIFICATION_aapSupprime, nextPage);

		return nextPage;
	}

	/**
	 * Action appel�e lors de la r�ception d'une notification par page de
	 * consultation que le contrat en question a �t� supprim�. Il faut donc
	 * mettre � jour la liste des contrats affich�s.
	 * 
	 * @param notification
	 *            the notification
	 */
	public void rechargerLesContrats(NSNotification notification) {
		NSArray<EOAap> resultats = null;
		try {
			resultats = finderAap().find();
			dgAap().setObjectArray(resultats);
			if (resultats.isEmpty())
				selectedAap = null;
		} catch (Exception e) {
			session().addSimpleErrorMessage("Erreur", e);
		}

	}

	/**
	 * Voir aap en consultation.
	 * 
	 * @return the wO action results
	 */
	public WOActionResults voirAapEnConsultation() {
		AapConsultation nextPage = (AapConsultation) pageWithName(AapConsultation.class.getName());
		nextPage.setAap(getSelectedAap());
		nextPage.setEdc(edc());
		nextPage.setReadOnly(true);
		return nextPage;
	}

	public String etatAcceptationDisplayString() {
		return (String) EOAap.Nomenclatures.getTypesEtatsAcceptation().valueForKey(getAapCourant().etatAcceptation());
	}

}