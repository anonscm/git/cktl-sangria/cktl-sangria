package org.cocktail.sangria.server.components.aap;

import org.cocktail.fwkcktlrecherche.server.metier.EOAap;

import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOEditingContext;

import er.extensions.eof.ERXEC;

public class AapNotesUI extends AapComponentUI {
	private static final long serialVersionUID = 7744286739707835234L;
	
	public final static String BINDING_aap = "aap";
	private EOAap aap;
	
	private EOEditingContext _editingContext = null;

	public AapNotesUI(WOContext context) {
        super(context);
    }
	
	public void setAap(EOAap aap) {
		this.aap = aap;
	}

	public EOAap aap() {
    	if(hasBinding(BINDING_aap)) {
    		EOAap _aap = (EOAap) valueForBinding(BINDING_aap);
    		aap  = _aap.localInstanceIn(edc());
    	}
    	return aap;	
    }
	
	public String notesContainerId() {
		return getComponentId() + "_notesContainer";
	}
	
	@Override
    public EOEditingContext edc() {
    	if (_editingContext == null) {
    		_editingContext = ERXEC.newEditingContext((EOEditingContext) valueForBinding(BINDING_editingContext));
    	}
    	return _editingContext;
    }
}