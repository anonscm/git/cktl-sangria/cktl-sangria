package org.cocktail.sangria.server.components.aap;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktldroitsutils.common.util.MyDateCtrl;
import org.cocktail.fwkcktlpersonne.common.eospecificites.EOStructureForGroupeSpec;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlrecherche.server.metier.EOAap;
import org.cocktail.fwkcktlrecherche.server.metier.EOProgramme;
import org.cocktail.fwkcktlrecherche.server.metier.EORepartTypeFinancementAap;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;

import er.extensions.eof.ERXFetchSpecification;
import er.extensions.eof.ERXQ;
import er.extensions.foundation.ERXArrayUtilities;

public class AapPartieFinUI extends AapComponentUI {


  private static Logger logger = Logger.getLogger(AapPartieFinUI.class);
	
  public final static String BINDING_aap = "aap";

  private EOAap aap;

  private String currentEtatMontage;

  private String currentTypeFinancementKey;
  private String currentTypeEnvoiFinanceur;

  private EOStructure currentFinanceur;
  private EOProgramme currentProgramme;
  private EOProgramme currentSousProgramme;

  private NSMutableDictionary<String, EORepartTypeFinancementAap> repartTypeFinancementAapDictionary = null;

  private EOIndividu destinataireSuiviFinancier;

  private EOEditingContext gestionnairesEditingContext = null;
  private EOEditingContext personnesEditingContext = null;

  public AapPartieFinUI(WOContext context) {
    super(context);
  }

  
  public EOAap aap() {
    return (EOAap) valueForBinding(BINDING_aap);
  }

  public NSMutableDictionary<String, EORepartTypeFinancementAap> repartTypeFinancementAapDictionary() {
    if (repartTypeFinancementAapDictionary == null) {
      repartTypeFinancementAapDictionary = new NSMutableDictionary<String, EORepartTypeFinancementAap>();
    }
    return repartTypeFinancementAapDictionary;
  }

  public String programmeUpdateContainerId() {
    return getComponentId() + "_programmeUpdateContainer";
  }

  public String sousProgrammeUpdateContainerId() {
    return getComponentId() + "_sousProgrammeUpdateContainer";
  }

  public String totalMontantDemandeContainerId() {
    return getComponentId() + "_totalMontantDemandeContainer";
  }

  public String totalMontantObtenuContainerId() {
    return getComponentId() + "_totalMontantObtenuContainer";
  }

  public String montantsContainerId() {
    return getComponentId() + "_montantsContainer";
  }

  public String tfMontantDemandeId() {
    return getComponentId() + "_MontantDemande_" + getCurrentTypeFinancementKey();
  }

  public String tfMontantObtenuId() {
    return getComponentId() + "_MontantObtenu_" + getCurrentTypeFinancementKey();
  }

  public void setSelectedEtablisssementGestionnaireFinancier(EOStructure selectedEtablisssementGestionnaireFinancier) {

    aap().setEtablisssementGestionnaireFinancier(selectedEtablisssementGestionnaireFinancier.localInstanceIn(edc()), getUtilisateurPersId());

  }

  public EOStructure selectedEtablisssementGestionnaireFinancier() {
    return aap().getEtablisssementGestionnaireFinancier();
  }

  public void setCurrentEtatMontage(String currentEtatMontage) {
    this.currentEtatMontage = currentEtatMontage;
  }

  public String getCurrentEtatMontage() {
    return currentEtatMontage;
  }

  public String etatMontageDisplayString() {
    return (String) EOAap.Nomenclatures.getTypeEtatMontage().valueForKey(currentEtatMontage);
  }

  public NSDictionary<String, String> typesDenvoisAuFinanceur() {
    return EOAap.Nomenclatures.getTypesDenvoisAuFinanceur();
  }

  public void setCurrentTypeEnvoiFinanceur(String currentTypeEnvoiFinanceur) {
    this.currentTypeEnvoiFinanceur = currentTypeEnvoiFinanceur;
  }

  public String getCurrentTypeEnvoiFinanceur() {
    return currentTypeEnvoiFinanceur;
  }

  public String getTypeFinancementDisplayString() {
    return (String) EOAap.Nomenclatures.getListeTypesFinancement().valueForKey(currentTypeFinancementKey);
  }

  public String getTypeEnvoiFinanceurDisplayString() {
    return (String) EOAap.Nomenclatures.getTypesDenvoisAuFinanceur().valueForKey(currentTypeEnvoiFinanceur);
  }

  public NSDictionary<String, String> listeTypesFinancement() {
    return EOAap.Nomenclatures.getListeTypesFinancement();
  }

  public void setCurrentTypeFinancementKey(String currentTypeFinancementKey) {
    this.currentTypeFinancementKey = currentTypeFinancementKey;
  }

  public String getCurrentTypeFinancementKey() {
    return currentTypeFinancementKey;
  }

  public EORepartTypeFinancementAap getCurrentRepartTypeFinancementAap() {
    if (repartTypeFinancementAapDictionary().valueForKey(currentTypeFinancementKey) == null) {
      EOQualifier qualifierForRepartFin = ERXQ.and(ERXQ.equals(EORepartTypeFinancementAap.AAP_KEY, aap()), ERXQ.equals(EORepartTypeFinancementAap.TYPE_FINANCEMENT_KEY, currentTypeFinancementKey));
      ERXFetchSpecification<EORepartTypeFinancementAap> fetchSpecification;

      fetchSpecification = new ERXFetchSpecification<EORepartTypeFinancementAap>(EORepartTypeFinancementAap.ENTITY_NAME, qualifierForRepartFin, null);
      fetchSpecification.setIncludeEditingContextChanges(true);

      NSArray<EORepartTypeFinancementAap> repartTypeFinancementAaps = fetchSpecification.fetchObjects(edc());
      EORepartTypeFinancementAap repartTypeFinancementAap = null;
      if (repartTypeFinancementAaps.isEmpty()) {
        repartTypeFinancementAap = EORepartTypeFinancementAap.creerInstance(edc());
        repartTypeFinancementAap.setDCreation(MyDateCtrl.getDateJour());
        repartTypeFinancementAap.setPersIdCreation(getUtilisateurPersId());
        repartTypeFinancementAap.setTypeFinancement(currentTypeFinancementKey);
        repartTypeFinancementAap.setAapRelationship(aap());

      }
      else {
        repartTypeFinancementAap = repartTypeFinancementAaps.get(0);
      }
      repartTypeFinancementAapDictionary.takeValueForKey(repartTypeFinancementAap, currentTypeFinancementKey);
    }
    return (EORepartTypeFinancementAap) repartTypeFinancementAapDictionary.valueForKey(currentTypeFinancementKey);
  }

  public Float totalMontantDemande() {

    Float total = 0F;
    for (String key : repartTypeFinancementAapDictionary.allKeys()) {
      EORepartTypeFinancementAap repartTypeFinancementAap = (EORepartTypeFinancementAap) repartTypeFinancementAapDictionary.valueForKey(key);
      if (repartTypeFinancementAap.montantDemande() != null)
        total += repartTypeFinancementAap.montantDemande();
    }
    return total;
  }

  public Float totalMontantObtenu() {
    Float total = 0F;
    for (String key : repartTypeFinancementAapDictionary.allKeys()) {
      EORepartTypeFinancementAap repartTypeFinancementAap = (EORepartTypeFinancementAap) repartTypeFinancementAapDictionary.valueForKey(key);
      if (repartTypeFinancementAap.montantObtenu() != null)
        total += repartTypeFinancementAap.montantObtenu();
    }
    return total;
  }

  public NSArray<EOStructure> listeDesFinanceurs() {
    
    try {
    	EOStructure groupeFinanceurs = EOStructureForGroupeSpec.getGroupeForParamKey(edc(), "GROUPE_FINANCEURS_AAP");
    	return
    			EOStructure.LL_STRUCTURE.ascInsensitives().sorted(
    					ERXArrayUtilities.arrayBySelectingInstancesOfClass(
    							EOStructureForGroupeSpec.getPersonnesInGroupe(groupeFinanceurs), 
    							EOStructure.class
    						)
    					);
    }
    catch (Exception e) {
    	logger.warn("Attention le groupe des financeurs n'est pas défini");
    	return new NSArray<EOStructure>();
    }
    
    
  }

  public NSArray<EOProgramme> listeDesProgrammes() {
    if (aap().getFinanceur() != null) {
      NSArray<EOProgramme> programmes = EOProgramme.programmesDuFinanceur(edc(), financeur(), aap().contrat().exerciceCocktail().exeExercice());
      if (programmes.isEmpty()) {
        aap().setProgrammeRelationship(null);
      }
      return programmes;
    }
    else
      return NSArray.emptyArray();
  }

  public NSArray<String> onUpdateProgrammeContainerList() {
    return new NSArray<String>(sousProgrammeUpdateContainerId());
  }

  public NSArray<EOProgramme> listeDesSousProgrammes() {
    if (aap().programme() != null) {
      return aap().programme().programmesFils(aap().contrat().exerciceCocktail().exeExercice());
    }
    else
      return NSArray.emptyArray();
  }

  public void setCurrentFinanceur(EOStructure currentFinanceur) {
    this.currentFinanceur = currentFinanceur;
  }

  public EOStructure getCurrentFinanceur() {
    return currentFinanceur;
  }

  public void setCurrentProgramme(EOProgramme currentProgramme) {
    this.currentProgramme = currentProgramme;
  }

  public EOProgramme getCurrentProgramme() {
    return currentProgramme;
  }

  public void setCurrentSousProgramme(EOProgramme currentSousProgramme) {
    this.currentSousProgramme = currentSousProgramme;
  }

  public EOProgramme getCurrentSousProgramme() {
    return currentSousProgramme;
  }

  public String destinataireSuiviFinancierUpdateContainerId() {
    return getComponentId() + "_destinataireSuiviFinancierUpdateContainer";
  }
  
  public EOStructure financeur() {
    return aap().getFinanceur();
  }
  
  public void setFinanceur(EOStructure financeur) {
    aap().setFinanceur(financeur, getUtilisateurPersId());
  }

  public WOActionResults onSelectDestinataireSuiviFinancier() {
    if (destinataireSuiviFinancier() != null) {

//      try {
//        destinataireSuiviFinancier().setValidationEditingContext(personnesEditingContext());
//        destinataireSuiviFinancier().setPersIdModification(utilisateurPersId());
//        personnesEditingContext().saveChanges();
//        aap().setDestinataireAlerteSuiviFinancier(destinataireSuiviFinancier().localInstanceIn(edc()), null);
        aap().setDestinataireAlerteSuiviFinancier(destinataireSuiviFinancier(), getUtilisateurPersId());
//      }
//      catch (ValidationException e) {
//        personnesEditingContext().revert();
//        session().addSimpleErrorMessage("Erreur", e);
//        setDestinataireSuiviFinancier(aap().getDestinataireAlerteSuiviFinancier());
//
//      }

    }
    return doNothing();

  }

  public void setDestinataireSuiviFinancier(EOIndividu destinataireSuiviFinancier) {
    this.destinataireSuiviFinancier = destinataireSuiviFinancier;
  }

  public EOIndividu destinataireSuiviFinancier() {
    if (destinataireSuiviFinancier == null) {
      destinataireSuiviFinancier = aap().getDestinataireAlerteSuiviFinancier();
    }
    return destinataireSuiviFinancier;
  }

  @Override
  public void validationFailedWithException(Throwable e, Object o, String s) {
    super.validationFailedWithException(e, o, s);
    if (s.equalsIgnoreCase("currentRepartTypeFinancementAap.montantDemande")) {
      session().addSimpleErrorMessage("Erreur", "Vous avez saisi un montant erroné");
    }
    if (s.equalsIgnoreCase("currentRepartTypeFinancementAap.montantObtenu")) {
      session().addSimpleErrorMessage("Erreur", "Vous avez saisi un montant erroné");
    }
  }

}