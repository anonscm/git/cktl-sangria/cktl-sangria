package org.cocktail.sangria.server.components.aap;

import org.apache.log4j.Logger;
import org.cocktail.cocowork.server.metier.convention.Contrat;
import org.cocktail.fwkcktljefyadmin.common.metier.EOExerciceCocktail;
import org.cocktail.fwkcktlpersonne.common.eospecificites.EOStructureForGroupeSpec;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlrecherche.server.metier.EOAap;
import org.cocktail.fwkcktlrecherche.server.metier.EOGdStrategieSangria;
import org.cocktail.fwkcktlrecherche.server.metier.finder.AucunCritereFourniException;
import org.cocktail.fwkcktlrecherche.server.metier.finder.FinderAap;
import org.cocktail.fwkcktlrecherche.server.metier.service.GroupeRechercheService;
import org.cocktail.sangria.server.components.commons.ASangriaComponent;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WODisplayGroup;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

import er.ajax.AjaxUpdateContainer;
import er.extensions.appserver.ERXDisplayGroup;
import er.extensions.eof.ERXS;
import er.extensions.foundation.ERXArrayUtilities;

/**
 * 
 * @author Julien Lafourcade
 *
 */
public class AapSrch extends ASangriaComponent {

	private static Logger logger = Logger.getLogger(AapSrch.class);
	private static final long serialVersionUID = -3978141857758913126L;
	private final String BINDING_selectedAap = "selectedAap";

	private Integer annee;
	private Integer numero;
	private EOStructure structureRechercheConcernee;
	private EOIndividu responsableScientifique;
	private EOStructure partenaire;
	private EOStructure financeur;
	private EOStructure currentFinanceur;
	private Boolean resetStructureRechercheAutoComplete = false;
	private FinderAap finderAap;
	private ERXDisplayGroup<EOAap> dgAap = null;
	private EOAap selectedAap;
	private String acronymeToken;
	private GroupeRechercheService groupeRechercheService;

	/**
	 * 
	 * @param context
	 *            le context
	 */
	public AapSrch(WOContext context) {
		super(context);
	}

	/**
	 * @return un instance de GroupeRechercheService
	 */
	public GroupeRechercheService getGroupeRechercheService() {
		if (groupeRechercheService == null) {
			groupeRechercheService = new GroupeRechercheService(edc());
		}
		return groupeRechercheService;
	}

	public String getFormulaireContainerId() {
		return getComponentId() + "_formulaireContainer";
	}

	public String getTableViewContainerId() {
		return getComponentId() + "_tableViewContainer";
	}

	public void setAnnee(Integer annee) {
		this.annee = annee;
	}

	public Integer getAnnee() {
		return annee;
	}

	public void setNumero(Integer numero) {
		this.numero = numero;
	}

	public Integer getNumero() {
		return numero;
	}

	public String getAcronymeToken() {
		return acronymeToken;
	}

	public void setAcronymeToken(String acronymeToken) {
		this.acronymeToken = acronymeToken;
	}

	public void setStructureRechercheConcernee(
			EOStructure structureRechercheConcernee) {
		this.structureRechercheConcernee = structureRechercheConcernee;
	}

	public EOStructure getStructureRechercheConcernee() {
		return structureRechercheConcernee;
	}

	public void setResponsableScientifique(EOIndividu responsableScientifique) {
		this.responsableScientifique = responsableScientifique;
	}

	public EOIndividu getResponsableScientifique() {
		return responsableScientifique;
	}

	public void setPartenaire(EOStructure partenaire) {
		this.partenaire = partenaire;
	}

	public EOStructure getPartenaire() {
		return partenaire;
	}

	public void setCurrentFinanceur(EOStructure currentFinanceur) {
		this.currentFinanceur = currentFinanceur;
	}

	public EOStructure getCurrentFinanceur() {
		return currentFinanceur;
	}

	public void setFinanceur(EOStructure financeur) {
		this.financeur = financeur;
	}

	public EOStructure getFinanceur() {
		return financeur;
	}

	public NSArray<EOStructure> listeDesFinanceurs() {
		try {
			EOStructure groupeFinanceurs = EOStructureForGroupeSpec
					.getGroupeForParamKey(edc(), "GROUPE_FINANCEURS_AAP");
			return EOStructure.LL_STRUCTURE.ascInsensitives().sorted(
					ERXArrayUtilities.arrayBySelectingInstancesOfClass(
							EOStructureForGroupeSpec
									.getPersonnesInGroupe(groupeFinanceurs),
							EOStructure.class));
		} catch (Exception e) {
			logger.warn("Attention le groupe des financeurs n'est pas défini");
			return new NSArray<EOStructure>();
		}

	}

	private FinderAap finderAap() {
		if (finderAap == null) {
			finderAap = new FinderAap(edc());
		}
		return finderAap;
	}

	public WOActionResults lancerRecherche() {

		finderAap().clear();

		finderAap().setExercice(getAnnee());
		finderAap().setOrdre(getNumero());
		finderAap().setAcronymeToken(getAcronymeToken());
		finderAap().setLaboratoire(getStructureRechercheConcernee());
		finderAap().setResponsableScientifique(getResponsableScientifique());
		finderAap().setFinanceur(getFinanceur());
		finderAap().setPartenaire(getPartenaire());
		// filtrage selon les périmetres de données
		EOQualifier qualifierPerimetresDeDonnees = getPerimetreProvider().getAllQualifiers(getStrategies(),
				session().connectedUserInfo());
		finderAap().setPerimetresQualifier(qualifierPerimetresDeDonnees);
		if (qualifierPerimetresDeDonnees == null) {
			session().addSimpleInfoMessage("Remarque", "Vous ne disposez pas de droits suffisants pour afficher les demandes de subventions.");
		}
		try {
			NSArray<EOAap> resultats = finderAap().find();

			dgAap().setObjectArray(resultats);
			if (resultats.isEmpty()) {
				setSelectedAap(null);
				session().addSimpleLocalizedInfoMessage(
						"aucunResultatDeRecherche");
			}
			if (updateContainerID() != null) {
				AjaxUpdateContainer.updateContainerWithID(updateContainerID(),
						context());
			}
		} catch (AucunCritereFourniException e) {
			session().addSimpleLocalizedErrorMessage("erreur",
					"auMoinsUnCritereFourni");
		}

		return doNothing();
	}

	private NSArray<EOGdStrategieSangria> getStrategies() {
		NSArray<EOGdStrategieSangria> strategies = new NSMutableArray<EOGdStrategieSangria>();
		strategies.add(EOGdStrategieSangria.strategieAccesConsultationDemandesSubventionStructure(edc()));
		strategies.add(EOGdStrategieSangria.strategieAccesConsultationToutesDemandesSubventionStructure(edc()));
		return strategies;
	}

	public Boolean hasResultatsDeRecherche() {
		return dgAap().allObjects().size() > 0;
	}

	public WOActionResults resetFormulaire() {

		// CHAMPS DU FORMULAIRE
		setAnnee(null);
		setNumero(null);
		setStructureRechercheConcernee(null);
		setResponsableScientifique(null);
		setPartenaire(null);
		setFinanceur(null);
		setAcronymeToken(null);
		setResetStructureRechercheAutoComplete(true);

		// RESET DU DG
		dgAap().setObjectArray(null);

		// MISE A JOUR DE L'INTERFACE
		AjaxUpdateContainer.updateContainerWithID(getTableViewContainerId(),
				context());
		if (updateContainerID() != null) {
			AjaxUpdateContainer.updateContainerWithID(updateContainerID(),
					context());
		}

		// DESLECTION DE L'AAP
		setSelectedAap(null);

		return doNothing();
	}

	/**
	 * Initialisation a la demande du displayGroup
	 * 
	 * @return le display group
	 */
	public ERXDisplayGroup<EOAap> dgAap() {
		if (dgAap == null) {
			dgAap = new ERXDisplayGroup<EOAap>();
			dgAap.setDelegate(this);
			dgAap.setSelectsFirstObjectAfterFetch(true);
		}
		return dgAap;
	}

	public Boolean listeDesResultatsEstVide() {
		return dgAap().allObjects().isEmpty();
	}

	public void setSelectedAap(EOAap selectedAap) {
		this.selectedAap = selectedAap;
		setValueForBinding(selectedAap, BINDING_selectedAap);
	}

	public EOAap selectedAap() {
		selectedAap = (EOAap) valueForBinding(BINDING_selectedAap);
		return selectedAap;
	}

	/**
	 * Méthode de délégation du display des AAP
	 * 
	 * @param group
	 *            Le display group
	 * 
	 */
	public void displayGroupDidChangeSelection(WODisplayGroup group) {
		if (dgAap().selectedObject() != null) {
			setSelectedAap(dgAap().selectedObject());
		}
	}

	public Boolean getResetStructureRechercheAutoComplete() {
		return resetStructureRechercheAutoComplete;
	}

	public void setResetStructureRechercheAutoComplete(
			Boolean resetStructureRechercheAutoComplete) {
		this.resetStructureRechercheAutoComplete = resetStructureRechercheAutoComplete;
	}

	public WOActionResults exportExcel() {
		AapExportExcel aapExportExcel = (AapExportExcel) pageWithName(AapExportExcel.class
				.getName());
		NSArray<EOSortOrdering> orderings = EOAap.CONTRAT
				.dot(Contrat.EXERCICE_COCKTAIL)
				.dot(EOExerciceCocktail.EXE_EXERCICE_KEY).desc()
				.then(EOAap.CONTRAT.dot(Contrat.CON_INDEX).desc());
		NSArray<EOAap> aaps = ERXS.sorted(dgAap().allObjects(), orderings);
		aapExportExcel.setAaps(aaps);
		return aapExportExcel;
	}
}