package org.cocktail.sangria.server.components.aap;

import org.cocktail.sangria.server.components.commons.ASangriaComponent;

import com.webobjects.appserver.WOContext;

public class AapComponentUI extends ASangriaComponent {
	
	private final static String BINDING_consultation = "consultation";
	private final static String BINDING_modification = "modification";

	
	public AapComponentUI(WOContext context) {
		super(context);
	}
	


    
    public Boolean consultation() {
    	if(hasBinding(BINDING_consultation)) {
    		return (Boolean) valueForBinding(BINDING_consultation);
    	}
    	else {
    		return false;
    	}
    }
    
    public Boolean modification() {
    	if(consultation()) {
    		return false;
    	}
    	else if(hasBinding(BINDING_modification)) {
    		return (Boolean) valueForBinding(BINDING_modification);
    	}
    	else {
    		return false;
    	}
    	
    }
    
	
}
