package org.cocktail.sangria.server.components.aap;

import org.cocktail.fwkcktlajaxwebext.serveur.component.CktlAjaxWindow;
import org.cocktail.fwkcktljefyadmin.common.metier.EOExerciceCocktail;
import org.cocktail.fwkcktlpersonne.common.metier.EOGrhumParametres;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartStructure;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypeGroupe;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;
import org.cocktail.fwkcktlrecherche.server.metier.EOAap;
import org.cocktail.fwkcktlrecherche.server.metier.factory.FactoryAap;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSForwardException;
import com.webobjects.foundation.NSTimestamp;

import er.ajax.AjaxUpdateContainer;
import er.extensions.appserver.ERXRedirect;
import er.extensions.eof.ERXEC;
import er.extensions.eof.ERXQ;
import er.extensions.foundation.ERXProperties;

public class AapCreationV2 extends AapComponent {

	private static final long serialVersionUID = -8772170177167529210L;

	private static final String PARAM_GROUPE_PARTENAIRES_RECHERCHE = "GROUPE_PARTENAIRES_RECHERCHE";

	private EOAap aap;

	private EOIndividu responsableScientifiqueAAjouter;
	private IPersonne partenaireAAjouter;
	private EOStructure currentLaboratoirePorteur;
	private EOStructure currentLaboratoire;
	private EOIndividu currentResponsableScientifique;
	private IPersonne currentPartenaire;
	private EOStructure selectedStructureRecherche;

//	private EOEditingContext personnesEditingContext = null;

	private Boolean resetPartenairesSearch = false;

	public AapCreationV2(WOContext context) {
		super(context);
	}

	/**
	 * Gets the aap.
	 * 
	 * @return the aap
	 */
	public EOAap aap() {
		if (aap == null) {

			FactoryAap factoryAap = new FactoryAap(edc(), false);
			try {
				aap = factoryAap.creerAapVide("", getUtilisateurPersId());
			} catch (Exception e) {
				throw NSForwardException._runtimeExceptionForThrowable(e);
			}
		}

		return aap;
	}

	public void setCurrentExercice(Integer currentExercice) {
		aap().contrat().setExerciceCocktailRelationship(
				EOExerciceCocktail.fetchFirstByQualifier(edc(), ERXQ.equals(
						EOExerciceCocktail.EXE_EXERCICE_KEY,currentExercice)));
	}

	public Integer currentExercice() {
		return aap().contrat().exerciceCocktail().exeExercice().intValue();
	}

	public String containerDomaineScientifiqueId() {
		return getComponentId() + "_containerDomaineScientifique";
	}
	
	public WOActionResults enregistrerLesModications() {

		try {
			aap().setDModification(new NSTimestamp());
			edc().saveChanges();
			session().addSimpleSuccessMessage(null,
					"La demande de subvention a été enregistrée avec succès");
		   AapConsultation nextPage = (AapConsultation) pageWithName(AapConsultation.class.getName());
		    EOEditingContext e = ERXEC.newEditingContext();
		    nextPage.setEdc(e);
		    nextPage.setAap(aap().localInstanceIn(e));
		    nextPage.setModification(true);
		    session().addSimpleSuccessMessage("Création OK", "La demande de subvention " + aap().contrat().exerciceEtIndex() + " a bien été créée");
		    ERXRedirect redirect = (ERXRedirect) pageWithName(ERXRedirect.class.getName());
		    redirect.setComponent(nextPage);
		    return redirect;

		} catch (Exception e) {
			session().addSimpleErrorMessage("Erreur", e);
		}
		return doNothing();
	}
//
//	public EOEditingContext personnesEditingContext() {
//		if (personnesEditingContext == null) {
//			personnesEditingContext = ERXEC.newEditingContext(edc());
//		}
//		return personnesEditingContext;
//	}

	public EOQualifier qualifierForGroupePartenairesRecherche() {
		if (ERXProperties
				.booleanForKey("org.cocktail.fwkcktlsangriaguiajax.contratsrecherche.AllerChercherDansPartenairesRecherche")) {
			String codeStructureGroupePartenairesRecherche = EOGrhumParametres
					.parametrePourCle(edc(), PARAM_GROUPE_PARTENAIRES_RECHERCHE);
			return EOStructure.C_STRUCTURE
					.eq(codeStructureGroupePartenairesRecherche);
		} else {
			return null;
		}

	}

	/*******************************************/
	/*** Gestion des laboratoires porteurs ***/

	public NSArray<EOStructure> listeDesLaboratoires() {
		EOQualifier qualifierForLabos = ERXQ
				.equals(EOStructure.TO_REPART_TYPE_GROUPES_KEY + "."
						+ EOTypeGroupe.TGRP_CODE_KEY, EOTypeGroupe.TGRP_CODE_LA);
		EOSortOrdering order = EOSortOrdering.sortOrderingWithKey(
				EOStructure.LL_STRUCTURE_KEY,
				EOSortOrdering.CompareCaseInsensitiveAscending);
		return EOStructure.fetchAll(edc(),
				qualifierForLabos, new NSArray<EOSortOrdering>(order));
	}
	
	public void setSelectedStructureRecherche(EOStructure selectedStructureRecherche) {
		this.selectedStructureRecherche = selectedStructureRecherche;
	}

	public EOStructure selectedStructureRecherche() {
		return selectedStructureRecherche;
	}
	
	public void setCurrentLaboratoirePorteur(
			EOStructure currentLaboratoirePorteur) {
		this.currentLaboratoirePorteur = currentLaboratoirePorteur;
	}

	public EOStructure currentLaboratoirePorteur() {
		return this.currentLaboratoirePorteur;
	}

	public String laboratoiresPorteursContainerId() {
		return getComponentId() + "_laboratoiresPorteursContainer";
	}

	public String laboratoirePorteurSearchWindowId() {
		return getComponentId() + "_laboratoirePorteurSearchWindow";
	}

	public WOActionResults onSuppressionLaboratoirePorteur() {
		try {
			aap().supprimerStructureRechercheConcernee(currentLaboratoirePorteur(), getUtilisateurPersId());
		} catch (Exception e) {
			session().addSimpleErrorMessage("Attention", "Une errreur est survenue lors de la suppression de la structure de recherche");
		}
		return doNothing();
	}

	public void setCurrentLaboratoire(EOStructure currentLaboratoire) {
		this.currentLaboratoire = currentLaboratoire;
	}

	public EOStructure currentLaboratoire() {
		return currentLaboratoire;
	}

	public WOActionResults selectionnerLaboratoire() {
		try {
			aap().ajouterStructureRechercheConcernee(selectedStructureRecherche());
			CktlAjaxWindow.close(context(), laboratoirePorteurSearchWindowId());
		} catch (Exception e) {
			throw NSForwardException._runtimeExceptionForThrowable(e);
		}

		return doNothing();

	}

	public WOActionResults annulerAjoutLaboratoire() {
		CktlAjaxWindow.close(context(), laboratoirePorteurSearchWindowId());
		return doNothing();
	}

	public Boolean resetPartenairesSearch() {
		return resetPartenairesSearch;
	}

	public void setResetPartenairesSearch(Boolean resetPartenairesSearch) {
		this.resetPartenairesSearch = resetPartenairesSearch;
	}

	/**********************************************/
	/*** Gestion des responsables scientifiques ***/

	public void setCurrentResponsableScientifique(
			EOIndividu currentResponsableScientifique) {
		this.currentResponsableScientifique = currentResponsableScientifique;
	}

	public EOIndividu currentResponsableScientifique() {
		return currentResponsableScientifique;
	}

	public String responsablesScientifiquesContainerId() {
		return getComponentId() + "_responsablesScientifiquesContainer_"
				+ currentLaboratoirePorteur().persId();
	}

	public String responsableScientifiqueSearchWindowId() {
		return getComponentId() + "_responsableScientifiqueSearchWindow_"
				+ currentLaboratoirePorteur().persId();
	}

	public WOActionResults onAjoutResponsableScientifique() {

		if(responsableScientifiqueAAjouter() == null) {
			session().addSimpleErrorMessage("Erreur", "Veuillez selectionner un responsable scientifique.");
			return doNothing();
		}

		try {
			aap().ajouterResponsableScientifiquePourStructureRecherche(
					responsableScientifiqueAAjouter(),
					currentLaboratoirePorteur());
		} catch (Exception e) {
			session().addSimpleErrorMessage("Attention", "Une errreur est survenue lors de l'ajout du responsable scientifique");		
		}

		CktlAjaxWindow.close(context(), responsableScientifiqueSearchWindowId());
		AjaxUpdateContainer.updateContainerWithID(laboratoiresPorteursContainerId(), context());

		return doNothing();
	}

	public WOActionResults annulerAjoutResponsableScientifique() {
		CktlAjaxWindow.close(context(), responsableScientifiqueSearchWindowId());
		return doNothing();
	}

	public WOActionResults onSuppressionResponsableScientifique() {
		try {
			aap().supprimerResponsableScientifiquePourStructureRecherche(currentResponsableScientifique(), currentLaboratoirePorteur(), getUtilisateurPersId());
		} catch (Exception e) {
			session().addSimpleErrorMessage("Attention", "Une errreur est survenue lors de la suppression du responsable scientifique");		}
		return doNothing();
	}

	public EOIndividu responsableScientifiqueAAjouter() {
		return responsableScientifiqueAAjouter;
	}

	public void setResponsableScientifiqueAAjouter(
			EOIndividu responsableScientifiqueAAjouter) {
		this.responsableScientifiqueAAjouter = responsableScientifiqueAAjouter;
	}

	public NSArray<EOIndividu> responsablesScientifiques() {
		return aap().responsablesScientifiquesPourStructureRecherche(
				currentLaboratoirePorteur());
	}

	public EOQualifier qualifierForResponsables() {
		if (currentLaboratoirePorteur() == null)
			return null;
		EOQualifier qualifier = ERXQ.equals(EOIndividu.TO_REPART_STRUCTURES_KEY
				+ "." + EORepartStructure.TO_STRUCTURE_GROUPE_KEY + "."
				+ EOStructure.C_STRUCTURE_KEY, currentLaboratoirePorteur()
				.cStructure());
		return qualifier;
	}

	/*******************************************/
	/*** Gestion des partenaires ***/

	public void setCurrentPartenaire(IPersonne currentPartenaire) {
		this.currentPartenaire = currentPartenaire;
	}

	public IPersonne currentPartenaire() {
		return currentPartenaire;
	}

	public IPersonne partenaireAAjouter() {
		return partenaireAAjouter;
	}

	public void setPartenaireAAjouter(IPersonne partenaireAAjouter) {
		this.partenaireAAjouter = partenaireAAjouter;
	}

	public String partenairesContainerId() {
		return getComponentId() + "_partenairesContainer";
	}

	public String partenaireSearchWindowId() {
		return getComponentId() + "_partenaireSearchWindow";
	}

	public WOActionResults onAjoutPartenaire() {
		try {
			aap().ajouterPartenaire(partenaireAAjouter());
			CktlAjaxWindow.close(context(), partenaireSearchWindowId());
			setResetPartenairesSearch(true);
		} catch (Exception e) {
			session().addSimpleLocalizedErrorMessage("attention", "erreur.ajout.partenaire");		
		}

		return doNothing();

	}

	public WOActionResults annulerAjoutPartenaire() {
		CktlAjaxWindow.close(context(), partenaireSearchWindowId());
		setResetPartenairesSearch(true);
		return doNothing();
	}

	public WOActionResults onSuppressionPartenaire() {
		try {
			aap().supprimerPartenaire(currentPartenaire(), getUtilisateurPersId());
		} catch (Exception e) {
			session().addSimpleLocalizedErrorMessage("attention", "erreur.suppression.partenaire");		
		}
		return doNothing();
	}
}

