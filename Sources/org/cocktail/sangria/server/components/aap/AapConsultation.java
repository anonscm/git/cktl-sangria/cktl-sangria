/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2010 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.cocktail.sangria.server.components.aap;

import org.cocktail.fwkcktlrecherche.server.metier.EOAap;
import org.cocktail.fwkcktlrecherche.server.metier.factory.FactoryAap;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.foundation.NSForwardException;
import com.webobjects.foundation.NSNotificationCenter;
import com.webobjects.foundation.NSTimestamp;

import er.ajax.AjaxUpdateContainer;
import er.extensions.appserver.ERXRedirect;

// TODO: Auto-generated Javadoc
/**
 * The Class AapConsultation.
 */
public class AapConsultation extends AapComponent {
	
	/** The aap. */
	private EOAap aap;
	
	/** The Constant NOTIFICATION_aapSupprime. */
	public final static String NOTIFICATION_aapSupprime = "aapSupprime";
	
    /**
     * Instantiates a new aap consultation.
     *
     * @param context the context
     */
    public AapConsultation(WOContext context) {
        super(context);
    }
    
	/**
	 * Sets the aap.
	 *
	 * @param aap the new aap
	 */
	public void setAap(EOAap aap) {
		this.aap = aap;
	}


	/**
	 * Gets the aap.
	 *
	 * @return the aap
	 */
	public EOAap aap() {
		return aap;
	}
	
	
    // GESTION DES ONGLETS
    
    /** The onglet identification selected. */
    private Boolean ongletIdentificationSelected = true;
    
    /** The onglet partie admin selected. */
    private Boolean ongletPartieAdminSelected = false;
    
    /** The onglet partie fin selected. */
    private Boolean ongletPartieFinSelected = false;
    
    /** The onglet partie pi selected. */
    private Boolean ongletPartiePiSelected = false;
    
    private Boolean ongletPartieNotesSelected = false;

    private Boolean ongletPartieDocumentsSelected = false;

    public final String documentsContainerId = getComponentId() + "_documentsContainer";

    
	/** The consultation. */
	private Boolean consultation = false;
	
	/** The modification. */
	private Boolean modification = false;
    
	/**
	 * Onglet identification selected.
	 *
	 * @return the boolean
	 */
	public Boolean ongletIdentificationSelected() {
		return ongletIdentificationSelected;
	}
	
	/**
	 * Sets the onglet identification selected.
	 *
	 * @param ongletIdentificationSelected the new onglet identification selected
	 */
	public void setOngletIdentificationSelected(Boolean ongletIdentificationSelected) {
		this.ongletIdentificationSelected = ongletIdentificationSelected;
	}
	
	/**
	 * Onglet partie admin selected.
	 *
	 * @return the boolean
	 */
	public Boolean ongletPartieAdminSelected() {
		return ongletPartieAdminSelected;
	}
	
	/**
	 * Sets the onglet partie admin selected.
	 *
	 * @param ongletPartieAdminSelected the new onglet partie admin selected
	 */
	public void setOngletPartieAdminSelected(Boolean ongletPartieAdminSelected) {
		this.ongletPartieAdminSelected = ongletPartieAdminSelected;
	}
	
	/**
	 * Onglet partie fin selected.
	 *
	 * @return the boolean
	 */
	public Boolean ongletPartieFinSelected() {
		return ongletPartieFinSelected;
	}
	
	/**
	 * Sets the onglet partie fin selected.
	 *
	 * @param ongletPartieFinSelected the new onglet partie fin selected
	 */
	public void setOngletPartieFinSelected(Boolean ongletPartieFinSelected) {
		this.ongletPartieFinSelected = ongletPartieFinSelected;
	}
	
	/**
	 * Onglet partie pi selected.
	 *
	 * @return the boolean
	 */
	public Boolean ongletPartiePiSelected() {
		return ongletPartiePiSelected;
	}
	
	/**
	 * Sets the onglet partie pi selected.
	 *
	 * @param ongletPartiePiSelected the new onglet partie pi selected
	 */
	public void setOngletPartiePiSelected(Boolean ongletPartiePiSelected) {
		this.ongletPartiePiSelected = ongletPartiePiSelected;
	}
    // INDENTIFIANTS DES COMPOSANTS GRAPHIQUES
    
	public void setOngletPartieNotesSelected(
			Boolean ongletPartieNotesSelected) {
		this.ongletPartieNotesSelected = ongletPartieNotesSelected;
	}

	public Boolean ongletPartieNotesSelected() {
		return ongletPartieNotesSelected;
	}
	
	public void setOngletPartieDocumentsSelected(
			Boolean ongletPartieDocumentsSelected) {
		this.ongletPartieDocumentsSelected = ongletPartieDocumentsSelected;
	}

	public Boolean ongletPartieDocumentsSelected() {
		return ongletPartieDocumentsSelected;
	}

	/**
     * Onglets container id.
     *
     * @return the string
     */
    public String ongletsContainerId() {
    	return getComponentId() + "_ongletsContainer";
    }
    
    /**
     * Onglet identification id.
     *
     * @return the string
     */
    public String ongletIdentificationId() {
    	return getComponentId() + "_ongletIdentification";
    }
    
    /**
     * Onglet partie admin id.
     *
     * @return the string
     */
    public String ongletPartieAdminId() {
    	return getComponentId() + "_ongletPartieAdmin";
    }
    
    /**
     * Onglet partie fin id.
     *
     * @return the string
     */
    public String ongletPartieFinId() {
    	return getComponentId() + "_ongletPartieFin";
    } 
    
    /**
     * Onglet partie pi id.
     *
     * @return the string
     */
    public String ongletPartiePiId() {
    	return getComponentId() + "_ongletPartiePi";
    } 
   
    public String ongletPartieNotesId() {
    	return getComponentId() + "_ongletPartieNotes";
    } 
   
    public String ongletPartieDocumentsId() {
    	return getComponentId() + "_ongletPartieDocuments";
    } 
    
    /**
     * Consultation.
     *
     * @return the boolean
     */
    public Boolean consultation() {
    	return consultation;
    }
    
    /**
     * Sets the consultation.
     *
     * @param consultation the new consultation
     */
    public void setConsultation(Boolean consultation) {
    	this.consultation = consultation;
    	
    	
    }
    
    /**
     * Modification.
     *
     * @return the boolean
     */
    public Boolean modification() {
    	if(consultation()) {
    		return false;
    	}
    	else {
    		return modification;
    	}
    }
    
    /**
     * Sets the modification.
     *
     * @param modification the new modification
     */
    public void setModification(Boolean modification) {
    	this.modification = modification;
    }

	/**
	 * Retour recherche.
	 *
	 * @return the wO action results
	 */
	public WOActionResults retourRecherche() {
		return session().getSavedPageWithName(AapSearch.class.getName());
	}
	

	public WOActionResults enregistrerLesModications() {
		
		try {
			aap().setDModification(new NSTimestamp()); 	
			edc().saveChanges();
			session().addSimpleSuccessMessage(null, "La demande de subvention a été enregistrée avec succès");
			if(ongletPartieDocumentsSelected()) {
				AjaxUpdateContainer.updateContainerWithID(documentsContainerId,  context());
		    }

		}
		catch (ValidationException e) {
			session().addSimpleErrorMessage("Erreur", e);
		}
		return doNothing();
	}


	
	public WOActionResults supprimerContratRecherche() {
		
		FactoryAap factory = new FactoryAap(edc(), false);
		try {
			factory.supprimerAap(aap());
			edc().saveChanges();	
			session().addSimpleSuccessMessage("Confirmation", "La demande de subvention a bien été supprimée");
			NSNotificationCenter.defaultCenter().postNotification(NOTIFICATION_aapSupprime, this);
		}
		catch (ValidationException e) {
			session().addSimpleErrorMessage("Erreur", e);
			return doNothing();
		} catch (Exception e) {
			throw NSForwardException._runtimeExceptionForThrowable(e);
		}
		
		ERXRedirect redirect = (ERXRedirect) pageWithName(ERXRedirect.class.getName());
		redirect.setComponent(session().getSavedPageWithName(AapSearch.class.getName())); 
		return redirect;
		
	}

    @Override
    public Boolean isReadOnly() {
    	if (aap().isAccepte()) {
    		setReadOnly(true);
    	}
    	return super.isReadOnly();
    }
}