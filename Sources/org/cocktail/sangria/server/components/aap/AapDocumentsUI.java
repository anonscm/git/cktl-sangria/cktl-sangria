package org.cocktail.sangria.server.components.aap;

import org.cocktail.fwkcktlrecherche.server.metier.EOAap;

import com.webobjects.appserver.WOContext;

/**
 * 
 * @author jlafourc
 *
 */
public class AapDocumentsUI extends AapComponentUI {
	
	public static final String BINDING_AAP = "aap";

	private EOAap aap;
	
	/**
	 * @param context Context
	 */
    public AapDocumentsUI(WOContext context) {
        super(context);
    }

		
	public void setAap(EOAap aap) {
		this.aap = aap;
	}

	public EOAap aap() {
    	if (hasBinding(BINDING_AAP)) {
    		EOAap _aap = (EOAap) valueForBinding(BINDING_AAP);
    		aap  = _aap.localInstanceIn(edc());
    	}
    	return aap;	
    }

    

}