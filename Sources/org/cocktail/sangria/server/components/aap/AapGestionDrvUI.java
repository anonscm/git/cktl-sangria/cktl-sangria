package org.cocktail.sangria.server.components.aap;

import org.cocktail.cocowork.server.metier.convention.Contrat;
import org.cocktail.cocowork.server.metier.convention.TypeClassificationContrat;
import org.cocktail.fwkcktljefyadmin.common.metier.EOExerciceCocktail;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.individu.controles.ResultatControle;
import org.cocktail.fwkcktlrecherche.server.metier.EOAap;
import org.cocktail.fwkcktlrecherche.server.metier.service.CreationContratService;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;

import er.ajax.AjaxUpdateContainer;

public class AapGestionDrvUI extends AapComponentUI {

	private static final long serialVersionUID = 2367784031105476922L;

	public final static String BINDING_aap = "aap";
	
	private String currentMoyenDepotKey;
	private String currentAvisKey;

	private String currentTypeEtatAcceptation;
	private String currentMoyenNotification;
	
	private EOIndividu destinataireAlerteSuiviAAjouter;
	private EOIndividu destinataireAlerteSuiviASupprimer;
	
	private String typeSuiviValoCourant;

	private EOIndividu chargeDuDossier;
		
//	private EOEditingContext personnesEditingContext = null;
	
	private static final Boolean DEFAULT_NOTES_FOLDED = false;
	private Boolean notesFolded = DEFAULT_NOTES_FOLDED;
	
	
	private Integer contratLieAnnee;
	private Integer contratLieNumero;
	
	private ResultatControle currentResultatControle;
	
	public AapGestionDrvUI(WOContext context) {
        super(context);
    } 
    
//	public EOEditingContext personnesEditingContext() {
//		if(personnesEditingContext == null) {
//			personnesEditingContext = ERXEC.newEditingContext(edc());
//		}
//		return personnesEditingContext;
//	}
	
	public String getRadioButtonCoordinationName() {
		return getComponentId() + "_radioButtonCoordinationName";
	}
	
	public String getEtatAcceptionContainerId() {
		return getComponentId() + "_etatAcceptionContainer";
	}

	public String getContratLieContainerId() {
		return getComponentId() + "_contratLieContainer";
	}
		
    public EOAap aap() {
    	return (EOAap) valueForBinding(BINDING_aap);
    }


	public NSDictionary<String, String> getTypeMoyenNotificationRefusAcceptation() {
		NSDictionary<String, String> typesMoyens = new NSMutableDictionary<String, String>();
		typesMoyens.takeValueForKey("Mail", "1_mail");
		typesMoyens.takeValueForKey("Courrier", "2_courrier");
		typesMoyens.takeValueForKey("Téléphone", "3_phone");
		return typesMoyens;		
	}
	
	public void setCurrentMoyenDepotKey(String currentMoyenDepotKey) {
		this.currentMoyenDepotKey = currentMoyenDepotKey;
	}

	public String getCurrentMoyenDepotKey() {
		return currentMoyenDepotKey;
	}

	public void setCurrentAvisKey(String currentAvisKey) {
		this.currentAvisKey = currentAvisKey;
	}

	public String getCurrentAvisKey() {
		return currentAvisKey;
	}
	
	public NSDictionary<String, String>  listeDesAvis() {
	    return EOAap.Nomenclatures.getListeDesAvis();
	    
	}
	
	public String avisDisplayString() {
	    return (String) EOAap.Nomenclatures.getListeDesAvis().valueForKey(currentAvisKey);
	}
	
	public NSDictionary<String, String> typesEtatsAcceptation() {
	    return EOAap.Nomenclatures.getTypesEtatsAcceptation();
	}

	public void setCurrentTypeEtatAcceptation(String currentTypeEtatAcceptation) {
		this.currentTypeEtatAcceptation = currentTypeEtatAcceptation;
	}

	public String getCurrentTypeEtatAcceptation() {
		return currentTypeEtatAcceptation;
	}

	public void setCurrentMoyenNotification(String currentMoyenNotification) {
		this.currentMoyenNotification = currentMoyenNotification;
	}

	public String getCurrentMoyenNotification() {
		return currentMoyenNotification;
	}

	public String getMoyenDepotDisplayString() {
		return (String) EOAap.Nomenclatures.getMoyensDeDepot().valueForKey(currentMoyenDepotKey);
	}
	
	public NSDictionary<String, String> moyensDeDepot() {
	    return EOAap.Nomenclatures.getMoyensDeDepot();
	}
	
	public String getTypeEtatAcceptationDisplayString() {
		return (String) EOAap.Nomenclatures.getTypesEtatsAcceptation().valueForKey(currentTypeEtatAcceptation);
	}
	
	public String getMoyenNotificationDisplayString() {
		return (String) getTypeMoyenNotificationRefusAcceptation().valueForKey(currentMoyenNotification);
	}
	
	
	// DESTINATAIRES ALERTE SUIVI VALO
	
	public void setDestinataireAlerteSuiviAAjouter(
			EOIndividu destinataireAlerteSuiviAAjouter) {
		this.destinataireAlerteSuiviAAjouter = destinataireAlerteSuiviAAjouter;
	}

	public EOIndividu getDestinataireAlerteSuiviAAjouter() {
		return destinataireAlerteSuiviAAjouter;
	}

	public void setDestinataireAlerteSuiviASupprimer(
			EOIndividu destinataireAlerteSuiviASupprimer) {
		this.destinataireAlerteSuiviASupprimer = destinataireAlerteSuiviASupprimer;
	}

	public EOIndividu getDestinataireAlerteSuiviASupprimer() {
		return destinataireAlerteSuiviASupprimer;
	}
	
	public void onAjouterDestinataireSuivi() {
		aap().ajouterDestinataireAlerteSuivi(destinataireAlerteSuiviAAjouter, getUtilisateurPersId());
	}
	
	public void onSupprimerDestinataireSuivi() {
		aap().supprimerDestinataireAlerteSuivi(destinataireAlerteSuiviASupprimer, getUtilisateurPersId());
	}
	
	public void setTypeSuiviValoCourant(String typeSuiviValoCourant) {
		this.typeSuiviValoCourant = typeSuiviValoCourant;
	}

	public String getTypeSuiviValoCourant() {
		return typeSuiviValoCourant;
	}
 
	public String suiviValoDisplayString() {
		return (String) EOAap.Nomenclatures.getTypeSuiviValo().valueForKey(typeSuiviValoCourant); 
	}
	
	/*********************************************/
	/*** Chargé du dossier ***/
	
	public String chargeDuDossierUpdateContainerId() {
		return getComponentId() + "_chargeDuDossierUpdateContainer";
	}
	
	public WOActionResults onSelectChargeDuDossier() {

//		try {
//			chargeDuDossier().setValidationEditingContext(personnesEditingContext());
//			chargeDuDossier().setPersIdModification(utilisateurPersId());
//			personnesEditingContext().saveChanges();
    aap().setChargeDuDossier(chargeDuDossier(), getUtilisateurPersId());
//	  aap().setChargeDuDossier(chargeDuDossier().localInstanceIn(edc()), null);
			
//		} catch (ValidationException e) {
//			personnesEditingContext().revert();
//			session().addSimpleErrorMessage("Erreur", "Des informations relatives au chargé du dosser sélectionné sont invalides. Vous ne pouvez pas utiliser cette personne pour le moment.");
//			session().addSimpleErrorMessage("Erreur", e);
//			setChargeDuDossier(aap().getChargeDuDossier());
//		}

		return doNothing();
		
	}
	
	public void setChargeDuDossier(EOIndividu chargeDuDossier) {
		this.chargeDuDossier = chargeDuDossier;
	}

	public EOIndividu chargeDuDossier() {
		if(chargeDuDossier == null) {
			chargeDuDossier = aap().getChargeDuDossier();
		}
		return chargeDuDossier;
	}
	
	public String divNoteContainerId() {
		return getComponentId() + "_divNoteContainer";
	}

	public String notesButtonsContainerId() {
    	return getComponentId() + "_notesButtonsContainer";
    }
    
    public String notesContainerId() {
    	return getComponentId() + "_notesContainer";
    }

    public Boolean getNotesFolded() {
		return notesFolded;
	}
    
	public void setNotesFolded(Boolean notesFolded) {
		this.notesFolded = notesFolded;
	}
	
	public WOActionResults foldNotes() {
//		AjaxUpdateContainer.updateContainerWithID(divNoteContainerId(), context());
		setNotesFolded(true);
		AjaxUpdateContainer.updateContainerWithID(divNoteContainerId(), context());
		return doNothing();
	}
	
	public WOActionResults unFoldNotes() {
//		AjaxUpdateContainer.updateContainerWithID(divNoteContainerId(), context());
		setNotesFolded(false);
		AjaxUpdateContainer.updateContainerWithID(divNoteContainerId(), context());
		return doNothing();
	}
	
	
	public WOActionResults creerContratLie() {
		CreationContratService creationContratService = new CreationContratService(edc());
		try {
			creationContratService.creerContratPourAap(aap(), getUtilisateurPersId());
			edc().saveChanges();
			session().addSimpleSuccessMessage(session().localizer().localizedStringForKey("app.creationContratLie.ok"));
		} catch (Exception e) {
			edc().revert();
			session().addSimpleErrorMessage(e);
		}
		return doNothing();
	}

	public ResultatControle getCurrentResultatControle() {
		return currentResultatControle;
	}

	public void setCurrentResultatControle(ResultatControle currentResultatControle) {
		this.currentResultatControle = currentResultatControle;
	}

	public Integer getContratLieAnnee() {
		return contratLieAnnee;
	}

	public void setContratLieAnnee(Integer contratLieAnnee) {
		this.contratLieAnnee = contratLieAnnee;
	}

	public Integer getContratLieNumero() {
		return contratLieNumero;
	}

	public void setContratLieNumero(Integer contratLieNumero) {
		this.contratLieNumero = contratLieNumero;
	}
	
	/**
	 * Action qui permet de lier un contrat existant 
	 * à l'aap
	 * @return rien
	 */
	public WOActionResults lierContratExistant() {

		if (getContratLieAnnee() == null || getContratLieNumero() == null) {
			session().addSimpleLocalizedErrorMessage("aap.rechercheContraLie.parametres.ko");
			return doNothing();
		}
		
		Contrat contrat = 
			Contrat.fetchContratSelonTypeClassificationEtExerciceEtIndex(
				edc(), 
				TypeClassificationContrat.defaultTypeClassificationContrat(edc()), 
				getContratLieAnnee(), 
				getContratLieNumero()
			);
		
		if (contrat == null) {
			session().addSimpleLocalizedErrorMessage("aap.rechercheContraLie.nonTrouve");
		} else {
			aap().setContratLieRelationship(contrat);
			try {
				edc().saveChanges();
				session().addSimpleLocalizedInfoMessage("aap.rechercheContraLie.ok");
			} catch (ValidationException e) {
				session().addSimpleErrorMessage(e);
				edc().revert();
			}
		}
		return doNothing();
	}

	
}