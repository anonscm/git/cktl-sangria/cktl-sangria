package org.cocktail.sangria.server.components.aap;

import java.math.BigDecimal;

import org.cocktail.fwkcktlajaxwebext.serveur.component.CktlAjaxWindow;
import org.cocktail.fwkcktlajaxwebext.serveur.component.tableview.column.CktlAjaxTableViewColumn;
import org.cocktail.fwkcktlajaxwebext.serveur.component.tableview.column.CktlAjaxTableViewColumnAssociation;
import org.cocktail.fwkcktlrecherche.server.metier.EOAap;
import org.cocktail.fwkcktlrecherche.server.metier.EORepartTypeFinancementAap;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WODisplayGroup;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;

import er.ajax.AjaxUpdateContainer;
import er.extensions.appserver.ERXDisplayGroup;
import er.extensions.eof.ERXEC;
import er.extensions.eof.ERXKey;
import er.extensions.foundation.ERXArrayUtilities;
import er.extensions.foundation.ERXStringUtilities;
import er.extensions.foundation.ERXTimestampUtilities;

public class AapListeFinancementsGestion extends AapComponentUI {
    
	public final static String BINDING_aap = "aap";

	
	public final static String MODE_AJOUT = "MODE_AJOUT";
	public final static String MODE_EDITION = "MODE_EDITION";

	private String mode;

	private EOAap aap;
	
	
	private ERXDisplayGroup<EORepartTypeFinancementAap> repartTypeFinancementDisplayGroup = null;
	private EORepartTypeFinancementAap selectedRepartTypeFinancementAap;
	
	private EORepartTypeFinancementAap editingRepartTypeFinancementAap;
	
	private EORepartTypeFinancementAap currentFinancement;
		
	private String currentTypeFinancement;

	private EOEditingContext editionFinancementEditingContext;
		
	
	public AapListeFinancementsGestion(WOContext context) {
        super(context);
    }



	public EOAap aap() {
		return (EOAap) valueForBinding(BINDING_aap);
	}
	
	
	public EOEditingContext getEditionFinancementEditingContext() {
		if(editionFinancementEditingContext == null) {
			editionFinancementEditingContext = ERXEC.newEditingContext(edc());
		}
		return editionFinancementEditingContext;
	}
    
	
	
	public String typesFinancementsTableViewId() {
		return getComponentId() + "_typesFinancementsTableView";
	}
	
	public String typesFinancementsTableViewContainerId() {
		return getComponentId() + "_typesFinancementsTableViewContainer";
	}
	
	public String typeFinancementDetailsContainerId() {
		return getComponentId() + "_typeFinancementDetailsContainer";
	}
	
	public String ajoutTypeFinancementWindowId() {
		return getComponentId() + "_ajoutTypeFinancementWindow";
	}
	
	public String boutonsAjoutTypeFinancementContainerId() {
		return getComponentId() + "_boutonsAjoutTypeFinancementContainer";
	}
	
	public String boutonsDetailsTypeFinancementContainerId() {
		return getComponentId() + "_boutonsDetailsTypeFinancementContainer";
	}
	
	
	
	public NSDictionary<String , String> listeTypesFinancement() {
	    return EOAap.Nomenclatures.getListeTypesFinancement();
	}
	
	public ERXDisplayGroup<EORepartTypeFinancementAap> repartTypeFinancementDisplayGroup() {
		if(repartTypeFinancementDisplayGroup== null) {
			repartTypeFinancementDisplayGroup = new ERXDisplayGroup<EORepartTypeFinancementAap>();
			repartTypeFinancementDisplayGroup.setDelegate(this);
			repartTypeFinancementDisplayGroup.setObjectArray(aap().typesFinancementAap());
		}
		
		return repartTypeFinancementDisplayGroup;
	}
	
	public void displayGroupDidChangeSelection(WODisplayGroup group) {
		setSelectedRepartTypeFinancementAap((EORepartTypeFinancementAap) group.selectedObject());
	}



	public EORepartTypeFinancementAap selectedRepartTypeFinancementAap() {
		return selectedRepartTypeFinancementAap;
	}



	public void setSelectedRepartTypeFinancementAap(EORepartTypeFinancementAap selectedRepartTypeFinancementAap) {
		this.selectedRepartTypeFinancementAap = selectedRepartTypeFinancementAap;
	}
	
	
	
	public EORepartTypeFinancementAap currentFinancement() {
		return currentFinancement;
	}



	public void setCurrentFinancement(EORepartTypeFinancementAap currentFinancement) {
		this.currentFinancement = currentFinancement;
	}
	
		


		public String currentTypeFinancement() {
			return currentTypeFinancement;
		}



		public void setCurrentTypeFinancement(String currentTypeFinancement) {
			this.currentTypeFinancement = currentTypeFinancement;
		}
		
		
		
		public String currentTypeFinancementDisplayString() {
			return (String) listeTypesFinancement().valueForKey(currentTypeFinancement());
		}




		public WOActionResults valider() {
			
			EOAap _aap = aap().localInstanceIn(edc());
			
			// nanardisation visant à squeezer la validation dans ce cas
			_aap.setValidatedWhenNested(false);
			
			if(getEditingRepartTypeFinancementAap().typeFinancement() == null) {
				session().addSimpleErrorMessage("Erreur", "Vous devez choisir un type de financement");
				return doNothing();
			}

			
			try {
				getEditionFinancementEditingContext().saveChanges();
				if(isModeAjout()) {
					repartTypeFinancementDisplayGroup().insertObjectAtIndex(
							editingRepartTypeFinancementAap, 
							repartTypeFinancementDisplayGroup().indexOfLastDisplayedObject()
					);
				}
				CktlAjaxWindow.close(context(), ajoutTypeFinancementWindowId());
				AjaxUpdateContainer.updateContainerWithID(typesFinancementsTableViewContainerId(), context());
			} catch (ValidationException e) {
				session().addSimpleErrorMessage("Erreur", e);
			}
			
			return doNothing();
		}
		
		public WOActionResults annulerAjoutNouveauFinancement() {
			getEditionFinancementEditingContext().revert();
			CktlAjaxWindow.close(context(), ajoutTypeFinancementWindowId());
			return doNothing();
		}



		
		@Override
		public void validationFailedWithException(Throwable e, Object obj, String keyPath) {
			super.validationFailedWithException(e, obj, keyPath);
		}



		public EORepartTypeFinancementAap getEditingRepartTypeFinancementAap() {
			return editingRepartTypeFinancementAap;
		}



		public void setEditingRepartTypeFinancementAap(
				EORepartTypeFinancementAap editingRepartTypeFinancementAap) {
			this.editingRepartTypeFinancementAap = editingRepartTypeFinancementAap;
		}



		public WOActionResults ajouter() {
			
			
			mode = MODE_AJOUT; 
			
			editingRepartTypeFinancementAap = EORepartTypeFinancementAap.creerInstance(getEditionFinancementEditingContext());
			editingRepartTypeFinancementAap.setAapRelationship(aap().localInstanceIn(getEditionFinancementEditingContext()));
			
			editingRepartTypeFinancementAap.setPersIdCreation(getUtilisateurPersId());
			editingRepartTypeFinancementAap.setPersIdModification(getUtilisateurPersId());
			
			editingRepartTypeFinancementAap.setDCreation(ERXTimestampUtilities.today());
			editingRepartTypeFinancementAap.setDModification(ERXTimestampUtilities.today());
			
			return doNothing();
			
		}
		
		public WOActionResults editer() {
			
			mode = MODE_EDITION;
			
			editingRepartTypeFinancementAap = selectedRepartTypeFinancementAap().localInstanceIn(getEditionFinancementEditingContext());
			return doNothing();
		}
		
		public WOActionResults supprimer() {
			editingRepartTypeFinancementAap = selectedRepartTypeFinancementAap().localInstanceIn(getEditionFinancementEditingContext());
			repartTypeFinancementDisplayGroup().deleteSelection();		
			editingRepartTypeFinancementAap.setAapRelationship(null);
			editingRepartTypeFinancementAap.delete();
			try {
				getEditionFinancementEditingContext().saveChanges();
				AjaxUpdateContainer.updateContainerWithID(typesFinancementsTableViewContainerId(), context());
			} catch (ValidationException e) {
				session().addSimpleErrorMessage("Erreur", e);
				getEditionFinancementEditingContext().revert();
			}
			return doNothing();
		}


		public Boolean isModeEdition() {
			return ERXStringUtilities.stringEqualsString(MODE_EDITION, mode);
		}
		
		public Boolean isModeAjout() {
			return ERXStringUtilities.stringEqualsString(MODE_AJOUT, mode);
		}
		
		public Double getTotalDemande() {
			return aap().getTotalDemande();
		}
		
		public Double getTotalObtenu() {
			return aap().getTotalObtenu();
		}

	
}