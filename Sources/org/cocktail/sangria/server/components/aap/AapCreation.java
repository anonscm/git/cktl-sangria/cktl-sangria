/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2010 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.cocktail.sangria.server.components.aap;

import org.cocktail.cocowork.server.metier.convention.factory.FactoryProjet;
import org.cocktail.fwkcktlrecherche.server.metier.EOAap;
import org.cocktail.fwkcktlrecherche.server.metier.EOProjetScientifique;
import org.cocktail.fwkcktlrecherche.server.metier.factory.FactoryAap;
import org.cocktail.sangria.server.components.assistants.IAssistantActionsDelegate;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOComponent;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSForwardException;

import er.extensions.appserver.ERXRedirect;
import er.extensions.eof.ERXEC;

// TODO: Auto-generated Javadoc
/**
 * The Class AapCreation.
 */
public class AapCreation extends AapComponent implements IAssistantActionsDelegate {

	
	
	/** The edc. */
	private EOEditingContext edc;
	
	/** The modules. */
	private NSArray modules;
	
	/** The etapes. */
	private NSArray<String> etapes;
	
	/** The index module actif. */
	private Integer indexModuleActif = 0;
	
	/** The aap. */
	private EOAap aap;
	
	private EOProjetScientifique projetScientifique; 
	
	private EOEditingContext editingContext = null;
	
	private IAssistantActionsDelegate assistantActionsDelegate = null;
	
    /**
     * Instantiates a new aap creation.
     *
     * @param context the context
     */
    public AapCreation(WOContext context) {
        super(context);
   }
    
    @Override
    public EOEditingContext edc() {
    	if(editingContext == null) {
    		editingContext = ERXEC.newEditingContext();
    	}
    	return editingContext;
    }
    
    
	/**
	 * Gets the aap.
	 *
	 * @return the aap
	 */
	public EOAap aap() {
		if(aap == null) {
			
			FactoryAap factoryAap = new FactoryAap(edc(), false);
			try {
				aap = factoryAap.creerAapVide("", getUtilisateurPersId());
			} catch (Exception e) {
				throw NSForwardException._runtimeExceptionForThrowable(e);
			}
		}
		
		return aap;
	}

	/**
	 * Modules.
	 *
	 * @return the nS array
	 */
	public NSArray<String> modules() {
			modules = new NSArray<String>(new String[]{
					"ModuleAapIdentification",
					"ModuleAapMontageFin",
					"ModuleAapMontageAdmin",
					
					});

		return modules;
	}
	
	
	/**
	 * Etapes.
	 *
	 * @return the nS array
	 */
	public NSArray<String> etapes() {
		etapes = new NSArray<String>(new String[]{
					"Identification",
					"Partie Fin.",
					"Gestion DRV",
					
		});
		
		return etapes;
	}

	/**
	 * Sets the index module actif.
	 *
	 * @param indexModuleActif the new index module actif
	 */
	public void setIndexModuleActif(Integer indexModuleActif) {
		this.indexModuleActif = indexModuleActif;
	}

	/**
	 * Gets the index module actif.
	 *
	 * @return the index module actif
	 */
	public Integer getIndexModuleActif() {
		return indexModuleActif;
	}
	
	/**
	 * Terminer.
	 *
	 * @return the wO action results
	 * @throws Exception the exception
	 */
	public WOActionResults terminer() {
		try {
			if(projetScientifique() != null) {
				FactoryProjet f = new FactoryProjet(edc());
				f.ajouterContrat(projetScientifique().localInstanceIn(edc()).projet(), aap().contrat());
			}
			edc().saveChanges();
		}
		catch(ValidationException e) {
			throw e;
		} catch (Exception e) {
			throw NSForwardException._runtimeExceptionForThrowable(e);
		}
		return doNothing();
	}
	

	public WOActionResults onAnnuler() {
		return assistantActionsDelegate().annuler();
	}
	
	public WOActionResults annuler() {
		return pageWithName(Aap.class.getName());
	}
	

	public WOActionResults onApresTerminer() {
		ERXRedirect redirect = (ERXRedirect) pageWithName(ERXRedirect.class.getName());
		redirect.setComponent((WOComponent) assistantActionsDelegate().apresTerminer()); 
		return redirect;
	}
	
	public WOActionResults apresTerminer() {
		
		AapConsultation nextPage = (AapConsultation) pageWithName(AapConsultation.class.getName());
		EOEditingContext e = ERXEC.newEditingContext();
		nextPage.setEdc(e);
		nextPage.setAap(aap().localInstanceIn(e));
		nextPage.setModification(true);
		session().addSimpleSuccessMessage("Création OK", "La demande de subvention " + aap().contrat().exerciceEtIndex() + " a bien été créée");

		return nextPage;
		
	}


	public EOProjetScientifique projetScientifique() {
		return projetScientifique;
	}


	public void setProjetScientifique(EOProjetScientifique projetScientifique) {
		this.projetScientifique = projetScientifique;
	}

	public void setAssistantActionsDelegate(IAssistantActionsDelegate assistantActionsDelegate) {
		this.assistantActionsDelegate = assistantActionsDelegate;
	}

	public IAssistantActionsDelegate assistantActionsDelegate() {
		if(assistantActionsDelegate == null) {
			assistantActionsDelegate = this;
		}
		return assistantActionsDelegate;
	}

}