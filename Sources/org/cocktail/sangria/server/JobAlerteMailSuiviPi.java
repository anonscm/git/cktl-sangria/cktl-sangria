package org.cocktail.sangria.server;

import java.util.Map;

import org.cocktail.cocowork.server.metier.convention.Avenant;
import org.cocktail.cocowork.server.metier.convention.AvenantEvenement;
import org.cocktail.cocowork.server.metier.convention.Contrat;
import org.cocktail.fwkcktlevenement.common.util.FwkCktlEvenementUtil;
import org.cocktail.fwkcktlevenement.common.util.quartz.MailComposer;
import org.cocktail.fwkcktlevenement.serveur.quartz.job.impl.JobAlerteMail;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlrecherche.server.metier.EOContratRecherche;
import org.cocktail.fwkcktlwebapp.server.CktlConfig;
import org.cocktail.fwkcktlwebapp.server.CktlWebApplication;

import com.webobjects.appserver.WOApplication;
import com.webobjects.eocontrol.EOQualifier;

import er.extensions.eof.ERXEC;

/**
 * 
 * @author jlafourc
 *
 */
public class JobAlerteMailSuiviPi extends JobAlerteMail {
	
	@Override
	protected void initializeMailContent() {
	    CktlConfig conf = ((CktlWebApplication)WOApplication.application()).config();
	    
		String grhumHostMail = conf.stringForKey("GRHUM_HOST_MAIL");
		String mailSender = conf.stringForKey("EVT_MAIL_SENDER");
		String mailReplyTo = conf.stringForKey("EVT_MAIL_REPLY_TO");
		
		MailComposer mailComposer = MailComposer.newInstance(grhumHostMail,	mailSender,	mailReplyTo); 
		
		String adressesMails = 
			getAdressesMailPersonnesConcernees().componentsJoinedByString(FwkCktlEvenementUtil.RECIPIENTS_SEPARATOR);

		mailComposer.setAdressesMailDestinataires(adressesMails);
		mailComposer.setInfosPersonnesInjoignables(getPersonnesConcerneesSansAdresseMail());
		
		EOQualifier qualifierContrat = 
			EOContratRecherche.SUIVI_VALO_EVENEMENT.eq(getEvenement());
		
		
		EOContratRecherche contrat = EOContratRecherche.fetchContratRecherche(ERXEC.newEditingContext(), qualifierContrat);
		
		String infosEvenement = 
				"<b>Convention " + contrat.contrat().exerciceEtIndex() + "</b><br/><br/>" +
				"<b>Objet:</b> " + contrat.contrat().conObjet() + "<br/><br/>" + 
				"<ul><b>Laboratoires:</b> ";
		
		for(EOStructure s : contrat.structureRechercheConcernees()) {
			infosEvenement += "<li>" + s.llStructure() + "</li>";
		}
		infosEvenement += "</ul>";
		infosEvenement += "<ul><b>Responsables scientifiques:</b> ";

		for(EOIndividu i : contrat.responsablesScientifiques()) {
			infosEvenement += "<li>" + i.nomPrenom() + "</li>";
		}
		infosEvenement += "</ul>";
		infosEvenement += getEvenement().getHtmlDescriptionPourMail();
		mailComposer.setSubject("[EVENEMENT] Alerte automatique - Contrat de recherche " + contrat.contrat().exerciceEtIndex());
		mailComposer.setInfosEvenement(infosEvenement);
//		mailComposer.setInfosTache(getTache().getHtmlDescriptionPourMail()); pas interessant pour le commun des mortels!!
		mailComposer.setInfosDelaiPrevenance(getTache().triggerOffsetInMinutesToString());
		mailComposer.setInfosProchainMail(getMailNextFireTime());
		mailComposer.setInfosMisfired(isMisfired());
		Map map = mailComposer.toPropertiesMap();

		getJobexecutioncontext().getMergedJobDataMap().putAll(map);
	}
}
