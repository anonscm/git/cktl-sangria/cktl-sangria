package org.cocktail.sangria.server;

import org.cocktail.fwkcktlrecherche.server.util.ContratRechercheMailService;
import org.cocktail.sangria.server.service.EOContratRechercheMailService;

import com.google.inject.AbstractModule;

/**
 *
 * Module pour la conf de l'injection dans Sangria
 *
 */
public class SangriaModule extends AbstractModule {

	@Override
	protected void configure() {
		bind(ContratRechercheMailService.class).to(EOContratRechercheMailService.class);
	}

}
