/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2010 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/

package org.cocktail.sangria.server;

import java.lang.reflect.InvocationTargetException;
import java.net.URL;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.TimeZone;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.log4j.Logger;
import org.cocktail.fwkcktlacces.server.handler.JarResourceRequestHandler;
import org.cocktail.fwkcktlajaxwebext.serveur.CocktailAjaxApplication;
import org.cocktail.fwkcktlged.serveur.metier.Configuration;
import org.cocktail.fwkcktlwebapp.common.CktlLog;
import org.cocktail.fwkcktlwebapp.common.config.CktlConfigurationTestException;
import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;
import org.cocktail.fwkcktlwebapp.server.CktlConfig;
import org.cocktail.fwkcktlwebapp.server.CktlMailBus;
import org.cocktail.fwkcktlwebapp.server.database.CktlDataBus;
import org.cocktail.fwkcktlwebapp.server.init.NSLegacyBundleMonkeyPatch;
import org.cocktail.fwkcktlwebapp.server.util.EOModelCtrl;
import org.cocktail.fwkcktlwebapp.server.version.A_CktlVersion;
import org.cocktail.sangria.server.components.Login;
import org.cocktail.sangria.server.components.Wrapper;

import com.google.inject.Module;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOMessage;
import com.webobjects.appserver.WOResourceManager;
import com.webobjects.appserver.WOResponse;
import com.webobjects.eoaccess.EOAdaptorContext;
import com.webobjects.eoaccess.EODatabaseChannel;
import com.webobjects.eoaccess.EODatabaseContext;
import com.webobjects.eoaccess.EOGeneralAdaptorException;
import com.webobjects.eoaccess.EOModel;
import com.webobjects.eoaccess.ERXModel;
import com.webobjects.eocontrol.EOCooperatingObjectStore;
import com.webobjects.eocontrol.EOObjectStoreCoordinator;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSNumberFormatter;
import com.webobjects.foundation.NSPropertyListSerialization;
import com.webobjects.foundation.NSTimeZone;
import com.webobjects.foundation.NSTimestampFormatter;
import com.webobjects.foundation._NSUtilities;
import com.webobjects.jdbcadaptor.JDBCAdaptor;
import com.webobjects.jdbcadaptor.JDBCContext;
import com.woinject.WOInject;

import er.extensions.appserver.ERXApplication;
import er.extensions.appserver.ERXMessageEncoding;
import er.extensions.eof.ERXEC;
import er.extensions.foundation.ERXProperties;

// TODO: Auto-generated Javadoc
/**
 * The Class Application.
 */
public class Application extends CocktailAjaxApplication {

	/** The Constant TYPE_APP_STR. */
	public static final String TYPE_APP_STR = "SANGRIA"; // ID de l'application
															// dans JefyAdmin

	/** The Constant CONFIG_FILE_NAME. */
	private static final String CONFIG_FILE_NAME = VersionMe.APPLICATIONINTERNALNAME + ".config";

	/** The Constant CONFIG_TABLE_NAME. */
	private static final String CONFIG_TABLE_NAME = "FwkCktlWebApp_GrhumParametres";

	/** The Constant MAIN_MODEL_NAME. */
	private static final String MAIN_MODEL_NAME = VersionMe.APPLICATIONINTERNALNAME;

	/**
	 * Liste des parametres obligatoires (dans fichier de config ou table
	 * grhum_parametres) pour que l'application se lance. Si un des parametre
	 * n'est pas initialise, il y a une erreur bloquante.
	 */
	public static final String[] MANDATORY_PARAMS = new String[] { "HOST_MAIL", "ADMIN_MAIL" };

	/**
	 * Liste des parametres optionnels (dans fichier de config ou table
	 * grhum_parametres). Si un des parametre n'est pas initialise, il y a un
	 * warning.
	 */
	public static final String[] OPTIONAL_PARAMS = new String[] {};

	/**
	 * Mettre a true pour que votre application renvoie les informations de
	 * collecte au serveur de collecte de Cocktail.
	 */
	public static final boolean APP_SHOULD_SEND_COLLECTE = false;

	/**
	 * boolean qui indique si on se trouve en mode developpement ou non. Permet
	 * de desactiver l'envoi de mail lors d'une exception par exemple
	 */
	public static boolean isModeDebug = false;

	/** The _app version. */
	private Version _appVersion;

	/** The ntz. */
	public static NSTimeZone ntz = null;
	/**
	 * Formatteur a deux decimales e utiliser pour les donnees numeriques non
	 * monetaires.
	 */
	public NSNumberFormatter app2DecimalesFormatter;
	/**
	 * Formatteur a 5 decimales a utiliser pour les pourcentages dans la
	 * repartition.
	 */
	public NSNumberFormatter app5DecimalesFormatter;

	/**
	 * Formatteur de dates.
	 */
	public NSTimestampFormatter appDateFormatter;

	/**
	 * Liste des emails des utilisateurs connectes.
	 */
	private static NSMutableArray utilisateurs; // Liste des emails des
												// utilisateurs connectes

	private final Logger logger = ERXApplication.log;

	
	private Configuration configurationGedConventions;
	
	
	/**
	 * The main method.
	 * 
	 * @param argv
	 *            the arguments
	 */
	public static void main(String[] argv) {
//		ERXApplication.main(argv, Application.class);
		NSLegacyBundleMonkeyPatch.apply();
		WOInject.init("org.cocktail.sangria.server.Application", argv);
	}

	/**
	 * Instantiates a new application.
	 */
	public Application() {
		super();
		
		registerRequestHandler(new JarResourceRequestHandler(), "_wr_");
		setAllowsConcurrentRequestHandling(false);
		setDefaultRequestHandler(requestHandlerForKey(directActionRequestHandlerKey()));
		_NSUtilities.setClassForName(Login.class, Login.class.getSimpleName());
		_NSUtilities.setClassForName(Wrapper.class, Wrapper.class.getSimpleName());
		setPageRefreshOnBacktrackEnabled(true);
		WOMessage.setDefaultEncoding("UTF-8");
		WOMessage.setDefaultURLEncoding("UTF-8");
		ERXMessageEncoding.setDefaultEncoding("UTF8");
		ERXMessageEncoding.setDefaultEncodingForAllLanguages("UTF8");
		utilisateurs = new NSMutableArray();
		ERXEC.setDefaultFetchTimestampLag(2000);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.cocktail.fwkcktlwebapp.server.CktlWebApplication#initApplication()
	 */
	public void initApplication() {
		System.out.println("Lancement de l'application serveur " + this.name() + "...");
		super.initApplication();
		rawLogModelInfos();
		// Verifier la coherence des dictionnaires de connexion des modeles de
		// donnees
		boolean isInitialisationErreur = !checkModel();

		isModeDebug = config().booleanForKey("MODE_DEBUG");

		configurationGedConventions = Configuration.configFromProperties("Acte.document");
		
	}

	@Override
	public void didFinishLaunching() {
		
		super.didFinishLaunching();
		
	}
	
	@Override
	protected Module[] modules() {
		return ArrayUtils.add(super.modules(), new SangriaModule());
	}
	
	/**
	 * Execute les operations au demarrage de l'application, juste apres
	 * l'initialisation standard de l'application.
	 */
	public void startRunning() {
		initFormatters();
		initTimeZones();
		this.appDateFormatter = new NSTimestampFormatter();
		this.appDateFormatter.setPattern("%d/%m/%Y");

		// Prefetch dans le sharedEditingContext des nomenclatures communes a
		// toute l'appli
		/**
		 * Prefetch dans le sharedEditingContext des nomenclatures communes a
		 * toute l'appli Il est necessaire de declarer dans l'eomodel, l'entite
		 * a prefetecher via l'inspecteur: 'Share all objects' --> creation d'un
		 * fetchspecificationnamed 'FetchAll' Il est indispensable d'utiliser
		 * l'api 'bindObjectsWithFetchSpecification'
		 */

		// EOSharedEditingContext sedc =
		// EOSharedEditingContext.defaultSharedEditingContext();
		// EOFetchSpecification fetchSpec =
		// EOFetchSpecification.fetchSpecificationNamed("FetchAll",
		// TypeClassificationContrat.ENTITY_NAME);
		// sedc.bindObjectsWithFetchSpecification(fetchSpec, "FetchAll");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see fr.univlr.cri.webapp.CRIBasicWebApplication#configFileName()
	 */
	public String configFileName() {
		return CONFIG_FILE_NAME;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see fr.univlr.cri.webapp.CRIBasicWebApplication#configTableName()
	 */
	public String configTableName() {
		return CONFIG_TABLE_NAME;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.cocktail.fwkcktlwebapp.server.CktlWebApplication#configMandatoryKeys
	 * ()
	 */
	public String[] configMandatoryKeys() {
		return MANDATORY_PARAMS;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.cocktail.fwkcktlwebapp.server.CktlWebApplication#configOptionalKeys()
	 */
	public String[] configOptionalKeys() {
		return OPTIONAL_PARAMS;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.cocktail.fwkcktlwebapp.server.CktlWebApplication#appShouldSendCollecte
	 * ()
	 */
	public boolean appShouldSendCollecte() {
		return APP_SHOULD_SEND_COLLECTE;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.cocktail.fwkcktlwebapp.server.CktlWebApplication#copyright()
	 */
	public String copyright() {
		return appVersion().copyright();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.cocktail.fwkcktlwebapp.server.CktlWebApplication#appCktlVersion()
	 */
	public A_CktlVersion appCktlVersion() {
		return appVersion();
	}

	/**
	 * App version.
	 * 
	 * @return the version
	 */
	public Version appVersion() {
		if (_appVersion == null) {
			_appVersion = new Version();
		}
		return _appVersion;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see fr.univlr.cri.webapp.CRIBasicWebApplication#mainModelName()
	 */
	public String mainModelName() {
		return MAIN_MODEL_NAME;
	}

	/**
	 * Inits the formatters.
	 */
	public void initFormatters() {
		this.app2DecimalesFormatter = new NSNumberFormatter();
		this.app2DecimalesFormatter.setDecimalSeparator(",");
		this.app2DecimalesFormatter.setThousandSeparator(" ");

		this.app2DecimalesFormatter.setHasThousandSeparators(true);
		this.app2DecimalesFormatter.setPattern("#,##0.00;0,00;-#,##0.00");

		this.app5DecimalesFormatter = new NSNumberFormatter();
		this.app5DecimalesFormatter.setDecimalSeparator(",");
		this.app5DecimalesFormatter.setThousandSeparator(" ");

		this.app5DecimalesFormatter.setHasThousandSeparators(true);
		this.app5DecimalesFormatter.setPattern("##0.00000;0,00000;-##0.00000");
	}

	/**
	 * App2 decimales formatter.
	 * 
	 * @return the nS number formatter
	 */
	public NSNumberFormatter app2DecimalesFormatter() {
		return this.app2DecimalesFormatter;
	}

	/**
	 * Gets the app5 decimales formatter.
	 * 
	 * @return the app5 decimales formatter
	 */
	public NSNumberFormatter getApp5DecimalesFormatter() {
		return this.app5DecimalesFormatter;
	}

	/**
	 * Initialise le TimeZone a utiliser pour l'application.
	 */
	protected void initTimeZones() {
		CktlLog.log("Initialisation du NSTimeZone");
		String tz = config().stringForKey("DEFAULT_NS_TIMEZONE");
		if (tz == null || tz.equals("")) {
			CktlLog.log("Le parametre DEFAULT_NS_TIMEZONE n'est pas defini dans le fichier .config.");
			TimeZone.setDefault(TimeZone.getTimeZone("Europe/Paris"));
			NSTimeZone.setDefaultTimeZone(NSTimeZone.timeZoneWithName("Europe/Paris", false));
		} else {
			ntz = NSTimeZone.timeZoneWithName(tz, false);
			if (ntz == null) {
				CktlLog.log("Le parametre DEFAULT_NS_TIMEZONE defini dans le fichier .config n'est pas valide (" + tz + ")");
				TimeZone.setDefault(TimeZone.getTimeZone("Europe/Paris"));
				NSTimeZone.setDefaultTimeZone(NSTimeZone.timeZoneWithName("Europe/Paris", false));
			} else {
				TimeZone.setDefault(ntz);
				NSTimeZone.setDefaultTimeZone(ntz);
			}
		}
		ntz = NSTimeZone.defaultTimeZone();
		CktlLog.log("NSTimeZone par defaut utilise dans l'application:" + NSTimeZone.defaultTimeZone());
		NSTimestampFormatter ntf = new NSTimestampFormatter();
		CktlLog.log("Les NSTimestampFormatter analyseront les dates avec le NSTimeZone: " + ntf.defaultParseTimeZone());
		CktlLog.log("Les NSTimestampFormatter afficheront les dates avec le NSTimeZone: " + ntf.defaultFormatTimeZone());
	}

	/**
	 * Retourne le mot de passe du super-administrateur. Il permet de se
	 * connecter a l'application avec le nom d'un autre utilisateur
	 * (l'authentification local et non celle CAS doit etre activee dans ce
	 * cas).
	 * 
	 * @return the root password
	 */
	public String getRootPassword() {
		// passpar2
		return "HO4LI8hKZb81k";
	}

	@Override
	public WOResponse handleException(Exception anException, WOContext context) {
		logger().error(anException.getMessage(), anException);
		WOResponse response = null;
		if (context != null && context.hasSession()) {
			Session session = (Session) context.session();
			sendMessageErreur(anException, context, session);
			response = createResponseInContext(context);
			NSMutableDictionary formValues = new NSMutableDictionary();
			formValues.setObjectForKey(session.sessionID(), "wosid");
			String applicationExceptionUrl = context.directActionURLForActionNamed("applicationException", formValues);
			response.appendContentString("<script>document.location.href='" + applicationExceptionUrl + "';</script>");
			cleanInvalidEOFState(anException, context);
			return response;
		} else {
			return super.handleException(anException, context);
		}
	}

	private String createMessageErreur(Exception anException, WOContext context, Session session) {
		String contenu;
		NSDictionary extraInfo = extraInformationForExceptionInContext(anException, context);
		contenu = "Date : " + DateCtrl.dateToString(DateCtrl.now(), "%d/%m/%Y %H:%M") + "\n";
		contenu += "OS: " + System.getProperty("os.name") + "\n";
		contenu += "Java vm version: " + System.getProperty("java.vm.version") + "\n";
		contenu += "WO version: " + ERXProperties.webObjectsVersion() + "\n\n";
		contenu += "User agent: " + context.request().headerForKey("user-agent") + "\n\n";
		contenu += "Utilisateur(Numero individu): " + session.connectedUserInfo().nomEtPrenom() + "(" + session.connectedUserInfo().noIndividu() + ")" + "\n";

		contenu += "\n\nException : " + "\n";
		if (anException instanceof InvocationTargetException) {
			contenu += getMessage(anException, extraInfo) + "\n";
			anException = (Exception) anException.getCause();
		}
		contenu += getMessage(anException, extraInfo) + "\n";
		contenu += "\n\n";
		return contenu;
	}

	private void sendMessageErreur(Exception anException, WOContext context, Session session) {
		try {
			CktlMailBus cmb = session.mailBus();
			String smtpServeur = config().stringForKey(CktlConfig.CONFIG_GRHUM_HOST_MAIL_KEY);
			String destinataires = config().stringForKey("ADMIN_MAIL");

			if (cmb != null && smtpServeur != null && smtpServeur.equals("") == false && destinataires != null && destinataires.equals("") == false) {
				String objet = "[SANGRIA]:Exception:[";
				objet += VersionMe.txtAppliVersion() + "]";
				String contenu = createMessageErreur(anException, context, session);
				// session.setMessageErreur(contenu);
				session.setGeneralErrorMessage(contenu);
				// session.addMessage(new
				// CktlUIErrorMessage("Une erreur critique est survenue",
				// contenu, anException));
				boolean retour = false;
				if (isModeDebug) {
					logger().info("!!!!!!!!!!!!!!!!!!!!!!!! MODE DEVELOPPEMENT : pas de mail !!!!!!!!!!!!!!!!");
					retour = false;
				} else {
					retour = cmb.sendMail(destinataires, destinataires, null, objet, contenu);
				}
				if (!retour) {
					logger().warn("!!!!!!!!!!!!!!!!!!!!!!!! IMPOSSIBLE d'ENVOYER le mail d'exception !!!!!!!!!!!!!!!!");
					logger().warn("\nMail:\n\n" + contenu);

				}
			} else {
				logger().warn("!!!!!!!!!!!!!!!!!!!!!!!! IMPOSSIBLE d'ENVOYER le mail d'exception !!!!!!!!!!!!!!!!");
				logger().warn("Veuillez verifier que les parametres HOST_MAIL et ADMIN_MAIL sont bien renseignes");
				logger().warn("HOST_MAIL = " + smtpServeur);
				logger().warn("ADMIN_MAIL = " + destinataires);
				logger().warn("cmb = " + cmb);
				logger().warn("\n\n\n");
			}
		} catch (Exception e) {
			logger().error("\n\n\n");
			logger().error("!!!!!!!!!!!!!!!!!!!!!!!! Exception durant le traitement d'une autre exception !!!!!!!!!!!!!!!!");
			logger().error(e.getMessage(), e);
			super.handleException(e, context);
			logger().error("\n");
			logger().error("Message Exception originale: " + anException.getMessage());
			logger().error("Stack Exception dans exception: " + String.valueOf(anException.getStackTrace()));
		}

	}

	private void cleanInvalidEOFState(Exception e, WOContext ctx) {
		if (e instanceof IllegalStateException || e instanceof EOGeneralAdaptorException) {
			ctx.session().defaultEditingContext().invalidateAllObjects();
		}
	}

	/**
	 * Gets the message.
	 * 
	 * @param e
	 *            the e
	 * @param extraInfo
	 *            the extra info
	 * @return the message
	 */
	protected String getMessage(Throwable e, NSDictionary extraInfo) {
		String message = "";
		if (e != null) {
			message = stackTraceToString(e, false) + "\n\n";
			message += "Info extra :\n";
			if (extraInfo != null) {
				message += NSPropertyListSerialization.stringFromPropertyList(extraInfo) + "\n\n";
			}
		}
		return message;
	}

	/**
	 * permet de recuperer la trace d'une exception au format string message +
	 * trace.
	 * 
	 * @param e
	 *            the e
	 * @param useHtml
	 *            the use html
	 * @return the string
	 */
	public static String stackTraceToString(Throwable e, boolean useHtml) {
		String tagCR = "\n";
		if (useHtml) {
			tagCR = "<br>";
		}
		String stackStr = e + tagCR + tagCR;
		StackTraceElement[] stack = e.getStackTrace();
		for (int i = 0; i < stack.length; i++) {
			stackStr += (stack[i]).toString() + tagCR;
		}
		return stackStr;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see fr.univlr.cri.webapp.CRIBasicWebApplication#
	 * handleSessionRestorationErrorInContext
	 * (com.webobjects.appserver.WOContext)
	 */
	@Override
	public WOResponse handleSessionRestorationErrorInContext(WOContext context) {
		WOResponse response;
		response = createResponseInContext(context);
		String sessionExpiredUrl = context.directActionURLForActionNamed("sessionExpired", null);
		response.appendContentString("<script>document.location.href='" + sessionExpiredUrl + "';</script>");

		return response;
	}

	/**
	 * Utilisateurs.
	 * 
	 * @return the nS mutable array
	 */
	public NSMutableArray utilisateurs() {
		return utilisateurs;
	}


	public Connection getJDBCConnection() {
		EOObjectStoreCoordinator osc = EOObjectStoreCoordinator.defaultCoordinator();
		NSArray<EOCooperatingObjectStore> dbCtxs = osc.cooperatingObjectStores();
		for (EOCooperatingObjectStore obj : dbCtxs) {
			EODatabaseContext dbCtx = (EODatabaseContext) obj;
			JDBCContext adaptorCtx = (JDBCContext) dbCtx.adaptorContext();
			JDBCAdaptor adaptor = (JDBCAdaptor) adaptorCtx.adaptor();
			if ("grhum".equals(adaptor.connectionDictionary().objectForKey("username"))) {
				// On recupere la connection jdbc pour jasper
				System.out.println("Application.startRunning() " + adaptorCtx.connection());
				return adaptorCtx.connection();
			}
		}
		return null;
	}

	public EOAdaptorContext getAdaptorContext() {

		NSArray<EODatabaseChannel> channels = CktlDataBus.adaptorContext().channels();
		for (EODatabaseChannel channel : channels) {
			String username = (String) ((ERXModel) channel.databaseContext().database().models().get(0)).connectionDictionary().valueForKey("username");
			if (username.equalsIgnoreCase("grhum")) {
				return channel.adaptorChannel().adaptorContext();
			}
		}
		return CktlDataBus.databaseContext().availableChannel().adaptorChannel().adaptorContext();
	}

	public String getReportsLocation(String fileName) {
		String param = config().stringForKey("REPORTS_LOCATION");
		if (param != null) {
			return param + "/" + fileName;
		} else {
			WOResourceManager rm = application().resourceManager();
			URL url = rm.pathURLForResourceNamed("report/" + fileName, null, null);
			if (url == null) {
				return null;
			}
			return url.getPath();
		}

		// final String aPath = path().concat("/Contents/Resources/report/");
		// return aPath;
	}

	public Logger logger() {
		return logger;
	}

	public static String serverBDId() {
		NSMutableArray<String> serverDBIds = new NSMutableArray<String>();
		final NSMutableDictionary mdlsDico = EOModelCtrl.getModelsDico();
		final Enumeration mdls = mdlsDico.keyEnumerator();
		while (mdls.hasMoreElements()) {
			final String mdlName = (String) mdls.nextElement();
			String serverDBId = EOModelCtrl.bdConnexionServerId((EOModel) mdlsDico.objectForKey(mdlName));
			if (serverDBId != null && !serverDBIds.containsObject(serverDBId)) {
				serverDBIds.addObject(serverDBId);
			}
		}
		return serverDBIds.componentsJoinedByString(",");
	}

	public Configuration getConfigurationGedConventions() {
		return configurationGedConventions;
	}
	
	public boolean _isSupportedDevelopmentPlatform() {
		return (super._isSupportedDevelopmentPlatform() || System.getProperty("os.name").startsWith("Win") || System.getProperty("os.name").startsWith("Linux"));
	}
}
