# language: fr
Fonctionnalité: Authentification hors CAS
  Afin d'accéder à l'application Sangria
  En tant qu'utilisateur du PGI Cocktail
  Je dois m'identifier à l'aide de mon identifiant et de mon mot de passe

  Scénario: Authentification d'un utilisateur habilité
    Soit grhumadm habilitée à utiliser Sangria
    Lorsqu'elle se connecte
    Alors elle se retrouve sur la page d'accueil de Sangria